/*
 * display.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <linux/fb.h>
#include <linux/omapfb.h>
#include <linux/videodev2.h>

#include <rendezvous.h>
#include <fifoutil.h>
#include "encode.h"
#include "display.h"

#define UYVY_BLACK			   0x10801080

#define DISPLAYDEVICEINITIALIZED	0x1
#define RESIZEDEVICEINITIALIZED		0x2


static int initDisplayDevice(int *numbuffers, struct v4l2_format *fmt, int displayWidth, int displayHeight);
static void cleanupDisplayDevice(int fd, int count);

static struct buf_info display_buff_info[NUM_DISPLAY_BUFS];


int Rszcopy_create(void)
{
	int fd;

    fd = open(RESIZER_DEVICE, O_RDWR);

    if (fd == -1) {
        ERR("Failed to open %s\n", RESIZER_DEVICE);
		return FAILURE;
    }

    return fd;
}

void Rszcopy_delete(int fd)
{
    if (fd != NULL) {
        close(fd);
    }
}

#if 0
int Rszcopy_config(Rszcopy_Handle hRszcopy, int width, int height, int srcPitch, int dstPitch , int rwidth, int rhight)
{

	int i=0;

    rsz_params_t params = {
        0,                              /* in_hsize (set at run time) */
        0,                              /* in_vsize (set at run time) */
        0,                              /* in_pitch (set at run time) */
        RSZ_INTYPE_YCBCR422_16BIT,      /* inptyp */
        0,                              /* vert_starting_pixel */
        0,                              /* horz_starting_pixel */
        0,                              /* cbilin */
        RSZ_PIX_FMT_UYVY,    		    /* pix_fmt */
        0,                              /* out_hsize (set at run time) */
        0,                              /* out_vsize (set at run time) */
        0,                              /* out_pitch (set at run time) */
        0,                              /* hstph */
        0,                              /* vstph */
        {                               /* hfilt_coeffs */
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0,
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0
        },
        {                               /* vfilt_coeffs */
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0,
            256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0, 256, 0, 0, 0
        },
        {                               /* yenh_params */
            RSZ_YENH_DISABLE,               /* type */
            0,                              /* gain */
            0,                              /* slop */
            0                               /* core */
        }
    };


    /* Set up the rszcopyr job */
		params.in_hsize = width;
		params.in_vsize = height;	/*only 1 filed of NTSC image */
		params.in_pitch = srcPitch;
		params.pix_fmt  = RSZ_PIX_FMT_YUYV;
		params.out_hsize = rwidth;
		params.out_vsize = rhight;
		params.out_pitch = dstPitch;
	
	if (rwidth >= 320){
		for (i = 0; i < 32; i++)
			params.hfilt_coeffs[i] = gRDRV_reszFilter4TapHighQuality[i];
	
		for (i = 0; i < 32; i++)
			params.vfilt_coeffs[i] = gRDRV_reszFilter4TapHighQuality[i];
	}
	else {
		for (i = 0; i < 32; i++)
			params.hfilt_coeffs[i] = gRDRV_reszFilter7TapHighQuality[i];
	
		for (i = 0; i < 32; i++)
			params.vfilt_coeffs[i] = gRDRV_reszFilter7TapHighQuality[i];
	} 

    hRszcopy->inSize = srcPitch * params.in_vsize;
    hRszcopy->outSize = dstPitch * params.out_vsize;
    

    if (ioctl(hRszcopy->rszFd, RSZ_S_PARAM, &params) == -1) {
        ERR("Rszcopy setting parameters failed.\n");
        return RSZCOPY_FAILURE;
    }

    return RSZCOPY_SUCCESS;
}
#endif


static int initDisplayDevice(int *numbuffers, struct v4l2_format *fmt, int displayWidth, int displayHeight)
{
	int	fd, a;
	struct v4l2_requestbuffers reqbuf;
	struct v4l2_buffer buf;
	struct v4l2_capability capability;
    struct v4l2_framebuffer framebuffer;
	int	i, ret;

	fd = open(FBVID_DEVICE, O_RDWR);

	if (fd == -1) {
		ERR("Failed to open fb device %s (%s)\n", FBVID_DEVICE,
												  strerror(errno));
		return FAILURE;
	}

	if (ioctl(fd, VIDIOC_QUERYCAP, &capability) < 0) {
		ERR("Failed VIDIOC_QUERYCAP on %s (%s)\n", FBVID_DEVICE, strerror(errno));
		return FAILURE;
	}

	if (capability.capabilities & V4L2_CAP_STREAMING)
		DBG("%s: Capable of streaming\n", DISPLAY_NAME);
	else {
		DBG("%s: Not capable of streaming\n", DISPLAY_NAME);
		return FAILURE;
	}

	fmt->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	ret = ioctl(fd, VIDIOC_G_FMT, fmt);
	if (ret < 0) {
		ERR("VIDIOC_G_FMT (%s)\n", strerror(errno));
		return FAILURE;
	}

	fmt->fmt.pix.width = displayWidth;
	fmt->fmt.pix.height = displayHeight;
	fmt->fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;

	fmt->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

	DBG("Input  Display pixformat %04dx%04d (%s), bytesperline %d, size %d field %d, colorspace %d!!\n",
		fmt->fmt.pix.width, fmt->fmt.pix.height, &fmt->fmt.pix.pixelformat,
		fmt->fmt.pix.bytesperline, fmt->fmt.pix.sizeimage,
		fmt->fmt.pix.field, fmt->fmt.pix.colorspace
	);

	ret = ioctl(fd, VIDIOC_S_FMT, fmt);
	if (ret < 0) {
		ERR("VIDIOC_S_FMT (%s)\n", strerror(errno));
		return FAILURE;
	}

	ret = ioctl(fd, VIDIOC_G_FMT, fmt);
	if (ret < 0) {
		ERR("VIDIOC_G_FMT (%s)\n", strerror(errno));
		return FAILURE;
	}

	if (fmt->fmt.pix.pixelformat != V4L2_PIX_FMT_UYVY) {
		ERR("%s: Requested pixel format not supported  (%s)\n",DISPLAY_NAME, strerror(errno));
		return FAILURE;
	}

	DBG("Output Display pixformat %04dx%04d (%s), bytesperline %d, size %d field %d, colorspace %d!!\n",
		fmt->fmt.pix.width, fmt->fmt.pix.height, &fmt->fmt.pix.pixelformat,
		fmt->fmt.pix.bytesperline, fmt->fmt.pix.sizeimage,
		fmt->fmt.pix.field, fmt->fmt.pix.colorspace
	);

	reqbuf.count = *numbuffers;
	reqbuf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	reqbuf.memory = V4L2_MEMORY_MMAP;
	ret = ioctl(fd, VIDIOC_REQBUFS, &reqbuf);
	if (ret < 0) {
		ERR("Cannot allocate memory  (%s)\n", strerror(errno));
		return FAILURE;
	}
	*numbuffers = reqbuf.count;
	DBG("%s: Number of requested buffers = %d\n", DISPLAY_NAME, (*numbuffers));
	
	memset(&buf, 0, sizeof(buf));

	for (i = 0; i < (*numbuffers); i++) {
		buf.type = reqbuf.type;
		buf.index = i;
		buf.memory = reqbuf.memory;
		ret = ioctl(fd, VIDIOC_QUERYBUF, &buf);
		if (ret < 0) {
			ERR("VIDIOC_QUERYCAP (%s)\n", strerror(errno));
			goto ERROR;
		}

		display_buff_info[i].length = buf.length;
		display_buff_info[i].index =  i;
		display_buff_info[i].start = mmap(NULL, buf.length,
				PROT_READ | PROT_WRITE, MAP_SHARED, fd,
				buf.m.offset);

		if (display_buff_info[i].start == MAP_FAILED) {
			ERR("Failed mmap on %s %d buffer (%s)\n", FBVID_DEVICE, i, strerror(errno));
			(*numbuffers) = i;
			goto ERROR;
		}

		printf("display_buff mmap[%d] 0x%p (%d) buffer\n", i, display_buff_info[i].start, display_buff_info[i].length );


		memset((void *) display_buff_info[i].start, 0x80,
			   display_buff_info[i].length);

		ret = ioctl(fd, VIDIOC_QBUF, &buf);
		if (ret < 0) {
			ERR("VIDIOC_QBUF (%s)\n", strerror(errno));
			(*numbuffers) = i + 1;
			goto ERROR;
		}
	}
	printf("%s: Init done successfully\n\n", DISPLAY_NAME);

	a = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = ioctl(fd, VIDIOC_STREAMON, &a);
	if (ret < 0) {
		perror("VIDIOC_STREAMON");
		goto ERROR;
	}
	printf("%s: Stream on...\n", DISPLAY_NAME);

	return fd;

ERROR:
	for (i = 0; i < *numbuffers; i++)
		munmap(display_buff_info[i].start, display_buff_info[i].length);

	return FAILURE;

}

static void cleanupDisplayDevice(int fd, int count)
{
	int i = 0;
	for (i = 0; i < count; i++)
		munmap(display_buff_info[i].start, display_buff_info[i].length);

	close(fd);
}

static void copyFrame(char *dis_ptr, DisplayBufferElement *de, int width, int height)
{
	int h;
	char *cap_ptr;
	
	cap_ptr = de->virtBuf;

	for (h = 0; h < height; h++) {
		memcpy(dis_ptr, cap_ptr, width * 2);

		cap_ptr += de->width * 2;
		dis_ptr += width * 2;
	}
}

void *displayThrFxn(void *arg)
{
	DisplayEnv	 			*envp = (DisplayEnv *) arg;
	DisplayBufferElement	dFlush = { DISPLAY_FLUSH };
	void		  			*status = THREAD_SUCCESS;
	unsigned int			initMask = 0;
	int			 			display_fd = FAILURE, rszaccel_fd = FAILURE;
	int			 			ret, displaysBufNum = NUM_DISPLAY_BUFS;
   	struct v4l2_format		display_fmt;
   	struct v4l2_buffer		display_buf;
	DisplayBufferElement	de;
	char 			*dis_ptr;

	display_fd = initDisplayDevice(&displaysBufNum, &display_fmt, envp->imageWidth, envp->imageHeight);
	if (display_fd == FAILURE) {
		cleanup(THREAD_FAILURE);
	}

	DBG("Video display device %d initialized.\n",display_fd);

	initMask |= DISPLAYDEVICEINITIALIZED;

	rszaccel_fd = Rszcopy_create();

	initMask |= RESIZEDEVICEINITIALIZED;

	DBG("Video display Resize device %d initialized.\n",rszaccel_fd);

	Rendezvous_meet(envp->hRendezvousInit);

    CLEAR(display_buf);
    display_buf.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    display_buf.index = 0;
    display_buf.memory = V4L2_MEMORY_MMAP;

	DBG("Entering display main loop.\n");

    while (!gblGetQuit()) {
		Pause_test(envp->hPause);

		if (FifoUtil_get(&envp->dispInFifo, &de) == FIFOUTIL_FAILURE) {		
			breakLoop(THREAD_FAILURE);
		}

		if (de.id == DISPLAY_FLUSH) {
			breakLoop(THREAD_SUCCESS);
		} else if (de.id == DISPLAY_PRIME) {
			Rendezvous_meet(envp->hRendezvousPrime);
			continue;
		}

		ret = ioctl(display_fd, VIDIOC_DQBUF, &display_buf);
		if (ret < 0) {
			ERR("VIDIOC_DQBUF failed (%s)\n", strerror(errno));
            breakLoop(THREAD_FAILURE);
		}
		
		dis_ptr = (char*)display_buff_info[display_buf.index].start;

		//copyFrame(dis_ptr, &de, display_fmt.fmt.pix.width, display_fmt.fmt.pix.height);

		if (FifoUtil_put(&envp->dispOutFifo, &de) == FIFOUTIL_FAILURE) {
			breakLoop(THREAD_FAILURE);
		}

		ret = ioctl(display_fd, VIDIOC_QBUF, &display_buf);
		if (ret < 0) {
			ERR("VIDIOC_QBUF failed (%s)\n", strerror(errno));
			breakLoop(THREAD_FAILURE);
		}

		gblIncFrames();
	}

cleanup:

	Rendezvous_force(envp->hRendezvousInit);
	Rendezvous_force(envp->hRendezvousPrime);
	Pause_off(envp->hPause);
	FifoUtil_put(&envp->dispOutFifo, &dFlush);
	Rendezvous_meet(envp->hRendezvousCleanup);

	if (initMask & RESIZEDEVICEINITIALIZED) {
		Rszcopy_delete(rszaccel_fd);
	}

	if (initMask & DISPLAYDEVICEINITIALIZED) {
		cleanupDisplayDevice(display_fd, displaysBufNum);
	}



	fprintf(stderr,"displayThrFxn closing...\n");
	return status;
}


