/*
 * video.c
 */


#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <linux/videodev.h>
#include <linux/videodev2.h>

#include <xdc/std.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/osal/Memory.h>

#include <ti/sdo/ce/video1/videnc1.h>
#include <ti/sdo/ce/image1/imgenc1.h>

#include <rendezvous.h>
#include <fifoutil.h>
#include "encode.h"
#include "video.h"
#include "capture.h"
#include "display.h"
#include "writer.h"

#define CLEAR(x) memset (&(x), 0, sizeof (x))

#define UYVY_BLACK				0x10801080

#define WRITER_BUFFERS			ENCODE_BUFFERS
#define IO_BUFFERS				2
#define CAP_BUFFERS				2

char *videoEncodeAlgNames[NUM_VIDEO_ENCODERS] = {
	"none",
	"jpegenc",
	"mpeg4enc",
	"h264enc"
	"lzo",
	"rle",
};

const IMGENC1_Params Ienc1_Params_DEFAULT = {
    sizeof(IMGENC1_Params),
    0,
    0,
    XDM_DEFAULT,
    XDM_BYTE,
    XDM_YUV_422P   
};

const IMGENC1_DynamicParams Ienc1_DynamicParams_DEFAULT = {
    sizeof(IMGENC1_DynamicParams),
    XDM_DEFAULT,
    XDM_YUV_422ILE,
    0,
    0,
    0,
    XDM_ENCODE_AU,
    75
};

static int timeval_subtract(struct timeval *result, struct timeval *x,
							 struct timeval *y)
{
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) /
			1000000 + 1;
		y->tv_usec -= 1000000 *	nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000) {
		int nsec = (x->tv_usec - y->tv_usec) /
			1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	return x->tv_sec < y->tv_sec;
}

IMGENC1_Status		 encStatus;
IMGENC1_Params		 params;
IMGENC1_DynamicParams dynamicParams;

int jpegEncodeAlgCreate(Engine_Handle hEngine, IMGENC1_Handle *hEncodePtr,
						VideoEncoder videoEncoder, int width, int height, int quality, int sacns)
{
	XDAS_Int32			status;
	char				*algName;
	IMGENC1_Handle		hEncode;

	algName = videoEncodeAlgNames[videoEncoder];

	params.size = sizeof(IMGENC1_Params);
	params.maxWidth = width;
	params.maxHeight = height;
	params.maxScans = XDM_DEFAULT;
	params.dataEndianness = XDM_BYTE;
	params.forceChromaFormat = XDM_YUV_422P;

	dynamicParams.size = sizeof(IMGENC1_DynamicParams);
	dynamicParams.numAU = XDM_DEFAULT;
	dynamicParams.inputWidth = params.maxWidth;	//width;
	dynamicParams.inputHeight = params.maxHeight;	//height;
	dynamicParams.captureWidth = params.maxWidth;
	dynamicParams.qValue = quality;
	dynamicParams.inputChromaFormat = XDM_YUV_422ILE;
	dynamicParams.generateHeader = XDM_ENCODE_AU;	//XDM_GENERATE_HEADER;

	hEncode = IMGENC1_create(hEngine, algName, &params);
	if (hEncode == NULL) {
		ERR("Failed to open jpeg encode algorithm: %s (0x%x)\n",
			algName, Engine_getLastError(hEngine));
		return FAILURE;
	}


    encStatus.data.buf = NULL;
    encStatus.size = sizeof(IMGENC1_Status);

	status = IMGENC1_control(hEncode, XDM_SETPARAMS, &dynamicParams, &encStatus);
	 if (status != IMGENC1_EOK) {
		ERR("XDM_SETPARAMS failed, status=%ld\n", status);
		return FAILURE;
	}

	status = IMGENC1_control(hEncode, XDM_GETBUFINFO, &dynamicParams, &encStatus);
	 if (status != IMGENC1_EOK) {
		ERR("XDM_GETBUFINFO failed, status=%d\n", status);
		return FAILURE;
	}

    DBG("Buffer size %u obtained from XDM_GETBUFINFO control call\n",
              (UInt) encStatus.bufInfo.minInBufSize[0]);


	*hEncodePtr = hEncode;

	return SUCCESS;
}


int jpegEncodeReSetting(IMGENC1_Handle hEncode)
{
    Int32 status;

    encStatus.data.buf = NULL;
    encStatus.size = sizeof(IMGENC1_Status);

	status = IMGENC1_control(hEncode, XDM_SETPARAMS, &dynamicParams, &encStatus);
	if (status != IMGENC1_EOK) {
		ERR("XDM_SETPARAMS failed, status=%ld\n", status);
		return FAILURE;
	}

	return SUCCESS;
}



static int jpegVideoBuffer(IMGENC1_Handle hEncode, char *inBuf, int inSize,
                             char *outBuf, int *outBufSize, int *frametype, int width,
                             int height)
{
    XDM1_BufDesc		    inBufDesc;
    XDM1_BufDesc            outBufDesc;
    Int32              		status;

    IMGENC1_InArgs          inArgs;
    IMGENC1_OutArgs         outArgs;

    inBufDesc.numBufs       	= 1;
    inBufDesc.descs[0].bufSize 	= inSize;
    inBufDesc.descs[0].buf 		= inBuf;

    outBufDesc.numBufs      	= 1;
    outBufDesc.descs[0].bufSize = width * height;
    outBufDesc.descs[0].buf		= outBuf;

    inArgs.size             	= sizeof(IMGENC1_InArgs);
    outArgs.size            	= sizeof(IMGENC1_OutArgs);

	/* perform the image (JPEG) encoding */
	status = IMGENC1_process(hEncode, &inBufDesc, &outBufDesc, &inArgs, &outArgs);

    if (status != IMGENC1_EOK) {
        ERR("IMGENC1_process() failed with error (%d ext: 0x%x)\n",
                  (Int)status, (Uns) outArgs.extendedError);
        return FAILURE;
    }

    //DBG("IMGENC1_process() Gen %d (%d ext: 0x%x)\n",outArgs.bytesGenerated, (Int)status, (Uns) outArgs.extendedError);

    *outBufSize = outArgs.bytesGenerated;
	*frametype 	= 1;

    return SUCCESS;
}


void *videoThrFxn(void *arg)
{
	CaptureBufferElement	cFlush		= { DISPLAY_FLUSH };
	WriterBufferElement		wFlush		= { WRITER_FLUSH };
	Engine_Handle			hEngine	 = NULL;
	IMGENC1_Handle      	hEncode_img       = NULL;
	VideoEnv				*envp		 = (VideoEnv *) arg;
	void					*status		= THREAD_SUCCESS;
	char					*captureBuffers[CAP_BUFFERS];
	char					*encodedBuffers[IO_BUFFERS];
	int						imageSize = 0, jpgquality = 0;
	int						bufIdx, ret, h;
	int						totalsize = 0,count = 0;
	WriterBufferElement		we;
	CaptureBufferElement	ce;
	struct timeval before, after, result;
	Memory_AllocParams			cMbparam;

	cMbparam.type = Memory_CONTIGPOOL;
	cMbparam.flags = Memory_CACHED;
	cMbparam.align = Memory_DEFAULTALIGNMENT;
	cMbparam.seg = 0;

	imageSize = envp->imageWidth * envp->imageHeight * SCREEN_BPP / 8;
	jpgquality = envp->quality;

	/* Reset, load, and start DSP Engine */
	hEngine = Engine_open(ENGINE_NAME, NULL, NULL);

	if (hEngine == NULL) {
		ERR("Video Failed to open codec engine %s\n", ENGINE_NAME);
		cleanup(THREAD_FAILURE);
	}

	DBG("Codec Engine opened in video thread\n");

	DBG("jpegEncodeAlgCreate\n");
	if (jpegEncodeAlgCreate(hEngine, &hEncode_img, JPEG_VIDEO_ENCODER,
                            envp->imageWidth, envp->imageHeight,
							jpgquality, 4) == FAILURE) {
		ERR("Failed to jpegEncodeAlgCreate %s\n", ENGINE_NAME);
        cleanup(THREAD_FAILURE);
    }

	/* Allocate buffers for encoded data and prime the writer thread */
	for (bufIdx=0; bufIdx<IO_BUFFERS; bufIdx++) {
		encodedBuffers[bufIdx] = //malloc(imageSize); //(char *)0xff;
						//Memory_alloc(imageSize, &cMbparam);
						Memory_contigAlloc(imageSize, Memory_DEFAULTALIGNMENT);

		if (encodedBuffers[bufIdx] == NULL) {
			ERR("Video Failed to allocate contiguous memory block.\n");
			cleanup(THREAD_FAILURE);
		}

		we.id = WRITER_PRIME;
		we.encodedBuffer = encodedBuffers[bufIdx];

		DBG("Encode buffer[%d] allocated at %#lx physical address %#lx\n",
			bufIdx, (unsigned long) we.encodedBuffer,
			Memory_getBufferPhysicalAddress(encodedBuffers[bufIdx], 4, NULL) );

		if (FifoUtil_put(envp->hWriterInFifo, &we) == FIFOUTIL_FAILURE) {
			ERR("Video hWriterInFifo Failed to put buffer in output fifo\n");
			cleanup(THREAD_FAILURE);
		}
	}

	for (bufIdx=0; bufIdx < CAP_BUFFERS; bufIdx++) {
		captureBuffers[bufIdx] = Memory_contigAlloc(imageSize, Memory_DEFAULTALIGNMENT);	
						//Memory_contigAlloc(imageSize, &cMbparam);
						//(char *)0xff;

		if (captureBuffers[bufIdx] == NULL) {
			ERR("Video Failed to allocate contiguous memory block.\n");
			cleanup(THREAD_FAILURE);
		}

		DBG("Capture buffer[%d] allocated at %#lx physical address %#lx\n", bufIdx, (unsigned long) captureBuffers[bufIdx], Memory_getBufferPhysicalAddress(encodedBuffers[bufIdx], 4, NULL) );

		ce.id	 = 0;
		ce.virtBuf = captureBuffers[bufIdx];
		ce.physBuf = Memory_getBufferPhysicalAddress(encodedBuffers[bufIdx], 4, NULL);	//	0xff;	0xff;	//	
		ce.bufSize = imageSize;
		ce.timestamp = -1;

		if (FifoUtil_put(envp->hCaptureInFifo, &ce) == FIFOUTIL_FAILURE) {
			ERR("Video Failed to put buffer in output fifo\n");
			cleanup(THREAD_FAILURE);
		}
	}

	Rendezvous_meet(envp->hRendezvousInit);

	DBG("Entering video main loop.\n");
	while (!gblGetQuit()) {
		/* Pause processing? */
		Pause_test(envp->hPause);

		totalsize = 0;
		//DBG("Get a buffer from the capture thread\n");
		if (FifoUtil_get(envp->hCaptureOutFifo, &ce) == FIFOUTIL_FAILURE) {
			ERR("Video Failed to get buffer in output fifo\n");
			breakLoop(THREAD_FAILURE);
		}

		/* Is the capture thread flushing the pipe? */ 
		if (ce.id == CAPTURE_FLUSH) {
			breakLoop(THREAD_SUCCESS);
		}
#if 1
		if (FifoUtil_get(envp->hWriterOutFifo, &we) == FIFOUTIL_FAILURE) {
			ERR("Failed to put buffer in output fifo\n");
			breakLoop(THREAD_FAILURE);
		}

		if (we.id == WRITER_FLUSH) {
			breakLoop(THREAD_SUCCESS);
		}

		//gettimeofday(&before, NULL);
		//DBG(" jpegVideoBuffer \n");
        //DBG("COM : # ce.virtBuf addr 0x%08X, ce.physBuf addr %#lx\n", ce.virtBuf, ce.physBuf);

		if (jpegVideoBuffer(hEncode_img, ce.virtBuf, ce.bufSize,
							we.encodedBuffer, &we.frameSize, &we.frameType,
							envp->imageWidth, envp->imageHeight) == FAILURE) {
			breakLoop(THREAD_FAILURE);
		}
		we.frameSize = roundup(we.frameSize, 32);
		//gettimeofday(&after, NULL);
 		//timeval_subtract(&result, &after, &before);
		//printf("#frameSize %d Result Time:\t%ld.%ld sec\n",we.frameSize, result.tv_sec, result.tv_usec);


		we.videotype = JPEG_VIDEO_ENCODER;
		we.timestamp = ce.timestamp;

		if (FifoUtil_put(envp->hWriterInFifo, &we) == FIFOUTIL_FAILURE) {
			ERR("Failed to put buffer in output fifo\n");
			breakLoop(THREAD_FAILURE);
		}
		totalsize += we.frameSize;
#endif
		if (FifoUtil_put(envp->hCaptureInFifo, &ce) == FIFOUTIL_FAILURE) {
			ERR("Video Failed to put buffer in output fifo\n");
			breakLoop(THREAD_FAILURE);
		}
		jpegEncodeReSetting(hEncode_img);

		gblIncVideoBytesEncoded(we.frameSize);
        //gblIncVideoBytesEncoded(totalsize);
	}

cleanup:

	Rendezvous_force(envp->hRendezvousInit);
	Pause_off(envp->hPause);
	FifoUtil_put(envp->hWriterInFifo, &wFlush);
	FifoUtil_put(envp->hCaptureInFifo, &cFlush);
	Rendezvous_meet(envp->hRendezvousCleanup);
	fprintf(stderr,"videoThrFxn closing...\n");

    if( hEncode_img){
		IMGENC1_delete(hEncode_img);
    }

    if (hEngine) {
        Engine_close(hEngine);
    }

    for (bufIdx=0; bufIdx < IO_BUFFERS; bufIdx++) {
        if (encodedBuffers[bufIdx]) {
            Memory_contigFree(encodedBuffers[bufIdx], imageSize);
        }
    }

    for (bufIdx=0; bufIdx < CAP_BUFFERS; bufIdx++) {
        if (captureBuffers[bufIdx]) {
            Memory_contigFree(captureBuffers[bufIdx], imageSize);
        }
    }



	return status;
}

