#include <stdio.h>
#include <string.h>

#include <rendezvous.h>
#include <fifoutil.h>
#include <pause.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include "encode.h"
#include "writer.h"

int32_t sendBuf[100000];
#define UDP_PACKET_SIZE 65026
#define SERVER_IP "192.168.0.8"

void *writerThrFxn(void *arg)
{
    WriterBufferElement		wFlush        = { WRITER_FLUSH };
    WriterEnv				*envp          = (WriterEnv *) arg;
    void					*status        = THREAD_SUCCESS;
    WriterBufferElement		we;
    int						ret = 0, cnt = 0, pnum = 0;
    FILE					*outputFp = NULL;
    double sumT = 0.0f, testT = 0.0f;
    int port = 2200;
    int opt = 1, sendret = 0;
    int ttl = 32;
    struct sockaddr_in addr;
    int sock = -1;
	struct timeval before, after, result;
	char * tmpPoint = NULL;
	char tmp[30];		

//    sock = socket(AF_INET, SOCK_DGRAM, NULL);
    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

#if 0 
 	addr.sin_addr.s_addr = inet_addr(envp->IP_ADDR);
    printf("\n\n WRITE IP_ADDR %s \n" , envp->IP_ADDR); 
    
    setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
#else
//    addr.sin_addr.s_addr = INADDR_BROADCAST;
//    printf("\n\n WRITE IP_ADDR 0x%X \n" , INADDR_BROADCAST); 

	addr.sin_addr.s_addr = inet_addr(SERVER_IP);
	printf("\n\n WRITE IP_ADDR %s, port %d \n" , SERVER_IP, port); 

	setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &opt, sizeof(opt));
#endif

    Rendezvous_meet(envp->hRendezvousInit);

    DBG("Entering writer main loop.\n");
    while (!gblGetQuit()) {
		cnt++;
		sendret = 0;
		//sprintf(tmp,"./raw/dsp_%03d_%03d%s",cnt, envp->quality, envp->videoFile); 
		//sprintf(tmp,"/media/ram/raw_%02d_%s",cnt,   envp->videoFile);

		sprintf(tmp,"/tmp/jpegdemo/image%d%s",cnt,   envp->videoFile);
		//sprintf(tmp,"/media/ram/jpegdemo/image%d%s",cnt,   envp->videoFile);
		//sprintf(tmp,"/media/ram/jpegdemo/image1.jpg");	
		
		if (cnt == 30) 
			cnt = 0;

	    if (envp->videoFile) {
		    outputFp = fopen(tmp, "wb");
		    if (outputFp == NULL) {
		        ERR("Failed to open %s for writing\n", envp->videoFile);
		        cleanup(THREAD_FAILURE);
		    }
		    //DBG("Video file successfully opened\n");
		}

        if (FifoUtil_get(&envp->wInFifo, &we) == FIFOUTIL_FAILURE) {        
            breakLoop(THREAD_FAILURE);
        }

        if (we.id == WRITER_FLUSH) {
            breakLoop(THREAD_SUCCESS);
        }


        if (we.id != WRITER_PRIME && we.frameSize) {

			aligned_copy_neon(sendBuf, (int *)we.encodedBuffer, ROUNDUP(we.frameSize, 64));
       	    if (envp->videoFile) {
       	    	//DBG("Writing the encoded data to video file\n");
	            if (fwrite(sendBuf, we.frameSize, 1, outputFp) != 1) {
	                ERR("Error writing the encoded data to video file\n");
	                breakLoop(THREAD_FAILURE);
	            }
	            fflush(outputFp);
	            fclose(outputFp);
	        }
#if 1
			//gettimeofday(&before, NULL);
			//tmpPoint = we.encodedBuffer;
			tmpPoint = sendBuf;
	        if ( sendto(sock, &we.frameSize,  sizeof(int), 0, (struct sockaddr*)&addr, sizeof(addr)) ) {
    			if ( we.frameSize > UDP_PACKET_SIZE )
    				ret = UDP_PACKET_SIZE;
    			else
    				ret = we.frameSize;

				while ( sendret < we.frameSize ) {
					ret = sendto(sock, tmpPoint, ret, 0, (struct sockaddr*)&addr, sizeof(addr)) ;
					if( ret < 0 ) {
					    ERR("Error sendto the encoded Data(%d, %d) to video file\n",we.frameSize ,sendret);
						cleanup(THREAD_FAILURE);	
					}
					sendret += ret;
					tmpPoint += ret;
	    			if ( (we.frameSize - sendret) > UDP_PACKET_SIZE )
	    				ret = UDP_PACKET_SIZE;
	    			else
	    				ret = we.frameSize - sendret;
					//printf("%d ",sendret);
		        }
		    	//printf("\n");
		    	//fprintf(stderr, "sendto %d\n",sendret);
		    } else {
			    ERR("Error sendto the encoded Size to video file\n");
                cleanup(THREAD_FAILURE);	
			}
			//gettimeofday(&after, NULL);
	    	//close(sock);

			//pnum++;
			//sumT += testT = (double)(after.tv_sec)+(double)(after.tv_usec)/1000000.0-(double)(before.tv_sec)-(double)(before.tv_usec)/1000000.0;
			//fprintf(stderr, "#%5d  time %0.6f (%0.6f): %5d \n",pnum, testT, (sumT / pnum), sendret);

#endif
        }
        else {
            we.id = WRITER_NORMAL;
        }
//	    sock = socket(AF_INET, SOCK_DGRAM, NULL);
//	    bzero(&addr, sizeof(addr));
//	    addr.sin_family = AF_INET;
//	    addr.sin_port = htons(port);
//
//	    addr.sin_addr.s_addr = INADDR_BROADCAST;
//		setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &opt, sizeof(opt));

        if (FifoUtil_put(&(envp->wOutFifo), &we) == FIFOUTIL_FAILURE) {
            ERR("Failed to put buffer in output fifo\n");
            breakLoop(THREAD_FAILURE);
        }


//	    addr.sin_addr.s_addr = inet_addr(envp->IP_ADDR);
//	    setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));
//	    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    }

cleanup:

    Rendezvous_force(envp->hRendezvousInit);
    Pause_off(envp->hPause);
    FifoUtil_put(&envp->wOutFifo, &wFlush);
    Rendezvous_meet(envp->hRendezvousCleanup);
    
    if ( sock >= 0 )
    	close(sock);
	
	fprintf(stderr,"writerThrFxn closing...\n");
    return status;
}
