/*
 * ctrl.h
 */

#ifndef _CTRL_H
#define _CTRL_H

#include "rendezvous.h"
#include "encode.h"
#include "pause.h"

#define FOREVER					-1


#define REMOTECONTROLLATENCY	100


typedef struct CtrlEnv {
	Rendezvous_Handle	hRendezvousInit;
	Rendezvous_Handle	hRendezvousCleanup;
	Pause_Handle		hPause;
	int					imageWidth;
	int					imageHeight;
	char				*videoFile;
	int					time;
} CtrlEnv;

#define TCP_PACKET_SIZE 65026


extern void *ctrlThrFxn(void *arg);
extern void *ispThrFxn(void *arg);

#endif /* _CTRL_H */
