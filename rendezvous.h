#ifndef _RENDEZVOUS_H
#define _RENDEZVOUS_H

#include <pthread.h>

typedef struct Rendezvous_Obj {
    int             count;
    pthread_mutex_t mutex;
    pthread_cond_t  cond;
} Rendezvous_Obj;


typedef Rendezvous_Obj *Rendezvous_Handle;


#define RENDEZVOUS_SUCCESS 0
#define RENDEZVOUS_FAILURE -1


static inline void Rendezvous_open(Rendezvous_Handle hRv, int count)
{
    pthread_mutex_init(&hRv->mutex, NULL);
    pthread_cond_init(&hRv->cond, NULL);

    hRv->count = count;
}


static inline void Rendezvous_meet(Rendezvous_Handle hRv)
{
    pthread_mutex_lock(&hRv->mutex);
    hRv->count--;

    if (hRv->count > 0) {
        pthread_cond_wait(&hRv->cond, &hRv->mutex);
    } 
    else {
        pthread_cond_broadcast(&hRv->cond);
    }

    pthread_mutex_unlock(&hRv->mutex);
}


static inline void Rendezvous_force(Rendezvous_Handle hRv)
{
    pthread_mutex_lock(&hRv->mutex);
    hRv->count = 0;
    pthread_cond_broadcast(&hRv->cond);
    pthread_mutex_unlock(&hRv->mutex);
}


static inline void Rendezvous_close(Rendezvous_Handle hRv)
{
    pthread_mutex_destroy(&hRv->mutex);
    pthread_cond_destroy(&hRv->cond);
}

#endif // _RENDEZVOUS_H
