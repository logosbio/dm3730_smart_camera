/* Standard Linux headers */
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/ioctl.h>
#include <sys/resource.h>

#include <xdc/std.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/trace/gt.h>
#include <ti/sdo/ce/CERuntime.h>
#include <ti/sdo/ce/utils/trace/TraceUtil.h>

#include <rendezvous.h>
#include <fifoutil.h>
#include <pause.h>
#include "encode.h"
#include "display.h"
#include "video.h"
#include "capture.h"
#include "writer.h"
#include "ctrl.h"


#define LOGSINITIALIZED				0x1
#define PAUSEOPENED					0x2
#define INITRENDEZVOUSOPENED		0x4
#define CLEANUPRENDEZVOUSOPENED 	0x8
#define PRIMERENDEZVOUSOPENED   	0x10
#define INFIFODISPLAYOPENED	 		0x20
#define OUTFIFODISPLAYOPENED		0x40
#define DISPLAYTHREADCREATED		0x80
#define INFIFOCAPTUREOPENED			0x100
#define OUTFIFOCAPTUREOPENED		0x200
#define CAPTURETHREADCREATED		0x400
#define INFIFOWRITEROPENED			0x800
#define OUTFIFOWRITEROPENED			0x1000
#define WRITERTHREADCREATED			0x2000
#define VIDEOTHREADCREATED			0x4000


typedef struct Args {
    char			*IP_ADDR;
	char			*writeFile;
	int				imageWidth;
	int				imageHeight;
	DisplayOption	displayOption;
	VideoEncoder	videoEncoder;
	int				statusOsdFlag;
	int				ccdcFlag;
	int				prevFlag;
	int				quality;
	int				time;
} Args;

GlobalData gbl = { 0, 1, 0, 0 };

int getArmCpuLoad(int *procLoad, int *cpuLoad)
{
	static unsigned long	prevIdle = 0;
	static unsigned long	prevTotal = 0;
	static unsigned long	prevProc = 0;
	int						cpuLoadFound = FALSE;
	unsigned long			user, nice, sys, idle, total, proc;
	unsigned long			uTime, sTime, cuTime, csTime;
	unsigned long			deltaTotal, deltaIdle, deltaProc;
	char					textBuf[4];
	FILE					*fptr;

	fptr = fopen("/proc/stat", "r");

	if (fptr == NULL) {
		ERR("/proc/stat not found. Is the /proc filesystem mounted?\n");
		return FAILURE;
	}

	while (fscanf(fptr, "%4s %lu %lu %lu %lu %*[^\n]", textBuf, &user, &nice, &sys, &idle) != EOF) {
		if (strcmp(textBuf, "cpu") == 0) {
			cpuLoadFound = TRUE;
			break;
		}
	}

	if (fclose(fptr) != 0) {
		return FAILURE;
	}

	if (!cpuLoadFound) {
		return FAILURE;
	}

	fptr = fopen("/proc/self/stat", "r");

	if (fptr == NULL) {
		ERR("/proc/self/stat not found. Is the /proc filesystem mounted?\n");
		return FAILURE;
	}

	if (fscanf(fptr, "%*d %*s %*s %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %lu "
					 "%lu %lu %lu", &uTime, &sTime, &cuTime, &csTime) != 4) {
		ERR("Failed to get process load information.\n");
		fclose(fptr);
		return FAILURE;
	}

	if (fclose(fptr) != 0) {
		return FAILURE;
	}

	total = user + nice + sys + idle;
	proc = uTime + sTime + cuTime + csTime; 

	if (prevIdle == 0 && prevTotal == 0 && prevProc == 0) {
		prevIdle = idle;
		prevTotal = total;
		prevProc = proc; 
		return SUCCESS;
	}

	deltaIdle = idle - prevIdle;
	deltaTotal = total - prevTotal;
	deltaProc = proc - prevProc; 

	prevIdle = idle;
	prevTotal = total;
	prevProc = proc;

	*cpuLoad = 100 - deltaIdle * 100 / deltaTotal;
	*procLoad = deltaProc * 100 / deltaTotal;

	return SUCCESS;
}

int main(int argc, char *argv[])
{
	unsigned int		initMask  = 0;
	int					status	= EXIT_SUCCESS;
	int					numThreads;
	struct sched_param	schedParam;
	Rendezvous_Obj		rendezvousInit;
	Rendezvous_Obj		rendezvousCleanup;
	Rendezvous_Obj		rendezvousPrime;
	Pause_Obj			pause;
	pthread_t			displayThread;
	pthread_t			captureThread;
	pthread_t			writerThread;
	pthread_t			videoThread;

	DisplayEnv			displayEnv;
	CaptureEnv			captureEnv;
	WriterEnv			writerEnv;
	VideoEnv			videoEnv;
	CtrlEnv				ctrlEnv;
	pthread_t			ispThread;

	pthread_attr_t		attr;
	void				*ret;
	Args				args = {
		"224.100.62.8",		//	"239.2.37.221",		//	"232.255.42.44",
		NULL,
		1280,
		720,
		NONE_DISPLAY,
		NONE_VIDEO_ENCODER,
		0,
		75,
		0,
		0,
		FOREVER,
	};


	if (argc > 1) {
		args.statusOsdFlag = atoi(argv[1]);
		//args.videoEncoder = atoi(argv[1]);	//	encodeType
		args.videoEncoder = 1;
	}

	if (argc > 2) {
		args.quality = atoi(argv[2]);
	}

	if (argc > 4) {
		args.imageWidth = atoi(argv[3]);
		args.imageHeight = atoi(argv[4]);
	}

	if (argc > 5) {
		args.prevFlag = strtol(argv[5], (char *)ret, 16);

		printf("\n prevFlag 0x%0X %d\n",args.prevFlag,args.prevFlag);
	}

	if (argc > 6) {
		args.ccdcFlag = strtol(argv[6], (char *)ret, 16);
		printf("\n ccdcFlag 0x%0X %d\n",args.ccdcFlag,args.ccdcFlag);
	}

	if (argc > 7) {
		args.writeFile = argv[7];
	} 

	printf("Smart Camera Encode started.\n");

	pthread_mutex_init(&gbl.mutex, NULL);
	setpriority(PRIO_PROCESS, 0, -20);

	CERuntime_init();
	DBG("Codec Engine initialized\n");

	initMask |= LOGSINITIALIZED;

	DBG("Logging initialized\n");

	Pause_open(&pause);

	initMask |= PAUSEOPENED;

	DBG("Pause object opened\n");

	numThreads = 5;

	Rendezvous_open(&rendezvousInit, numThreads);

	initMask |= INITRENDEZVOUSOPENED;

	DBG("Init rendezvous opened for %d threads\n", numThreads);

	Rendezvous_open(&rendezvousCleanup, numThreads);

	initMask |= CLEANUPRENDEZVOUSOPENED;

	DBG("Cleanup rendezvous opened for %d threads\n", numThreads);

	Rendezvous_open(&rendezvousPrime, 1);

	initMask |= PRIMERENDEZVOUSOPENED;

	DBG("Priming rendezvous opened for 2 threads\n");


	if (pthread_attr_init(&attr)) {
		ERR("Failed to initialize thread attrs\n");
		cleanup(EXIT_FAILURE);
	}

	if (pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED)) {
		ERR("Failed to set schedule inheritance attribute\n");
		cleanup(EXIT_FAILURE);
	}

	if (pthread_attr_setschedpolicy(&attr, SCHED_FIFO)) {
		ERR("Failed to set FIFO scheduling policy\n");
		cleanup(EXIT_FAILURE);
	}

/* //////////////////////////////////////////////////////////////////////////////////////// */

	if (FifoUtil_open(&displayEnv.dispInFifo, sizeof(DisplayBufferElement)) == FIFOUTIL_FAILURE) {
		ERR("Failed to open input fifo\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= INFIFODISPLAYOPENED;

	if (FifoUtil_open(&displayEnv.dispOutFifo, sizeof(DisplayBufferElement)) == FIFOUTIL_FAILURE) {
		ERR("Failed to open output fifo\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= OUTFIFODISPLAYOPENED;

	schedParam.sched_priority = sched_get_priority_max(SCHED_FIFO) - 3;
	if (pthread_attr_setschedparam(&attr, &schedParam)) {
		ERR("Failed to set scheduler parameters\n");
		cleanup(EXIT_FAILURE);
	}

	displayEnv.hRendezvousInit		= &rendezvousInit;
	displayEnv.hRendezvousCleanup	= &rendezvousCleanup;
	displayEnv.hRendezvousPrime		= &rendezvousPrime;
	displayEnv.hPause				= &pause;
	displayEnv.displayOption		= args.displayOption;
	displayEnv.imageWidth			= args.imageWidth;
	displayEnv.imageHeight			= args.imageHeight;

	if (pthread_create(&displayThread, &attr, displayThrFxn, &displayEnv)) {
		ERR("Failed to create display thread\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= DISPLAYTHREADCREATED;

	DBG("Display thread created\n");

	if (FifoUtil_open(&captureEnv.capInFifo, sizeof(CaptureBufferElement)) == FIFOUTIL_FAILURE) {
		ERR("Failed to open capture input fifo\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= INFIFOCAPTUREOPENED;

	if (FifoUtil_open(&captureEnv.capOutFifo, sizeof(CaptureBufferElement)) == FIFOUTIL_FAILURE) {
		ERR("Failed to open capture output fifo\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= OUTFIFOCAPTUREOPENED;

	schedParam.sched_priority = sched_get_priority_max(SCHED_FIFO) - 1;
	if (pthread_attr_setschedparam(&attr, &schedParam)) {
		ERR("Failed to set scheduler parameters\n");
		cleanup(EXIT_FAILURE);
	}

	captureEnv.hRendezvousInit = &rendezvousInit;
	captureEnv.hRendezvousCleanup = &rendezvousCleanup;
	captureEnv.hRendezvousPrime = &rendezvousPrime;
	captureEnv.hPause = &pause;
	captureEnv.hDisplayOutFifo = &displayEnv.dispOutFifo;
	captureEnv.hDisplayInFifo = &displayEnv.dispInFifo;
	captureEnv.imageWidth = args.imageWidth;
	captureEnv.imageHeight = args.imageHeight;
	captureEnv.statusOsdFlag = args.statusOsdFlag;
	captureEnv.prevFlag = args.prevFlag;
	captureEnv.ccdcFlag = args.ccdcFlag;
	captureEnv.videoEncoder		  = args.videoEncoder;

	if (pthread_create(&captureThread, &attr, captureThrFxn, &captureEnv)) {
		ERR("Failed to create capture thread\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= CAPTURETHREADCREATED;

	DBG("Capture thread created\n");

	if (FifoUtil_open(&writerEnv.wInFifo, sizeof(WriterBufferElement)) == FIFOUTIL_FAILURE) {
		ERR("Failed to open input writer fifo\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= INFIFOWRITEROPENED;

	if (FifoUtil_open(&writerEnv.wOutFifo, sizeof(WriterBufferElement)) == FIFOUTIL_FAILURE) {
		ERR("Failed to open output writer fifo\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= OUTFIFOWRITEROPENED;

	schedParam.sched_priority = sched_get_priority_max(SCHED_FIFO) - 2;
	if (pthread_attr_setschedparam(&attr, &schedParam)) {
		ERR("Failed to set scheduler parameters\n");
		cleanup(EXIT_FAILURE);
	}

	/* Create the writer thread */
	writerEnv.hRendezvousInit		= &rendezvousInit;
	writerEnv.hRendezvousCleanup 	= &rendezvousCleanup;
	writerEnv.hPause			 	= &pause;
	writerEnv.videoFile				= args.writeFile;
	writerEnv.imageWidth			= args.imageWidth;
	writerEnv.imageHeight			= args.imageHeight;
	writerEnv.quality				= args.quality;
	writerEnv.IP_ADDR				= args.IP_ADDR;

	if (pthread_create(&writerThread, &attr, writerThrFxn, &writerEnv)) {
		ERR("Failed to create writer thread\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= WRITERTHREADCREATED;

	DBG("Writer thread created\n");

	schedParam.sched_priority = sched_get_priority_max(SCHED_FIFO);
	if (pthread_attr_setschedparam(&attr, &schedParam)) {
		ERR("Failed to set scheduler parameters\n");
		cleanup(EXIT_FAILURE);
	}

	/* Create the video thread */
	videoEnv.hRendezvousInit	= &rendezvousInit;
	videoEnv.hRendezvousCleanup	= &rendezvousCleanup;
	videoEnv.hPause				= &pause;
	videoEnv.hCaptureOutFifo	= &captureEnv.capOutFifo;
	videoEnv.hCaptureInFifo		= &captureEnv.capInFifo;
	videoEnv.hWriterOutFifo		= &writerEnv.wOutFifo;
	videoEnv.hWriterInFifo		= &writerEnv.wInFifo;
	videoEnv.videoEncoder		= args.videoEncoder;
	videoEnv.imageWidth			= args.imageWidth;
	videoEnv.imageHeight		= args.imageHeight;
	videoEnv.quality			= args.quality;

	if (pthread_create(&videoThread, &attr, videoThrFxn, &videoEnv)) {
		ERR("Failed to create video thread\n");
		cleanup(EXIT_FAILURE);
	}

	initMask |= VIDEOTHREADCREATED;

	if (pthread_create(&ispThread, &attr, ispThrFxn, &ctrlEnv)) {
		ERR("Failed to create video thread\n");
		cleanup(EXIT_FAILURE);
	}



	ctrlEnv.hRendezvousInit		= &rendezvousInit;
	ctrlEnv.hRendezvousCleanup	= &rendezvousCleanup;
	ctrlEnv.hPause				= &pause;
	ctrlEnv.imageWidth			= args.imageWidth;
	ctrlEnv.imageHeight			= args.imageHeight;
	ctrlEnv.videoFile			= args.writeFile;

	ret = ctrlThrFxn(&ctrlEnv);

	if (ret == THREAD_FAILURE) {
		status = EXIT_FAILURE;
	}

cleanup:
	Rendezvous_force(&rendezvousInit);
	Pause_off(&pause);

	if (initMask & VIDEOTHREADCREATED) {
		if (pthread_join(videoThread, &ret) == 0) {
			if (ret == THREAD_FAILURE) {
				status = EXIT_FAILURE;
			}
		}
	}

	if (initMask & WRITERTHREADCREATED) {
		if (pthread_join(writerThread, &ret) == 0) {
			if (ret == THREAD_FAILURE) {
				status = EXIT_FAILURE;
			}
		}
	}

	if (initMask & OUTFIFOWRITEROPENED) {
		FifoUtil_close(&writerEnv.wOutFifo);
	}

	if (initMask & INFIFOWRITEROPENED) {
		FifoUtil_close(&writerEnv.wInFifo);
	}

	if (initMask & CAPTURETHREADCREATED) {
		if (pthread_join(captureThread, &ret) == 0) {
			if (ret == THREAD_FAILURE) {
				status = EXIT_FAILURE;
			}
		}
	}

	if (initMask & OUTFIFOCAPTUREOPENED) {
		FifoUtil_close(&captureEnv.capOutFifo);
	}

	if (initMask & INFIFOCAPTUREOPENED) {
		FifoUtil_close(&captureEnv.capInFifo);
	}

	if (initMask & DISPLAYTHREADCREATED) {
		if (pthread_join(displayThread, &ret) == 0) {
			if (ret == THREAD_FAILURE) {
				status = EXIT_FAILURE;
			}
		}
	}

	if (initMask & OUTFIFODISPLAYOPENED) {
		FifoUtil_close(&displayEnv.dispOutFifo);
	}

	if (initMask & INFIFODISPLAYOPENED) {
		FifoUtil_close(&displayEnv.dispInFifo);
	}

	if (initMask & PRIMERENDEZVOUSOPENED) {
		Rendezvous_close(&rendezvousPrime);
	}

	if (initMask & CLEANUPRENDEZVOUSOPENED) {
		Rendezvous_close(&rendezvousCleanup);
	}

	if (initMask & INITRENDEZVOUSOPENED) {
		Rendezvous_close(&rendezvousInit);
	}

	if (initMask & PAUSEOPENED) {
		Pause_close(&pause);
	}

	pthread_mutex_destroy(&gbl.mutex);

	exit(status);
}
