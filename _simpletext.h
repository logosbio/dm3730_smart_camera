#ifndef _SIMPLETEXT_H
#define _SIMPLETEXT_H

#define TEXT_MAX_TEXT_LENGTH 100

typedef struct _simpletext {
    int x;                            // The x position of the button.
    int y;                            // The y position of the button.
    int h;                            // The height of the font.
    char txt[TEXT_MAX_TEXT_LENGTH];   // The text to render.
} _simpletext;

#endif // _SIMPLETEXT_H
