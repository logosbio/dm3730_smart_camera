/*
 * preview.h
 */

#ifndef _PREVIEW_H
#define _PREVIEW_H

#include <rendezvous.h>
#include "encode.h"

#define PREVIEW_FLUSH		-1
#define PREVIEW_PRIME		-2

#define RGB_MAX				3

#define PREV_IOC_BASE			'P'
#define PREV_REQBUF				_IOWR(PREV_IOC_BASE, 1, struct v4l2_requestbuffers)
#define PREV_QUERYBUF			_IOWR(PREV_IOC_BASE, 2, struct v4l2_buffer)
#define PREV_SET_PARAM			_IOW(PREV_IOC_BASE, 3, struct prev_params)
#define PREV_GET_PARAM			_IOWR(PREV_IOC_BASE, 4, struct prev_params)
#define PREV_PREVIEW			_IOR(PREV_IOC_BASE, 5, int)
#define PREV_GET_STATUS			_IOR(PREV_IOC_BASE, 6, char)
#define PREV_GET_CROPSIZE		_IOR(PREV_IOC_BASE, 7, struct prev_cropsize)
#define PREV_QUEUEBUF			_IOWR(PREV_IOC_BASE, 8, struct v4l2_buffer)
#define PREV_IOC_MAXNR			8

#define VIDIOC_PRIVATE_ISP_CCDC_CFG	\
	_IOWR('V', BASE_VIDIOC_PRIVATE + 1, struct ispccdc_update_config)
#define VIDIOC_PRIVATE_ISP_PRV_CFG \
	_IOWR('V', BASE_VIDIOC_PRIVATE + 2, struct ispprv_update_config)
#define VIDIOC_PRIVATE_ISP_AEWB_CFG \
	_IOWR('V', BASE_VIDIOC_PRIVATE + 4, struct isph3a_aewb_config)
#define VIDIOC_PRIVATE_ISP_AEWB_REQ \
	_IOWR('V', BASE_VIDIOC_PRIVATE + 5, struct isph3a_aewb_data)
#define VIDIOC_PRIVATE_ISP_HIST_CFG \
	_IOWR('V', BASE_VIDIOC_PRIVATE + 6, struct isp_hist_config)
#define VIDIOC_PRIVATE_ISP_HIST_REQ \
	_IOWR('V', BASE_VIDIOC_PRIVATE + 7, struct isp_hist_data)
#define VIDIOC_PRIVATE_ISP_AF_CFG \
	_IOWR('V', BASE_VIDIOC_PRIVATE + 8, struct af_configuration)
#define VIDIOC_PRIVATE_ISP_AF_REQ \
	_IOWR('V', BASE_VIDIOC_PRIVATE + 9, struct isp_af_data)
#define VIDIOC_PRIVATE_OMAP34XXCAM_SENSOR_INFO	\
	_IOWR('V', BASE_VIDIOC_PRIVATE + 10, struct omap34xxcam_sensor_info)

#define LUMA_TABLE_SIZE			128
#define GAMMA_TABLE_SIZE		1024
#define CFA_COEFF_TABLE_SIZE		576
#define NOISE_FILTER_TABLE_SIZE		256

#define MAX_IMAGE_WIDTH			3300

#define PREV_INWIDTH_8BIT		0	/* pixel width of 8 bits */
#define PREV_INWIDTH_10BIT		1	/* pixel width of 10 bits */

#define PREV_32BYTES_ALIGN_MASK		0xFFFFFFE0
#define PREV_16PIX_ALIGN_MASK		0xFFFFFFF0

#define ISP_ABS_CCDC_ALAW			(1 << 0)
#define ISP_ABS_CCDC_LPF 			(1 << 1)
#define ISP_ABS_CCDC_BLCLAMP		(1 << 2)
#define ISP_ABS_CCDC_BCOMP			(1 << 3)
#define ISP_ABS_CCDC_FPC			(1 << 4)
#define ISP_ABS_CCDC_CULL			(1 << 5)
#define ISP_ABS_CCDC_COLPTN			(1 << 6)
#define ISP_ABS_CCDC_CONFIG_LSC		(1 << 7)
#define ISP_ABS_TBL_LSC				(1 << 8)


#define ISPPRV_BRIGHT_STEP		0x1
#define ISPPRV_BRIGHT_DEF		0x1
#define ISPPRV_BRIGHT_LOW		0x0
#define ISPPRV_BRIGHT_HIGH		0xFF
#define ISPPRV_BRIGHT_UNITS		0x1

#define ISPPRV_CONTRAST_STEP		0x1
#define ISPPRV_CONTRAST_DEF		0x10
#define ISPPRV_CONTRAST_LOW		0x0
#define ISPPRV_CONTRAST_HIGH		0xFF
#define ISPPRV_CONTRAST_UNITS		0x1

#define ISP_ABS_PREV_LUMAENH		(1 << 0)
#define ISP_ABS_PREV_INVALAW		(1 << 1)
#define ISP_ABS_PREV_HRZ_MED		(1 << 2)
#define ISP_ABS_PREV_CFA		(1 << 3)
#define ISP_ABS_PREV_CHROMA_SUPP	(1 << 4)
#define ISP_ABS_PREV_WB			(1 << 5)
#define ISP_ABS_PREV_BLKADJ		(1 << 6)
#define ISP_ABS_PREV_RGB2RGB		(1 << 7)
#define ISP_ABS_PREV_COLOR_CONV		(1 << 8)
#define ISP_ABS_PREV_YC_LIMIT		(1 << 9)
#define ISP_ABS_PREV_DEFECT_COR		(1 << 10)
#define ISP_ABS_PREV_GAMMABYPASS	(1 << 11)
#define ISP_ABS_TBL_NF 			(1 << 12)
#define ISP_ABS_TBL_REDGAMMA		(1 << 13)
#define ISP_ABS_TBL_GREENGAMMA		(1 << 14)
#define ISP_ABS_TBL_BLUEGAMMA		(1 << 15)

#define ISPPRV_NF_TBL_SIZE		64
#define ISPPRV_CFA_TBL_SIZE		576
#define ISPPRV_GAMMA_TBL_SIZE		1024
#define ISPPRV_YENH_TBL_SIZE		128

#define NO_AVE				0x0
#define AVE_2_PIX			0x1
#define AVE_4_PIX			0x2
#define AVE_8_PIX			0x3
#define AVE_ODD_PIXEL_DIST		(1 << 4) /* For Bayer Sensors */
#define AVE_EVEN_PIXEL_DIST		(1 << 2)

#define WB_GAIN_MAX			4

/* Features list */
#define PREV_AVERAGER			(1 << 0)
#define PREV_INVERSE_ALAW 		(1 << 1)
#define PREV_HORZ_MEDIAN_FILTER		(1 << 2)
#define PREV_NOISE_FILTER 		(1 << 3)
#define PREV_CFA			(1 << 4)
#define PREV_GAMMA_BYPASS		(1 << 5)
#define PREV_LUMA_ENHANCE		(1 << 6)
#define PREV_CHROMA_SUPPRESS		(1 << 7)
#define PREV_DARK_FRAME_SUBTRACT	(1 << 8)
#define PREV_LENS_SHADING		(1 << 9)
#define PREV_DARK_FRAME_CAPTURE		(1 << 10)
#define PREV_DEFECT_COR			(1 << 11)

#define ISP_NF_TABLE_SIZE 		(1 << 10)

#define ISP_GAMMA_TABLE_SIZE 		(1 << 10)

/* Table addresses */
#define ISPPRV_TBL_ADDR_RED_G_START  0x00
#define ISPPRV_TBL_ADDR_BLUE_G_START  0x800
#define ISPPRV_TBL_ADDR_GREEN_G_START  0x400

/*
 *Enumeration Constants for input and output format
 */
typedef enum preview_setmode {
	PREV_MODE_NONE,
	PREV_MODE_LE,
	PREV_MODE_IALAW,
	PREV_MODE_HM,
	PREV_MODE_CFA,
	PREV_MODE_CS,
	PREV_MODE_WB,
	PREV_MODE_BA,
	PREV_MODE_R,
	PREV_MODE_CC,
	PREV_MODE_YC,
	PREV_MODE_DC,
	PREV_MODE_GB,
	PREV_MODE_NF,
	PREV_MODE_RG,
	PREV_MODE_GG,
	PREV_MODE_BG,
	PREV_MODE_ALL,
}preview_setmode;

enum preview_input {
	PRV_RAW_CCDC,
	PRV_RAW_MEM,
	PRV_RGBBAYERCFA,
	PRV_COMPCFA,
	PRV_CCDC_DRKF,
	PRV_OTHERS
};
enum preview_output {
	PREVIEW_RSZ,
	PREVIEW_MEM
};
/*
 * Configure byte layout of YUV image
 */
enum preview_ycpos_mode {
	YCPOS_YCrYCb = 0,
	YCPOS_YCbYCr = 1,
	YCPOS_CbYCrY = 2,
	YCPOS_CrYCbY = 3
};

enum alaw_ipwidth {
	ALAW_BIT12_3 = 0x3,
	ALAW_BIT11_2 = 0x4,
	ALAW_BIT10_1 = 0x5,
	ALAW_BIT9_0 = 0x6
};

/**
 * struct ispccdc_lsc_config - Structure for LSC configuration.
 * @offset: Table Offset of the gain table.
 * @gain_mode_n: Vertical dimension of a paxel in LSC configuration.
 * @gain_mode_m: Horizontal dimension of a paxel in LSC configuration.
 * @gain_format: Gain table format.
 * @fmtsph: Start pixel horizontal from start of the HS sync pulse.
 * @fmtlnh: Number of pixels in horizontal direction to use for the data
 *          reformatter.
 * @fmtslv: Start line from start of VS sync pulse for the data reformatter.
 * @fmtlnv: Number of lines in vertical direction for the data reformatter.
 * @initial_x: X position, in pixels, of the first active pixel in reference
 *             to the first active paxel. Must be an even number.
 * @initial_y: Y position, in pixels, of the first active pixel in reference
 *             to the first active paxel. Must be an even number.
 * @size: Size of LSC gain table. Filled when loaded from userspace.
 */
struct ispccdc_lsc_config {
	__u16 offset;
	__u8 gain_mode_n;
	__u8 gain_mode_m;
	__u8 gain_format;
	__u16 fmtsph;
	__u16 fmtlnh;
	__u16 fmtslv;
	__u16 fmtlnv;
	__u8 initial_x;
	__u8 initial_y;
	__u32 size;
};

/**
 * struct ispccdc_bclamp - Structure for Optical & Digital black clamp subtract
 * @obgain: Optical black average gain.
 * @obstpixel: Start Pixel w.r.t. HS pulse in Optical black sample.
 * @oblines: Optical Black Sample lines.
 * @oblen: Optical Black Sample Length.
 * @dcsubval: Digital Black Clamp subtract value.
 */
struct ispccdc_bclamp {
	__u8 obgain;
	__u8 obstpixel;
	__u8 oblines;
	__u8 oblen;
	__u16 dcsubval;
};

/**
 * ispccdc_fpc - Structure for FPC
 * @fpnum: Number of faulty pixels to be corrected in the frame.
 * @fpcaddr: Memory address of the FPC Table
 */
struct ispccdc_fpc {
	__u16 fpnum;
	__u32 fpcaddr;
};

struct set_ispccdc_fpc {
	__u16 fpnum;
	__u32 fpcaddr[5];
};
/**
 * ispccdc_blcomp - Structure for Black Level Compensation parameters.
 * @b_mg: B/Mg pixels. 2's complement. -128 to +127.
 * @gb_g: Gb/G pixels. 2's complement. -128 to +127.
 * @gr_cy: Gr/Cy pixels. 2's complement. -128 to +127.
 * @r_ye: R/Ye pixels. 2's complement. -128 to +127.
 */
struct ispccdc_blcomp {
	__u8 b_mg;
	__u8 gb_g;
	__u8 gr_cy;
	__u8 r_ye;
};


/**
 * ispccdc_culling - Structure for Culling parameters.
 * @v_pattern: Vertical culling pattern.
 * @h_odd: Horizontal Culling pattern for odd lines.
 * @h_even: Horizontal Culling pattern for even lines.
 */
struct ispccdc_culling {
	__u8 v_pattern;
	__u16 h_odd;
	__u16 h_even;
};

/**
 * ispccdc_update_config - Structure for CCDC configuration.
 * @update: Specifies which CCDC registers should be updated.
 * @flag: Specifies which CCDC functions should be enabled.
 * @alawip: Enable/Disable A-Law compression.
 * @bclamp: Black clamp control register.
 * @blcomp: Black level compensation value for RGrGbB Pixels. 2's complement.
 * @fpc: Number of faulty pixels corrected in the frame, address of FPC table.
 * @cull: Cull control register.
 * @colptn: Color pattern of the sensor.
 * @lsc: Pointer to LSC gain table.
 */
struct ispccdc_update_config {
	__u16 update;
	__u16 flag;
	enum alaw_ipwidth alawip;
	struct ispccdc_bclamp *bclamp;
	struct ispccdc_blcomp *blcomp;
	struct ispccdc_fpc *fpc;
	struct ispccdc_lsc_config *lsc_cfg;
	struct ispccdc_culling *cull;
	__u32 colptn;
	__u8 *lsc;
};

/**
 * struct ispprev_rgbtorgb - Structure for RGB to RGB Blending.
 * @matrix: Blending values(S12Q8 format)
 *              [RR] [GR] [BR]
 *              [RG] [GG] [BG]
 *              [RB] [GB] [BB]
 * @offset: Blending offset value for R,G,B in 2's complement integer format.
 */
struct ispprev_rgbtorgb {
	unsigned short matrix[3][3];
	unsigned short offset[3];
};

/**
 * struct ispprev_csc - Structure for Color Space Conversion from RGB-YCbYCr
 * @matrix: Color space conversion coefficients(S10Q8)
 *              [CSCRY]  [CSCGY]  [CSCBY]
 *              [CSCRCB] [CSCGCB] [CSCBCB]
 *              [CSCRCR] [CSCGCR] [CSCBCR]
 * @offset: CSC offset values for Y offset, CB offset and CR offset respectively
 */
struct ispprev_csc {
	unsigned short matrix[RGB_MAX][RGB_MAX];
	unsigned short offset[RGB_MAX];
};

/**
 * struct ispprev_gtable - Structure for Gamma Correction.
 * @redtable: Pointer to the red gamma table.
 * @greentable: Pointer to the green gamma table.
 * @bluetable: Pointer to the blue gamma table.
 */
struct ispprev_gtable {
	unsigned int *redtable;
	unsigned int *greentable;
	unsigned int *bluetable;
};
struct ispprev_hmed {
	unsigned char odddist;
	unsigned char evendist;
	unsigned char thres;
};

/*
 * Enumeration for CFA Formats supported by preview
 */
enum cfa_fmt {
	CFAFMT_BAYER, CFAFMT_SONYVGA, CFAFMT_RGBFOVEON,
	CFAFMT_DNSPL, CFAFMT_HONEYCOMB, CFAFMT_RRGGBBFOVEON
};

/**
 * struct ispprev_cfa - Structure for CFA Inpterpolation.
 * @cfafmt: CFA Format Enum value supported by preview.
 * @cfa_gradthrs_vert: CFA Gradient Threshold - Vertical.
 * @cfa_gradthrs_horz: CFA Gradient Threshold - Horizontal.
 * @cfa_table: Pointer to the CFA table.
 */
struct ispprev_cfa {
	enum cfa_fmt cfafmt;
	unsigned char cfa_gradthrs_vert;
	unsigned char cfa_gradthrs_horz;
	unsigned int *cfa_table;
};

/**
 * struct ispprev_csup - Structure for Chrominance Suppression.
 * @gain: Gain.
 * @thres: Threshold.
 * @hypf_en: Flag to enable/disable the High Pass Filter.
 */
struct ispprev_csup {
	unsigned char gain;
	unsigned char thres;
	unsigned char hypf_en;
};

/**
 * struct ispprev_wbal - Structure for White Balance.
 * @dgain: Digital gain (U10Q8).
 * @coef3: White balance gain - COEF 3 (U8Q5).
 * @coef2: White balance gain - COEF 2 (U8Q5).
 * @coef1: White balance gain - COEF 1 (U8Q5).
 * @coef0: White balance gain - COEF 0 (U8Q5).
 */
struct ispprev_wbal {
	unsigned short dgain;
	unsigned char coef3;
	unsigned char coef2;
	unsigned char coef1;
	unsigned char coef0;
};

/**
 * struct ispprev_blkadj - Structure for Black Adjustment.
 * @red: Black level offset adjustment for Red in 2's complement format
 * @green: Black level offset adjustment for Green in 2's complement format
 * @blue: Black level offset adjustment for Blue in 2's complement format
 */
struct ispprev_blkadj {
	/*Black level offset adjustment for Red in 2's complement format */
	unsigned char red;
	/*Black level offset adjustment for Green in 2's complement format */
	unsigned char green;
	/* Black level offset adjustment for Blue in 2's complement format */
	unsigned char blue;
};

/**
 * struct ispprev_yclimit - Structure for Y, C Value Limit.
 * @minC: Minimum C value
 * @maxC: Maximum C value
 * @minY: Minimum Y value
 * @maxY: Maximum Y value
 */
struct ispprev_yclimit {
	unsigned char minC;
	unsigned char maxC;
	unsigned char minY;
	unsigned char maxY;
};

/**
 * struct ispprev_dcor - Structure for Defect correction.
 * @couplet_mode_en: Flag to enable or disable the couplet dc Correction in NF
 * @detect_correct: Thresholds for correction bit 0:10 detect 16:25 correct
 */
struct ispprev_dcor {
	unsigned char couplet_mode_en;
	unsigned int detect_correct[4];
};

/**
 * struct ispprev_nf - Structure for Noise Filter
 * @spread: Spread value to be used in Noise Filter
 * @table: Pointer to the Noise Filter table
 */
struct ispprev_nf {
	unsigned char spread;
	unsigned int table[ISPPRV_NF_TBL_SIZE];
};

struct set_ispprev_nf {
	unsigned char spread;
	unsigned int table;
};

/**
 * struct prev_white_balance - Structure for White Balance 2.
 * @wb_dgain: White balance common gain.
 * @wb_gain: Individual color gains.
 * @wb_coefmatrix: Coefficient matrix
 */
struct prev_white_balance {
	unsigned short wb_dgain; /* white balance common gain */
	unsigned char wb_gain[WB_GAIN_MAX]; /* individual color gains */
	unsigned char wb_coefmatrix[WB_GAIN_MAX][WB_GAIN_MAX];
};

/**
 * struct prev_size_params - Structure for size parameters.
 * @hstart: Starting pixel.
 * @vstart: Starting line.
 * @hsize: Width of input image.
 * @vsize: Height of input image.
 * @pixsize: Pixel size of the image in terms of bits.
 * @in_pitch: Line offset of input image.
 * @out_pitch: Line offset of output image.
 */
struct prev_size_params {
	unsigned int hstart;
	unsigned int vstart;
	unsigned int hsize;
	unsigned int vsize;
	unsigned char pixsize;
	unsigned short in_pitch;
	unsigned short out_pitch;
};

/**
 * struct prev_rgb2ycbcr_coeffs - Structure RGB2YCbCr parameters.
 * @coeff: Color conversion gains in 3x3 matrix.
 * @offset: Color conversion offsets.
 */
struct prev_rgb2ycbcr_coeffs {
	short coeff[RGB_MAX][RGB_MAX];
	short offset[RGB_MAX];
};

/**
 * struct prev_darkfrm_params - Structure for Dark frame suppression.
 * @addr: Memory start address.
 * @offset: Line offset.
 */
struct prev_darkfrm_params {
	unsigned int addr;
	 unsigned int offset;
 };

/**
 * struct isptables_update - Structure for Table Configuration.
 * @update: Specifies which tables should be updated.
 * @flag: Specifies which tables should be enabled.
 * @prev_nf: Pointer to structure for Noise Filter
 * @lsc: Pointer to LSC gain table. (currently not used)
 * @red_gamma: Pointer to red gamma correction table.
 * @green_gamma: Pointer to green gamma correction table.
 * @blue_gamma: Pointer to blue gamma correction table.
 */
struct isptables_update {
	unsigned short update;
	unsigned short flag;
	struct ispprev_nf *prev_nf;
	unsigned int *lsc;
	unsigned int *red_gamma;
	unsigned int *green_gamma;
	unsigned int *blue_gamma;
	struct ispprev_cfa *prev_cfa;
};

/**
 * struct ispprv_update_config - Structure for Preview Configuration (user).
 * @update: Specifies which ISP Preview registers should be updated.
 * @flag: Specifies which ISP Preview functions should be enabled.
 * @yen: Pointer to luma enhancement table.
 * @shading_shift: 3bit value of shift used in shading compensation.
 * @prev_hmed: Pointer to structure containing the odd and even distance.
 *             between the pixels in the image along with the filter threshold.
 * @prev_cfa: Pointer to structure containing the CFA interpolation table, CFA.
 *            format in the image, vertical and horizontal gradient threshold.
 * @csup: Pointer to Structure for Chrominance Suppression coefficients.
 * @prev_wbal: Pointer to structure for White Balance.
 * @prev_blkadj: Pointer to structure for Black Adjustment.
 * @rgb2rgb: Pointer to structure for RGB to RGB Blending.
 * @prev_csc: Pointer to structure for Color Space Conversion from RGB-YCbYCr.
 * @yclimit: Pointer to structure for Y, C Value Limit.
 * @prev_dcor: Pointer to structure for defect correction.
 * @prev_nf: Pointer to structure for Noise Filter
 * @red_gamma: Pointer to red gamma correction table.
 * @green_gamma: Pointer to green gamma correction table.
 * @blue_gamma: Pointer to blue gamma correction table.
 */
struct ispprv_update_config {
	unsigned short update;
	unsigned short flag;
	void *yen;
	unsigned int shading_shift;
	struct ispprev_hmed *prev_hmed;
	struct ispprev_cfa *prev_cfa;
	struct ispprev_csup *csup;
	struct ispprev_wbal *prev_wbal;
	struct ispprev_blkadj *prev_blkadj;
	struct ispprev_rgbtorgb *rgb2rgb;
	struct ispprev_csc *prev_csc;
	struct ispprev_yclimit *yclimit;
	struct ispprev_dcor *prev_dcor;
	struct ispprev_nf *prev_nf;
	unsigned int *red_gamma;
	unsigned int *green_gamma;
	unsigned int *blue_gamma;
};

/**
 * struct prev_params - Structure for all configuration
 * @features: Set of features enabled.
 * @cfa: CFA coefficients.
 * @csup: Chroma suppression coefficients.
 * @ytable: Pointer to Luma enhancement coefficients.
 * @nf: Noise filter coefficients.
 * @dcor: Noise filter coefficients.
 * @gtable: Gamma coefficients.
 * @wbal: White Balance parameters.
 * @blk_adj: Black adjustment parameters.
 * @rgb2rgb: RGB blending parameters.
 * @rgb2ycbcr: RGB to ycbcr parameters.
 * @hmf_params: Horizontal median filter.
 * @size_params: Size parameters.
 * @drkf_params: Darkframe parameters.
 * @lens_shading_shift:
 * @average: Downsampling rate for averager.
 * @contrast: Contrast.
 * @brightness: Brightness.
 */
struct prev_params {
	unsigned short features;
	enum preview_ycpos_mode pix_fmt;
	struct ispprev_cfa cfa;
	struct ispprev_csup csup;
	unsigned int *ytable;
	struct ispprev_nf nf;
	struct ispprev_dcor dcor;
	struct ispprev_gtable gtable;
	struct ispprev_wbal wbal;
	struct ispprev_blkadj blk_adj;
	struct ispprev_rgbtorgb rgb2rgb;
	struct ispprev_csc rgb2ycbcr;
	struct ispprev_hmed hmf_params;
	struct prev_size_params size_params;
	struct prev_darkfrm_params drkf_params;
	unsigned char lens_shading_shift;
	unsigned char average;
	unsigned char contrast;
	unsigned char brightness;
};


struct set_ispccdc_update_config {
	unsigned short update;
	unsigned short flag;
	enum alaw_ipwidth alawip;
	struct ispccdc_bclamp bclamp;
	struct ispccdc_blcomp blcomp;
	struct set_ispccdc_fpc fpc;
	struct ispccdc_lsc_config lsc_cfg;
	struct ispccdc_culling cull;
	unsigned int colptn;
	__u8 *lsc;
};


struct set_ispprv_update_config {
	unsigned short update;
	unsigned short flag;
	unsigned int yen;
	unsigned int shading_shift;
	struct ispprev_hmed hmed_p;
	struct ispprev_cfa cfa_p;
	struct ispprev_csup csup_p;
	struct ispprev_wbal wbal_p;
	struct ispprev_blkadj blkadj_p;
	struct ispprev_rgbtorgb rgb2rgb_p;
	struct ispprev_csc csc_p;
	struct ispprev_yclimit yclimit_p;
	struct ispprev_dcor ispprev_dcor_t;
	struct set_ispprev_nf ispprev_nf_t;
	unsigned int red_gamma;
	unsigned int green_gamma;
	unsigned int blue_gamma;
};


typedef struct set_isp_config_t {
	unsigned short raw_save;
	unsigned short flag;
	struct set_ispccdc_update_config ccdc;
	struct set_ispprv_update_config prev;
	unsigned char lsc_Gz;
	unsigned char lsc_Gu;
}set_isp_config;

extern set_isp_config g_Isp;
#endif /* _PREVIEW_H */
