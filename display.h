/*
 * display.h
 */

#ifndef _DISPLAY_H
#define _DISPLAY_H

#include <pthread.h>
#include <fifoutil.h>
#include <rendezvous.h>
#include <pause.h>

/* Display thread commands */
#define DISPLAY_FLUSH -1
#define DISPLAY_PRIME -2

#define RESIZER_DEVICE	"/dev/omap-resizer"
#define FIR_RND_SCALE   256
#define NUM_COEFS		32
#define SCREEN_BPP		16

typedef struct DisplayBufferElement {
	int			id;
	int			width;
	int			height;
	char		*virtBuf;
	unsigned long physBuf;
} DisplayBufferElement;

typedef struct DisplayEnv {
	Rendezvous_Handle	hRendezvousInit;
	Rendezvous_Handle	hRendezvousCleanup;
	Rendezvous_Handle	hRendezvousPrime;
	Pause_Handle		hPause;
	DisplayOption		displayOption;
	FifoUtil_Obj		dispOutFifo;
	FifoUtil_Obj		dispInFifo;
	int					imageWidth;
	int					imageHeight;	
} DisplayEnv;

struct buf_info {
	int index;
	unsigned int length;
	char *start;
};

extern void *displayThrFxn(void *arg);

#endif /* _DISPLAY_H */
