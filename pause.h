/**
 * @file    pause.h
 */

#ifndef _PAUSE_H
#define _PAUSE_H

#include <pthread.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


typedef struct Pause_Obj {
    int             pause;
    pthread_mutex_t mutex;
    pthread_cond_t  cond;
} Pause_Obj;

typedef Pause_Obj *Pause_Handle;

#define PAUSE_SUCCESS 0
#define PAUSE_FAILURE -1


static inline void Pause_open(Pause_Handle hPause)
{
    pthread_mutex_init(&hPause->mutex, NULL);
    pthread_cond_init(&hPause->cond, NULL);

    hPause->pause = FALSE;
}

static inline void Pause_test(Pause_Handle hPause)
{
    pthread_mutex_lock(&hPause->mutex);
    
    if (hPause->pause == TRUE) {
        pthread_cond_wait(&hPause->cond, &hPause->mutex);
    }

    pthread_mutex_unlock(&hPause->mutex);
}

static inline void Pause_on(Pause_Handle hPause)
{
    pthread_mutex_lock(&hPause->mutex);

    hPause->pause = TRUE;

    pthread_mutex_unlock(&hPause->mutex);
}

static inline void Pause_off(Pause_Handle hPause)
{
    pthread_mutex_lock(&hPause->mutex);

    if (hPause->pause == TRUE) {
        hPause->pause = FALSE;
        pthread_cond_broadcast(&hPause->cond);
    }

    pthread_mutex_unlock(&hPause->mutex);
}

static inline void Pause_close(Pause_Handle hPause)
{
    pthread_mutex_destroy(&hPause->mutex);
    pthread_cond_destroy(&hPause->cond);
}

#endif // _PAUSE_H
