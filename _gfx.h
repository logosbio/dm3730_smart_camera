#ifndef __GFX_H
#define __GFX_H

#include "simplewidget.h"
#include "_simplescreen.h"
#include "_simplebutton.h"

extern void gfx_render_button(_simplebutton *btnp, _simplescreen *scrp,
                              int r, int g, int b);
extern void draw_rectangle(_simplescreen *scrp, int x, int y, int w, int h,
                           int r, int g, int b);
#endif // __GFX_H
