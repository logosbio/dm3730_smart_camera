/*
 * capture.c
 */

/* Standard Linux headers */
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/types.h>
#include <linux/videodev2.h>

#include <xdc/std.h>
#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/osal/Memory.h>

#include <simplewidget.h>
#include <rendezvous.h>
#include <fifoutil.h>
#include <pause.h>
#include "encode.h"
#include "capture.h"
#include "preview.h"
#include "isp_table.h"
#include "video.h"
#include "display.h"

#define UYVY_BLACK			   0x10801080

#define CAPTUREINITIALIZED				0x1
#define PREVIEWINITIALIZED				0x2
#define OSDINITIALIZED					0x4

typedef struct CaptureBuffer {
    void         *start;
    unsigned long offset;
    size_t        length;
} CaptureBuffer;

int	v4l2_fd = 0;
int memtype = V4L2_MEMORY_USERPTR;

unsigned int fpc_tbl[5];
#define MAX_IMAGE_WIDTH		3280
#define MAX_IMAGE_HEIGHT	2464
static __u8 lsc_tbl[MAX_IMAGE_WIDTH * MAX_IMAGE_HEIGHT];
static int lsc_tbl_size;		/* Table size in bytes */
static int lsc_tbl_width;		/* Width of table in paxels */
static int lsc_tbl_height;		/* Height of table in paxels */

struct ispccdc_bclamp bclamp_t;
struct ispccdc_blcomp blcomp_t;
struct ispccdc_fpc fpc_t;
struct ispccdc_culling culling_t;
struct ispccdc_lsc_config ispccdc_lsc_config_t;
struct ispccdc_update_config arg_ccdc_t;

simplewidget_screen	simpleScreens[NUM_CAPTURE_BUFS] = {NULL,NULL,NULL};


int resetPrev(int fd)
{
	int ret;
	struct ispprev_hmed hmed_p;

	hmed_p.odddist = 0x2; /* For Bayer sensor */
	hmed_p.evendist = 0x2;
	hmed_p.thres = 0x10;

	static __u32 redgamma_table[] = {
	#include "redgamma_table.h"
	};

	static __u32 greengamma_table[] = {
	#include "greengamma_table.h"
	};

	static __u32 bluegamma_table[] = {
	#include "bluegamma_table.h"
	};

	struct ispprev_cfa cfa_p;

	cfa_p.cfafmt = CFAFMT_BAYER;
	cfa_p.cfa_gradthrs_vert = 0x28; /* Default values */
	cfa_p.cfa_gradthrs_horz = 0x28; /* Default values */
	cfa_p.cfa_table = cfa_coef_table;

	struct ispprev_csup csup_p;

	csup_p.gain = 0x0D;
	csup_p.thres = 0xEB;
	csup_p.hypf_en = 0;

	/* Structure for White Balance */
	struct ispprev_wbal wbal_p;

	/* These are the default wb coef used by ES1 */
	wbal_p.dgain = 0x100;
	wbal_p.coef3 = 0x39;
	wbal_p.coef2 = 0x20;
	wbal_p.coef1 = 0x20;
	wbal_p.coef0 = 0x23;

	struct ispprev_blkadj blkadj_p;

	blkadj_p.red = 0x0;
	blkadj_p.green = 0x0;
	blkadj_p.blue = 0x0;

	struct ispprev_rgbtorgb rgb2rgb_p;

	rgb2rgb_p.matrix[0][0] = 0x01E2;
	rgb2rgb_p.matrix[0][1] = 0x0F30;
	rgb2rgb_p.matrix[0][2] = 0x0FEE;
	rgb2rgb_p.matrix[1][0] = 0x0F9B;
	rgb2rgb_p.matrix[1][1] = 0x01AC;
	rgb2rgb_p.matrix[1][2] = 0x0FB9;
	rgb2rgb_p.matrix[2][0] = 0x0FE0;
	rgb2rgb_p.matrix[2][1] = 0x0EC0;
	rgb2rgb_p.matrix[2][2] = 0x0260;

	rgb2rgb_p.offset[0] = 0x0000;
	rgb2rgb_p.offset[1] = 0x0000;
	rgb2rgb_p.offset[2] = 0x0000;

	struct ispprev_csc csc_p = {
		/*CSC Coef Matrix No Effect*/
		{
#if 0
			{66, 129, 25},
			{-38, -75, 112},
			{112, -94, -18}
#else
			{0x04C, 0x098, 0x01C},	
			{0x3D4, 0x3AC, 0x080},
			{0x080, 0x39E, 0x3EC}
#endif
		},
		/* CSC Offset */
		{0x0, 0x0, 0x0}
	};

	struct ispprev_yclimit yclimit_p;

	yclimit_p.minC = 0x00;	/* Default values */
	yclimit_p.maxC = 0xFF;
	yclimit_p.minY = 0x00;
	yclimit_p.maxY = 0xFF;

	struct ispprev_dcor ispprev_dcor_t;

	ispprev_dcor_t.couplet_mode_en = 1;
	ispprev_dcor_t.detect_correct[0] = 0xE;	/* Default values */
	ispprev_dcor_t.detect_correct[1] = 0xE;
	ispprev_dcor_t.detect_correct[2] = 0xE;
	ispprev_dcor_t.detect_correct[3] = 0xE;

	struct ispprv_update_config preview_struct;

	__u32 table_t[64];
	int all;
	struct ispprev_nf ispprev_nf_t;

	for (all = 0; all < 32; all++)
		table_t[all] = 16; /*16 Default*/

	for (all = 32; all < 64; all++)
		table_t[all] = 31; /*31 Default*/

	ispprev_nf_t.spread = 3;
	memcpy(ispprev_nf_t.table, table_t, sizeof(ispprev_nf_t.table));

	preview_struct.red_gamma = (void *)redgamma_table;
	preview_struct.green_gamma = (void *)greengamma_table;
	preview_struct.blue_gamma = (void *)bluegamma_table;
	preview_struct.prev_nf = &ispprev_nf_t;


	preview_struct.yen = luma_enhance_table;
	preview_struct.shading_shift = 0;
	preview_struct.prev_hmed = &hmed_p;
	preview_struct.prev_cfa = &cfa_p;
	preview_struct.csup = &csup_p;
	preview_struct.prev_wbal = &wbal_p;
	preview_struct.prev_blkadj = &blkadj_p;
	preview_struct.rgb2rgb = &rgb2rgb_p;
	preview_struct.prev_csc = &csc_p;
	preview_struct.yclimit = &yclimit_p;
	preview_struct.prev_dcor = &ispprev_dcor_t;

	preview_struct.update = 0xffffffff;	//	ISP_ABS_PREV_LUMAENH
	preview_struct.flag = 0xffffffff;	//	~ISP_ABS_PREV_LUMAENH;
#if 0
	if ((!strcasecmp(previewOption, "le"))) {
		preview_struct.update = ISP_ABS_PREV_LUMAENH;
		preview_struct.flag = ~ISP_ABS_PREV_LUMAENH;

	} else if ((!strcasecmp(previewOption, "ialaw"))) {
		preview_struct.update = ISP_ABS_PREV_INVALAW;
                preview_struct.flag = ~ISP_ABS_PREV_INVALAW;

	} else if ((!strcasecmp(previewOption, "hm"))) {
		preview_struct.update = ISP_ABS_PREV_HRZ_MED;
		preview_struct.flag = ~ISP_ABS_PREV_HRZ_MED;

	} else if ((!strcasecmp(previewOption, "cfa"))) {
		preview_struct.update = ISP_ABS_PREV_CFA;

	} else if ((!strcasecmp(previewOption, "cs"))) {
		preview_struct.update = ISP_ABS_PREV_CHROMA_SUPP;

	} else if ((!strcasecmp(previewOption, "wb"))) {
		preview_struct.update = ISP_ABS_PREV_WB;
		/* NOTE: Added default table */
	} else if ((!strcasecmp(previewOption, "ba"))) {
		preview_struct.update = ISP_ABS_PREV_BLKADJ;
		preview_struct.flag = ISP_ABS_PREV_BLKADJ;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "r"))) {
		preview_struct.update = ISP_ABS_PREV_RGB2RGB;
		preview_struct.flag = ISP_ABS_PREV_RGB2RGB;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "cc"))) {
		preview_struct.update = ISP_ABS_PREV_COLOR_CONV;
		preview_struct.flag = ISP_ABS_PREV_COLOR_CONV;
		/* NOTE: Added default table */
	} else if ((!strcasecmp(previewOption, "yc"))) {
		preview_struct.update = ISP_ABS_PREV_YC_LIMIT;
		preview_struct.flag = ISP_ABS_PREV_YC_LIMIT;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "dc"))) {
		preview_struct.update = ISP_ABS_PREV_DEFECT_COR;

	} else if ((!strcasecmp(previewOption, "gb"))) {

	} else if ((!strcasecmp(previewOption, "nf"))) {
		preview_struct.update = ISP_ABS_TBL_NF;

	} else if ((!strcasecmp(previewOption, "rg"))) {
		preview_struct.update = ISP_ABS_TBL_REDGAMMA;
		preview_struct.flag = ISP_ABS_TBL_GREENGAMMA;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "gg"))) {
		preview_struct.update = ISP_ABS_TBL_GREENGAMMA;
		preview_struct.flag = ISP_ABS_TBL_GREENGAMMA;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "bg"))) {
		preview_struct.update = ISP_ABS_TBL_BLUEGAMMA;
		preview_struct.flag = ISP_ABS_TBL_BLUEGAMMA;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "none"))) {
		return 1;
	} else {
		printf("Wrong option %s, please enter correct"
				" option\n", previewOption);
		return 0;
	}
#endif

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_PRV_CFG, &preview_struct);

	if (ret == -1) {
		printf("\nerror\n");
		return 0;
	} else
		printf("abs Layer VIDIOC_S_CTRL successful\n");

	return 1;
}

int updatePrev(int fd)
{
	int ret;
	struct ispprev_hmed hmed_p;

	hmed_p.odddist = g_Isp.prev.hmed_p.odddist;
	hmed_p.evendist = g_Isp.prev.hmed_p.evendist;
	hmed_p.thres = g_Isp.prev.hmed_p.thres;

	static __u32 redgamma_table[] = {
	#include "redgamma_table.h"
	};

	static __u32 greengamma_table[] = {
	#include "greengamma_table.h"
	};

	static __u32 bluegamma_table[] = {
	#include "bluegamma_table.h"
	};

	struct ispprev_cfa cfa_p;

	cfa_p.cfafmt = g_Isp.prev.cfa_p.cfafmt;
	cfa_p.cfa_gradthrs_vert = g_Isp.prev.cfa_p.cfa_gradthrs_vert;
	cfa_p.cfa_gradthrs_horz = g_Isp.prev.cfa_p.cfa_gradthrs_horz;
	cfa_p.cfa_table = cfa_coef_table;

	struct ispprev_csup csup_p;

	csup_p.gain = g_Isp.prev.csup_p.gain;
	csup_p.thres = g_Isp.prev.csup_p.thres;
	csup_p.hypf_en = g_Isp.prev.csup_p.hypf_en;

	/* Structure for White Balance */
	struct ispprev_wbal wbal_p;

	/* These are the default wb coef used by ES1 */
	wbal_p.dgain = g_Isp.prev.wbal_p.dgain;
	wbal_p.coef3 = g_Isp.prev.wbal_p.coef3;
	wbal_p.coef2 = g_Isp.prev.wbal_p.coef2;
	wbal_p.coef1 = g_Isp.prev.wbal_p.coef1;
	wbal_p.coef0 = g_Isp.prev.wbal_p.coef0;

	struct ispprev_blkadj blkadj_p;

	blkadj_p.red = g_Isp.prev.blkadj_p.red;
	blkadj_p.green = g_Isp.prev.blkadj_p.green;
	blkadj_p.blue = g_Isp.prev.blkadj_p.blue;

	struct ispprev_rgbtorgb rgb2rgb_p;

	rgb2rgb_p.matrix[0][0] = g_Isp.prev.rgb2rgb_p.matrix[0][0];
	rgb2rgb_p.matrix[0][1] = g_Isp.prev.rgb2rgb_p.matrix[0][1];
	rgb2rgb_p.matrix[0][2] = g_Isp.prev.rgb2rgb_p.matrix[0][2];
	rgb2rgb_p.matrix[1][0] = g_Isp.prev.rgb2rgb_p.matrix[1][0];
	rgb2rgb_p.matrix[1][1] = g_Isp.prev.rgb2rgb_p.matrix[1][1];
	rgb2rgb_p.matrix[1][2] = g_Isp.prev.rgb2rgb_p.matrix[1][2];
	rgb2rgb_p.matrix[2][0] = g_Isp.prev.rgb2rgb_p.matrix[2][0];
	rgb2rgb_p.matrix[2][1] = g_Isp.prev.rgb2rgb_p.matrix[2][1];
	rgb2rgb_p.matrix[2][2] = g_Isp.prev.rgb2rgb_p.matrix[2][2];

	rgb2rgb_p.offset[0] = g_Isp.prev.rgb2rgb_p.offset[0];
	rgb2rgb_p.offset[1] = g_Isp.prev.rgb2rgb_p.offset[1];
	rgb2rgb_p.offset[2] = g_Isp.prev.rgb2rgb_p.offset[2];

	struct ispprev_csc csc_p;
	csc_p.matrix[0][0] = g_Isp.prev.csc_p.matrix[0][0];
	csc_p.matrix[0][1] = g_Isp.prev.csc_p.matrix[0][1];
	csc_p.matrix[0][2] = g_Isp.prev.csc_p.matrix[0][2];
	csc_p.matrix[1][0] = g_Isp.prev.csc_p.matrix[1][0];
	csc_p.matrix[1][1] = g_Isp.prev.csc_p.matrix[1][1];
	csc_p.matrix[1][2] = g_Isp.prev.csc_p.matrix[1][2];
	csc_p.matrix[2][0] = g_Isp.prev.csc_p.matrix[2][0];
	csc_p.matrix[2][1] = g_Isp.prev.csc_p.matrix[2][1];
	csc_p.matrix[2][2] = g_Isp.prev.csc_p.matrix[2][2];

	csc_p.offset[0] = g_Isp.prev.csc_p.offset[0];
	csc_p.offset[1] = g_Isp.prev.csc_p.offset[1];
	csc_p.offset[2] = g_Isp.prev.csc_p.offset[2];
	
	struct ispprev_yclimit yclimit_p;

	yclimit_p.minC = g_Isp.prev.yclimit_p.minC;	/* Default values */
	yclimit_p.maxC = g_Isp.prev.yclimit_p.maxC;
	yclimit_p.minY = g_Isp.prev.yclimit_p.minY;
	yclimit_p.maxY = g_Isp.prev.yclimit_p.maxY;

	struct ispprev_dcor ispprev_dcor_t;

	ispprev_dcor_t.couplet_mode_en	 = g_Isp.prev.ispprev_dcor_t.couplet_mode_en;
	ispprev_dcor_t.detect_correct[0] = g_Isp.prev.ispprev_dcor_t.detect_correct[0];
	ispprev_dcor_t.detect_correct[1] = g_Isp.prev.ispprev_dcor_t.detect_correct[1];
	ispprev_dcor_t.detect_correct[2] = g_Isp.prev.ispprev_dcor_t.detect_correct[2];
	ispprev_dcor_t.detect_correct[3] = g_Isp.prev.ispprev_dcor_t.detect_correct[3];

	struct ispprv_update_config preview_struct;

	__u32 table_t[64];
	int all;
	struct ispprev_nf ispprev_nf_t;

	for (all = 0; all < 32; all++)
		table_t[all] = 40; /*16 Default*/

	for (all = 32; all < 64; all++)
		table_t[all] = 20; /*31 Default*/

	ispprev_nf_t.spread = 3;
	memcpy(ispprev_nf_t.table, table_t, sizeof(ispprev_nf_t.table));

	preview_struct.red_gamma = (void *)redgamma_table;
	preview_struct.green_gamma = (void *)greengamma_table;
	preview_struct.blue_gamma = (void *)bluegamma_table;
	preview_struct.prev_nf = &ispprev_nf_t;

	preview_struct.yen = luma_enhance_table;
	preview_struct.shading_shift = 0;
	preview_struct.prev_hmed = &hmed_p;
	preview_struct.prev_cfa = &cfa_p;
	preview_struct.csup = &csup_p;
	preview_struct.prev_wbal = &wbal_p;
	preview_struct.prev_blkadj = &blkadj_p;
	preview_struct.rgb2rgb = &rgb2rgb_p;
	preview_struct.prev_csc = &csc_p;
	preview_struct.yclimit = &yclimit_p;
	preview_struct.prev_dcor = &ispprev_dcor_t;

	preview_struct.update = g_Isp.prev.update;
	preview_struct.flag = g_Isp.prev.flag;

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_PRV_CFG, &preview_struct);

	if (ret == -1) {
		printf("\nerror\n");
		return 0;
	} else
		printf("abs Layer VIDIOC_S_CTRL successful\n");

	return 1;
}



int resetCcdc(int fd)
{
	int ret;

	/* ok - reset default */
	bclamp_t.obgain = 0x10;
	bclamp_t.obstpixel = 0;
	bclamp_t.oblines = 0;
	bclamp_t.oblen = 0;
	bclamp_t.dcsubval = 64;

	/* ok - reset default */
	blcomp_t.b_mg = 0;
	blcomp_t.gb_g = 0;
	blcomp_t.gr_cy = 0;
	blcomp_t.r_ye = 0;

	fpc_tbl[0] = ((10 << 19) | (10 << 5) | (1 << 0));
	fpc_tbl[1] = ((10 << 19) | (20 << 5) | (1 << 0));
	fpc_tbl[2] = ((10 << 19) | (30 << 5) | (1 << 0));
	fpc_tbl[3] = ((10 << 19) | (40 << 5) | (1 << 0));
	fpc_tbl[4] = ((10 << 19) | (50 << 5) | (1 << 0));
	fpc_t.fpnum = 5;
	fpc_t.fpcaddr = (__u32 )fpc_tbl;

	/* ok - reset default */
	culling_t.v_pattern = 0xFF;
	culling_t.h_odd = 0xFF;
	culling_t.h_even = 0xFF;

	ispccdc_lsc_config_t.offset = 124;	//0x60;
	ispccdc_lsc_config_t.gain_mode_n = 6;
	ispccdc_lsc_config_t.gain_mode_m = 6;
	ispccdc_lsc_config_t.gain_format = 4;
	ispccdc_lsc_config_t.fmtsph = 0;
	ispccdc_lsc_config_t.fmtlnh = 0;
	ispccdc_lsc_config_t.fmtslv = 0;
	ispccdc_lsc_config_t.fmtlnv = 0;
	ispccdc_lsc_config_t.initial_x =	g_Isp.ccdc.lsc_cfg.initial_x;
	ispccdc_lsc_config_t.initial_y =	g_Isp.ccdc.lsc_cfg.initial_y;
	ispccdc_lsc_config_t.size = sizeof(ispccdc_lsc_tbl);

	arg_ccdc_t.update = 0;
	arg_ccdc_t.flag = 0;
	arg_ccdc_t.alawip = 0;
	arg_ccdc_t.bclamp = &bclamp_t;
	arg_ccdc_t.blcomp = &blcomp_t;
	arg_ccdc_t.fpc = &fpc_t;
	arg_ccdc_t.cull = &culling_t;

	arg_ccdc_t.lsc_cfg = &ispccdc_lsc_config_t;
	arg_ccdc_t.lsc = (void *)ispccdc_lsc_tbl;
//	0xBB11BB11

	arg_ccdc_t.update = 0;	//	ISP_ABS_TBL_LSC;		//	ISP_ABS_CCDC_CONFIG_LSC
	arg_ccdc_t.flag = 0;

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_CCDC_CFG, &arg_ccdc_t);
	if (ret == -1) {
		printf("\nerror\n");
		return 0;
	}
#if 0
	//arg_ccdc_t.update =	 ~0	;	//	ISP_ABS_CCDC_ALAW | ISP_ABS_CCDC_BCOMP | ISP_ABS_CCDC_CULL | ISP_ABS_CCDC_COLPTN;
	//arg_ccdc_t.flag =	 ~0	;	//	ISP_ABS_CCDC_ALAW | ISP_ABS_CCDC_BCOMP | ISP_ABS_CCDC_CULL | ISP_ABS_CCDC_COLPTN;
	arg_ccdc_t.update =	 ISP_ABS_CCDC_ALAW | ISP_ABS_CCDC_BCOMP | ISP_ABS_CCDC_CULL | ISP_ABS_CCDC_COLPTN;
	arg_ccdc_t.flag =	 ISP_ABS_CCDC_ALAW | ISP_ABS_CCDC_BCOMP | ISP_ABS_CCDC_CULL | ISP_ABS_CCDC_COLPTN;

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_CCDC_CFG, &arg_ccdc_t);
	if (ret == -1) {
		printf("\nerror\n");
		return 0;
	}

	/* Special case for 'bcl'.
	 * First we reset to default values then call ioclt again
	 * with only 'update' field set, which has the effect of
	 * turning it off.
	 */
	arg_ccdc_t.update = ISP_ABS_CCDC_BLCLAMP;
	arg_ccdc_t.flag = 0;

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_CCDC_CFG, &arg_ccdc_t);
	if (ret == -1) {
		printf("\nerror\n");
		return 0;
	}
#endif
	return 1;
}

int updateCcdc(int fd)
{
	int ret;
	int x, y;
	int paxel_width, paxel_height;
	__u8 *ptr;
	__u8 gain;

	bclamp_t.obgain = g_Isp.ccdc.bclamp.obgain ;
	bclamp_t.obstpixel = g_Isp.ccdc.bclamp.obstpixel ;
	bclamp_t.oblines = g_Isp.ccdc.bclamp.oblines ;
	bclamp_t.oblen = g_Isp.ccdc.bclamp.oblen ;
	bclamp_t.dcsubval = g_Isp.ccdc.bclamp.dcsubval ;

	blcomp_t.b_mg = g_Isp.ccdc.blcomp.b_mg;
	blcomp_t.gb_g = g_Isp.ccdc.blcomp.gb_g;
	blcomp_t.gr_cy = g_Isp.ccdc.blcomp.gr_cy;
	blcomp_t.r_ye = g_Isp.ccdc.blcomp.r_ye;

	fpc_tbl[0] = g_Isp.ccdc.fpc.fpcaddr[0];
	fpc_tbl[1] = g_Isp.ccdc.fpc.fpcaddr[0];
	fpc_tbl[2] = g_Isp.ccdc.fpc.fpcaddr[0];
	fpc_tbl[3] = g_Isp.ccdc.fpc.fpcaddr[0];
	fpc_tbl[4] = g_Isp.ccdc.fpc.fpcaddr[0];
	fpc_t.fpnum = g_Isp.ccdc.fpc.fpnum;
	fpc_t.fpcaddr = fpc_tbl;

	culling_t.v_pattern =	g_Isp.ccdc.cull.v_pattern;
	culling_t.h_odd =	g_Isp.ccdc.cull.h_odd;
	culling_t.h_even =	g_Isp.ccdc.cull.h_even;

	ispccdc_lsc_config_t.offset =	g_Isp.ccdc.lsc_cfg.offset;
	ispccdc_lsc_config_t.gain_mode_n =	g_Isp.ccdc.lsc_cfg.gain_mode_n;
	ispccdc_lsc_config_t.gain_mode_m =	g_Isp.ccdc.lsc_cfg.gain_mode_m;
	ispccdc_lsc_config_t.gain_format =	g_Isp.ccdc.lsc_cfg.gain_format;
	ispccdc_lsc_config_t.fmtsph =	g_Isp.ccdc.lsc_cfg.fmtsph;
	ispccdc_lsc_config_t.fmtlnh =	g_Isp.ccdc.lsc_cfg.fmtlnh;
	ispccdc_lsc_config_t.fmtslv =	g_Isp.ccdc.lsc_cfg.fmtslv;
	ispccdc_lsc_config_t.fmtlnv =	g_Isp.ccdc.lsc_cfg.fmtlnv;
	ispccdc_lsc_config_t.initial_x =	g_Isp.ccdc.lsc_cfg.initial_x;
	ispccdc_lsc_config_t.initial_y =	g_Isp.ccdc.lsc_cfg.initial_y;

	if ((ispccdc_lsc_config_t.gain_mode_m < 2) || (ispccdc_lsc_config_t.gain_mode_m > 6) ||
	    (ispccdc_lsc_config_t.gain_mode_n < 2) || (ispccdc_lsc_config_t.gain_mode_n > 6)) {
		ispccdc_lsc_config_t.gain_mode_m = 6;
		ispccdc_lsc_config_t.gain_mode_n = 6;
	}
	
	paxel_width  = 1 << ispccdc_lsc_config_t.gain_mode_n;
	paxel_height = 1 << ispccdc_lsc_config_t.gain_mode_m;
	lsc_tbl_width  = ((ispccdc_lsc_config_t.fmtlnh + ispccdc_lsc_config_t.initial_x + paxel_width - 1)
			/ paxel_width) + 1;
	lsc_tbl_height = ((ispccdc_lsc_config_t.fmtlnv + ispccdc_lsc_config_t.initial_y + paxel_height - 1)
			/ paxel_height) + 1;
	lsc_tbl_size = lsc_tbl_width * lsc_tbl_height * 4;

	ptr = (__u8 *)&lsc_tbl;

	/* Create a gain table (grid of 0,1,0):
	 *   0 1 0 1 ..
	 *   1 0 1 0 ..
	 *   0 1 0 1 ..
	 */
	for (y = 0; y < lsc_tbl_height; y++) {
		for (x = 0; x < lsc_tbl_width; x++) {
			gain = (((y%2)+x)%2) ? g_Isp.lsc_Gu : g_Isp.lsc_Gz;
			*(ptr++) = gain;
			*(ptr++) = gain;
			*(ptr++) = gain;
			*(ptr++) = gain;
		}
	}
	__u8 *tmp = (__u8 *)&lsc_tbl;
	printf("### 0x%08X(0x%08X)\n 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X 0x%X\n",
		tmp, (__u8 *)&lsc_tbl
		,*(tmp++) ,*(tmp++) ,*(tmp++) ,*(tmp++) ,*(tmp++) ,*(tmp++) ,*(tmp++) ,*(tmp++) 
	);

	ispccdc_lsc_config_t.size = lsc_tbl_size;

	printf("lsc_Gain u&z(%dx%d) (0x%0X x 0x%0X) , size %d \n",x,y, g_Isp.lsc_Gu, g_Isp.lsc_Gz ,lsc_tbl_size );

	arg_ccdc_t.update =	g_Isp.ccdc.update;
	arg_ccdc_t.flag =	g_Isp.ccdc.flag;
	arg_ccdc_t.alawip =	g_Isp.ccdc.alawip;
	arg_ccdc_t.bclamp =	&bclamp_t;
	arg_ccdc_t.blcomp =	&blcomp_t;
	arg_ccdc_t.fpc =	&fpc_t;
	arg_ccdc_t.cull =	&culling_t;
	arg_ccdc_t.colptn =	g_Isp.ccdc.colptn;
	arg_ccdc_t.lsc_cfg =	&ispccdc_lsc_config_t;
	arg_ccdc_t.lsc = (void *)lsc_tbl;

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_CCDC_CFG, &arg_ccdc_t);
	if (ret == -1) {
		printf("\nerror\n");
		return	0;
	}
	//printf("abs Layer VIDIOC_S_CTRL successful\n");

	return 1;
}


int osdDrawCreate(DrawTextParams *drawParams)
{
	int i, width, height;

	width = drawParams->imageWidth;
	height = drawParams->imageHeight;

	for ( i = 0; i < drawParams->numBuf; i++ ) {
	    if (simplewidget_screen_init(drawParams->osdDisplays[i], width, height, &simpleScreens[i]) == -1) {
	        ERR("Failed to initialize simplewidget screen\n");
	        osdDelete(drawParams);
	        return FAILURE;
	    }
	}

    return SUCCESS;
}

int osdDelete(DrawTextParams *drawParams)
{
	int i=0;
	for ( i = 0; i < drawParams->numBuf; i++ ) {
	    if (simpleScreens[i]) {
	        simplewidget_screen_exit(simpleScreens[i]);
	        simpleScreens[i] = NULL;
	    }
	}

    return SUCCESS;
}

int osdDrawText(char *string, int x, int y, int screenIdx)
{
    simplewidget_text simpleText;

    if (simplewidget_text_create(x, y, PTSIZE, string, &simpleText) == -1) {
        ERR("Failed to create dynamic text\n");
        return FAILURE;
    }

    if (simplewidget_text_show(simpleText, simpleScreens[screenIdx]) == -1) {
        ERR("Failed to draw dynamic text\n");
        return FAILURE;
    }

    simplewidget_text_delete(simpleText);

    return SUCCESS;
}

static double GetTimeStamp( struct timeval *ptimestamp )
{

	double vsec = (double)ptimestamp->tv_sec;
	double vusec = (double)ptimestamp->tv_usec;
	double curtime = vsec*1000.0 + vusec/1000.0;

	return curtime;
}

static int initCaptureDevice(CaptureBuffer **capBufsPtr, unsigned int *numCapBufsPtr,
                             int captureWidth, int captureHeight)
{
	int ret = 0, i = 0;
	struct v4l2_requestbuffers	req;
	struct v4l2_buffer			buf;
	struct v4l2_capability		capability;
    struct v4l2_format          fmt;
    enum v4l2_buf_type          type;
    int                         fd;
    CaptureBuffer              *capBufs;
    int                         numCapBufs;
	int	*bufPtr;

    DBG("captureWidth = %d, captureHeight = %d\n", captureWidth, captureHeight);

    fd = open(V4L2_DEVICE, O_RDWR);		//		 | O_NONBLOCK
    if (fd == -1) {
        ERR("Cannot open %s (%s)\n", V4L2_DEVICE, strerror(errno));
        return FAILURE;
    }

	if (ioctl(fd, VIDIOC_QUERYCAP, &capability) < 0) {
		perror("VIDIOC_QUERYCAP");
		return FAILURE;
	}
	if (capability.capabilities & V4L2_CAP_STREAMING)
		printf("%s: Capable of streaming\n", V4L2_DEVICE);
	else {
		printf("%s: Not capable of streaming\n", V4L2_DEVICE);
		return FAILURE;
	}

    CLEAR(fmt);

	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = ioctl(fd, VIDIOC_G_FMT, &fmt);
	if (ret < 0) {
		perror("VIDIOC_G_FMT");
		return FAILURE;
	}

    fmt.fmt.pix.width       = captureWidth;
    fmt.fmt.pix.height      = captureHeight;
    fmt.fmt.pix.sizeimage	= fmt.fmt.pix.width * fmt.fmt.pix.height * 2;
	fmt.fmt.pix.bytesperline = captureWidth;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_UYVY;
    fmt.fmt.pix.field       = V4L2_FIELD_NONE;

    /* Set the video capture format */
    if (ioctl(fd, VIDIOC_S_FMT, &fmt) == -1) {
        ERR("VIDIOC_S_FMT failed on %s (%s)\n", V4L2_DEVICE, strerror(errno));
		return FAILURE;
    }

	gRaw_data_size = fmt.fmt.pix.sizeimage;
	DBG("Output Capture pixformat %04dx%04d (%s), bl %d, s %d field %d, colorspace %d!!\n",
		fmt.fmt.pix.width, fmt.fmt.pix.height, &fmt.fmt.pix.pixelformat,
		fmt.fmt.pix.bytesperline, fmt.fmt.pix.sizeimage,
		fmt.fmt.pix.field, fmt.fmt.pix.colorspace
	);

    CLEAR(req);
    req.count  = NUM_CAPTURE_BUFS;
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = memtype;	//	 V4L2_MEMORY_MMAP;

    if (ioctl(fd, VIDIOC_REQBUFS, &req) == -1) {
        ERR("VIDIOC_REQBUFS failed on %s (%s)\n", V4L2_DEVICE,
                                                  strerror(errno));
        return FAILURE;
    }

    DBG("%d capture buffers were successfully allocated.\n", req.count);

    if (req.count < NUM_CAPTURE_BUFS) {
        ERR("Insufficient buffer memory on %s\n", V4L2_DEVICE);
        return FAILURE;
    }

    capBufs = calloc(req.count, sizeof(*capBufs));

    if (!capBufs) {
        ERR("Failed to allocate memory for capture buffer structs.\n");
        return FAILURE;
    }

    /* Map the allocated buffers to user space */
    for (numCapBufs = 0; numCapBufs < req.count; numCapBufs++) {
        CLEAR(buf);
        buf.index = numCapBufs;
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = memtype;

		if (memtype == V4L2_MEMORY_USERPTR) {
	        capBufs[numCapBufs].length = fmt.fmt.pix.sizeimage;
			capBufs[numCapBufs].start = Memory_contigAlloc(fmt.fmt.pix.sizeimage, Memory_DEFAULTALIGNMENT);
			capBufs[numCapBufs].offset = Memory_getBufferPhysicalAddress(capBufs[numCapBufs].start, 4, NULL);

		} else {
	        if (ioctl(fd, VIDIOC_QUERYBUF, &buf) == -1) {
	            ERR("Failed VIDIOC_QUERYBUF on %s (%s)\n", V4L2_DEVICE, strerror(errno));
	            return FAILURE;
	        }

	        capBufs[numCapBufs].length = buf.length;
	        capBufs[numCapBufs].offset = buf.m.offset;
	        capBufs[numCapBufs].start  = mmap(NULL,
	                                          buf.length,
	                                          PROT_READ | PROT_WRITE,
	                                          MAP_SHARED,
	                                          fd, buf.m.offset);

	        if (capBufs[numCapBufs].start == MAP_FAILED) {
	            ERR("Failed to mmap buffer on %s (%s)\n", V4L2_DEVICE, strerror(errno));
	            return FAILURE;
	        }
		}
		if (memtype == V4L2_MEMORY_USERPTR) {
	        buf.m.userptr = (UInt32)capBufs[numCapBufs].start;
	        buf.length = (fmt.fmt.pix.sizeimage + 4096) & (~0xFFF);

	        DBG("Capture driver buffer %d at physical address 0x%08X mapped to "
	            "virtual address %#lx size %d\n", numCapBufs, (unsigned long)
	            capBufs[numCapBufs].start, capBufs[numCapBufs].offset,buf.length);

			bufPtr  = (Int32*)capBufs[numCapBufs].start;

			for (i = 0; i < buf.length >> 2; i++) {
			    bufPtr[i] = UYVY_BLACK;
			}
		} else {
	        DBG("Capture driver buffer %d at physical address 0x%08X mapped to "
	            "virtual address %#lx\n", numCapBufs, (unsigned long)
	            capBufs[numCapBufs].start, capBufs[numCapBufs].offset);
		}

        if (ioctl(fd, VIDIOC_QBUF, &buf) == -1) {
            ERR("VIODIOC_QBUF failed on %s (%s)\n", V4L2_DEVICE, strerror(errno));
            return FAILURE;
        }
    }

    /* Start the video streaming */
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (ioctl(fd, VIDIOC_STREAMON, &type) == -1) {
        ERR("VIDIOC_STREAMON failed on %s (%s)\n", V4L2_DEVICE, strerror(errno));
        return FAILURE;
    }
	DBG("CAPTURE : Stream on...\n");

    *capBufsPtr = capBufs;
    *numCapBufsPtr = numCapBufs;

    return fd;
}

static void cleanupCaptureDevice(int fd, CaptureBuffer *capBufs, int numCapBufs)
{
    enum v4l2_buf_type type;
    unsigned int       i;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd, VIDIOC_STREAMOFF, &type) == -1) {
        ERR("VIDIOC_STREAMOFF failed (%s)\n", strerror(errno));
    }

    if (close(fd) == -1) {
        ERR("Failed to close capture device (%s)\n", strerror(errno));
    }

    for (i = 0; i < numCapBufs; i++) {
		if (memtype == V4L2_MEMORY_USERPTR) {
	 		Memory_contigFree(capBufs[i].start , capBufs[i].length);
	 	} else {
	        if (munmap(capBufs[i].start, capBufs[i].length) == -1) {
	            ERR("Failed to unmap capture buffer %d\n", i);
	        }
	    }
    }

    free(capBufs);
}

int resetPreview(int fd, int pset)
{
	int ret;
	struct ispprev_hmed hmed_p;

	hmed_p.odddist = 0x2; /* For Bayer sensor */
	hmed_p.evendist = 0x2;
	hmed_p.thres = 0x10;

	static __u32 redgamma_table[] = {
	#include "redgamma_table.h"
	};

	static __u32 greengamma_table[] = {
	#include "greengamma_table.h"
	};

	static __u32 bluegamma_table[] = {
	#include "bluegamma_table.h"
	};

	struct ispprev_cfa cfa_p;

	cfa_p.cfafmt = CFAFMT_BAYER;
	cfa_p.cfa_gradthrs_vert = 0x28; /* Default values */
	cfa_p.cfa_gradthrs_horz = 0x28; /* Default values */
	cfa_p.cfa_table = cfa_coef_table;

	struct ispprev_csup csup_p;

	csup_p.gain = 0x0D;
	csup_p.thres = 0xEB;
	csup_p.hypf_en = 0;

	/* Structure for White Balance */
	struct ispprev_wbal wbal_p;

	/* These are the default wb coef used by ES1 */
	wbal_p.dgain = 0x100;
	wbal_p.coef3 = 0x39;
	wbal_p.coef2 = 0x20;
	wbal_p.coef1 = 0x20;
	wbal_p.coef0 = 0x23;

	struct ispprev_blkadj blkadj_p;

	blkadj_p.red = 0x0;
	blkadj_p.green = 0x0;
	blkadj_p.blue = 0x0;

	struct ispprev_rgbtorgb rgb2rgb_p;

	rgb2rgb_p.matrix[0][0] = 0x01E2;
	rgb2rgb_p.matrix[0][1] = 0x0F30;
	rgb2rgb_p.matrix[0][2] = 0x0FEE;
	rgb2rgb_p.matrix[1][0] = 0x0F9B;
	rgb2rgb_p.matrix[1][1] = 0x01AC;
	rgb2rgb_p.matrix[1][2] = 0x0FB9;
	rgb2rgb_p.matrix[2][0] = 0x0FE0;
	rgb2rgb_p.matrix[2][1] = 0x0EC0;
	rgb2rgb_p.matrix[2][2] = 0x0260;

	rgb2rgb_p.offset[0] = 0x0000;
	rgb2rgb_p.offset[1] = 0x0000;
	rgb2rgb_p.offset[2] = 0x0000;

	struct ispprev_csc csc_p = {
		/*CSC Coef Matrix No Effect*/
		{
			{66, 129, 25},
			{-38, -75, 112},
			{112, -94, -18}
		},
		/* CSC Offset */
		{0x0, 0x0, 0x0}
	};

	struct ispprev_yclimit yclimit_p;

	yclimit_p.minC = 0x00;	/* Default values */
	yclimit_p.maxC = 0xFF;
	yclimit_p.minY = 0x00;
	yclimit_p.maxY = 0xFF;

	struct ispprev_dcor ispprev_dcor_t;

	ispprev_dcor_t.couplet_mode_en = 1;
	ispprev_dcor_t.detect_correct[0] = 0xE;	/* Default values */
	ispprev_dcor_t.detect_correct[1] = 0xE;
	ispprev_dcor_t.detect_correct[2] = 0xE;
	ispprev_dcor_t.detect_correct[3] = 0xE;

	struct ispprv_update_config preview_struct;

	__u32 table_t[64];
	int all;
	struct ispprev_nf ispprev_nf_t;

	for (all = 0; all < 32; all++)
		table_t[all] = 40; /*16 Default*/

	for (all = 32; all < 64; all++)
		table_t[all] = 20; /*31 Default*/

	ispprev_nf_t.spread = 3;
	memcpy(ispprev_nf_t.table, table_t, sizeof(ispprev_nf_t.table));

	preview_struct.red_gamma = (void *)redgamma_table;
	preview_struct.green_gamma = (void *)greengamma_table;
	preview_struct.blue_gamma = (void *)bluegamma_table;
	preview_struct.prev_nf = &ispprev_nf_t;


	preview_struct.yen = luma_enhance_table;
	preview_struct.shading_shift = 0;
	preview_struct.prev_hmed = &hmed_p;
	preview_struct.prev_cfa = &cfa_p;
	preview_struct.csup = &csup_p;
	preview_struct.prev_wbal = &wbal_p;
	preview_struct.prev_blkadj = &blkadj_p;
	preview_struct.rgb2rgb = &rgb2rgb_p;
	preview_struct.prev_csc = &csc_p;
	preview_struct.yclimit = &yclimit_p;
	preview_struct.prev_dcor = &ispprev_dcor_t;

	preview_struct.update = 0xffffffff;	//	ISP_ABS_PREV_LUMAENH
	preview_struct.flag = 0xffffffff;	//	~ISP_ABS_PREV_LUMAENH;
#if 0
	if ((!strcasecmp(previewOption, "le"))) {
		preview_struct.update = ISP_ABS_PREV_LUMAENH;
		preview_struct.flag = ~ISP_ABS_PREV_LUMAENH;

	} else if ((!strcasecmp(previewOption, "ialaw"))) {
		preview_struct.update = ISP_ABS_PREV_INVALAW;
                preview_struct.flag = ~ISP_ABS_PREV_INVALAW;

	} else if ((!strcasecmp(previewOption, "hm"))) {
		preview_struct.update = ISP_ABS_PREV_HRZ_MED;
		preview_struct.flag = ~ISP_ABS_PREV_HRZ_MED;

	} else if ((!strcasecmp(previewOption, "cfa"))) {
		preview_struct.update = ISP_ABS_PREV_CFA;

	} else if ((!strcasecmp(previewOption, "cs"))) {
		preview_struct.update = ISP_ABS_PREV_CHROMA_SUPP;

	} else if ((!strcasecmp(previewOption, "wb"))) {
		preview_struct.update = ISP_ABS_PREV_WB;
		/* NOTE: Added default table */
	} else if ((!strcasecmp(previewOption, "ba"))) {
		preview_struct.update = ISP_ABS_PREV_BLKADJ;
		preview_struct.flag = ISP_ABS_PREV_BLKADJ;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "r"))) {
		preview_struct.update = ISP_ABS_PREV_RGB2RGB;
		preview_struct.flag = ISP_ABS_PREV_RGB2RGB;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "cc"))) {
		preview_struct.update = ISP_ABS_PREV_COLOR_CONV;
		preview_struct.flag = ISP_ABS_PREV_COLOR_CONV;
		/* NOTE: Added default table */
	} else if ((!strcasecmp(previewOption, "yc"))) {
		preview_struct.update = ISP_ABS_PREV_YC_LIMIT;
		preview_struct.flag = ISP_ABS_PREV_YC_LIMIT;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "dc"))) {
		preview_struct.update = ISP_ABS_PREV_DEFECT_COR;

	} else if ((!strcasecmp(previewOption, "gb"))) {

	} else if ((!strcasecmp(previewOption, "nf"))) {
		preview_struct.update = ISP_ABS_TBL_NF;

	} else if ((!strcasecmp(previewOption, "rg"))) {
		preview_struct.update = ISP_ABS_TBL_REDGAMMA;
		preview_struct.flag = ISP_ABS_TBL_GREENGAMMA;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "gg"))) {
		preview_struct.update = ISP_ABS_TBL_GREENGAMMA;
		preview_struct.flag = ISP_ABS_TBL_GREENGAMMA;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "bg"))) {
		preview_struct.update = ISP_ABS_TBL_BLUEGAMMA;
		preview_struct.flag = ISP_ABS_TBL_BLUEGAMMA;
		/* NOTE: Table is same as kernels default */
	} else if ((!strcasecmp(previewOption, "none"))) {
		return 1;
	} else {
		printf("Wrong option %s, please enter correct"
				" option\n", previewOption);
		return 0;
	}
#endif

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_PRV_CFG, &preview_struct);

	if (ret == -1) {
		printf("\nerror\n");
		return 0;
	} else
		printf("abs Layer VIDIOC_S_CTRL successful\n");

	return 1;
}

int updatePreview(int fd, int pset)
{
	int ret;
	__u32 red_gamma_t[1024];
	__u32 green_gamma_t[1024];
	__u32 blue_gamma_t[1024];
	__u32 table_t[64];
	int all;

	struct ispprev_hmed hmed_p;
	struct ispprev_cfa cfa_p;
	struct ispprev_csup csup_p;
	struct ispprev_wbal wbal_p;
	struct ispprev_blkadj blkadj_p;
	struct ispprev_rgbtorgb rgb2rgb_p;
	struct ispprev_yclimit yclimit_p;
	struct ispprev_dcor ispprev_dcor_t;
	struct ispprev_nf ispprev_nf_t;
	struct ispprv_update_config preview_struct;
	struct ispprev_csc csc_p = 	{
			/*CSC Coef Matrix Sepia*/
#if 0
		{
			{ 66, 129, 25},
			{ -38, -75, 112},
			{ 112, -94 , -18}
		},
		{0x0, 0x0, 0x0}
#else
		{
			{66, 129, 25},
			{0, 0, 0},
			{0, 0, 0}
		},		/* CSC Offset */
		{0x0, 0xE7, 0x14}
#endif
	};

	hmed_p.odddist = 0x2; /* For Bayer sensor */
	hmed_p.evendist = 0x2;
	hmed_p.thres = 0x10;

	cfa_p.cfafmt = CFAFMT_BAYER;
	cfa_p.cfa_gradthrs_vert = 0x28; /* Default values */
	cfa_p.cfa_gradthrs_horz = 0x28; /* Default values */
	cfa_p.cfa_table = cfa_coef_table;

	csup_p.gain = 0x0D;
	csup_p.thres = 0xEB;
	csup_p.hypf_en = 0;


	wbal_p.dgain = 0x100;
	wbal_p.coef3 = 0x94;		//	0x0;	//
	wbal_p.coef2 = 0x5C;        //	0x0;	//
	wbal_p.coef1 = 0x5C;        //	0x0;	//
	wbal_p.coef0 = 0x68;        //	0x0;	//

	blkadj_p.red = 0x0;
	blkadj_p.green = 0x0;
	blkadj_p.blue = 0x0;

	rgb2rgb_p.matrix[0][0] = 0x01E2;
	rgb2rgb_p.matrix[0][1] = 0x0F30;
	rgb2rgb_p.matrix[0][2] = 0x0FEE;
	rgb2rgb_p.matrix[1][0] = 0x0F9B;
	rgb2rgb_p.matrix[1][1] = 0x01AC;
	rgb2rgb_p.matrix[1][2] = 0x0FB9;
	rgb2rgb_p.matrix[2][0] = 0x0FE0;
	rgb2rgb_p.matrix[2][1] = 0x0EC0;
	rgb2rgb_p.matrix[2][2] = 0x0260;

	rgb2rgb_p.offset[0] = 0x0000;
	rgb2rgb_p.offset[1] = 0x0000;
	rgb2rgb_p.offset[2] = 0x0000;

	yclimit_p.minC = 0x00;	/* Default values */
	yclimit_p.maxC = 0xFF;
	yclimit_p.minY = 0x00;
	yclimit_p.maxY = 0xFF;

	ispprev_dcor_t.couplet_mode_en = 1;
	ispprev_dcor_t.detect_correct[0] = 0xE;	/* Default values */
	ispprev_dcor_t.detect_correct[1] = 0xE;
	ispprev_dcor_t.detect_correct[2] = 0xE;
	ispprev_dcor_t.detect_correct[3] = 0xE;

	for (all = 0; all < 32; all++)
		table_t[all] = 40; /*16 Default*/

	for (all = 32; all < 64; all++)
		table_t[all] = 20; /*31 Default*/

	ispprev_nf_t.spread = 3;
	memcpy(ispprev_nf_t.table, table_t, sizeof(ispprev_nf_t.table));

	for (all = 0; all < 1024; all++) {
		red_gamma_t[all] = 0;
	}

	preview_struct.yen = luma_enhance_table;
	preview_struct.shading_shift = 0;
	preview_struct.prev_hmed = &hmed_p;
	preview_struct.prev_cfa = &cfa_p;
	preview_struct.csup = &csup_p;
	preview_struct.prev_wbal = &wbal_p;
	preview_struct.prev_blkadj = &blkadj_p;
	preview_struct.rgb2rgb = &rgb2rgb_p;
	preview_struct.prev_csc = &csc_p;
	preview_struct.yclimit = &yclimit_p;
	preview_struct.prev_dcor = &ispprev_dcor_t;
	preview_struct.prev_nf = &ispprev_nf_t;
	preview_struct.red_gamma = (void *)red_gamma_t;
	preview_struct.green_gamma = (void *)green_gamma_t;
	preview_struct.blue_gamma = (void *)blue_gamma_t;


	if ( pset == -1) {
		preview_struct.update = 0x1;
		preview_struct.flag = ISP_ABS_PREV_LUMAENH |
			ISP_ABS_PREV_CHROMA_SUPP | ISP_ABS_PREV_CFA |
			ISP_ABS_PREV_DEFECT_COR;
	} else if ( pset == 0){
		preview_struct.update = 0x0;
		preview_struct.flag = 0x0;
	} else {
		DBG("\n\n\n\n PREVIEW_Update\n");
		DBG("---- Options for Preview_Updates: \n");

		if ( pset & ISP_ABS_PREV_LUMAENH ) {
			preview_struct.update |= ISP_ABS_PREV_LUMAENH;
			preview_struct.flag |= ISP_ABS_PREV_LUMAENH;
			DBG("\t Luma Enhancement\n");
		}

		if ( pset & ISP_ABS_PREV_INVALAW ) {
			preview_struct.update |= ISP_ABS_PREV_INVALAW;
			preview_struct.flag |= ISP_ABS_PREV_INVALAW;
			DBG("\t Inverse A law\n");
		}

		if ( pset & ISP_ABS_PREV_HRZ_MED ) {
			preview_struct.update |= ISP_ABS_PREV_HRZ_MED;
			preview_struct.flag |= ISP_ABS_PREV_HRZ_MED;
			DBG("\t Horizontal Median filter \n");
		}

		if ( pset & ISP_ABS_PREV_CFA ) {
			preview_struct.update |= ISP_ABS_PREV_CFA;
			preview_struct.flag |= ISP_ABS_PREV_CFA;
			DBG("\t CFA interpolation \n");
		}

		if ( pset & ISP_ABS_PREV_CHROMA_SUPP ) {
			preview_struct.update |= ISP_ABS_PREV_CHROMA_SUPP;
			preview_struct.flag |= ISP_ABS_PREV_CHROMA_SUPP;
			DBG("\t Chroma Suppression \n");
		}

		if ( pset & ISP_ABS_PREV_WB ) {
			preview_struct.update |= ISP_ABS_PREV_WB;
			preview_struct.flag |= ISP_ABS_PREV_WB;
			DBG("\t White balance \n");
		}

		if ( pset & ISP_ABS_PREV_BLKADJ ) {
			preview_struct.update |= ISP_ABS_PREV_BLKADJ;
			preview_struct.flag |= ISP_ABS_PREV_BLKADJ;
			DBG("\t Black adjustment \n");
		}

		if ( pset & ISP_ABS_PREV_RGB2RGB ) {
			preview_struct.update |= ISP_ABS_PREV_RGB2RGB;
			preview_struct.flag |= ISP_ABS_PREV_RGB2RGB;
			DBG("\t RGB 2 RGB \n");
		}

		if ( pset & ISP_ABS_PREV_COLOR_CONV ) {
			preview_struct.update |= ISP_ABS_PREV_COLOR_CONV;
			preview_struct.flag |= ISP_ABS_PREV_COLOR_CONV;
			DBG("\t Color conversion \n");
		}

		if ( pset & ISP_ABS_PREV_YC_LIMIT ) {
			preview_struct.update |= ISP_ABS_PREV_YC_LIMIT;
			preview_struct.flag |= ISP_ABS_PREV_YC_LIMIT;
			DBG("\t YC \n");
		}
		if ( pset & ISP_ABS_PREV_DEFECT_COR ) {
			preview_struct.update |= ISP_ABS_PREV_DEFECT_COR;
			preview_struct.flag |= ISP_ABS_PREV_DEFECT_COR;
			DBG("\t Defect Correction \n");
		}

		if ( pset & ISP_ABS_PREV_GAMMABYPASS ) {
			//preview_struct.update |= ISP_ABS_PREV_GAMMABYPASS;
			preview_struct.flag |= ISP_ABS_PREV_GAMMABYPASS;
			DBG("\t  \n");
		}

		DBG("\n---- Options for Tables_Updates: \n");

		if ( pset & ISP_ABS_TBL_NF ) {
			preview_struct.update |= ISP_ABS_TBL_NF;
			preview_struct.flag |= ISP_ABS_TBL_NF;
			DBG("\t Noise Filter Table \n");
		}

		if ( pset & ISP_ABS_TBL_REDGAMMA ) {
			preview_struct.update |= ISP_ABS_TBL_REDGAMMA;
			preview_struct.flag |= ISP_ABS_TBL_REDGAMMA;
			DBG("\t Blue Gamma Table Update \n");
		}

		if ( pset & ISP_ABS_TBL_GREENGAMMA ) {
			preview_struct.update |= ISP_ABS_TBL_GREENGAMMA;
			preview_struct.flag |= ISP_ABS_TBL_GREENGAMMA;
			DBG("\t Green Gamma Table \n");
		}

		if ( pset & ISP_ABS_TBL_BLUEGAMMA ) {
			preview_struct.update |= ISP_ABS_TBL_BLUEGAMMA;
			preview_struct.flag |= ISP_ABS_TBL_BLUEGAMMA;
			DBG("\t Red Gamma Table Update \n");
		}
		DBG("\n\n");
	}

	ret = ioctl(fd, VIDIOC_PRIVATE_ISP_PRV_CFG, &preview_struct);

	if (ret == -1) {
		printf("\nerror\n");
		return 0;
	} else
		printf("abs Layer VIDIOC_S_CTRL successful\n");

	return 1;
}

void *captureThrFxn(void *arg)
{
    CaptureBufferElement	cFlush            = { CAPTURE_FLUSH };
    DisplayBufferElement	dFlush            = { DISPLAY_FLUSH };
    CaptureEnv				*envp             = (CaptureEnv *) arg;
    void					*status           = THREAD_SUCCESS;
    int						captureFd         = FAILURE;

    struct v4l2_buffer		v4l2buf;
    struct v4l2_buffer		v4l2buf_tmp;

    unsigned int			numCapBufs;
    CaptureBuffer			*capBufs;
    DisplayBufferElement	de;
    CaptureBufferElement	ce;

    int						checkflag = ENCODE_BUFFERS;
	unsigned int			initMask = 0;

	struct timeval			tv;
	struct tm				*timePassed;
	int						procLoad, armLoad;
	int						ret_val, i = 0;
	char					tempString[20];
	DrawTextParams 			drawParams;

    /* Initialize the video capture device */
    captureFd = initCaptureDevice(&capBufs, &numCapBufs, envp->imageWidth, envp->imageHeight);

    if (captureFd == FAILURE) {
    	v4l2_fd = 0;
        cleanup(THREAD_FAILURE);
    }else{
		v4l2_fd = captureFd;
    }
	initMask |= CAPTUREINITIALIZED;

    DBG("Video capture initialized and started\n");

	envp->ccdcFlag;
	//updatePreview(captureFd, envp->prevFlag);

	resetPreview(captureFd, envp->prevFlag);
	arg_ccdc_t.update =	0xffff;
	arg_ccdc_t.flag =	0;

	ret_val = ioctl(captureFd, VIDIOC_PRIVATE_ISP_CCDC_CFG, &arg_ccdc_t);
	if (ret_val == -1) {
		printf("\nerror\n");
		return	0;
	}

	initMask |= PREVIEWINITIALIZED;
    DBG("Preview ISP initialized and started\n");


	if (envp->statusOsdFlag == 1) {
		drawParams.imageWidth = envp->imageWidth;
		drawParams.imageHeight = envp->imageHeight;
		drawParams.numBuf = NUM_CAPTURE_BUFS;

		for ( i = 0; i < drawParams.numBuf; i++ ) {
			drawParams.osdDisplays[i] = capBufs[i].start;
		}

		osdDrawCreate(&drawParams);
		initMask |= OSDINITIALIZED;

	    DBG("Video capture Status OSD started\n");
	}

    Rendezvous_meet(envp->hRendezvousInit);

    DBG("Entering capture main loop.\n");
//	printf("Reg Dump Start\n\n");
//	system("cat /sys/kernel/debug/isp/isp_regs\n");
//	system("cat /sys/kernel/debug/isp/ispccdc_regs\n");

    while (!gblGetQuit()) {
        /* Pause processing? */
        Pause_test(envp->hPause);

        CLEAR(v4l2buf);
        v4l2buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        v4l2buf.memory = memtype;

        /* Get a frame buffer with captured data */
        if (ioctl(captureFd, VIDIOC_DQBUF, &v4l2buf) == -1) {
            if (errno == EAGAIN) {
                continue;
            }

            ERR("VIDIOC_DQBUF failed (%s)\n", strerror(errno));
            breakLoop(THREAD_FAILURE);
        }

		de.virtBuf     = capBufs[v4l2buf.index].start;
        de.physBuf     = capBufs[v4l2buf.index].offset;
        de.width       = envp->imageWidth;
        de.height      = envp->imageHeight;

        /* Display raw frame while encoding it */
        if (FifoUtil_put(envp->hDisplayInFifo, &de) == FIFOUTIL_FAILURE) {
            ERR("Failed to put buffer in output fifo\n");
            breakLoop(THREAD_FAILURE);
        }

		if( checkflag != 0 )
	    {
	        if (FifoUtil_get(&envp->capInFifo, &ce) == FIFOUTIL_FAILURE) {
	            breakLoop(THREAD_FAILURE);
	        }
		}

        /* Is the video thread flushing the pipe? */
        if (ce.id == CAPTURE_FLUSH) {
            breakLoop(THREAD_SUCCESS);
        }

		ce.virtBuf = capBufs[v4l2buf.index].start;
		ce.physBuf = capBufs[v4l2buf.index].offset;
		ce.timestamp = GetTimeStamp(&(v4l2buf.timestamp));

		if (gRaw_apply != 0) {
//			printf("Reg Dump Start\n\n");
//			system("cat /sys/kernel/debug/isp/isp_regs\n");
//			system("cat /sys/kernel/debug/isp/ispccdc_regs\n");
			if (g_Isp.flag == SET_CCDC_RESET) {
				DBG("\n\t\tSET_CCDC_RESET.\n");
//				printf("Reg Dump Start\n\n");
//				system("cat /sys/kernel/debug/isp/isp_regs\n");
//				system("cat /sys/kernel/debug/isp/ispccdc_regs\n");

				resetCcdc(captureFd);

//				printf("Reg Setting\n\n");
//				system("cat /sys/kernel/debug/isp/isp_regs\n");
//				system("cat /sys/kernel/debug/isp/ispccdc_regs\n");
				gRaw_apply = 0;
			}

			if (g_Isp.flag & SET_CCDC_ENABLE == SET_CCDC_ENABLE) {
				DBG("\n\t\SET_CCDC_ENABLE.\n");
				updateCcdc(captureFd);
				gRaw_apply = 0;
			}
//			printf("Reg Setting\n\n");
//			system("cat /sys/kernel/debug/isp/isp_regs\n");
//			system("cat /sys/kernel/debug/isp/ispccdc_regs\n");
//			printf("Reg Dump END\n\n");

			if (g_Isp.flag == SET_PREV_RESET) {
				DBG("\n\t\tSET_PREV_RESET.\n");
				resetPrev(captureFd);
				gRaw_apply = 0;
			}


			if (g_Isp.flag & SET_PREV_ENABLE == SET_PREV_ENABLE) {
				DBG("\n\t\SET_CCDC_ENABLE.\n");
				updatePrev(captureFd);
				gRaw_apply = 0;
			}


		}

		if (gRaw_save == 1) {
			gRaw_data_size = envp->imageWidth * envp->imageHeight * 2;
			//aligned_copy_neon(&&gRDS.gRawData, (int *)ce.virtBuf, ROUNDUP(gRaw_data_size, 64));
			//memcpy(&&gRDS.gRawData, (int *)ce.virtBuf, gRaw_data_size);
			aligned_copy_neon(&gRDS.gRawData, (int *)ce.virtBuf, ROUNDUP(gRaw_data_size, 64));
			//memcpy(&&gRDS.gRawData, (int *)ce.virtBuf, gRaw_data_size);
			gRaw_save = 0;
		}

		if (envp->statusOsdFlag == 1) {
			timePassed = localtime(&v4l2buf.timestamp.tv_sec);
			if (timePassed == NULL) {
				breakLoop(THREAD_FAILURE);
			}

			sprintf(tempString, "v4l2 Time : %.2d:%.2d:%.2d.%.3d, %2d fps, ARM : %3d\%, DSP : %3d\%", timePassed->tm_hour,
												  timePassed->tm_min,
												  timePassed->tm_sec, v4l2buf.timestamp.tv_usec / 1000,
												  gEncFps, gArmLoad , gDspLoad);

			osdDrawText(tempString, 20, 30, v4l2buf.index);

			if (gettimeofday(&tv, NULL) == -1) {
				ERR("Failed to get os time\n");
				breakLoop(THREAD_FAILURE);;
			}

			timePassed = localtime(&tv.tv_sec);
			if (timePassed == NULL) {
				breakLoop(THREAD_FAILURE);
			}

			sprintf(tempString, "Cap Time : %.2d:%.2d:%.2d.%.3d", timePassed->tm_hour,
												  timePassed->tm_min,
												  timePassed->tm_sec, tv.tv_usec / 1000);

			osdDrawText(tempString, 20, 60, v4l2buf.index);
		}

        if (FifoUtil_put(&envp->capOutFifo, &ce) == FIFOUTIL_FAILURE) {
            ERR("Failed to put buffer in input fifo\n");
            breakLoop(THREAD_FAILURE);
        }

        if (FifoUtil_get(envp->hDisplayOutFifo, &de) == FIFOUTIL_FAILURE) {
            breakLoop(THREAD_FAILURE);
        }

        if (de.id == DISPLAY_FLUSH) {
            breakLoop(THREAD_SUCCESS);
        }
		if( checkflag != 0 )
			checkflag--;

		if( checkflag == 0 )
		{

			if (FifoUtil_get(&envp->capInFifo, &ce) == FIFOUTIL_FAILURE) {
				breakLoop(THREAD_FAILURE);
			}

			if (ioctl(captureFd, VIDIOC_QBUF, &v4l2buf_tmp) == -1) {
				ERR("VIDIOC_QBUF failed (%s)\n", strerror(errno));
				breakLoop(THREAD_FAILURE);
			}
	    }
	    v4l2buf_tmp = v4l2buf;


    }

cleanup:
    Rendezvous_force(envp->hRendezvousInit);

    Pause_off(envp->hPause);

    FifoUtil_put(&envp->capOutFifo, &cFlush);

    FifoUtil_put(envp->hDisplayInFifo, &dFlush);

    /* Meet up with other threads before cleaning up */
    Rendezvous_meet(envp->hRendezvousCleanup);

	if (initMask & OSDINITIALIZED) {
		osdDelete(&drawParams);
	}

	if (initMask & CAPTUREINITIALIZED) {
	    if (captureFd != FAILURE) {
	        cleanupCaptureDevice(captureFd, capBufs, numCapBufs);
	    }
	}

    fprintf(stderr,"captureThrFxn closing...\n");

    return status;
}
