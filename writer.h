/*
 * writer.h
 */

#ifndef _WRITER_H
#define _WRITER_H

#include <fifoutil.h>
#include <rendezvous.h>

/* Writer thread commands */
#define WRITER_NORMAL 0
#define WRITER_FLUSH  -1
#define WRITER_PRIME  -2

typedef struct WriterBufferElement {
	int		id;
	int		frameSize;
	int		frameType;
	int		videotype;
	char	*encodedBuffer;
	double	timestamp;
} WriterBufferElement;

typedef struct WriterEnv {
	Rendezvous_Handle	hRendezvousInit;
	Rendezvous_Handle	hRendezvousCleanup;
	Pause_Handle		hPause;
	FifoUtil_Obj		wOutFifo;
	FifoUtil_Obj		wInFifo;
	int					imageWidth;
	int					imageHeight;
	int					quality;
	char				*videoFile;
	char				*IP_ADDR
} WriterEnv;

extern void *writerThrFxn(void *arg);

#endif /* _WRITER_H */
