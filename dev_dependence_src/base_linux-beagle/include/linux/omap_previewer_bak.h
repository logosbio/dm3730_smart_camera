/*
 * drivers/media/video/isp/omap_previewer.h
 *
 * Header file for Preview module wrapper in TI's OMAP3430 ISP
 *
 * Copyright (C) 2008 Texas Instruments, Inc.
 *
 * Contributors:
 * 	Leonides Martinez <leonides.martinez@ti.com>
 * 	Sergio Aguirre <saaguirre@ti.com>
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * THIS PACKAGE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */


#if 0
#include <linux/videodev2.h>
#include <mach/isp_user.h>


#ifndef OMAP_ISP_PREVIEW_WRAP_H
#define OMAP_ISP_PREVIEW_WRAP_H


#define PREV_IOC_BASE			'P'
#define PREV_REQBUF			_IOWR(PREV_IOC_BASE, 1,\
						struct v4l2_requestbuffers)
#define PREV_QUERYBUF			_IOWR(PREV_IOC_BASE, 2,\
							struct v4l2_buffer)
#define PREV_SET_PARAM			_IOW(PREV_IOC_BASE, 3,\
							struct prev_params)
#define PREV_GET_PARAM			_IOWR(PREV_IOC_BASE, 4,\
							struct prev_params)
#define PREV_PREVIEW			_IOR(PREV_IOC_BASE, 5, int)
#define PREV_GET_STATUS			_IOR(PREV_IOC_BASE, 6, char)
#define PREV_GET_CROPSIZE		_IOR(PREV_IOC_BASE, 7,\
							struct prev_cropsize)
#define PREV_QUEUEBUF			_IOWR(PREV_IOC_BASE, 8,\
							struct v4l2_buffer)
#define PREV_IOC_MAXNR			8

#define LUMA_TABLE_SIZE			128
#define GAMMA_TABLE_SIZE		1024
#define CFA_COEFF_TABLE_SIZE		576
#define NOISE_FILTER_TABLE_SIZE		256

#define MAX_IMAGE_WIDTH			3300

#define PREV_INWIDTH_8BIT		0	/* pixel width of 8 bits */
#define PREV_INWIDTH_10BIT		1	/* pixel width of 10 bits */

#define PREV_32BYTES_ALIGN_MASK		0xFFFFFFE0
#define PREV_16PIX_ALIGN_MASK		0xFFFFFFF0

#define PREV_IOC_BASE			'P'
#define PREV_REQBUF			_IOWR(PREV_IOC_BASE, 1,\
						struct v4l2_requestbuffers)
#define PREV_QUERYBUF			_IOWR(PREV_IOC_BASE, 2,\
							struct v4l2_buffer)
#define PREV_SET_PARAM			_IOW(PREV_IOC_BASE, 3,\
							struct prev_params)
#define PREV_GET_PARAM			_IOWR(PREV_IOC_BASE, 4,\
							struct prev_params)
#define PREV_PREVIEW			_IOR(PREV_IOC_BASE, 5, int)
#define PREV_GET_STATUS			_IOR(PREV_IOC_BASE, 6, char)
#define PREV_GET_CROPSIZE		_IOR(PREV_IOC_BASE, 7,\
							struct v4l2_rect)
#define PREV_QUEUEBUF			_IOWR(PREV_IOC_BASE, 8,\
							struct v4l2_buffer)
#define PREV_IOC_MAXNR			8



#define ISPPRV_BRIGHT_STEP		0x1
#define ISPPRV_BRIGHT_DEF		0x1
#define ISPPRV_BRIGHT_LOW		0x0
#define ISPPRV_BRIGHT_HIGH		0xFF
#define ISPPRV_BRIGHT_UNITS		0x1

#define ISPPRV_CONTRAST_STEP		0x1
#define ISPPRV_CONTRAST_DEF		0x10
#define ISPPRV_CONTRAST_LOW		0x0
#define ISPPRV_CONTRAST_HIGH		0xFF
#define ISPPRV_CONTRAST_UNITS		0x1

#define NO_AVE				0x0
#define AVE_2_PIX			0x1
#define AVE_4_PIX			0x2
#define AVE_8_PIX			0x3
#define AVE_ODD_PIXEL_DIST		(1 << 4) /* For Bayer Sensors */
#define AVE_EVEN_PIXEL_DIST		(1 << 2)

#define WB_GAIN_MAX			4

/* Features list */
#define PREV_AVERAGER			(1 << 0)
#define PREV_INVERSE_ALAW 		(1 << 1)
#define PREV_HORZ_MEDIAN_FILTER		(1 << 2)
#define PREV_NOISE_FILTER 		(1 << 3)
#define PREV_CFA			(1 << 4)
#define PREV_GAMMA_BYPASS		(1 << 5)
#define PREV_LUMA_ENHANCE		(1 << 6)
#define PREV_CHROMA_SUPPRESS		(1 << 7)
#define PREV_DARK_FRAME_SUBTRACT	(1 << 8)
#define PREV_LENS_SHADING		(1 << 9)
#define PREV_DARK_FRAME_CAPTURE		(1 << 10)
#define PREV_DEFECT_COR			(1 << 11)


#define ISP_NF_TABLE_SIZE 		(1 << 10)

#define ISP_GAMMA_TABLE_SIZE 		(1 << 10)

/* Table addresses */
#define ISPPRV_TBL_ADDR_RED_G_START  0x00
#define ISPPRV_TBL_ADDR_BLUE_G_START  0x800
#define ISPPRV_TBL_ADDR_GREEN_G_START  0x400

/*
 *Enumeration Constants for input and output format
 */
enum preview_input {
	PRV_RAW_CCDC,
	PRV_RAW_MEM,
	PRV_RGBBAYERCFA,
	PRV_COMPCFA,
	PRV_CCDC_DRKF,
	PRV_OTHERS
};
enum preview_output {
	PREVIEW_RSZ,
	PREVIEW_MEM
};
/*
 * Configure byte layout of YUV image
 */
enum preview_ycpos_mode {
	YCPOS_YCrYCb = 0,
	YCPOS_YCbYCr = 1,
	YCPOS_CbYCrY = 2,
	YCPOS_CrYCbY = 3
};

/**
 * struct ispprev_gtable - Structure for Gamma Correction.
 * @redtable: Pointer to the red gamma table.
 * @greentable: Pointer to the green gamma table.
 * @bluetable: Pointer to the blue gamma table.
 */
struct ispprev_gtable {
	u32 *redtable;
	u32 *greentable;
	u32 *bluetable;
};

/**
 * struct prev_white_balance - Structure for White Balance 2.
 * @wb_dgain: White balance common gain.
 * @wb_gain: Individual color gains.
 * @wb_coefmatrix: Coefficient matrix
 */
struct prev_white_balance {
	u16 wb_dgain; /* white balance common gain */
	u8 wb_gain[WB_GAIN_MAX]; /* individual color gains */
	u8 wb_coefmatrix[WB_GAIN_MAX][WB_GAIN_MAX];
};

/**
 * struct prev_size_params - Structure for size parameters.
 * @hstart: Starting pixel.
 * @vstart: Starting line.
 * @hsize: Width of input image.
 * @vsize: Height of input image.
 * @pixsize: Pixel size of the image in terms of bits.
 * @in_pitch: Line offset of input image.
 * @out_pitch: Line offset of output image.
 */
struct prev_size_params {
	unsigned int hstart;
	unsigned int vstart;
	unsigned int hsize;
	unsigned int vsize;
	unsigned char pixsize;
	unsigned short in_pitch;
	unsigned short out_pitch;
};

/**
 * struct prev_rgb2ycbcr_coeffs - Structure RGB2YCbCr parameters.
 * @coeff: Color conversion gains in 3x3 matrix.
 * @offset: Color conversion offsets.
 */
struct prev_rgb2ycbcr_coeffs {
	short coeff[RGB_MAX][RGB_MAX];
	short offset[RGB_MAX];
};

/**
 * struct prev_darkfrm_params - Structure for Dark frame suppression.
 * @addr: Memory start address.
 * @offset: Line offset.
 */
struct prev_darkfrm_params {
	u32 addr;
	 u32 offset;
 };

/**
 * struct prev_params - Structure for all configuration
 * @features: Set of features enabled.
 * @cfa: CFA coefficients.
 * @csup: Chroma suppression coefficients.
 * @ytable: Pointer to Luma enhancement coefficients.
 * @nf: Noise filter coefficients.
 * @dcor: Noise filter coefficients.
 * @gtable: Gamma coefficients.
 * @wbal: White Balance parameters.
 * @blk_adj: Black adjustment parameters.
 * @rgb2rgb: RGB blending parameters.
 * @rgb2ycbcr: RGB to ycbcr parameters.
 * @hmf_params: Horizontal median filter.
 * @size_params: Size parameters.
 * @drkf_params: Darkframe parameters.
 * @lens_shading_shift:
 * @average: Downsampling rate for averager.
 * @contrast: Contrast.
 * @brightness: Brightness.
 */
struct prev_params {
	u16 features;
	enum preview_ycpos_mode pix_fmt;
	struct ispprev_cfa cfa;
	struct ispprev_csup csup;
	u32 *ytable;
	struct ispprev_nf nf;
	struct ispprev_dcor dcor;
	struct ispprev_gtable gtable;
	struct ispprev_wbal wbal;
	struct ispprev_blkadj blk_adj;
	struct ispprev_rgbtorgb rgb2rgb;
	struct ispprev_csc rgb2ycbcr;
	struct ispprev_hmed hmf_params;
	struct prev_size_params size_params;
	struct prev_darkfrm_params drkf_params;
	u8 lens_shading_shift;
	u8 average;
	u8 contrast;
	u8 brightness;
};

/**
 * struct isptables_update - Structure for Table Configuration.
 * @update: Specifies which tables should be updated.
 * @flag: Specifies which tables should be enabled.
 * @prev_nf: Pointer to structure for Noise Filter
 * @lsc: Pointer to LSC gain table. (currently not used)
 * @red_gamma: Pointer to red gamma correction table.
 * @green_gamma: Pointer to green gamma correction table.
 * @blue_gamma: Pointer to blue gamma correction table.
 */
struct isptables_update {
	u16 update;
	u16 flag;
	struct ispprev_nf *prev_nf;
	u32 *lsc;
	u32 *red_gamma;
	u32 *green_gamma;
	u32 *blue_gamma;
	struct ispprev_cfa *prev_cfa;
};

/**
 * struct isp_prev_device - Structure for storing ISP Preview module information
 * @prevout_w: Preview output width.
 * @prevout_h: Preview output height.
 * @previn_w: Preview input width.
 * @previn_h: Preview input height.
 * @prev_inpfmt: Preview input format.
 * @prev_outfmt: Preview output format.
 * @hmed_en: Horizontal median filter enable.
 * @nf_en: Noise filter enable.
 * @dcor_en: Defect correction enable.
 * @cfa_en: Color Filter Array (CFA) interpolation enable.
 * @csup_en: Chrominance suppression enable.
 * @yenh_en: Luma enhancement enable.
 * @fmtavg: Number of horizontal pixels to average in input formatter. The
 *          input width should be a multiple of this number.
 * @brightness: Brightness in preview module.
 * @contrast: Contrast in preview module.
 * @color: Color effect in preview module.
 * @cfafmt: Color Filter Array (CFA) Format.
 *
 * This structure is used to store the OMAP ISP Preview module Information.
 */
struct isp_prev_device {
	u8 update_color_matrix;
	u8 update_rgb_blending;
	u8 update_rgb_to_ycbcr;
	u8 hmed_en;
	u8 nf_en;
	u8 dcor_en;
	u8 cfa_en;
	u8 csup_en;
	u8 yenh_en;
	u8 rg_update;
	u8 gg_update;
	u8 bg_update;
	u8 cfa_update;
	u8 nf_enable;
	u8 nf_update;
	u8 fmtavg;
	u8 brightness;
	u8 contrast;
	enum v4l2_colorfx color;
	enum cfa_fmt cfafmt;
	struct ispprev_nf prev_nf_t;
	struct prev_params params;
	int shadow_update;
	u32 sph;
	u32 slv;
	struct device *dev;
	spinlock_t lock;
};


#endif

#else
/*
 * drivers/media/video/isp/omap_previewer.h
 *
 * Header file for Preview module wrapper in TI's OMAP3430 ISP
 *
 * Copyright (C) 2008 Texas Instruments, Inc.
 *
 * Contributors:
 * 	Leonides Martinez <leonides.martinez@ti.com>
 * 	Sergio Aguirre <saaguirre@ti.com>
 *
 * This package is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * THIS PACKAGE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#include "isppreview.h"

#ifndef OMAP_ISP_PREVIEW_WRAP_H
#define OMAP_ISP_PREVIEW_WRAP_H

#define PREV_IOC_BASE			'P'
#define PREV_REQBUF			_IOWR(PREV_IOC_BASE, 1,\
						struct v4l2_requestbuffers)
#define PREV_QUERYBUF			_IOWR(PREV_IOC_BASE, 2,\
							struct v4l2_buffer)
#define PREV_SET_PARAM			_IOW(PREV_IOC_BASE, 3,\
							struct prev_params)
#define PREV_GET_PARAM			_IOWR(PREV_IOC_BASE, 4,\
							struct prev_params)
#define PREV_PREVIEW			_IOR(PREV_IOC_BASE, 5, int)
#define PREV_GET_STATUS			_IOR(PREV_IOC_BASE, 6, char)
#define PREV_GET_CROPSIZE		_IOR(PREV_IOC_BASE, 7,\
							struct prev_cropsize)
#define PREV_QUEUEBUF			_IOWR(PREV_IOC_BASE, 8,\
							struct v4l2_buffer)
#define PREV_IOC_MAXNR			8

#define LUMA_TABLE_SIZE			128
#define GAMMA_TABLE_SIZE		1024
#define CFA_COEFF_TABLE_SIZE		576
#define NOISE_FILTER_TABLE_SIZE		256

#define MAX_IMAGE_WIDTH			3300

#define PREV_INWIDTH_8BIT		0	/* pixel width of 8 bits */
#define PREV_INWIDTH_10BIT		1	/* pixel width of 10 bits */

#define PREV_32BYTES_ALIGN_MASK		0xFFFFFFE0
#define PREV_16PIX_ALIGN_MASK		0xFFFFFFF0

/**
 * struct prev_rgbblending - Structure for RGB2RGB blending parameters
 * @blending: Color correlation 3x3 matrix.
 * @offset: Color correlation offsets.
 */
struct prev_rgbblending {
	short blending[RGB_MAX][RGB_MAX];	/* color correlation 3x3
						 * matrix.
						 */
	short offset[RGB_MAX];			/* color correlation offsets */
};

/**
 * struct prev_cfa_coeffs - Structure for CFA coefficients
 * @hthreshold: Horizontal threshold.
 * @vthreshold: Vertical threshold.
 * @coeffs: CFA coefficients
 */
struct prev_cfa_coeffs {
	char hthreshold, vthreshold;
	int coeffs[CFA_COEFF_TABLE_SIZE];
};

/**
 * struct prev_gamma_coeffs - Structure for Gamma Coefficients
 * @red: Table of gamma correction values for red color.
 * @green: Table of gamma correction values for green color.
 * @blue: Table of gamma correction values for blue color.
 */
struct prev_gamma_coeffs {
	unsigned char red[GAMMA_TABLE_SIZE];
	unsigned char green[GAMMA_TABLE_SIZE];
	unsigned char blue[GAMMA_TABLE_SIZE];
};

/**
 * struct prev_noiseflt_coeffs - Structure for Noise Filter Coefficients.
 * @noise: Noise filter table.
 * @strength: Used to find out weighted average.
 */
struct prev_noiseflt_coeffs {
	unsigned char noise[NOISE_FILTER_TABLE_SIZE];
	unsigned char strength;
};

/**
 * struct prev_chroma_spr - Structure for Chroma Suppression.
 * @hpfy: High passed version of Y or normal Y.
 * @threshold: Threshold for chroma suppress.
 * @gain: Chroma suppression gain
 */
struct prev_chroma_spr {
	unsigned char hpfy;
	char threshold;
	unsigned char gain;
};

/**
 * struct prev_status - Structure to know status of the hardware
 * @hw_busy: Flag to indicate if Hardware is Busy.
 */
struct prev_status {
	char hw_busy;
};

/**
 * struct prev_cropsize - Structure to know crop size.
 * @hcrop: Horizontal size of crop window.
 * @vcrop: Vertical size of crop window.
 */
struct prev_cropsize {
	int hcrop;
	int vcrop;
};

/**
 * struct prev_device - Global device information structure.
 * @params: Pointer to structure containing preview parameters.
 * @opened: State of the device.
 * @wfc: Wait for completion. Used for locking operations.
 * @prevwrap_mutex: Mutex for preview wrapper use.
 * @inout_vbq_lock: Spinlock for in/out videobuf queues.
 * @lsc_vbq_lock: Spinlock for LSC videobuf queues.
 * @vbq_ops: Videobuf queue operations
 * @isp_addr_read: Input/Output address
 * @isp_addr_read: LSC address
 */
struct prev_device {
	unsigned char opened;
	struct completion wfc;
	struct mutex prevwrap_mutex;
	spinlock_t inout_vbq_lock; /* Spinlock for in/out videobuf queues. */
	spinlock_t lsc_vbq_lock; /* Spinlock for LSC videobuf queues. */
	struct videobuf_queue_ops vbq_ops;
	dma_addr_t isp_addr_read;
	dma_addr_t isp_addr_lsc;
	struct device *isp;
	struct prev_size_params size_params;
};

/**
 * struct prev_fh - Per-filehandle data structure
 * @inout_type: Used buffer type for I/O.
 * @inout_vbq: I/O Videobuffer queue.
 * @lsc_type: Used buffer type for LSC.
 * @lsc_vbq: LSC Videobuffer queue.
 * @device: Pointer to device information structure.
 */
struct prev_fh {
	enum v4l2_buf_type inout_type;
	struct videobuf_queue inout_vbq;
	enum v4l2_buf_type lsc_type;
	struct videobuf_queue lsc_vbq;
	struct prev_device *device;
};
#endif

#endif