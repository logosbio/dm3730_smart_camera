
src_list := src.list
 pwd := $(shell pwd)


 .PHONY: all

# make all is the first target
 all:

define find_file
 $(shell find $(1) \( -name "$(2)" -a -type f \) | \
      sed -e '/\/CVS\//d' -e '/\/RCS\//d' -e '/\.svn\//d' -e 's/^\.\///')
 endef

define change1suffix
 $(foreach f,$(1), \
     $(addprefix $(dir $(f)),$(patsubst .%.o.cmd,%.o,$(notdir $(f)))))
 endef

define _change2suffix
 $(wildcard $(dir $(1))$(patsubst %.o,.%.$(2),$(notdir $(1))))
 endef

define change2suffix
 $(foreach f,$(1), \
     $(call _change2suffix,$(f),$(2)))
 endef

define _obj2src
 $(wildcard $(patsubst %.o,%.$(2),$(1)))
 endef

define obj2src
 $(eval _src_f := $(call _obj2src,$(1),c)) \
 $(eval _src_f += $(call _obj2src,$(1),h)) \
 $(eval _src_f += $(call _obj2src,$(1),l)) \
 $(eval _src_f += $(call _obj2src,$(1),y)) \
 $(eval _src_f += $(call _obj2src,$(1),S)) \
 $(eval _src_f += $(call _obj2src,$(1),C)) \
 $(eval _src_f += $(call _obj2src,$(1),cpp)) \
 $(eval _src_f += $(call _obj2src,$(1),java)) \
 $(_src_f)
 endef

define get_src_from_var
 $(eval _all_files:=) \
 $(foreach v,$(1), \
     $(eval _files := $(subst $(pwd)/,,$($(v)))) \
     $(eval _files := $(patsubst /%,,$(_files))) \
     $(eval _all_files := $(sort $(_all_files) $(wildcard $(_files))))) \
 $(_all_files) \
 $(eval _all_files:=)
 endef

define get_one_src_from_var
 $(eval file:=) \
 $(eval _file := $(subst $(pwd)/,,$($(1)))) \
 $(info 1111111111_file=$(_file)) \
 $(eval _file := $(patsubst /%,,$(_file))) \
 $(info 2222222222 _file=$(_file)) \
 $(eval file := $(sort $(file) $(wildcard $(_file))))) \
 $(file) \
 $(eval file:=)
 endef


 DEP_FILE := $(call find_file,.,*.o.cmd)
 $(if $(DEP_FILE),, \
     $(eval DEP_FILE := $(call find_file,.,.depend)) \
     $(eval nokernel := true))

ifneq ($(nokernel),true)
 ################ Find all kernel source files ######################
 dep_objs := $(call change1suffix,$(DEP_FILE))
 dep_vars := $(addprefix deps_,$(dep_objs))
 ### New kernel ###
 dep_vars += $(addprefix source_,$(dep_objs))

include $(DEP_FILE)

######## Only find the kernel directory src and header files ####
 autoconf_header := $(shell test -d include && find include -name autoconf.h)
 all_src_files := $(autoconf_header)

all_src_files += $(call get_src_from_var,$(dep_vars))

else # $(nokernel) == true
 ############### Not kernel ##########################################
 O_SRC := $(call find_file,.,*.o))
 all_src_files := $(call obj2src,$(O_SRC))

#################### Find all source file by .depend ############
 ifneq ($(DEP_FILE),)
 $(foreach f,$(DEP_FILE), \
     $(shell sed 's/\(^[a-zA-Z0-9_][a-zA-Z0-9_-]*\.o\):/\1:=/' $(f) > $(f).tmp))

dep_vars := $(notdir $(O_SRC))
 include $(addsuffix .tmp,$(DEP_FILE))
 all_src_files := $(sort $(all_src_files) $(call get_src_from_var,$(dep_vars)))
 endif  # DEP_FILE

endif # end of nokernel


 all:
  $(foreach f,$(all_src_files),$(shell echo $(f) >> $(src_list)))
  @echo "----------- generate source file success ----------"

