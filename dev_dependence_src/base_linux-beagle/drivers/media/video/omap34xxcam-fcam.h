/*
 * fcam-drivers/omap34xxcam-fcam.h
 *
 * Copyright (C) 2010 Stanford University
 *
 * Contact: Andrew Adams <abadams@stanford.edu>
 *
 * Additional V4L2 ioctls and custom controls for the FCam camera control API.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */
 
#ifndef OMAP34XXCAM_FCAM_H
#define OMAP34XXCAM_FCAM_H

#include <linux/videodev2.h>

/* VIDIOCS, for kernel space and user space */
#define VIDIOC_FCAM_INSTALL _IO('V', BASE_VIDIOC_PRIVATE + 25)
#define VIDIOC_FCAM_WAIT_FOR_HS_VS _IOWR('V', BASE_VIDIOC_PRIVATE + 26, struct timeval)

/* Extra sensor controls */
#define V4L2_CID_FRAME_TIME (V4L2_CTRL_CLASS_CAMERA | 0x10ff)
#define V4L2_CID_GAIN_EXACT (V4L2_CTRL_CLASS_CAMERA | 0x10fe)

/* Kernel-side structures */

#ifdef __KERNEL__

struct omap34xxcam_fcam_client {
	/* The file handle of the fcam process (or NULL if there isn't one) */
	struct file *file; 

	/* A semaphore to use for waiting for HS_VS */
	struct semaphore sem; 

	/* Timestamp of most recent HS_VS */
	struct timeval timestamp;

	/* TODO: Add a next pointer, to make a linked list in order to handle multiple fcam clients */
};

#endif

#endif
