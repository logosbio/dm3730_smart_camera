#include <winsock2.h>
#include <stdlib.h>
#include <stdio.h>
#include "resource.h"
#include <SDL/SDL.h>
#include <SDL/SDL_keysym.h>
#include <SDL/SDL_syswm.h>
#include <jpeglib.h>
#include <setjmp.h>
#include <ws2tcpip.h>
#include <math.h>

#include<stdio.h>  
#include<time.h>  

#define BUFSIZE     0XFFFFFFFF	//5	// 버퍼 크기 
#define CHATTING    0  // 메시지 타입
#define DRAWING     1  // 메시지 타입
#define WM_DRAWIT   WM_USER+5 // 사용자 정의 윈도우 메시지


#define CONTROL_START	0xA000 // 버퍼 크기
#define RGB_MAX						3
#define ISPPRV_NF_TBL_SIZE			64
#define RAW_IMG_MAX_SIZE 			2592 * 1944 * 2 + 1024

// 체크 메시지
#define SET_CCDC_ENABLE			0x1
#define SET_PREV_ENABLE			0x2
#define SET_RAW_DATA_SAVE		0x4
#define SET_CCDC_RESET			0x8
#define SET_PREV_RESET			0x10


#define ISP_ABS_CCDC_ALAW			(1 << 0)
#define ISP_ABS_CCDC_LPF 			(1 << 1)
#define ISP_ABS_CCDC_BLCLAMP		(1 << 2)
#define ISP_ABS_CCDC_BCOMP			(1 << 3)
#define ISP_ABS_CCDC_FPC			(1 << 4)
#define ISP_ABS_CCDC_CULL			(1 << 5)
#define ISP_ABS_CCDC_COLPTN			(1 << 6)
#define ISP_ABS_CCDC_CONFIG_LSC		(1 << 7)
#define ISP_ABS_TBL_LSC				(1 << 8)

#define ISP_ABS_PREV_LUMAENH		(1 << 0)
#define ISP_ABS_PREV_INVALAW		(1 << 1)
#define ISP_ABS_PREV_HRZ_MED		(1 << 2)
#define ISP_ABS_PREV_CFA		(1 << 3)
#define ISP_ABS_PREV_CHROMA_SUPP	(1 << 4)
#define ISP_ABS_PREV_WB			(1 << 5)
#define ISP_ABS_PREV_BLKADJ		(1 << 6)
#define ISP_ABS_PREV_RGB2RGB		(1 << 7)
#define ISP_ABS_PREV_COLOR_CONV		(1 << 8)
#define ISP_ABS_PREV_YC_LIMIT		(1 << 9)
#define ISP_ABS_PREV_DEFECT_COR		(1 << 10)
#define ISP_ABS_PREV_GAMMABYPASS	(1 << 11)
#define ISP_ABS_TBL_NF 			(1 << 12)
#define ISP_ABS_TBL_REDGAMMA		(1 << 13)
#define ISP_ABS_TBL_GREENGAMMA		(1 << 14)
#define ISP_ABS_TBL_BLUEGAMMA		(1 << 15)

#define ISPCCDC_COLPTN_R_Ye			0x0
#define ISPCCDC_COLPTN_Gr_Cy			0x1
#define ISPCCDC_COLPTN_Gb_G			0x2
#define ISPCCDC_COLPTN_B_Mg			0x3
#define ISPCCDC_COLPTN_CP0PLC0_SHIFT		0
#define ISPCCDC_COLPTN_CP0PLC1_SHIFT		2
#define ISPCCDC_COLPTN_CP0PLC2_SHIFT		4
#define ISPCCDC_COLPTN_CP0PLC3_SHIFT		6
#define ISPCCDC_COLPTN_CP1PLC0_SHIFT		8
#define ISPCCDC_COLPTN_CP1PLC1_SHIFT		10
#define ISPCCDC_COLPTN_CP1PLC2_SHIFT		12
#define ISPCCDC_COLPTN_CP1PLC3_SHIFT		14
#define ISPCCDC_COLPTN_CP2PLC0_SHIFT		16
#define ISPCCDC_COLPTN_CP2PLC1_SHIFT		18
#define ISPCCDC_COLPTN_CP2PLC2_SHIFT		20
#define ISPCCDC_COLPTN_CP2PLC3_SHIFT		22
#define ISPCCDC_COLPTN_CP3PLC0_SHIFT		24
#define ISPCCDC_COLPTN_CP3PLC1_SHIFT		26
#define ISPCCDC_COLPTN_CP3PLC2_SHIFT		28
#define ISPCCDC_COLPTN_CP3PLC3_SHIFT		30

struct ispccdc_lsc_config {
	UINT16 offset;
	UINT8 gain_mode_n;
	UINT8 gain_mode_m;
	UINT8 gain_format;
	UINT16 fmtsph;
	UINT16 fmtlnh;
	UINT16 fmtslv;
	UINT16 fmtlnv;
	UINT8 initial_x;
	UINT8 initial_y;
	UINT size;
};


struct ispccdc_bclamp {
	UINT8 obgain;
	UINT8 obstpixel;
	UINT8 oblines;
	UINT8 oblen;
	UINT16 dcsubval;
};

struct set_ispccdc_fpc {
	UINT16 fpnum;
	UINT fpcaddr[5];
};


struct ispccdc_blcomp {
	UINT8 b_mg;
	UINT8 gb_g;
	UINT8 gr_cy;
	UINT8 r_ye;
};

struct ispccdc_culling {
	UINT8 v_pattern;
	UINT16 h_odd;
	UINT16 h_even;
};

struct set_ispccdc_update_config {
	UINT16 update;
	UINT16 flag;
	UINT alawip;
	struct ispccdc_bclamp bclamp;
	struct ispccdc_blcomp blcomp;
	struct set_ispccdc_fpc fpc;
	struct ispccdc_lsc_config lsc_cfg;
	struct ispccdc_culling cull;
	UINT colptn;
	UINT8 *lsc;
};



enum cfa_fmt {
	CFAFMT_BAYER, CFAFMT_SONYVGA, CFAFMT_RGBFOVEON,
	CFAFMT_DNSPL, CFAFMT_HONEYCOMB, CFAFMT_RRGGBBFOVEON
};

struct ispprev_hmed {
	UINT8 odddist;
	UINT8 evendist;
	UINT8 thres;
};

struct ispprev_cfa {
	UINT cfafmt;
	UINT8 cfa_gradthrs_vert;
	UINT8 cfa_gradthrs_horz;
	UINT *cfa_table;
};

struct ispprev_csup {
	UINT8 gain;
	UINT8 thres;
	UINT8 hypf_en;
};

struct ispprev_wbal {
	UINT16 dgain;
	UINT8 coef3;
	UINT8 coef2;
	UINT8 coef1;
	UINT8 coef0;
};

struct ispprev_blkadj {
	UINT8 red;
	UINT8 green;
	UINT8 blue;
};

struct ispprev_rgbtorgb {
	UINT16 matrix[3][3];
	UINT16 offset[3];
};

struct ispprev_csc {
	UINT16 matrix[3][3];
	INT16 offset[3];
};

struct ispprev_yclimit {
	UINT8 minC;
	UINT8 maxC;
	UINT8 minY;
	UINT8 maxY;
};

struct ispprev_dcor {
	UINT8 couplet_mode_en;
	UINT detect_correct[4];
};

struct set_ispprev_nf {
	UINT8 spread;
	UINT table;
};

struct set_ispprv_update_config {
	UINT16 update;
	UINT16 flag;
	UINT yen;
	UINT shading_shift;
	struct ispprev_hmed hmed_p;
	struct ispprev_cfa cfa_p;
	struct ispprev_csup csup_p;
	struct ispprev_wbal wbal_p;
	struct ispprev_blkadj blkadj_p;
	struct ispprev_rgbtorgb rgb2rgb_p;
	struct ispprev_csc csc_p;
	struct ispprev_yclimit yclimit_p;
	struct ispprev_dcor ispprev_dcor_t;
	struct set_ispprev_nf ispprev_nf_t;
	UINT red_gamma;
	UINT green_gamma;
	UINT blue_gamma;
};


typedef struct set_isp_config_t {
	UINT16 raw_save;
	UINT16 flag;
	struct set_ispccdc_update_config ccdc;
	struct set_ispprv_update_config prev;
	UINT8 lsc_Gz;
	UINT8 lsc_Gu;
}set_isp_config;


struct my_error_mgr {
	struct jpeg_error_mgr pub;	/* "public" fields */
	jmp_buf setjmp_buffer;	/* for return to caller */
};

typedef struct RawDataSendInfo_t {
	unsigned int		imageSize;
	int					imageWidth;
	int					imageHeight;
	int					imageType;
} rdsInfo;

typedef struct RawDataSend_t {
	rdsInfo				info;
	char gRawData[RAW_IMG_MAX_SIZE];
} RawDataSend;

UINT colptn[4];
////////////////////////////////////////////////////////////////////////////////////////////

// 대화상자 메시지 처리
BOOL CALLBACK DlgProc(HWND hDlg, UINT uMsg, 
	WPARAM wParam, LPARAM lParam);

// 유틸리티 함수
void err_quit(char *msg);
BOOL GetIPAddr(char *name, IN_ADDR *addr);
BOOL real_number_check(char * s, int checkStringNum);

static u_short port;

static HANDLE hReadEvent;

static BOOL bConnectServerFlag = FALSE;

static IN_ADDR ipaddr;

//static int retval;

static HWND hMainDlg;
static HWND hRawViewhDlg;
static HWND hDIOCheck[12];
static HWND hButtonSend;

static SOCKET listen_sock;
static SOCKET client_sock;
static SOCKADDR_IN clientaddr;
static int	client_addr_size = 0;


#define SERVER					"192.168.1.255"
#define RETURN_OK				0

#define CAPTURE_IMAGE_WIDTH		2592
#define CAPTURE_IMAGE_HEIGHT	1944
#define CAPTURE_IMAGE_SIZE		((CAPTURE_IMAGE_WIDTH)*(CAPTURE_IMAGE_HEIGHT) * 3)


#define USE_FULL_SCREEN			0
#define FB_BLUE_ONLY			0
#define BOX_PACK_DEFAULT_PAD	1
#define MAX_IMAGE_BUFFER		4

#define STREAM_IP "239.2.37.221"	//	"224.100.62.8"	//	"239.2.37.221"
#define UDP_PACKET_MAX_SIZE 65026	//	61440

HINSTANCE hInst;

SDL_Surface *sdl_view;
SDL_Color sdl_colors[256];
set_isp_config g_Isp;
SDL_Surface *screen;  
struct SDL_SysWMinfo wmInfo;


int capFps = 0;
int DispFps = 0;
int recvErrFps = 0;
int decodeErrFps = 0;
int totaldecodeErrFps = 0;
int sdl_flags = 0;

SOCKET  sock;
struct sockaddr_in addr;
struct sockaddr_in client;
socklen_t socklen;

volatile int g_rawdata_capture_flag = 0;
volatile int g_rawdata_view_on_flag = 0;
volatile int g_rawdata_view_repaint = 0;
volatile int g_stream_flag = 0;
volatile int g_read_jpg_thread_loop = 0;
volatile int g_display_thread_loop = 0;
volatile int g_display_flag = 0;
volatile int g_sdl_flag = 0;
volatile int g_decoder_thread_loop = 0;
volatile int g_decoder_flag = 0;
volatile int g_save_flag = 0;

unsigned char g_jpg_buffer[MAX_IMAGE_BUFFER][512000];
unsigned char g_image_buffer[MAX_IMAGE_BUFFER][CAPTURE_IMAGE_SIZE];
int g_image_buffer_index = 0;
int g_image_buffer_decode_index = 0;
int g_image_buffer_draw_index = 0;

int g_filesave_raw_count = 0;
int g_filesave_count = 0;

int g_screen_width = 1280, g_screen_height = 720;
int rawCnt = 0;

BITMAPINFO Bitmapinfo;
BITMAPINFOHEADER bmpinfo;
BYTE* YUV422_Buffer;
BYTE YUV444_Buffer[CAPTURE_IMAGE_SIZE];
BYTE g_RGB_Buffer[CAPTURE_IMAGE_SIZE];

static HANDLE g_rawdata_capture_thread_id;
static HANDLE g_read_jpg_thread_id, g_decoder_thread_id, g_display_thread_id;
static DWORD g_rawdata_capture_thread, g_read_jpg_thread, g_decoder_thread, g_display_thread;

typedef struct my_error_mgr * my_error_ptr;
RawDataSend recvData;

////////////////////////////////////////////////////////////////////////////////////////////
BYTE Clamp(int n)
{
    if (n >= 255)
        n = 255;
    else if(n<=0)
        n=0;
    return n;
}

#define Y(Col,Row) YUV444_Buffer[    3*((Col) + g_screen_width * (Row))]
#define U(Col,Row) YUV444_Buffer[1 + 3*((Col) + g_screen_width * (Row))]
#define V(Col,Row) YUV444_Buffer[2 + 3*((Col) + g_screen_width * (Row))]

#define TB(Col,Row) g_RGB_Buffer[    3*((Col) + g_screen_width * (Row))]
#define TG(Col,Row) g_RGB_Buffer[1 + 3*((Col) + g_screen_width * (Row))]
#define TR(Col,Row) g_RGB_Buffer[2 + 3*((Col) + g_screen_width * (Row))]

BYTE* YUV422toYUV444toRGB888(BYTE *YUV422_Buffer,UINT screen_width,UINT screen_height)
{
	UINT i=0, SYpos = 1, SUpos = 0, SVpos = 2, Row, Col;		

	if ( ( NULL == YUV422_Buffer ) || ( screen_width == 0 ) || ( screen_height == 0 ) )
	{
		MessageBox(0,"YUV422_Buffer or screen_width or screen_height Value Not Received",0,0);
		return 0;	
	}

	ZeroMemory(YUV444_Buffer,sizeof(YUV444_Buffer));	

	for (Row = i * screen_height; Row < screen_height * (i+1); Row++)
	{
		for (Col = 0; Col < screen_width; Col +=2)
		{			
			Y(Col,Row) = YUV422_Buffer[SYpos];	SYpos+=2;	
			U(Col,Row) = YUV422_Buffer[SUpos];	
			V(Col,Row) = YUV422_Buffer[SVpos];	

			Y(Col+1,Row) = YUV422_Buffer[SYpos];	SYpos+=2;	
			U(Col+1,Row) = YUV422_Buffer[SUpos];	SUpos+=4;	
			V(Col+1,Row) = YUV422_Buffer[SVpos];	SVpos+=4;	
		}		
	}

	Row=0,Col=0;
	for (Row =0 ; Row < screen_height; Row ++)
	{
		for (Col = 0; Col < screen_width; Col ++)
		{			
			TB(Col,Row) = Clamp(( 298 * (Y(Col,Row)-16) + 516 * (U(Col,Row)-128) + 128) >> 8);			
			TG(Col,Row) = Clamp(( 298 * (Y(Col,Row)-16) - 100 * (U(Col,Row)-128) - 208 * (V(Col,Row)-128) + 128) >> 8);			
			TR(Col,Row) = Clamp(( 298 * (Y(Col,Row)-16) + 409 * (V(Col,Row)-128) + 128) >> 8);
		}		
	}

	return YUV444_Buffer;
}

void GetTime( char *str )
{
	tm my_tm;
	time_t my_time;
	time( &my_time );
	my_tm = *localtime( &my_time );
	strftime( str, 127, "%H:%M:%S ", &my_tm );
}


METHODDEF(void)
my_error_exit (j_common_ptr cinfo)
{
	my_error_ptr myerr = (my_error_ptr) cinfo->err;
	(*cinfo->err->output_message) (cinfo);

	longjmp(myerr->setjmp_buffer, 1);
}



// epoch time으로 변환할 상수
#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

// for timezone
struct timezone
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};

// gettimeofday in windows
int gettimeofday(struct timeval *tv, struct timezone *tz)
{
  FILETIME ft;
  unsigned __int64 tmpres = 0;
  static int tzflag;

  if (NULL != tv)
  {
    // system time을 구하기
    GetSystemTimeAsFileTime(&ft);

    // unsigned 64 bit로 만들기
    tmpres |= ft.dwHighDateTime;
    tmpres <<= 32;
    tmpres |= ft.dwLowDateTime;

    // 100nano를 1micro로 변환하기
    tmpres /= 10;

    // epoch time으로 변환하기
    tmpres -= DELTA_EPOCH_IN_MICROSECS;    

    // sec와 micorsec으로 맞추기
    tv->tv_sec = (tmpres / 1000000UL);
    tv->tv_usec = (tmpres % 1000000UL);
  }

  // timezone 처리
  if (NULL != tz)
  {
    if (!tzflag)
    {
      _tzset();
      tzflag++;
    }
    tz->tz_minuteswest = _timezone / 60;
    tz->tz_dsttime = _daylight;
  }

  return 0;
}

int stream_init(void)
{
	int port = 2200;
	int opt = 1;
	//int ttl = 32;
	WSADATA         wsaData;
	int ret;
	unsigned long nonblocking = 1;

	if( WSAStartup( MAKEWORD(2,2), &wsaData ) != 0 )
	{
		printf( "WSAStartup fail \n" );
		exit(0);
	}


    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        printf("socket() failed");

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);

#if 0
	addr.sin_addr.s_addr = inet_addr(SERVER);
	printf("\n\n WRITE IP_ADDR %s, port %d \n" , SERVER, port);

#else
#if 1
//	addr.sin_addr.s_addr = INADDR_BROADCAST;
//	printf("\n\n WRITE IP_ADDR 0x%X, port %d \n" , INADDR_BROADCAST, port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);  //inet_addr(SERVER);
//	addr.sin_addr.s_addr = inet_addr(SERVER);
	printf("\n\n WRITE IP_ADDR %s, port %d \n" , SERVER, port);

	setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*)&opt, sizeof(opt));

	if ( sock < 0 )
		exit(0);

#else
	addr.sin_addr.s_addr = inet_addr(STREAM_IP);

	printf("\n\n Recv : IP_ADDR %s, port %d \n" , STREAM_IP, port);

	mreq.imr_multiaddr = addr.sin_addr;
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);

	setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
#endif
#endif
	socklen = (socklen_t)sizeof(client);

	bind(sock, (SOCKADDR*) &addr, sizeof(addr));

	fflush(stderr);
	return 0;
}

int recv_jpg_image(unsigned char *buffer)
{
	int ret, recvLen = UDP_PACKET_MAX_SIZE;
	int sBuf_size = 0, sendret = 0;;
	char * tmpPoint = (char *)buffer;
	int * jpgBufsize = (int *)buffer;
	unsigned char control_buffer[2];
	int nTransferred;

	do
	{
		ret = recvfrom(sock, tmpPoint, UDP_PACKET_MAX_SIZE, 0, (struct sockaddr*)&client, &socklen);
		//ret = recvfrom(sock, tmpPoint, UDP_PACKET_MAX_SIZE,  0, NULL, 0);
		sBuf_size = jpgBufsize[0];
	} while (ret != 4);
	capFps++;


	while ( sendret < sBuf_size ) {
		ret = recvfrom(sock, tmpPoint, recvLen, 0, (struct sockaddr*)&client, &socklen);

		if( ret < 0 ) {
			 fprintf(stderr,"Error recv Data(%d, %d)\n",sBuf_size ,sendret);
			 fflush(stderr);
			 recvErrFps++;
			 return 0;
		}
		sendret += ret;
		tmpPoint += ret;
		if ( (sBuf_size - sendret) > UDP_PACKET_MAX_SIZE )
			recvLen = UDP_PACKET_MAX_SIZE;
		else
			recvLen = sBuf_size - sendret;

	 }

	if (jpgBufsize[0] == 0xC4FFD8FF) {
		return sendret;
	} else {
		return -1;
	}
}



DWORD WINAPI rawdata_capture_thread_proc(LPVOID arg)
{
		
	int retval, recvDatasize;
	int sendret	= 0;
	UINT8 * tmpPoint;
	int	ret;

	EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_BUTTON), FALSE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_BUTTON_CCDC_RESET), FALSE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_BUTTON_PREV_RESET), FALSE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_BUTTON), TRUE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_RAWDATA_SAVE), FALSE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_RAWDATA_SAVE_VIEW), FALSE);

	if (g_rawdata_capture_flag == 1) {
			
		g_Isp.raw_save = 1;

		retval = send(client_sock, (char *)&g_Isp, sizeof(g_Isp), 0);
		if(retval == SOCKET_ERROR){
			MessageBox(hMainDlg, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR", "소켓에러", MB_ICONERROR);
			SetDlgItemText(hMainDlg, IDC_EDIT_STREAM_STATUS, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR");
			return TRUE;
		}

		retval = recvfrom( client_sock, (char *)&recvDatasize, sizeof(UINT), 0,  (SOCKADDR*)&clientaddr,  &client_addr_size );
		if(retval == SOCKET_ERROR){
			MessageBox(hMainDlg, "RAW DATA SIZE RECV ERR !!!!", "소켓에러", MB_ICONERROR);
			SetDlgItemText(hMainDlg, IDC_EDIT_STREAM_STATUS, "RAW DATA SIZE RECV ERR !!!!");
		}

		tmpPoint = (UINT8 *)&recvData;	//	mPoint;

		while ( sendret < recvDatasize ) {
			ret = recvfrom(client_sock, (char *)tmpPoint, recvDatasize, 0, (SOCKADDR*)&clientaddr,  &client_addr_size );
			if( ret < 0 ) {
				MessageBox(hMainDlg, "RAW DATA RECV ERR !!!!", "소켓에러", MB_ICONERROR);
			}
			sendret += ret;
			tmpPoint += ret;
						
		}
		g_screen_width = recvData.info.imageWidth, g_screen_height = recvData.info.imageHeight;

		g_Isp.raw_save = 0;
		g_rawdata_capture_flag = 0;
		g_rawdata_view_on_flag = 0;
		SetDlgItemText(hMainDlg, IDC_SCOPE_RAWDATA_SAVE_VIEW, "RAW Data\nVIEW On");
	}

	YUV422toYUV444toRGB888(YUV422_Buffer, g_screen_width, g_screen_height);						

	if (g_rawdata_view_on_flag == 1 && g_rawdata_view_repaint == 0) {
			g_rawdata_view_repaint = 1;
	}
	
	EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_RAWDATA_SAVE), TRUE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_RAWDATA_SAVE_VIEW), TRUE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_BUTTON_CCDC_RESET), TRUE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_BUTTON_PREV_RESET), TRUE);
	EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_BUTTON), TRUE);

	

	return 0;
}


DWORD WINAPI read_jpg_thread_proc(LPVOID arg)
{
	unsigned char read_jpg_buffer[512000];
	int nCaptureRet;

	unsigned char *samples;
	char filename[256];
	FILE *fp;

	g_image_buffer_index = 0;

	g_read_jpg_thread_loop = 1;

	while(1) {
		while (g_read_jpg_thread_loop)
		{
			nCaptureRet = recv_jpg_image(g_jpg_buffer[g_image_buffer_index]);

			if (nCaptureRet > 0)
			{
				g_image_buffer_decode_index = g_image_buffer_index++;
				if (g_image_buffer_index == MAX_IMAGE_BUFFER)
					g_image_buffer_index = 0;

				if (g_save_flag)
				{
					sprintf(filename, "H:/work/isp_raw/%04d.jpg", g_filesave_count++);
					memcpy(read_jpg_buffer, g_jpg_buffer[g_image_buffer_decode_index], nCaptureRet);
					fp = fopen(filename, "wb");
					fwrite(read_jpg_buffer, nCaptureRet, 1, fp);
					fflush(fp);
					fclose(fp);
					g_save_flag = 0;
				}

				if (g_decoder_flag != 1)
					g_decoder_flag = 1;
			}
			Sleep(0);
		}
		Sleep(0);
	}
	closesocket(sock);

}


DWORD WINAPI decoder_thread_proc(LPVOID arg)
{
	int n_samples;
	unsigned char *samples;

	g_image_buffer_index = 0;
	g_decoder_thread_loop = 1;

	while(1) {
		while (g_decoder_thread_loop)
		{
			struct jpeg_error_mgr err;
			struct jpeg_decompress_struct cinfo = {0};
			struct my_error_mgr jerr = {0};

			if (g_decoder_flag)
			{
				g_image_buffer_draw_index = g_image_buffer_decode_index++;
				if (g_image_buffer_decode_index == MAX_IMAGE_BUFFER)
					g_image_buffer_decode_index = 0;

				/* create decompressor */
				jpeg_create_decompress(&cinfo);

				cinfo.err = jpeg_std_error(&jerr.pub);
				jerr.pub.error_exit = my_error_exit;
				if (setjmp(jerr.setjmp_buffer)) {
					jpeg_destroy_decompress(&cinfo);
					//fprintf(stderr, "#jpeg_destroy_decompress\n\n");
					//fflush(stderr);
					decodeErrFps++;
					totaldecodeErrFps++;
					continue;

				}

				cinfo.do_fancy_upsampling = FALSE;

				/* set source buffer */
				jpeg_mem_src(&cinfo, g_jpg_buffer[g_image_buffer_draw_index], 512000);

				 /* read jpeg header */
				jpeg_read_header(&cinfo, 1);

				/* decompress */
				jpeg_start_decompress(&cinfo);
				n_samples = 0;
				samples = g_image_buffer[g_image_buffer_draw_index];


				g_screen_width = cinfo.output_width;
				g_screen_height = cinfo.output_height;

				while (cinfo.output_scanline < cinfo.output_height)
				{
					n_samples = jpeg_read_scanlines(&cinfo, &samples, 1);
					samples += n_samples * cinfo.image_width * cinfo.num_components;
				}


				/* clean up */
				jpeg_finish_decompress(&cinfo);
				jpeg_destroy_decompress(&cinfo);

				g_decoder_flag = 0;

				if (g_display_flag != 1)
					g_display_flag = 1;
			}
			Sleep(0);
		}
		Sleep(0);
	}
}

DWORD WINAPI display_thread_sdl_proc(LPVOID arg)
{
	unsigned char *pbuffer;
	unsigned char *psdl;
	unsigned char pixel;
	int i, sw,sh;
	struct tm				*timePassed;
	time_t					spentTime;

	static unsigned long	firstTime = 0;
	static unsigned long	prevTime = 0;
	unsigned long			deltaTime = 0;
	unsigned long			newTime = 0;
	time_t nSec = time(NULL);
	struct timeval			tv;
	int	cfps = 0, dfps = 0, refps = 0, defps = 0;
	char tempString[128];

	g_display_thread_loop = 1;
	g_display_flag = 0;
	while(1) {
		while (g_display_thread_loop)
		{
			if (gettimeofday(&tv, NULL) == -1) {
				fprintf(stderr, "Failed to get os time\n");
				exit (1);
			}

			newTime = tv.tv_sec * 1000 + tv.tv_usec / 1000;
			if (!firstTime) {
				firstTime = newTime;
				prevTime = newTime;
			}

			deltaTime = newTime - prevTime;
			if (deltaTime >= 1000) {
				fprintf(stderr, "## STREAM Recv %2d(Err %2d) fps, Display %2d(Err %d) fps, decode Err %d\n", capFps, recvErrFps, DispFps, decodeErrFps, totaldecodeErrFps);
				cfps = capFps;
				dfps = DispFps;
				refps = recvErrFps;
				defps = decodeErrFps;

				capFps = 0;
				DispFps = 0;
				recvErrFps = 0;
				decodeErrFps = 0;
				prevTime = newTime;
				fflush(stderr);
			}
			nSec = time(NULL);
			timePassed = localtime(&nSec);

			sprintf(tempString, "Time %.2d:%.2d:%.2d.%06d, STREAM Recv %2d(Err %2d) fps, Display %2d(Err %d) fps, decode Err %d, %dx%d", timePassed->tm_hour,
												  timePassed->tm_min,
												  timePassed->tm_sec, (int)tv.tv_usec
												  ,cfps, refps, dfps, defps, totaldecodeErrFps
												  ,g_screen_width, g_screen_height);

			SetDlgItemText(hMainDlg, IDC_EDIT_STREAM_STATUS, tempString);

			if (g_display_flag && g_sdl_flag == 1)
			{

				pbuffer = (unsigned char *) g_image_buffer[g_image_buffer_draw_index];
				psdl = (unsigned char *) sdl_view->pixels;
				SDL_LockSurface(sdl_view);

				for (i = 0; i < g_screen_width * g_screen_height * 3; i += 3)
				{
					if ( sdl_view->pixels != NULL) {
						*psdl++ = pbuffer[i+2];		//
						*psdl++ = pbuffer[i+1];
						*psdl++ = pbuffer[i];
						*psdl++ = 0;
					} else {
						totaldecodeErrFps++;
						decodeErrFps++;
						break;
					}
				}

				SDL_UnlockSurface(sdl_view);
				SDL_Flip(sdl_view);

				DispFps++;
				g_display_flag = 0;
				sw = g_screen_width;
				sh = g_screen_height;
			}

			if (sw != g_screen_width || sh != g_screen_height) {
				if ((sdl_view = SDL_SetVideoMode(g_screen_width, g_screen_height, 32, sdl_flags )) == NULL)
				{
					fprintf(stderr, "Error: failed to set SDL video mode\n");
					exit(2);
				}
				sw = g_screen_width;
				sh = g_screen_height;
			}

			Sleep(15); 
		}
	}
}


void capture(void)
{
	if (stream_init() != RETURN_OK)
	{
		return;
	}

	g_read_jpg_thread_id = CreateThread(NULL, 0, read_jpg_thread_proc, NULL, 0, &g_read_jpg_thread);
}

	
void decoder(void)
{
	g_decoder_thread_id = CreateThread(NULL, 0, decoder_thread_proc, NULL, 0, &g_decoder_thread);
}

void display(void)
{
	g_display_thread_id = CreateThread(NULL, 0, display_thread_sdl_proc, NULL, 0, &g_display_thread);
}


// 메인 함수
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE,
	LPSTR lpCmdLine, int nCmdShow)
{
	// 윈속 초기화
	WSADATA wsa;

	if(WSAStartup(MAKEWORD(2,2), &wsa) != 0)
		return -1;

	colptn[0] = ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP0PLC0_SHIFT | ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP0PLC1_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP0PLC2_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP0PLC3_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP1PLC0_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP1PLC1_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP1PLC2_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP1PLC3_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP2PLC0_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP2PLC1_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP2PLC2_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP2PLC3_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP3PLC0_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP3PLC1_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP3PLC2_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP3PLC3_SHIFT;
	colptn[1] = ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP0PLC0_SHIFT | ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP0PLC1_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP0PLC2_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP0PLC3_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP1PLC0_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP1PLC1_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP1PLC2_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP1PLC3_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP2PLC0_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP2PLC1_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP2PLC2_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP2PLC3_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP3PLC0_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP3PLC1_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP3PLC2_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP3PLC3_SHIFT;
	colptn[2] =	ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP0PLC0_SHIFT | ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP0PLC1_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP0PLC2_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP0PLC3_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP1PLC0_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP1PLC1_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP1PLC2_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP1PLC3_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP2PLC0_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP2PLC1_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP2PLC2_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP2PLC3_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP3PLC0_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP3PLC1_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP3PLC2_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP3PLC3_SHIFT;
	colptn[3] = ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP0PLC0_SHIFT | ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP0PLC1_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP0PLC2_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP0PLC3_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP1PLC0_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP1PLC1_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP1PLC2_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP1PLC3_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP2PLC0_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP2PLC1_SHIFT |        ISPCCDC_COLPTN_Gr_Cy << ISPCCDC_COLPTN_CP2PLC2_SHIFT |        ISPCCDC_COLPTN_R_Ye  << ISPCCDC_COLPTN_CP2PLC3_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP3PLC0_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP3PLC1_SHIFT |        ISPCCDC_COLPTN_B_Mg  << ISPCCDC_COLPTN_CP3PLC2_SHIFT |        ISPCCDC_COLPTN_Gb_G  << ISPCCDC_COLPTN_CP3PLC3_SHIFT;

	// 이벤트 생성
	hReadEvent = CreateEvent(NULL, FALSE, TRUE, NULL);
	if(hReadEvent == NULL) return -1;

	bConnectServerFlag = FALSE;
	hInst = hInstance;
	YUV422_Buffer = (BYTE *)&recvData.gRawData;

	DialogBox(hInstance, MAKEINTRESOURCE(IDD_MONITOR_DIALOG), NULL, DlgProc);

	// 이벤트 제거
	CloseHandle(hReadEvent);

	// 윈속 종료
	WSACleanup();

	return 0;
}


void RawDataPaint(HWND hDlg)
{		
	HDC Bhdc = GetWindowDC(hDlg);
	SetBkMode(Bhdc,TRANSPARENT);
	StretchDIBits(Bhdc,4, 25 + g_screen_height, g_screen_width, -g_screen_height, 0, 0, g_screen_width, g_screen_height, g_RGB_Buffer, &Bitmapinfo,DIB_RGB_COLORS, SRCCOPY);	
	DeleteDC(Bhdc);
	//SetWindowPos(hDlg,HWND_BOTTOM,100,100,1280,745,SWP_SHOWWINDOW);
}

INT_PTR CALLBACK RawView(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	hRawViewhDlg = hDlg;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			//UINT Dx=5, Dy=5, DWidth = GetSystemMetrics(SM_CXSCREEN) - 30, DHeight = GetSystemMetrics(SM_CYSCREEN) - 95;		
			//SetWindowPos(hDlg,HWND_BOTTOM,100,100,recvData.info.imageWidth ,recvData.info.imageHeight, SWP_SHOWWINDOW);
			SetWindowPos(hDlg,HWND_BOTTOM,100,100,  g_screen_width + 8, g_screen_height + 25, SWP_SHOWWINDOW);

		}
		return (INT_PTR)TRUE;
	case WM_PAINT:
		{			
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hDlg, &ps);
			RawDataPaint(hDlg);
			DeleteDC(hdc);

		}break;

	case WM_COMMAND:
		{
			if (LOWORD(wParam) == IDCANCEL)
			{
				EndDialog(hDlg, LOWORD(wParam));
				g_rawdata_view_on_flag = 0;
				return (INT_PTR)TRUE;
			}			
		}
		break;
	}
	return (INT_PTR)FALSE;
}


int SetResetPREV(HWND hDlg) 
{
	UINT i = 0;
	UINT idxIdcNum = IDC_PREV_EDIT_START;

	for (i = IDC_CHECK_PREV_LE; i<IDC_CHECK_PREV_END; i++) {
		SendMessage(GetDlgItem(hDlg, i),BM_SETCHECK,BST_UNCHECKED,0);
	}
	
	
	g_Isp.prev.hmed_p.odddist = 0x2; /* For Bayer sensor */
	g_Isp.prev.hmed_p.evendist = 0x2;
	g_Isp.prev.hmed_p.thres = 0x10;

	g_Isp.prev.cfa_p.cfafmt = CFAFMT_BAYER;
	g_Isp.prev.cfa_p.cfa_gradthrs_vert = 0x28; /* Default values */
	g_Isp.prev.cfa_p.cfa_gradthrs_horz = 0x28; /* Default values */

	g_Isp.prev.csup_p.gain = 0x0D;
	g_Isp.prev.csup_p.thres = 0xEB;
	g_Isp.prev.csup_p.hypf_en = 0;

	g_Isp.prev.wbal_p.dgain = 0x100;
	g_Isp.prev.wbal_p.coef3 = 0x39;
	g_Isp.prev.wbal_p.coef2 = 0x20;
	g_Isp.prev.wbal_p.coef1 = 0x20;
	g_Isp.prev.wbal_p.coef0 = 0x23;

	g_Isp.prev.blkadj_p.red = 0x0;
	g_Isp.prev.blkadj_p.green = 0x0;
	g_Isp.prev.blkadj_p.blue = 0x0;

	g_Isp.prev.rgb2rgb_p.matrix[0][0] = 0x01E2;
	g_Isp.prev.rgb2rgb_p.matrix[0][1] = 0x0F30;
	g_Isp.prev.rgb2rgb_p.matrix[0][2] = 0x0FEE;
	g_Isp.prev.rgb2rgb_p.matrix[1][0] = 0x0F9B;
	g_Isp.prev.rgb2rgb_p.matrix[1][1] = 0x01AC;
	g_Isp.prev.rgb2rgb_p.matrix[1][2] = 0x0FB9;
	g_Isp.prev.rgb2rgb_p.matrix[2][0] = 0x0FE0;
	g_Isp.prev.rgb2rgb_p.matrix[2][1] = 0x0EC0;
	g_Isp.prev.rgb2rgb_p.matrix[2][2] = 0x0260;

	g_Isp.prev.rgb2rgb_p.offset[0] = 0x0000;
	g_Isp.prev.rgb2rgb_p.offset[1] = 0x0000;
	g_Isp.prev.rgb2rgb_p.offset[2] = 0x0000;

	struct ispprev_csc csc_p = {
		{	/*  Coef Matrix No Effect */
#if 0			
			{66, 129, 25},
			{-38, -75, 112},
			{112, -94 , -18}
#else
			{0x04C, 0x098, 0x01C},	
			{0x3D4, 0x3AC, 0x080},
			{0x080, 0x39E, 0x3EC}
#endif
		},	/* CSC Offset */
		{0x0, 0x0, 0x0}
	};

	g_Isp.prev.csc_p = csc_p;

	g_Isp.prev.yclimit_p.minC = 0x00;	/* Default values */
	g_Isp.prev.yclimit_p.maxC = 0xFF;
	g_Isp.prev.yclimit_p.minY = 0x00;
	g_Isp.prev.yclimit_p.maxY = 0xFF;

	g_Isp.prev.ispprev_dcor_t.couplet_mode_en = 1;
	g_Isp.prev.ispprev_dcor_t.detect_correct[0] = 0xE;	/* Default values */
	g_Isp.prev.ispprev_dcor_t.detect_correct[1] = 0xE;
	g_Isp.prev.ispprev_dcor_t.detect_correct[2] = 0xE;
	g_Isp.prev.ispprev_dcor_t.detect_correct[3] = 0xE;

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.hmed_p.odddist, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.hmed_p.evendist, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.hmed_p.thres, 0 );


	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.cfa_p.cfafmt, 0);
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.cfa_p.cfa_gradthrs_vert, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.cfa_p.cfa_gradthrs_horz, 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csup_p.gain, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csup_p.thres, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csup_p.hypf_en, 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.wbal_p.dgain, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.wbal_p.coef3, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.wbal_p.coef2, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.wbal_p.coef1, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.wbal_p.coef0, 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.blkadj_p.red, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.blkadj_p.green, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.blkadj_p.blue, 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[0][0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[0][1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[0][2], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[1][0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[1][1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[1][2], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[2][0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[2][1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.matrix[2][2], 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.offset[0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.offset[1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.rgb2rgb_p.offset[2], 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[0][0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[0][1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[0][2], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[1][0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[1][1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[1][2], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[2][0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[2][1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.matrix[2][2], 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.offset[0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.offset[1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.csc_p.offset[2], 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.yclimit_p.minC, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.yclimit_p.maxC, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.yclimit_p.minY, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.yclimit_p.maxY, 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.ispprev_dcor_t.couplet_mode_en, 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.ispprev_dcor_t.detect_correct[0], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.ispprev_dcor_t.detect_correct[1], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.ispprev_dcor_t.detect_correct[2], 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.prev.ispprev_dcor_t.detect_correct[3], 0 );

	return 0;
}

int SetResetCCDC(HWND hDlg) 
{
	UINT i = 0;
	UINT idxIdcNum = IDC_CCDC_EDIT_START;

	for (i = IDC_CHECK_CCDC_ALAW; i<IDC_CHECK_CCDC_END; i++) {
		SendMessage(GetDlgItem(hDlg, i),BM_SETCHECK,BST_UNCHECKED,0);
	}

	CheckRadioButton(hDlg, IDC_RADIO_ALAW1, IDC_RADIO_ALAW4, IDC_RADIO_ALAW3);
	g_Isp.lsc_Gz = 0x0;
	g_Isp.lsc_Gu = 0x40;
	g_Isp.ccdc.bclamp.obgain = 0x10;
	g_Isp.ccdc.bclamp.obstpixel = 0;
	g_Isp.ccdc.bclamp.oblines = 0;
	g_Isp.ccdc.bclamp.oblen = 0;
	g_Isp.ccdc.bclamp.dcsubval = 64;


	g_Isp.ccdc.blcomp.b_mg = 0;
	g_Isp.ccdc.blcomp.gb_g = 0;
	g_Isp.ccdc.blcomp.gr_cy = 0;
	g_Isp.ccdc.blcomp.r_ye = 0;

	g_Isp.ccdc.fpc.fpnum = 5;
	g_Isp.ccdc.fpc.fpcaddr[0] = ((10 << 19) || (10 << 5) || (1 << 0));
	g_Isp.ccdc.fpc.fpcaddr[1] = ((10 << 19) || (20 << 5) || (1 << 0));
	g_Isp.ccdc.fpc.fpcaddr[2] = ((10 << 19) || (30 << 5) || (1 << 0));
	g_Isp.ccdc.fpc.fpcaddr[3] = ((10 << 19) || (40 << 5) || (1 << 0));
	g_Isp.ccdc.fpc.fpcaddr[4] = ((10 << 19) || (50 << 5) || (1 << 0));

	g_Isp.ccdc.cull.v_pattern = 0xff;
	g_Isp.ccdc.cull.h_odd = 0xFF;
	g_Isp.ccdc.cull.h_even = 0xFF;

	g_Isp.ccdc.colptn = colptn[0];

	g_Isp.ccdc.lsc_cfg.offset = 124;
	g_Isp.ccdc.lsc_cfg.gain_mode_n = 6;
	g_Isp.ccdc.lsc_cfg.gain_mode_m = 6;
	g_Isp.ccdc.lsc_cfg.gain_format = 4;
	g_Isp.ccdc.lsc_cfg.fmtsph = 0;
	g_Isp.ccdc.lsc_cfg.fmtlnh = 1920;
	g_Isp.ccdc.lsc_cfg.fmtslv = 0;
	g_Isp.ccdc.lsc_cfg.fmtlnv = 1080;

	SetDlgItemText(hDlg, idxIdcNum++ , "16");
	SetDlgItemText(hDlg, idxIdcNum++ , "0");
	SetDlgItemText(hDlg, idxIdcNum++ , "0");
	SetDlgItemText(hDlg, idxIdcNum++ , "0");
	SetDlgItemText(hDlg, idxIdcNum++ , "64");

	SetDlgItemText(hDlg, idxIdcNum++ , "0");
	SetDlgItemText(hDlg, idxIdcNum++ , "0");
	SetDlgItemText(hDlg, idxIdcNum++ , "0");
	SetDlgItemText(hDlg, idxIdcNum++ , "0");

	SetDlgItemText(hDlg, idxIdcNum++ , "5");
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.fpc.fpcaddr[0] , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.fpc.fpcaddr[1] , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.fpc.fpcaddr[2] , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.fpc.fpcaddr[3] , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.fpc.fpcaddr[4] , 0 );

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.cull.v_pattern , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.cull.h_odd , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.cull.h_even , 0 );

//	g_Isp.ccdc.colptn
	CheckRadioButton(hDlg, IDC_RADIO_ALAW5, IDC_RADIO_ALAW8, IDC_RADIO_ALAW5);

	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.offset , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.gain_mode_n , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.gain_mode_m , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.gain_format , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.fmtsph , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.fmtlnh , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.fmtslv , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.fmtlnv , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.initial_x , 0 );
	SetDlgItemInt(hDlg, idxIdcNum++ ,g_Isp.ccdc.lsc_cfg.initial_y , 0 );

	SetDlgItemInt(hDlg, IDC_EDIT_LSC_GZ ,g_Isp.lsc_Gz , 0 );
	SetDlgItemInt(hDlg, IDC_EDIT_LSC_GU ,g_Isp.lsc_Gu , 0 );
	

	return 0;
}


int GetCheckBox(HWND hDlg) 
{
	int i = 0;

	g_Isp.flag = 0;
		//	BST_INDETERMINATE
	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_ENABLE), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.flag &= ~SET_CCDC_ENABLE;
	}else {
		g_Isp.flag |= SET_CCDC_ENABLE;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_ENABLE), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.flag &= ~SET_PREV_ENABLE;
	}else {
		g_Isp.flag |= SET_PREV_ENABLE;
	}

	g_Isp.ccdc.update = 0;
	g_Isp.ccdc.flag = 0;
	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_ALAW), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_ALAW;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_ALAW;
	}else {
		g_Isp.ccdc.update |= ISP_ABS_CCDC_ALAW;
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_ALAW;
	}

	//	BST_INDETERMINATE
	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_LPF), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_LPF;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_LPF;
	}else {
		g_Isp.ccdc.update |= ISP_ABS_CCDC_LPF;
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_LPF;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_BCL), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_BLCLAMP;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_BLCLAMP;
	}else {
		g_Isp.ccdc.update |= ISP_ABS_CCDC_BLCLAMP;
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_BLCLAMP;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_BCOMP), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_BCOMP;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_BCOMP;
	}else {
		g_Isp.ccdc.update |= ISP_ABS_CCDC_BCOMP;
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_BCOMP;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_CULL), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_CULL;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_CULL;
	}else {
		g_Isp.ccdc.update |= ISP_ABS_CCDC_CULL;
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_CULL;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_COL), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_COLPTN;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_COLPTN;
	}else {
		g_Isp.ccdc.update |= ISP_ABS_CCDC_COLPTN;
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_COLPTN;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_LC), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_CONFIG_LSC;
		g_Isp.ccdc.update &= ~ISP_ABS_TBL_LSC;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_CONFIG_LSC;
	}else {
		g_Isp.ccdc.update |= (ISP_ABS_CCDC_CONFIG_LSC | ISP_ABS_TBL_LSC);
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_CONFIG_LSC;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_CCDC_FPC), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.ccdc.update &= ~ISP_ABS_CCDC_FPC;
		g_Isp.ccdc.flag &= ~ISP_ABS_CCDC_FPC;
	}else {
		g_Isp.ccdc.update |= ISP_ABS_CCDC_FPC;
		g_Isp.ccdc.flag |= ISP_ABS_CCDC_FPC;
	}

	g_Isp.prev.update = 0;
	g_Isp.prev.flag = 0;

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_LE), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_LUMAENH;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_LUMAENH;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_LUMAENH;
		g_Isp.prev.flag |= ISP_ABS_PREV_LUMAENH;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_IALAW), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_INVALAW;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_INVALAW;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_INVALAW;
		g_Isp.prev.flag |= ISP_ABS_PREV_INVALAW;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_HM), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_HRZ_MED;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_HRZ_MED;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_HRZ_MED;
		g_Isp.prev.flag |= ISP_ABS_PREV_HRZ_MED;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_CFA), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_CFA;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_CFA;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_CFA;
		g_Isp.prev.flag |= ISP_ABS_PREV_CFA;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_CS), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_CHROMA_SUPP;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_CHROMA_SUPP;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_CHROMA_SUPP;
		g_Isp.prev.flag |= ISP_ABS_PREV_CHROMA_SUPP;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_WB), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_WB;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_WB;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_WB;
		g_Isp.prev.flag |= ISP_ABS_PREV_WB;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_BA), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_BLKADJ;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_BLKADJ;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_BLKADJ;
		g_Isp.prev.flag |= ISP_ABS_PREV_BLKADJ;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_R), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_RGB2RGB;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_RGB2RGB;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_RGB2RGB;
		g_Isp.prev.flag |= ISP_ABS_PREV_RGB2RGB;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_CC), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_COLOR_CONV;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_COLOR_CONV;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_COLOR_CONV;
		g_Isp.prev.flag |= ISP_ABS_PREV_COLOR_CONV;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_YC), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_YC_LIMIT;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_YC_LIMIT;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_YC_LIMIT;
		g_Isp.prev.flag |= ISP_ABS_PREV_YC_LIMIT;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_DC), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_DEFECT_COR;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_DEFECT_COR;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_DEFECT_COR;
		g_Isp.prev.flag |= ISP_ABS_PREV_DEFECT_COR;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_GB), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_PREV_GAMMABYPASS;
		g_Isp.prev.flag &= ~ISP_ABS_PREV_GAMMABYPASS;
	}else {
		g_Isp.prev.update |= ISP_ABS_PREV_GAMMABYPASS;
		g_Isp.prev.flag |= ISP_ABS_PREV_GAMMABYPASS;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_NF), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_TBL_NF;
		g_Isp.prev.flag &= ~ISP_ABS_TBL_NF;
	}else {
		g_Isp.prev.update |= ISP_ABS_TBL_NF;
		g_Isp.prev.flag |= ISP_ABS_TBL_NF;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_RG), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_TBL_REDGAMMA;
		g_Isp.prev.flag &= ~ISP_ABS_TBL_REDGAMMA;
	}else {
		g_Isp.prev.update |= ISP_ABS_TBL_REDGAMMA;
		g_Isp.prev.flag |= ISP_ABS_TBL_REDGAMMA;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_GG), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_TBL_GREENGAMMA;
		g_Isp.prev.flag &= ~ISP_ABS_TBL_GREENGAMMA;
	}else {
		g_Isp.prev.update |= ISP_ABS_TBL_GREENGAMMA;
		g_Isp.prev.flag |= ISP_ABS_TBL_GREENGAMMA;
	}

	if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_PREV_BG), BM_GETCHECK,0,0)==BST_UNCHECKED) {
		g_Isp.prev.update &= ~ISP_ABS_TBL_BLUEGAMMA;
		g_Isp.prev.flag &= ~ISP_ABS_TBL_BLUEGAMMA;
	}else {
		g_Isp.prev.update |= ISP_ABS_TBL_BLUEGAMMA;
		g_Isp.prev.flag |= ISP_ABS_TBL_BLUEGAMMA;
	}

	return 0;
}

int GetEditBox(HWND hDlg) 
{
	UINT i, cfafmt, tmp;
	UINT idxIdcNum;

	if ( (g_Isp.flag & SET_CCDC_ENABLE) == SET_CCDC_ENABLE) 
	{
		idxIdcNum = IDC_CCDC_EDIT_START;
		for ( i = IDC_RADIO_ALAW1; i <= IDC_RADIO_ALAW4; i++ ) {
			if ( SendMessage(GetDlgItem(hDlg, i), BM_GETCHECK,0,0) == BST_CHECKED) {
				g_Isp.ccdc.alawip  = i - IDC_RADIO_ALAW1 + 3;
				break;
			}
		}

		g_Isp.ccdc.bclamp.obgain = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.bclamp.obstpixel = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.bclamp.oblines = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.bclamp.oblen = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.bclamp.dcsubval = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);


		g_Isp.ccdc.blcomp.b_mg = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.blcomp.gb_g = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.blcomp.gr_cy = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.blcomp.r_ye = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.ccdc.fpc.fpnum = GetDlgItemInt(hDlg, idxIdcNum, NULL, FALSE);
		if (g_Isp.ccdc.fpc.fpnum < 1) {
			g_Isp.ccdc.fpc.fpnum = 5;
			SetDlgItemText(hDlg, idxIdcNum++ , "5");
		}else 
			idxIdcNum++;

		g_Isp.ccdc.fpc.fpcaddr[0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.fpc.fpcaddr[1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.fpc.fpcaddr[2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.fpc.fpcaddr[3] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.fpc.fpcaddr[4] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.ccdc.cull.v_pattern = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.cull.h_odd = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.cull.h_even = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		//g_Isp.ccdc.colptn = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		for ( i = IDC_RADIO_ALAW5; i <= IDC_RADIO_ALAW8; i++ ) {
			if ( SendMessage(GetDlgItem(hDlg, i), BM_GETCHECK,0,0) == BST_CHECKED) {
				g_Isp.ccdc.colptn  = colptn[i - IDC_RADIO_ALAW5];
				break;
			}
		}
		g_Isp.ccdc.lsc_cfg.offset = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.gain_mode_n = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.gain_mode_m = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.gain_format = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.fmtsph = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.fmtlnh = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.fmtslv = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.fmtlnv = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.initial_x = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.ccdc.lsc_cfg.initial_y = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		tmp = (UINT8)GetDlgItemInt(hDlg, IDC_EDIT_LSC_GZ, NULL, FALSE);
		if (tmp > 0xff)
			tmp = 0xff;
		g_Isp.lsc_Gz = tmp;
		g_Isp.lsc_Gu = (UINT8)GetDlgItemInt(hDlg, IDC_EDIT_LSC_GU, NULL, FALSE);
	}

	if ( (g_Isp.flag & SET_PREV_ENABLE) == SET_PREV_ENABLE) 
	{
		idxIdcNum = IDC_PREV_EDIT_START;

		g_Isp.prev.hmed_p.odddist = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.hmed_p.evendist = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.hmed_p.thres = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		cfafmt = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		if (cfafmt < CFAFMT_BAYER || cfafmt > CFAFMT_RRGGBBFOVEON) {
			cfafmt = CFAFMT_BAYER;
			SetDlgItemInt(hDlg, IDC_EDIT_CFA_S ,cfafmt, 0 );
		}

		g_Isp.prev.cfa_p.cfafmt = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.cfa_p.cfa_gradthrs_vert = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.cfa_p.cfa_gradthrs_horz = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.csup_p.gain = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csup_p.thres = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csup_p.hypf_en = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.wbal_p.dgain = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.wbal_p.coef3 = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.wbal_p.coef2 = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.wbal_p.coef1 = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.wbal_p.coef0 = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.blkadj_p.red = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.blkadj_p.green = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.blkadj_p.blue = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.rgb2rgb_p.matrix[0][0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[0][1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[0][2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[1][0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[1][1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[1][2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[2][0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[2][1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.matrix[2][2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.rgb2rgb_p.offset[0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.offset[1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.rgb2rgb_p.offset[2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.csc_p.matrix[0][0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[0][1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[0][2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[1][0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[1][1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[1][2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[2][0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[2][1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.matrix[2][2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.csc_p.offset[0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.offset[1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.csc_p.offset[2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.yclimit_p.minC = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.yclimit_p.maxC = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.yclimit_p.minY = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.yclimit_p.maxY = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);

		g_Isp.prev.ispprev_dcor_t.couplet_mode_en = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.ispprev_dcor_t.detect_correct[0] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.ispprev_dcor_t.detect_correct[1] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
		g_Isp.prev.ispprev_dcor_t.detect_correct[2] = GetDlgItemInt(hDlg, idxIdcNum++, NULL, FALSE);
	}
	
	return 0;
}


FILE *m_file_hndl = NULL;
// 대화상자 메시지 처리
BOOL CALLBACK DlgProc(HWND hDlg, UINT uMsg, 
	WPARAM wParam, LPARAM lParam)
{
	struct jpeg_error_mgr err;
	struct jpeg_decompress_struct cinfo = {0};
	struct my_error_mgr jerr = {0};
	static DWORD ServerThreadId;
	static DWORD ClientThreadId;
	static char temp[128];
	
	int retval;
	int checkcount		= 8;
	int	ret;

	int checkStringNum = 0;
	UINT fpc_tbl, idxIdcNum = IDC_CCDC_EDIT_START;

	static HWND hAeEdit;

	static HWND hStaticText;
	static HWND hCheck;
	char buf[128];
	int sendret = 0;;
	UINT8 * tmpPoint = NULL;	//	

	SDL_Overlay *overlay;
	SDL_Rect rect;

	hMainDlg = hDlg;

	switch(uMsg){
		case WM_INITDIALOG:
		{
			hButtonSend	= GetDlgItem(hDlg, IDC_SCOPE_BUTTON);
			EnableWindow(hButtonSend, FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_CCDC_RESET), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_PREV_RESET), FALSE);

			SetDlgItemText(hDlg, IDC_IPADDRESS				, "192.168.0.222");
			SetDlgItemText(hDlg, IDC_PORTNUM				, "666");
			client_addr_size = sizeof(clientaddr);
						
			SetResetCCDC(hDlg) ;
			SetResetPREV(hDlg) ;

			GetCheckBox(hDlg);
			GetEditBox(hDlg);

			if (SDL_Init(SDL_INIT_VIDEO))
			{
				fprintf(stderr, "Error : failed to init sdl\n");
				exit (1);
			}

			sdl_flags = SDL_SWSURFACE;
			sdl_flags |= SDL_RESIZABLE;
		//	sdl_flags |= SDL_NOFRAME;

		//	sdl_flags |= SDL_OPENGL;
		//	sdl_flags |= SDL_GL_DOUBLEBUFFER;
		//	sdl_flags |= SDL_DOUBLEBUF;
		//	sdl_flags |= SDL_HWPALETTE;
		//	sdl_flags |= SDL_HWSURFACE;
		//	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		//	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		//	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		//	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		//	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		//	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 0);


			return TRUE;
		}

		case WM_COMMAND:
		{
			switch(LOWORD(wParam)) {

				case IDC_SCOPE_STREAM:
				{
					if ( g_stream_flag == 0) {
						g_stream_flag = 1;
						display();
						decoder();
						capture();
					}

					if ( g_sdl_flag == 0) {
						SDL_putenv("SDL_VIDEO_WINDOW_POS=10,30");
						SetDlgItemText(hMainDlg, IDC_SCOPE_STREAM, "STREAM\nView Off");

						if ((sdl_view = SDL_SetVideoMode(g_screen_width, g_screen_height, 32, sdl_flags )) == NULL)
						{
							fprintf(stderr, "Error: failed to set SDL video mode\n");
							exit(2);
						}
						SDL_SetColors(sdl_view, sdl_colors, 0, 256);
						SDL_WM_SetCaption("view", "view");
						g_sdl_flag = 1;
						g_display_flag = 0;
					} else if (g_sdl_flag == 1) {
						SetDlgItemText(hMainDlg, IDC_SCOPE_STREAM, "STREAM\nView On");
						g_sdl_flag = 0;
						g_display_flag = 0;
						SDL_FreeSurface(sdl_view);
						if (SDL_WasInit(SDL_INIT_VIDEO)) 
							SDL_Quit();
					}

					return TRUE;
				}

				case IDC_SCOPE_STREAM_START:
				{
					g_decoder_thread_loop = 1;
					g_read_jpg_thread_loop = 1;
					g_display_thread_loop = 1;
					return TRUE;
				}

				case IDC_SCOPE_STREAM_STOP:
				{
					g_decoder_thread_loop = 0;
					g_read_jpg_thread_loop = 0;
					g_display_thread_loop = 0;
					return TRUE;
				}

				case IDC_SCOPE_STREAM_SAVE:
				{
					g_save_flag = 1;
					return TRUE;
				}

				case IDC_BUTTON_CCDC_RESET:
				{
					SetResetCCDC(hDlg);
					g_Isp.flag = SET_CCDC_RESET;
					//g_Isp.ccdc.flag = 0xffff;

					retval = send(client_sock, (char *)&g_Isp, sizeof(g_Isp), 0);
					if(retval == SOCKET_ERROR){
						MessageBox(hMainDlg, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR", "소켓에러", MB_ICONERROR);
						SetDlgItemText(hMainDlg, IDC_EDIT_STREAM_STATUS, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR");
						return TRUE;
					}
					//g_Isp.ccdc.flag = 0x0;
					GetCheckBox(hDlg);
					GetEditBox(hDlg);
					return TRUE;
				}

				case IDC_BUTTON_PREV_RESET:
				{
					SetResetPREV(hDlg);
					g_Isp.flag = SET_PREV_RESET;
					retval = send(client_sock, (char *)&g_Isp, sizeof(g_Isp), 0);
					if(retval == SOCKET_ERROR){
						MessageBox(hMainDlg, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR", "소켓에러", MB_ICONERROR);
						SetDlgItemText(hMainDlg, IDC_EDIT_STREAM_STATUS, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR");
						return TRUE;
					}
					GetCheckBox(hDlg);
					GetEditBox(hDlg);

					return TRUE;
				}

				case IDC_SCOPE_BUTTON:
				{
					GetCheckBox(hDlg);
					GetEditBox(hDlg);
					
					retval = send(client_sock, (char *)&g_Isp, sizeof(g_Isp), 0);
					if(retval == SOCKET_ERROR){
						MessageBox(hMainDlg, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR", "소켓에러", MB_ICONERROR);
						SetDlgItemText(hMainDlg, IDC_EDIT_STREAM_STATUS, "!!! IDC_SCOPE_RAWDATA_SAVE Send data ERR");
						return TRUE;
					}

					return TRUE;
				}
				
				case IDC_SCOPE_RAWDATA_CAPTURE:
				{
					//retval = sizeof(g_Isp);
					if (g_rawdata_capture_flag == 0) {
						rawCnt++;
						ZeroMemory(&Bitmapinfo,sizeof(Bitmapinfo));
						ZeroMemory(&bmpinfo,sizeof(bmpinfo));

						bmpinfo.biSize = sizeof(BITMAPINFOHEADER);
						bmpinfo.biWidth = g_screen_width;
						bmpinfo.biHeight = g_screen_height;
						bmpinfo.biPlanes = 1;			// we only have one bitplane
						bmpinfo.biBitCount = 24;		// RGB mode is 24 bits
						bmpinfo.biCompression = BI_RGB;	
						bmpinfo.biSizeImage = 0;		// can be 0 for 24 bit images
						bmpinfo.biXPelsPerMeter = 0;     // paint and PSP use this values
						bmpinfo.biYPelsPerMeter = 0;     
						bmpinfo.biClrUsed = 0;			// we are in RGB mode and have no palette
						bmpinfo.biClrImportant = 0;    // all colors are important
						HANDLE File_Handle;              // file handle							

						Bitmapinfo.bmiHeader = bmpinfo;		

						g_rawdata_capture_thread_id = CreateThread(NULL, 0, rawdata_capture_thread_proc, NULL, 0, &g_rawdata_capture_thread);
						GetCheckBox(hDlg);
						GetEditBox(hDlg);

						while(g_rawdata_capture_flag == 1) {
							Sleep(0);
						}
						g_rawdata_capture_flag = 1;
					}

					return TRUE;
				}

				case IDC_SCOPE_RAWDATA_SAVE_VIEW:
				{
#if 0
					if ( g_rawdata_view_repaint == 1) {
						PAINTSTRUCT ps;
						HDC hdc = BeginPaint(hRawViewhDlg, &ps);
						RawDataPaint(hRawViewhDlg);
						DeleteDC(hdc);
						g_rawdata_view_repaint = 0;
					}
#endif
					if (g_rawdata_view_on_flag == 0) {
						EnableWindow(GetDlgItem(hMainDlg, IDC_SCOPE_RAWDATA_SAVE_VIEW), FALSE);
						CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG_RAW_VIEW), hDlg, RawView);
						g_rawdata_view_on_flag = 1;
					}

					return TRUE;
				}

				
				case IDC_SCOPE_RAWDATA_SAVE:
				{
					BITMAPFILEHEADER bmfh;
					BITMAPINFOHEADER info;
					FILE *fp;
					UINT paddedsize = g_screen_width * g_screen_height * 3;


					// andinitialize them to zero
					memset ( &bmfh, 0, sizeof (BITMAPFILEHEADER ) );
					memset ( &info, 0, sizeof (BITMAPINFOHEADER ) );

					// fill the fileheader with data
					bmfh.bfType = 0x4d42;       // 0x4d42 = 'BM'
					bmfh.bfReserved1 = 0;
					bmfh.bfReserved2 = 0;
					bmfh.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + paddedsize;
					bmfh.bfOffBits = 0x36;		// number of bytes to start of bitmap bits

					// fill the infoheader

					info.biSize = sizeof(BITMAPINFOHEADER);
					info.biWidth = g_screen_width;
					info.biHeight = -g_screen_height;
					info.biPlanes = 1;				// we only have one bitplane
					info.biBitCount = 24;			// RGB mode is 24 bits
					info.biCompression = BI_RGB;
					info.biSizeImage = 0;			// can be 0 for 24 bit images
					info.biXPelsPerMeter = 0x0ec4;	// paint and PSP use this values
					info.biYPelsPerMeter = 0x0ec4;
					info.biClrUsed = 0;				// we are in RGB mode and have no palette
					info.biClrImportant = 0;		// all colors are important

	
					sprintf_s( buf, "H:/work/isp_raw/RawUYVYtoBMP_%dx%d_%04d.bmp",g_screen_width, g_screen_height, rawCnt);
					fp = fopen(buf, "wb");
	
					fwrite(&bmfh, sizeof ( BITMAPFILEHEADER ), 1, fp);
					fwrite(&info, sizeof ( BITMAPINFOHEADER ), 1, fp);
					fwrite(&g_RGB_Buffer, paddedsize, 1, fp);
					fflush(fp);
					fclose(fp);


					sprintf_s( buf, "H:/work/isp_raw/UYVY_%dx%d_raw_%04d.yuv",g_screen_width, g_screen_height, rawCnt);
					m_file_hndl = fopen( buf, "wb" );
					retval = fwrite(  &recvData.gRawData[0], recvData.info.imageSize, 1, m_file_hndl );
					fclose( m_file_hndl );

					return TRUE;
				}


				case IDC_CONNECT_CHECK_BUTTON:
				{
					GetDlgItemText(hDlg, IDC_IPADDRESS, temp, 128);

					bConnectServerFlag ^= TRUE;

					if ( bConnectServerFlag == TRUE ) {
						if(GetIPAddr(temp, &ipaddr) == FALSE){
							MessageBox(hDlg, "잘못된 주소입니다.",
								"실패", MB_ICONERROR);
							return TRUE;
						}
						port = GetDlgItemInt(hDlg, IDC_PORTNUM, NULL, FALSE);
						client_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
						if(client_sock == INVALID_SOCKET) err_quit("socket()");	
						// connect()
						ZeroMemory(&clientaddr, sizeof(clientaddr));
						clientaddr.sin_family = AF_INET;
						clientaddr.sin_port = htons(port);
						clientaddr.sin_addr = ipaddr;
						retval = connect(client_sock, (SOCKADDR *)&clientaddr, sizeof(clientaddr));
						if(retval == SOCKET_ERROR) {
							MessageBox(hDlg, "SOCKET_ERROR!!"
								"\r\n서버 상태 또는 접속 확인 요망",
								"실패", MB_ICONERROR);
							bConnectServerFlag = FALSE;
						// 클라이언트 스레드 시작
						} else {
							EnableWindow(hButtonSend, TRUE);
							EnableWindow(GetDlgItem(hDlg, IDC_SCOPE_RAWDATA_CAPTURE), TRUE);

							EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_CCDC_RESET), TRUE);
							EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_PREV_RESET), TRUE);

							SetDlgItemText(hDlg, IDC_CONNECT_CHECK_BUTTON, "&DECONNECT");
						}
					} else {
						closesocket(client_sock);

						EnableWindow(hButtonSend, FALSE);

						EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_CCDC_RESET), FALSE);
						EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_PREV_RESET), FALSE);

						EnableWindow(GetDlgItem(hDlg, IDC_SCOPE_RAWDATA_CAPTURE), FALSE);

						SetDlgItemText(hDlg, IDC_STATIC_CONNECT, "&Control Connect : Off");
						SetDlgItemText(hDlg, IDC_CONNECT_CHECK_BUTTON, "&CONNECT");
						SetDlgItemText(hDlg, IDC_IPADDRESS, "192.168.0.222");
						SetDlgItemText(hDlg, IDC_PORTNUM, "666");
					}
					return TRUE;
				}

				case IDCANCEL:
				{
					// 애플리케이션 종료
					if(MessageBox(hDlg, "종료하시겠습니까?", 
						"확인", MB_YESNO) == IDYES)
					{
						EndDialog(hDlg, 0);
						closesocket(client_sock);
					}
					return TRUE;
				}

				default:    
					return TRUE;
			}
		}
	}
	return FALSE;
}


// 소켓 함수 오류 출력 후 종료
void err_quit(char *msg)
{
	LPVOID lpMsgBuf;
	FormatMessage( 
		FORMAT_MESSAGE_ALLOCATE_BUFFER|
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(-1);
}

// 도메인 이름 -> IP 주소
BOOL GetIPAddr(char *name, IN_ADDR *addr)
{
	HOSTENT *ptr = gethostbyname(name);
	if(ptr == NULL)
		return FALSE;
	memcpy(addr, ptr->h_addr, ptr->h_length);
	return TRUE;
}


BOOL real_number_check(char * s, int checkStringNum)
{

	for (int i = 0; i < checkStringNum; i++ ) {
		if ( ('0' > s[i]) || ( '9' < s[i]) )  {
			if ( s[i] != '.' && s[i] != '-' && s[i] != 'E' && s[i] != 'e' && s[i] != '+')
				return FALSE;
		}
	}
	return TRUE;
}
