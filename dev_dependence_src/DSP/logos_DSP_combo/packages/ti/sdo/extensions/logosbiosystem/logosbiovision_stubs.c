/*
 *  ======== logosbiovision_stubs.c ========
 *  These functions are the "app-side" of a "remote" implementation.
 *
 */
#include <xdc/std.h>
#include <ti/sdo/ce/visa.h>
#include <ti/sdo/codecs/logosbiovision/ilogosbiovision.h>
#include <ti/sdo/ce/osal/Memory.h>

#include "logosbiovision.h"
#include "_logosbiovision.h"

static XDAS_Int32 ceImageConverter(ILOGOSBIOVISION_Handle handle,
							   XDAS_UInt8 *inBuf,
							   ILOGOSBIOVISION_ImageConverterArgs *inArgs);

static XDAS_Int32 logosJpegEncode(ILOGOSBIOVISION_Handle handle,
							  XDAS_UInt8 *inBuf,
							  XDAS_UInt8 *outBuf,
							  ILOGOSBIOVISION_logosJpegEncodeInArgs *inArgs,
							  ILOGOSBIOVISION_logosJpegEncodeOutArgs *outArgs);

static XDAS_Int32 process(ILOGOSBIOVISION_Handle handle,
						  XDAS_UInt8 *yuyvBuf,
						  ILOGOSBIOVISION_processInArgs *InArgs);

ILOGOSBIOVISION_Fxns LOGOSBIOVISION_STUBS = {
	{&LOGOSBIOVISION_STUBS, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL},
	ceImageConverter, logosJpegEncode, process,
};

/*
 *  ======== process ========
 *  This is the stub-implementation for the process method
 */
static XDAS_Int32 ceImageConverter(ILOGOSBIOVISION_Handle handle,
						XDAS_UInt8 *inBuf,
						ILOGOSBIOVISION_ImageConverterArgs *inArgs)
{
	XDAS_Int32 retVal;
	VISA_Handle visa = (VISA_Handle)handle;
	_LOGOSBIOVISION_Msg *msg;

	/* get a message appropriate for this algorithm */
	if ((msg = (_LOGOSBIOVISION_Msg *)VISA_allocMsg(visa)) == NULL) {
		return (LOGOSBIOVISION_ERUNTIME);
	}

	/* Specify the processing command that the skeleton should do */
	msg->visa.cmd = _LOGOSBIOVISION_CIMAGECONVERTER;


	/* inArgs has no pointers, so simply copy the struct fields into the msg */
	msg->cmd.ceImageConverter.inArgs = *inArgs;

	/* inBuf is a pointer, so we have to convert it */
	msg->cmd.ceImageConverter.inBuf = (XDAS_UInt8 *)
		Memory_getBufferPhysicalAddress(inBuf, inArgs->size_scale_yuyv, NULL);
	if (msg->cmd.ceImageConverter.inBuf == NULL) {
		retVal = LOGOSBIOVISION_ERUNTIME;
		goto exit_ceImageConverter;
	}

	/* send the message to the skeleton and wait for completion */
	retVal = VISA_call(visa, (VISA_Msg *)&msg);

	/* copy out the outArgs */
	*inArgs = msg->cmd.ceImageConverter.inArgs;

	/* Note that we need not copy inArgs out of the msg. */

	/*
	 * Note that we don't have to do any reverse address translation, as the
	 * originally provided buffers haven't changed.
	 */

	VISA_freeMsg(visa, (VISA_Msg)msg);

exit_ceImageConverter:
	return (retVal);
}

static XDAS_Int32 logosJpegEncode(ILOGOSBIOVISION_Handle handle,
					  XDAS_UInt8 *inBuf,
					  XDAS_UInt8 *outBuf,
					  ILOGOSBIOVISION_logosJpegEncodeInArgs *inArgs,
					  ILOGOSBIOVISION_logosJpegEncodeOutArgs *outArgs)
{
	XDAS_Int32 retVal;
	VISA_Handle visa = (VISA_Handle)handle;
	_LOGOSBIOVISION_Msg *msg;

	/* get a message appropriate for this algorithm */
	if ((msg = (_LOGOSBIOVISION_Msg *)VISA_allocMsg(visa)) == NULL) {
		return (LOGOSBIOVISION_ERUNTIME);
	}

	/* Specify the processing command that the skeleton should do */
	msg->visa.cmd = _LOGOSBIOVISION_CLOGOSJPEGENCODE;

	msg->cmd.logosJpegEncode.inBuf = (XDAS_UInt8 *)
		Memory_getBufferPhysicalAddress(inBuf, inArgs->sizeOfinputImg, NULL);

	if (msg->cmd.logosJpegEncode.inBuf == NULL) {
		retVal = LOGOSBIOVISION_ERUNTIME;
		goto exit_logosJpegEncode;
	}

	msg->cmd.logosJpegEncode.outBuf = (XDAS_UInt8 *)
		Memory_getBufferPhysicalAddress(outBuf, outArgs->sizeOfoutputImg, NULL);

	if (msg->cmd.logosJpegEncode.outBuf == NULL) {
		retVal = LOGOSBIOVISION_ERUNTIME;
		goto exit_logosJpegEncode;
	}

	msg->cmd.logosJpegEncode.inArgs		= *inArgs;
	msg->cmd.logosJpegEncode.outArgs	= *outArgs;

	retVal = VISA_call(visa, (VISA_Msg *)&msg);

	*inArgs = msg->cmd.logosJpegEncode.inArgs;
	*outArgs = msg->cmd.logosJpegEncode.outArgs;

	inBuf = msg->cmd.logosJpegEncode.inBuf;
	Memory_cacheInv(inBuf, msg->cmd.logosJpegEncode.inArgs.sizeOfinputImg);

	outBuf = msg->cmd.logosJpegEncode.outBuf;
	Memory_cacheInv(outBuf, msg->cmd.logosJpegEncode.outArgs.sizeOfoutputImg);

exit_logosJpegEncode:
	VISA_freeMsg(visa, (VISA_Msg)msg);

	return (retVal);
}


static XDAS_Int32 process(ILOGOSBIOVISION_Handle handle,
						  XDAS_UInt8 *yuyvBuf,
						  ILOGOSBIOVISION_processInArgs *InArgs)
{
	XDAS_Int32 retVal;
	VISA_Handle visa = (VISA_Handle)handle;
	_LOGOSBIOVISION_Msg *msg;


	/* get a message appropriate for this algorithm */
	if ((msg = (_LOGOSBIOVISION_Msg *)VISA_allocMsg(visa)) == NULL) {
		return (LOGOSBIOVISION_ERUNTIME);
	}

	/* Specify the processing command that the skeleton should do */
	msg->visa.cmd = _LOGOSBIOVISION_CPROCESS;

	msg->cmd.process.yuyvBuf = (XDAS_UInt8 *)
		Memory_getBufferPhysicalAddress(yuyvBuf, InArgs->yuyvBufSize, NULL);

	if (msg->cmd.process.yuyvBuf == NULL) {
		retVal = LOGOSBIOVISION_ERUNTIME;
		goto exit_process;
	}

	msg->cmd.process.InArgs = *InArgs;

	retVal = VISA_call(visa, (VISA_Msg *)&msg);

	*InArgs = msg->cmd.process.InArgs;



exit_process:
	VISA_freeMsg(visa, (VISA_Msg)msg);

	return (retVal);
}

