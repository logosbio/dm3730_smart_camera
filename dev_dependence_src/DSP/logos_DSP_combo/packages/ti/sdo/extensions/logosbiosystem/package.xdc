/*!
 *  ======== package.xdc ========
 *  Abstract Scale Algorithm Interfaces
 *
 *  This package contains an example scaling interfaces.
 *  In addition, it provides APIs that
 *  clients can call to create and run these algorithms.
 */

//requires ti.sdo.ce;

package ti.sdo.extensions.logosbiosystem [1, 0, 1]{
    interface ILOGOSBIOVISION;
}
