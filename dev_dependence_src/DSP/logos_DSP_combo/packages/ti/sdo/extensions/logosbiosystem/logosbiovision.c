#include <xdc/std.h>

#include <ti/xdais/ialg.h>
#include <ti/sdo/ce/visa.h>

#include <ti/sdo/ce/osal/Log.h>
#include <ti/sdo/ce/trace/gt.h>

#include "logosbiovision.h"
#include "_logosbiovision.h"

/* default params*/

LOGOSBIOVISION_InitParams LOGOSBIOVISION_PARAMS = {      /* default vision factor */
    sizeof(LOGOSBIOVISION_InitParams),
    40,
    0,
    0,
    0
};

#define GTNAME "ti.sdo.extensions.logosbiosystem.ILOGOSBIOVISION"

static GT_Mask curTrace = {NULL,NULL};


/*
 *  ======== LOGOSBIOVISION_create ========
 */
LOGOSBIOVISION_Handle LOGOSBIOVISION_create(Engine_Handle server, String name,
    LOGOSBIOVISION_InitParams *params)
{
    LOGOSBIOVISION_Handle visa;
    static Bool curInit = FALSE;

    if (curInit != TRUE) {
        curInit = TRUE;
        GT_create(&curTrace, GTNAME);
    }

    GT_3trace(curTrace, GT_ENTER, "LOGOSBIOVISION_create> "
        "Enter (server=0x%x, name='%s', params=0x%x)\n",
        server, name, params);

    if (params == NULL) {
        params = &LOGOSBIOVISION_PARAMS;
    }

    visa = VISA_create(server, name, (IALG_Params *)params,
        sizeof (_LOGOSBIOVISION_Msg), "ti.sdo.extensions.logosbiosystem.ILOGOSBIOVISION");

    GT_1trace(curTrace, GT_ENTER, "LOGOSBIOVISION_create> return (0x%x)\n", visa);

    return (visa);
}


/*
 *  ======== LOGOSBIOVISION_delete ========
 */
Void LOGOSBIOVISION_delete(LOGOSBIOVISION_Handle handle)
{
    GT_1trace(curTrace, GT_ENTER, "LOGOSBIOVISION_delete> Enter (handle=0x%x)\n",
        handle);

    VISA_delete(handle);

    GT_0trace(curTrace, GT_ENTER, "LOGOSBIOVISION_delete> return\n");
}

/*
 *  ======== LOGOSBIOVISION_process ========
 *  This method must be the same for both local and remote invocation;
 *  each call site in the client might be calling different implementations
 *  (one that marshalls & sends and one that simply calls).
 */
XDAS_Int32 LOGOSBIOVISION_ceImageConverter(LOGOSBIOVISION_Handle handle,
							   XDAS_UInt8 *inBuf,
							   LOGOSBIOVISION_ImageConverterArgs *inArgs)
{
    XDAS_Int32 retVal = LOGOSBIOVISION_EFAIL;

    GT_2trace(curTrace, GT_ENTER, "LOGOSBIOVISION_ceImageConverter> "
        "Enter (handle=0x%x, inArgs=0x%x)\n",
         handle, inArgs);

    if (handle) {
        ILOGOSBIOVISION_Fxns *fxns =
            (ILOGOSBIOVISION_Fxns *)VISA_getAlgFxns((VISA_Handle)handle);

        ILOGOSBIOVISION_Handle alg = VISA_getAlgHandle((VISA_Handle)handle);

        if (fxns && (alg != NULL)) {
            VISA_enter((VISA_Handle)handle);
            retVal = fxns->ceImageConverter(alg, inBuf, inArgs);
            VISA_exit((VISA_Handle)handle);
        }
    }

    GT_2trace(curTrace, GT_ENTER, "LOGOSBIOVISION_ceImageConverter> "
        "Exit (handle=0x%x, inArgs=0x%x)\n", handle, inArgs);

    return (retVal);
}

XDAS_Int32 LOGOSBIOVISION_logosJpegEncode(LOGOSBIOVISION_Handle handle,
						XDAS_UInt8 *inBuf,
						XDAS_UInt8 *outBuf,
						LOGOSBIOVISION_logosJpegEncodeInArgs *inArgs,
						LOGOSBIOVISION_logosJpegEncodeOutArgs *outArgs  )
{
    XDAS_Int32 retVal = LOGOSBIOVISION_EFAIL;

    GT_5trace(curTrace, GT_ENTER, "LOGOSBIOVISION_logosJpegEncode> "
        "Enter (handle=0x%x, inBuf=0x%x, outBuf=0x%x, inArgs=0x%x, outArgs=0x%x)\n",
         handle, inBuf, outBuf, inArgs, outArgs);

    if (handle) {
        ILOGOSBIOVISION_Fxns *fxns =
            (ILOGOSBIOVISION_Fxns *)VISA_getAlgFxns((VISA_Handle)handle);
        ILOGOSBIOVISION_Handle alg = VISA_getAlgHandle((VISA_Handle)handle);

        if (fxns && (alg != NULL)) {
            VISA_enter((VISA_Handle)handle);
            retVal = fxns->logosJpegEncode(alg, inBuf, outBuf, inArgs, outArgs);
            VISA_exit((VISA_Handle)handle);
        }
    }

    return (retVal);
}

XDAS_Int32 LOGOSBIOVISION_process(LOGOSBIOVISION_Handle handle,
						  XDAS_UInt8 *yuyvBuf,
						  LOGOSBIOVISION_processInArgs *InArgs)
{
    XDAS_Int32 retVal = LOGOSBIOVISION_EFAIL;


    GT_3trace(curTrace, GT_ENTER, "LOGOSBIOVISION_process> "
        "Enter (handle=0x%x, yuyvBuf=0x%x, outArgs=0x%x)\n",
         handle, yuyvBuf, InArgs);

    if (handle) {
        ILOGOSBIOVISION_Fxns *fxns =
            (ILOGOSBIOVISION_Fxns *)VISA_getAlgFxns((VISA_Handle)handle);
        ILOGOSBIOVISION_Handle alg = VISA_getAlgHandle((VISA_Handle)handle);

        if (fxns && (alg != NULL)) {
            VISA_enter((VISA_Handle)handle);
            retVal = fxns->process(alg, yuyvBuf, InArgs);
            VISA_exit((VISA_Handle)handle);
        }
    }

    return (retVal);
}
