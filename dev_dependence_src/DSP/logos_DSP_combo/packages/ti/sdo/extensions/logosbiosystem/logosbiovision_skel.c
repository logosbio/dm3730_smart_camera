/*
 *  ======== logosbiovision_skel.c ========
 */
#include <xdc/std.h>
#include <ti/sdo/ce/skel.h>
#include <ti/sdo/ce/osal/Memory.h>

#include "logosbiovision.h"
#include "_logosbiovision.h"

//#define LBVDSP_DEBUG

/*
 *  ======== call ========
 */
static VISA_Status call(VISA_Handle visaHandle, VISA_Msg visaMsg)
{
	_LOGOSBIOVISION_Msg *msg  = (_LOGOSBIOVISION_Msg *)visaMsg;
	LOGOSBIOVISION_Handle handle = (LOGOSBIOVISION_Handle)visaHandle;
	XDAS_UInt8 *inputImg, *outputImg;

#ifdef LBVDSP_DEBUG
    {
        volatile int i = 1;

        while(i);
    }
#endif

	switch (msg->visa.cmd) {

		case _LOGOSBIOVISION_CIMAGECONVERTER:
		{
			inputImg = msg->cmd.ceImageConverter.inBuf;
			Memory_cacheInv(inputImg, msg->cmd.ceImageConverter.inArgs.size_scale_yuyv);

			msg->visa.status = LOGOSBIOVISION_ceImageConverter(handle,
				inputImg, &(msg->cmd.ceImageConverter.inArgs));
		} break;

		case _LOGOSBIOVISION_CLOGOSJPEGENCODE:
		{
			inputImg  = msg->cmd.logosJpegEncode.inBuf;
			Memory_cacheInv(inputImg, msg->cmd.logosJpegEncode.inArgs.sizeOfinputImg);

			outputImg  = msg->cmd.logosJpegEncode.outBuf;
			Memory_cacheInv(outputImg, msg->cmd.logosJpegEncode.outArgs.sizeOfoutputImg);

			msg->visa.status = LOGOSBIOVISION_logosJpegEncode(handle, inputImg,
				outputImg, &(msg->cmd.logosJpegEncode.inArgs), &(msg->cmd.logosJpegEncode.outArgs));

			outputImg = msg->cmd.logosJpegEncode.outBuf;

//			Memory_cacheInv(outputImg, msg->cmd.logosJpegEncode.outArgs.sizeOfoutputImg);
			Memory_cacheWb(outputImg, msg->cmd.logosJpegEncode.outArgs.sizeOfoutputImg);
		} break;

		case _LOGOSBIOVISION_CPROCESS:
		{
			/* unmarshall inBuf and outBuf */
			inputImg  = msg->cmd.process.yuyvBuf;
			/* invalidate cache for input and output buffers */
			Memory_cacheInv(inputImg, msg->cmd.process.InArgs.yuyvBufSize);

			/* make the ceImageConverter call */
			msg->visa.status = LOGOSBIOVISION_process(handle,
				inputImg, &(msg->cmd.process.InArgs));

			inputImg  = msg->cmd.process.yuyvBuf;

//			Memory_cacheInv(inputImg, msg->cmd.process.InArgs.yuyvBufSize);
			Memory_cacheWb(inputImg, msg->cmd.process.InArgs.yuyvBufSize);
		} break;

		default:
		{
			msg->visa.status = VISA_EFAIL;
		} break;
	}
	return (VISA_EOK);
}

/*
 *  ======== LOGOSBIOVISION_SKEL ========
 */
SKEL_Fxns LOGOSBIOVISION_SKEL = {
	call,
	(SKEL_CREATEFXN)LOGOSBIOVISION_create,
	(SKEL_DESTROYFXN)LOGOSBIOVISION_delete,
};
