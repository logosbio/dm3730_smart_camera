/*!
 *  ======== ILOGOSBIOVISION ========
 *  Scale Codec Configuration interface
 *
 *  All Scale Codec modules must implement this configuration interface.
 */
metaonly interface ILOGOSBIOVISION inherits ti.sdo.ce.ICodec
{
    override config String serverFxns	=	"LOGOSBIOVISION_SKEL";
    override config String stubFxns	=	"LOGOSBIOVISION_STUBS";


    //override readonly config Int rpcProtocolVersion = 3;
    override readonly config Int rpcProtocolVersion = 1;
}
