/*
 *  ======== _logosbiovision.h ========
 */
#ifndef extensions_logosbiovision__LOGOSBIOVISION_H_
#define extensions_logosbiovision__LOGOSBIOVISION_H_

#include <ti/sdo/ce/visa.h>
#include <logosbiovision.h>

#ifdef __cplusplus
extern "C" {
#endif

#define _LOGOSBIOVISION_CIMAGECONVERTER		0
#define _LOGOSBIOVISION_CLOGOSJPEGENCODE		1
#define _LOGOSBIOVISION_CPROCESS			2

/* msgq message to decode */
typedef struct {
	VISA_MsgHeader  visa;
	union {
		struct {
			XDAS_UInt8				*inBuf;
			LOGOSBIOVISION_ImageConverterArgs	inArgs;
		} ceImageConverter;
		struct {
			XDAS_UInt8				*inBuf;
			XDAS_UInt8				*outBuf;
			LOGOSBIOVISION_logosJpegEncodeInArgs	inArgs;
			LOGOSBIOVISION_logosJpegEncodeOutArgs	outArgs;
		} logosJpegEncode;
		struct {
			XDAS_UInt8				*yuyvBuf;
			LOGOSBIOVISION_processInArgs		InArgs;
		} process;
	} cmd;
} _LOGOSBIOVISION_Msg;

#ifdef __cplusplus
}
#endif

#endif
