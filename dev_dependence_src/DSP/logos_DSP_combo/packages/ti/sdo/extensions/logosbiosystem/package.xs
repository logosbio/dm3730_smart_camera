/*
 *  ======== package.xs ========
 */
function getLibs(prog)
{

    /* "mangle" program build attrs into an appropriate directory name */
    //var name = this.profile == "debug" ? "lib/logosbiovision_debug" : "lib/logosbiovision";
    var name = this.profile == "debug" ? "lib/debug/logosbiovision" :
                                         "lib/release/logosbiovision";
                                         
    /* return the library name: name.a<arch> */
    var lib = name + ".a" + prog.build.target.suffix;
    //var lib = name + ".a" + suffix;
    print("    will link with " + this.$name + ":" + lib);

    return (lib);
}
