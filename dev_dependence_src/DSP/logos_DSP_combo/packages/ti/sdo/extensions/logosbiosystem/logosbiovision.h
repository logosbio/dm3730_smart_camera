/**
 *  @file       ti/sdo/extensions/logosbiosystem/logosbiovision.h
 */
#ifndef extensions_logosbiosystem_LOGOSBIOVISION_H_
#define extensions_logosbiosystem_LOGOSBIOVISION_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <ti/sdo/codecs/logosbiovision/ilogosbiovision.h>

#include <ti/sdo/ce/Engine.h>
#include <ti/sdo/ce/visa.h>
#include <ti/sdo/ce/skel.h>
#include <ti/xdais/xdas.h>

#define LOGOSBIOVISION_EOK		ILOGOSBIOVISION_EOK
#define LOGOSBIOVISION_EFAIL		ILOGOSBIOVISION_EFAIL
#define LOGOSBIOVISION_ERUNTIME		ILOGOSBIOVISION_ERUNTIME

#define HISTOGRAM_LENGTH	256

// @brief	Opaque handle to a LOGOSBIOVISION codec.
typedef VISA_Handle LOGOSBIOVISION_Handle;


//@brief	This structure defines the parameters necessary to create an
typedef struct ILOGOSBIOVISION_InitParams LOGOSBIOVISION_InitParams;

extern LOGOSBIOVISION_InitParams LOGOSBIOVISION_PARAMS;   /**< Default instance parameters. */

// the structures for methods
typedef ILOGOSBIOVISION_ImageConverterArgs		LOGOSBIOVISION_ImageConverterArgs;
typedef ILOGOSBIOVISION_logosJpegEncodeInArgs		LOGOSBIOVISION_logosJpegEncodeInArgs;
typedef ILOGOSBIOVISION_logosJpegEncodeOutArgs		LOGOSBIOVISION_logosJpegEncodeOutArgs;
typedef ILOGOSBIOVISION_processInArgs			LOGOSBIOVISION_processInArgs;

// @cond INTERNAL
extern SKEL_Fxns LOGOSBIOVISION_SKEL;

extern ILOGOSBIOVISION_Fxns LOGOSBIOVISION_STUBS;

extern LOGOSBIOVISION_Handle LOGOSBIOVISION_create(Engine_Handle e, String name,
    LOGOSBIOVISION_InitParams *params);

//////////////////////////////////////////////////////////////////////////
// interface funcs
extern XDAS_Int32 LOGOSBIOVISION_ceImageConverter(LOGOSBIOVISION_Handle handle,
							XDAS_UInt8 *inBuf,
							LOGOSBIOVISION_ImageConverterArgs *inArgs);

extern XDAS_Int32 LOGOSBIOVISION_logosJpegEncode(LOGOSBIOVISION_Handle handle,
							XDAS_UInt8 *inBuf,
							XDAS_UInt8 *outBuf,
							LOGOSBIOVISION_logosJpegEncodeInArgs *inArgs,
							LOGOSBIOVISION_logosJpegEncodeOutArgs *outArgs);

extern XDAS_Int32 LOGOSBIOVISION_process(LOGOSBIOVISION_Handle handle,
							XDAS_UInt8 *yuyvBuf,
							LOGOSBIOVISION_processInArgs *InArgs);

extern Void LOGOSBIOVISION_delete(LOGOSBIOVISION_Handle handle);

//////////////////////////////////////////////////////////////////////////
#ifdef __cplusplus
}
#endif

/*@}*/  /* ingroup */

#endif
