/*
 *  ======== logosbiovision_vs.h ========
 */
#ifndef codecs_logosbiovision_LOGOSBIOVISION_VS_H_
#define codecs_logosbiovision_LOGOSBIOVISION_VS_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 *  ======== LOGOSBIOVISION_VS_LOGOSBIOVISION ========
 *  Our implementation of the LOGOSBIOVISION interface
 */
extern ILOGOSBIOVISION_Fxns LOGOSBIOVISION_VS_LOGOSBIOVISION;

#ifdef __cplusplus
}
#endif

#endif
