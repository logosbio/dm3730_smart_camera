/*
 *  Copyright 2009.07
 *  Logosbiosystems Co. By rovhr
 *
 *  All rights reserved.  Property of Logosbiosystems Co. By rovhr
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */



/*
 *  ======== ilogosbiovision.h ========
 *  The logosbiovision interface.
 */

#ifndef codecs_logosbiovision_ILOGOSBIOVISION_H_
#define codecs_logosbiovision_ILOGOSBIOVISION_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <ti/xdais/xdas.h>
#include <ti/xdais/ialg.h>


// Errors while  logosbiovision codecs is opeating
#define ILOGOSBIOVISION_EOK						0
#define ILOGOSBIOVISION_EFAIL						-1
#define ILOGOSBIOVISION_ERUNTIME					-2

typedef IALG_Cmd ILOGOSBIOVISION_Cmd;

#define ILOGOSBIOVISION_IMAGECONVERTER_EOK		 		0
#define ILOGOSBIOVISION_MAKEPATTREN_EOK		 			0
#define ILOGOSBIOVISION_PROCESS_EOK		 			0


//////////////////////////////////////////////////////////////////////////
// This must be the first field of all ILOGOSBIOVISION instance objects
typedef struct ILOGOSBIOVISION_Obj {
	struct ILOGOSBIOVISION_Fxns *fxns;
} ILOGOSBIOVISION_Obj;

//////////////////////////////////////////////////////////////////////////
// Opaque handle to an ILOGOSBIOVISION object.
typedef struct ILOGOSBIOVISION_Obj *ILOGOSBIOVISION_Handle;

//////////////////////////////////////////////////////////////////////////
// the structure for inializing the stroage of the input image infomation
// and template image information
typedef struct ILOGOSBIOVISION_InitParams {
	XDAS_Int32		size;				// This variable must be specified in first column
	XDAS_Int32		reserved1;
	XDAS_Int32		reserved2;
	XDAS_Int32		reserved3;
	XDAS_Int32		reserved4;
} ILOGOSBIOVISION_InitParams;

// the structure for the input image
typedef struct ILOGOSBIOVISION_ImageConverterArgs {
	XDAS_Int32	gray_size;
	XDAS_Int32	gray_width;
	XDAS_Int32	gray_height;
	XDAS_Int32	yuyvBufSize;
	XDAS_Int32	width_Image;
	XDAS_Int32	height_Image;
	XDAS_UInt32	phy_adds_buf;
	XDAS_Int32	size_scale_yuyv;
} ILOGOSBIOVISION_ImageConverterArgs;

// the structure for the make pattern
typedef struct ILOGOSBIOVISION_logosJpegEncodeInArgs {
	XDAS_Int32 sizeOfinputImg;
	XDAS_Int32 left;
	XDAS_Int32 right;
	XDAS_Int32 top;
	XDAS_Int32 bottom;
} ILOGOSBIOVISION_logosJpegEncodeInArgs;

// the structure for the make pattern
typedef struct ILOGOSBIOVISION_logosJpegEncodeOutArgs {
	XDAS_Int32 sizeOfoutputImg;
	XDAS_Int32 widthOfoutputImg;
	XDAS_Int32 heightOfoutputImg;
} ILOGOSBIOVISION_logosJpegEncodeOutArgs;

// the structure for the process
typedef struct ILOGOSBIOVISION_processInArgs {
	XDAS_Int32	gray_size;
	XDAS_Int32	gray_width;
	XDAS_Int32	gray_height;
	XDAS_Int32	yuyvBufSize;
	XDAS_Int32	width_Image;
	XDAS_Int32	height_Image;
	XDAS_UInt32	phy_adds_buf
} ILOGOSBIOVISION_processInArgs;

//////////////////////////////////////////////////////////////////////////
typedef struct ILOGOSBIOVISION_Fxns{
	IALG_Fxns   ialg;
	XDAS_Int32 (*ceImageConverter)(ILOGOSBIOVISION_Handle handle,
		XDAS_UInt8 *inBuf, ILOGOSBIOVISION_ImageConverterArgs *inArgs);
	XDAS_Int32 (*logosJpegEncode)(ILOGOSBIOVISION_Handle handle, XDAS_UInt8 *inBuf,
		XDAS_UInt8 *outBuf, ILOGOSBIOVISION_logosJpegEncodeInArgs *inArgs, ILOGOSBIOVISION_logosJpegEncodeOutArgs *outArgs);
	XDAS_Int32 (*process)(ILOGOSBIOVISION_Handle handle,
		XDAS_UInt8 *yuyvBuf, ILOGOSBIOVISION_processInArgs *InArgs);
} ILOGOSBIOVISION_Fxns;

#ifdef __cplusplus
}
#endif

/*@}*/  /* ingroup */

#endif
