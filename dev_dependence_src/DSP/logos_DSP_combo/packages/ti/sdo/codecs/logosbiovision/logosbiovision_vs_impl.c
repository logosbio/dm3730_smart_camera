/*
 *  Copyright 2009.07
 *  Logosbiosystems Co. By rovhr
 *
 *  All rights reserved.  Property of Logosbiosystems Co. By rovhr
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */
/*
 *  ======== logosbiovision.c ========
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */
#include <xdc/std.h>
#include <ctype.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <ti/sdo/ce/trace/gt.h>

#include "ilogosbiovision.h"
#include "logosbiovision_vs.h"
#include "logosbiovision_vs_priv.h"


XDAS_Int32                  frameSize;


extern IALG_Fxns LOGOSBIOVISION_VS_IALG;


#define IALGFXNS  \
	&LOGOSBIOVISION_VS_IALG,	/* module ID					*/	\
	NULL,				/* activate					*/	\
	LOGOSBIOVISION_VS_alloc,	/* alloc					*/	\
	NULL,				/* control (NULL => no control ops)		*/	\
	NULL,				/* deactivate					*/	\
	LOGOSBIOVISION_VS_free,		/* free						*/	\
	LOGOSBIOVISION_VS_initObj,	/* init						*/	\
	NULL,				/* moved					*/	\
	NULL				/* numAlloc (NULL => IALG_MAXMEMRECS)		*/

//////////////////////////////////////////////////////////////////////////
// ======== LOGOSBIOVISION_VS_ILOGOSBIOVISION ========
// This structure defines VS's plementation of the ILOGOSBIOVISION interface
// for the LOGOSBIOVISION_VS module.
ILOGOSBIOVISION_Fxns LOGOSBIOVISION_VS_LOGOSBIOVISION = {
	{IALGFXNS},
	LOGOSBIOVISION_VS_ceImageConverter,
	LOGOSBIOVISION_VS_logosJpegEncode,
	LOGOSBIOVISION_VS_process,
};
//////////////////////////////////////////////////////////////////////////
// ======== LOGOSBIOVISION_VS_IALG ========
// This structure defines VS's implementation of the IALG interface
// for the LOGOSBIOVISIONCOPY_VS module.
#ifdef _VS_

asm("_LOGOSBIOVISION_VS_IALG .set _LOGOSBIOVISION_VS_LOGOSBIOVISION");

#else

//////////////////////////////////////////////////////////////////////////
// We duplicate the structure here to allow this code to be compiled and
// run non-DSP platforms at the expense of unnecessary data space
// consumed by the definition below.
IALG_Fxns LOGOSBIOVISION_VS_IALG = {	  // module_vendor_interface
	IALGFXNS
};

#endif

ILOGOSBIOVISION_InitParams ILOGOSBIOVISION_PARAMS = {   // default logosbiovision factor
	sizeof(ILOGOSBIOVISION_InitParams),
	0,
	0,
	0,
	0
};

#define GTNAME "codecs.logosbiovision"
static GT_Mask curTrace = {NULL,NULL};

#define PREV_PROC_WIDTH 640
#define PREV_PROC_HEIGHT 480
//#define PREV_PROC_STRIDE (PREV_PROC_WIDTH * 2)
#define PREV_PROC_STRIDE (PREV_PROC_WIDTH << 1)
#define PREV_PROC_SIZE (PREV_PROC_STRIDE * PREV_PROC_HEIGHT)
unsigned char* canvas;

XDAS_UInt8 *frameData ;
XDAS_UInt8 *yuyv_frameData;
//XDAS_UInt8 *frameTrackResult;
//XDAS_UInt8 *frameDetectResult;


//#pragma CODE_SECTION (yuyv2gray, 	".textint:yuyv2gray")
void yuyv2gray(XDAS_Int32 width, XDAS_Int32 height,XDAS_UInt8* src, XDAS_UInt8* dest)
{
	XDAS_Int32 NumPixels = width * height;
	XDAS_Int32 i = (NumPixels << 1) - 1;
	XDAS_Int32 j = NumPixels - 1;
	XDAS_Int32 y0, y1;

	while (i > 0)
	{
			y1 = (XDAS_UInt8)src[i--];
			y0 = (XDAS_UInt8)src[--i];
			dest[j--] = y1;
			dest[j--] = y0;
			i = i - 2;
	}
}


//////////////////////////////////////////////////////////////////////////
// ======== SPHDECCOPY_VS_alloc ========

Int LOGOSBIOVISION_VS_alloc(	const IALG_Params *algParams, IALG_Fxns **fxns,
					IALG_MemRec memTab[]					)
{
	static Bool curInit = FALSE;
	if (curInit != TRUE) {
		curInit = TRUE;
		GT_create(&curTrace, GTNAME);
	}

	GT_3trace(curTrace, GT_ENTER, "LOGOSBIOVISION_VS_alloc(0x%lx, 0x%lx, 0x%lx)\n",algParams, fxns, memTab);

	/* Allocate space for the extension */
	memTab[0].size = sizeof(LOGOSBIOVISION_VS_Obj);
	memTab[0].alignment = 4;
	memTab[0].space = IALG_EXTERNAL;
	memTab[0].attrs = IALG_PERSIST;

   return (LOGOSBIOVISION_MEMRECS);
}


//////////////////////////////////////////////////////////////////////////
// ======== LOGOSBIOVISION_VS_free ========
Int LOGOSBIOVISION_VS_free(IALG_Handle handle, IALG_MemRec memTab[])
{

	GT_2trace(curTrace, GT_ENTER, "LOGOSBIOVISION_VS_free(0x%lx, 0x%lx)\n",
		handle, memTab);

	LOGOSBIOVISION_VS_alloc(NULL, NULL, memTab);

	return (LOGOSBIOVISION_MEMRECS);
}


//////////////////////////////////////////////////////////////////////////
// ======== LOGOSBIOVISION_VS_initObj ========
Int LOGOSBIOVISION_VS_initObj(	IALG_Handle handle,
				const IALG_MemRec memTab[], IALG_Handle p,
				const IALG_Params *algParams)
{
	ILOGOSBIOVISION_InitParams *params = (ILOGOSBIOVISION_InitParams *)algParams;
	LOGOSBIOVISION_VS_Obj *inst = (LOGOSBIOVISION_VS_Obj *)handle;

	if(	params == NULL){
		params = &ILOGOSBIOVISION_PARAMS;
	}

	GT_4trace(curTrace, GT_ENTER, "LOGOSBIOVISION_VS_initObj(0x%lx, 0x%lx, 0x%lx, "
		"0x%lx)\n", handle, memTab, p, params);
	// Initializing the LOGOSBIOVISION_VS_Obj structure
	inst->scaleFactor = 1; /* default scale is 1 */

	return (IALG_EOK);
}


/******************************************************************************
[ interface funcs.. / logosbiovision_vs_impl.c ]
******************************************************************************/
XDAS_Int32 LOGOSBIOVISION_VS_ceImageConverter(ILOGOSBIOVISION_Handle handle,
						 XDAS_UInt8 *inBuf,
						 ILOGOSBIOVISION_ImageConverterArgs *inArgs	)
{
	XDAS_Int32 retVal=ILOGOSBIOVISION_IMAGECONVERTER_EOK;

	XDAS_Int32 width;
	XDAS_Int32 height;
	int size_scale;
	
	width  = inArgs->gray_width;
	height = inArgs->gray_height;
	size_scale = inArgs->size_scale_yuyv;
	GT_3trace(curTrace, GT_ENTER, "LOGOSBIOVISION_VS_ceImageConverter(0x%lx, 0x%lx, 0x%lx \n"
	          , handle, inBuf, inArgs);

	yuyv2gray(width, height, inBuf, frameData);

	GT_0trace(curTrace, GT_ENTER, "Init Gaussian!...\n");

	return retVal;
}

XDAS_Int32 LOGOSBIOVISION_VS_logosJpegEncode(ILOGOSBIOVISION_Handle handle,
						XDAS_UInt8 *inBuf,
						XDAS_UInt8 *outBuf,
						ILOGOSBIOVISION_logosJpegEncodeInArgs *inArgs,
						ILOGOSBIOVISION_logosJpegEncodeOutArgs *outArgs )
{
	XDAS_Int32 retVal = ILOGOSBIOVISION_IMAGECONVERTER_EOK;
	LOGOSBIOVISION_VS_Obj *inst = (LOGOSBIOVISION_VS_Obj *)handle;

	GT_5trace(curTrace, GT_ENTER, "LOGOSBIOVISION_VS_logosJpegEncode(0x%lx, 0x%lx, 0x%lx, 0x%lx , 0x%lx\n"
	          , handle, inBuf, outBuf, inArgs, outArgs);

	return retVal;
}


XDAS_Int32 LOGOSBIOVISION_VS_process(ILOGOSBIOVISION_Handle handle,
					 XDAS_UInt8 *yuyvBuf,
					 ILOGOSBIOVISION_processInArgs *InArgs)
{

	XDAS_Int32 retVal = ILOGOSBIOVISION_EOK;

	frameSize 	= InArgs->gray_size;

	yuyv_frameData 	= yuyvBuf;

	frameData 	= (XDAS_UInt8*)malloc(frameSize * sizeof(XDAS_UInt8));
	canvas	= (XDAS_UInt8*)malloc(InArgs->yuyvBufSize  * sizeof(XDAS_UInt8));

	return retVal;
	//////////////////////////////////////////////////////////////////////////
}


/******************************************************************************
[ internal core funcs.. / logosbiovision_vs_impl.c ]
******************************************************************************/

void CreateGlobalData()
{
	;
}


void Image2Array(XDAS_Int8 *in_image_data, XDAS_Int32 width, XDAS_Int32 height, XDAS_Int32 stride, XDAS_UInt8* out_array)
{
	XDAS_Int32 y;
	XDAS_Int32 x;

	for (y = 0; y < height; y++)
	{
		for (x = 0; x < width; x++)
		{
			*(out_array + y * width + x) = *(in_image_data + y * stride + x);
		}
	}
}


void Array2Image(XDAS_Int8 *out_image_data, XDAS_Int32 width, XDAS_Int32 height, XDAS_Int32 stride, XDAS_UInt8* in_array)
{
	XDAS_Int32 y;
	XDAS_Int32 x;

	for (y = 0; y != height; y++)
	{
		for (x = 0; x != width; x++)
		{
			*(out_image_data + y * stride + x) = *(in_array + y * width + x);
		}
	}
}
