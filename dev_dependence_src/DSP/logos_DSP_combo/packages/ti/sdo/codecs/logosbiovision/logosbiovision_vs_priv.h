/*
 *  Copyright 2009.07
 *  Logosbiosystems Co. By rovhr
 *
 *  All rights reserved.  Property of Logosbiosystems Co. By rovhr
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */
 
/*
 *  ======== logosbiovision_vs_priv.h ========
 */

#ifndef LOGOSBIOVISION_VS_PRIV_
#define LOGOSBIOVISION_VS_PRIV_

#include <ti/xdais/ialg.h>

/* Input Image Size */

#define LOGOSBIOVISION_MEMRECS		 1		// Number of MEMRECS requested by adapter 

//////////////////////////////////////////////////////////////////////////
// logosbiovision process object information
typedef struct LOGOSBIOVISION_VS_Obj {
	IALG_Obj		alg;			// MUST be first field of all XDAS algs 
	XDAS_Int32  scaleFactor;
}LOGOSBIOVISION_VS_Obj;

//////////////////////////////////////////////////////////////////////////
//	local structure.. 

/* End define data structure */
//////////////////////////////////////////////////////////////////////////


/******************************************************************************
[ codecs base funcs.. / logosbiovision_vs_priv.h ]
******************************************************************************/
extern Int LOGOSBIOVISION_VS_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
	IALG_MemRec memTab[]); 

extern Int LOGOSBIOVISION_VS_free(IALG_Handle handle, IALG_MemRec memTab[]);

extern Int LOGOSBIOVISION_VS_initObj(IALG_Handle handle,
	const IALG_MemRec memTab[], IALG_Handle parent,
	const IALG_Params *algParams);


/******************************************************************************
[ interface funcs.. / logosbiovision_vs_priv.h ]
******************************************************************************/
extern XDAS_Int32 LOGOSBIOVISION_VS_ceImageConverter(ILOGOSBIOVISION_Handle handle,
							 XDAS_UInt8 *inBuf,
							 ILOGOSBIOVISION_ImageConverterArgs *inArgs);

extern XDAS_Int32 LOGOSBIOVISION_VS_logosJpegEncode(ILOGOSBIOVISION_Handle handle, 
							XDAS_UInt8 *inBuf,
							XDAS_UInt8 *outBuf,
							ILOGOSBIOVISION_logosJpegEncodeInArgs *inArgs, 
							ILOGOSBIOVISION_logosJpegEncodeOutArgs *outArgs);

extern XDAS_Int32 LOGOSBIOVISION_VS_process(ILOGOSBIOVISION_Handle handle, 
							XDAS_UInt8 *yuyvBuf,
							ILOGOSBIOVISION_processInArgs *InArgs);


/******************************************************************************
[ internal core funcs .. / logosbiovision_vs_priv.h ]
******************************************************************************/

void ReleaseFrame();
void ProcessReset();
void CreateGlobalData();

#endif
