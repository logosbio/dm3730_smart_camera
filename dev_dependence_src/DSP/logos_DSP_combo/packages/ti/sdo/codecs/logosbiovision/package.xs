/*
 *  Copyright 2009.07
 *  Logosbiosystems Co. By rovhr
 *
 *  All rights reserved.  Property of Logosbiosystems Co. By rovhr
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */

/*
 *  ======== package.xs ========
 *
 */
function getLibs(prog)
{
    /* "mangle" program build attrs into an appropriate directory name */
    var name = "lib/logosbiovision_vs.a" + prog.build.target.suffix;

    /* return the library name: name.a<arch> */
    print("    will link with " + this.$name + ":" + name);

    return (name);
}
/*
 *  @(#) codec_engine_1_02 1,0,0,147 7-14-2006 ce-d14
*/

