requires ti.sdo.codecs.logosbiovision;
requires ti.sdo.extensions.logosbiosystem;

/*!
 *  ======== vision ========
 *  Simple Codec Engine "logosbiovision"  [1, 0, 1]
 */

package ti.sdo.codecs.logosbiovision [1, 0, 1]{
    module LOGOSBIOVISION_VS;
}
