/*****************************************************************************
****************************************************************************
*
* NAME:        VPP_DataCpy.h
*
* DESCRIPTION: Wrapper declaration for  QDMA and memcpy functions
*
* AUTHORS:     Patil  
*
* CREATED:     02 Sep 2006
*
* HISTORY:
* ---------------------------------------------------------------------------
* STR   REV     DATE         AUTHOR        DESCRIPTION
* ---------------------------------------------------------------------------
*
* ---------------------------------------------------------------------------
*
****************************************************************************/

#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

enum {DATA_XFRID_WAITNONE};

/* keep it for this Relese*/    
#define MAX_CHANNELS   4

#define VPPD_DATA_1D2D 0
#define VPPD_DATA_2D1D 1
#define VPPD_DATA_2D2D 2

typedef enum dmaID {
    DATA_1D1D=0,
    DATA_2D1D,
    DATA_1D2D,
    DATA_2D2D,
    DATA_LINK
} dmaID;


uint32_t  VPP_DataCpy2d(uint32_t  type, void *src, void *dst, uint16_t lineLen, 
                uint16_t lineCnt, uint16_t linePitch);
void VPP_DataWait(uint32_t id_PingIn);
uint32_t  VPP_DataCpy(void *src, void *dst, uint32_t numOfBytes);
uint32_t VPP_DataOpen(int32_t chaNum,int32_t priority,uint32_t flags);
void VPP_DataClose();

uint16_t VPP_LinkCpy( void     *restrict        src[],
                      void     *restrict        dst[],
                      int32_t                   num_bytes[],
                      int32_t                   num_transfers	);


