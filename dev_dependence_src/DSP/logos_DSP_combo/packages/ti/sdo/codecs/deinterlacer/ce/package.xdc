/*requires ti.sdo.ce.IUniversal;*/
requires ti.sdo.codecs.deinterlacer;

/*!
 *  ======== package.xdc ========
 *  Provides IUniversal interface adapter for ti.sdo.codecs.deinterlacer codec.
 */

package ti.sdo.codecs.deinterlacer.ce [1, 0, 0] {
    module I2P;
}
