/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's DM         *
 *  class of SOCs.                                                             *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  bios_rts.h
 *  @brief  This file contains Non-Bios version of MEM, SYS, HWI, and
 *          DBC implementation
 *
 *  @rev  1.0
 *******************************************************************************
 */

#ifndef _BIOS_RTS_
#define _BIOS_RTS_

#ifdef _cplusplus
extern "C"
{
#endif /* _cplusplus */
/*******************************************************************************
*                             Link Control Pragmas
*******************************************************************************/
/* None */

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <stdlib.h>

/*-------------------------program files -------------------------------------*/
/* None */

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/**
 * Allocate memory with particular value
 */
#define MEM_alloc(segid, size, align)  MEM_valloc(segid, size, align, 0)
#define MEM_calloc(segid, size, align) MEM_valloc(segid, size, align, 0)

#if DBC_ASSERTS
  #define DBC_assert(c) if (!(c)) {                                         \
          SYS_abort("Assertion Failed: file=%s, line=%d.\n", __FILE__,      \
          __LINE__); }

  #define DBC_require	DBC_assert
  #define DBC_ensure 	DBC_assert
#else
  #define DBC_assert(c)
  #define DBC_require(c)
  #define DBC_ensure(c)
#endif  /* DBC_ASSERTS */

/*--------------------------Data declarations --------------------------------*/

/*--------------------------Enumerated Types  --------------------------------*/
/* None */

/*------------------------- Strutures ----------------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/

#if DBC_ASSERTS
  /**
   * System abort function prototype
   */
  extern void SYS_abort(char *s, ...);
#endif

/**
 * API for allocating memory with particular value
 */
extern Void * MEM_valloc(Int segid, size_t size, size_t align, Char val);

/**
 * API for freeing memory 
 */
extern Bool MEM_free(Int segid, Void * buf, size_t size);

/**
 * API for task enabling 
 */
extern Void TSK_enable(Void);

/**
 * API for task disabling 
 */
extern Void TSK_disable(Void);

#ifdef _cplusplus
}
#endif/* __cplusplus */

#endif /* _BIOS_RTS_ */

/* bios_rts.h - EOF */

