/*!
 *  ======== I2P ========
 *  I2P codec specification
 *
 *  This file specifies information necessary to integrate with the Codec
 *  Engine.
 *
 *  By inheriting ti.sdo.ce.universal.IUNIVERSAL, I2P declares that it "is
 *  a" IUNIVERSAL algorithm.  This allows the codec engine to automatically
 *  supply simple stubs and skeletons for transparent execution of DSP
 *  codecs by the GPP.
 *
 *  In addition to declaring the type of the I2P algorithm, we
 *  declare the external symbol required by xDAIS that identifies the
 *  algorithms implementation functions.
 */
metaonly module I2P inherits ti.sdo.ce.universal.IUNIVERSAL
{
    readonly config ti.sdo.codecs.deinterlacer.I2P.Module alg =
        ti.sdo.codecs.deinterlacer.I2P;
    
    override readonly config String ialgFxns = "I2P_TI_II2P";
    
    override readonly config String idma3Fxns = "I2P_TI_IDMA3";

}
