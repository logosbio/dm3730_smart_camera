/*
 *  ======== package.xs ========
 *
 */
function getLibs(prog)
{
    var libList = null;

    if (prog.build.target.isa == '64P') {
        libList = "lib/i2p_ti.l64p";
	print("    will link with " + this.$name + ":" + libList);
    }

    return (libList);
}
