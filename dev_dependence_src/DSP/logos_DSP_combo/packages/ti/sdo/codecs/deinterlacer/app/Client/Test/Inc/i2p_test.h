/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's DM         *
 *  class of SOCs.                                                             *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  i2p_test.h
 *  @brief  This local header file contains structure definitions, function
 *          Headers for the Deinterlacer component
 *
 *  @rev  1.0
 *******************************************************************************
 */

#ifndef _I2P_TEST_H
#define _I2P_TEST_H

#ifdef _cplusplus
extern "C"
{
#endif /* _cplusplus */

/*******************************************************************************
*                             Link Control pragmas
*******************************************************************************/
/* None */

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
/* None */

/*-------------------------program files -------------------------------------*/
/* None */

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/

/* Million Cycles per frame */
#ifndef _MCPF
#define _MCPF
#endif

/**
 *  Default frame size is defined, which is a PAL settings
 *  LINE_SZ           - 720
 *  NUM_LINES         - 576
 *  MAX_FRAME_SIZE    - LINE_SZ*NUM_LINES*2
*/

#define LINE_SZ         (720)
#define NUM_LINES       (576)
#define MAX_FRAME_SIZE  (LINE_SZ*NUM_LINES*2)

/**
 *  NOOFPRMS - Number of input parameters from config file */
#define NOOFPRMS        (4)
#define PARAMSBUFSIZE   (2000)

/**
 * Number of algorithms applying to frame (NUMALGS)
 * Number of input buffers    - 2
 * Number of Output buffers   - 1
 * Bits per pixel is BPP      - 2
 * Size of an arry for storeing name of the file - 1024
 */
#define NUMALGS         (1)
#define NUM_INPUT_BUFS  (2)
#define NUM_OUTPUT_BUFS (1)
#define BPP             (2)
#define FILENAMESIZE    (1024)

/*--------------------------Data declarations --------------------------------*/

/*--------------------------Enumerated Types  --------------------------------*/
/* None */

/*------------------------- function prototypes ------------------------------*/
/* None */

/*------------------------- Strutures ----------------------------------------*/
/* None */

#ifdef _cplusplus
}
#endif/* __cplusplus */

#endif /* _I2P_TEST_H */

/* i2p_test.h - EOF */
