/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's            *
 *  DM class of SOCs.                                                          *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *  @file  alg_malloc.c
 *  @brief  This module implements an algorithm memory management "policy" in
 *          which no memory is shared among algorithm objects.  Memory is, 
 *          however reclaimed when objects are deleted.
 *
 *  @desc   This module implements an algorithm memory management "policy" in 
 *          which no memory is shared among algorithm objects. Memory is, 
 *          however reclaimed when objects are deleted.
 *
 *          preemption      sharing             object deletion
 *          ----------      -------             ---------------
 *          yes(*)          none                yes
 *
 *          Note 1: this module uses run-time support functions malloc() and 
 *          free() to allocate and free memory.  Since these functions are *not*
 *          reentrant, either all object creation and deletion must be performed
 *          by a single thread or reentrant versions or these functions must be 
 *          created.
 *
 *          Note 2: Memory alignment is supported for the c5000 targets with the
 *          memalign5000() function which allocates 'size + align' memory.  This
 *          is necessary since the compiler's run-time library does not
 *          support memalign().  This wastes 'align' extra memory.
 *
 *  @rev  1.0
 *******************************************************************************
 */

/*******************************************************************************
*                             Link Control pragmas
*******************************************************************************/
/*------------------------- pragma definitions -------------------------------*/
#pragma CODE_SECTION(ALG_init,         ".text:init")
#pragma CODE_SECTION(ALG_exit,         ".text:exit")
#pragma CODE_SECTION(_ALG_allocMemory, ".text:create")
#pragma CODE_SECTION(_ALG_freeMemory,  ".text:create")

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <stdlib.h>     /* malloc/free declarations */
#include <string.h>     /* memset declaration */
#include <std.h>
#include <alg.h>
#include <ti/xdais/ialg.h>
/*-------------------------program files -------------------------------------*/
/* None */

/*******************************************************************************
 * EXTERNAL REFERENCES NOTE : only use if not found in header file
*******************************************************************************/

/*------------------------ data declarations ---------------------------------*/
/* None */

/*----------------------- function prototypes --------------------------------*/
/* None */

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/**
 * Architecture specific macros
 */
#define C54x         54
#define C55x         55
#define C28x         28

/* These are coresponding to C54x and C55x family */
#if defined (_C54x_) || (_C55x_) || (_C28x_)
#define myMemalign  mem_align
#define myFree      mem_free
#else
#define myMemalign  memalign
#define myFree      free
#endif

/*------------------------ data declarations ---------------------------------*/
/* None */

/*----------------------- function prototypes --------------------------------*/
/* These are coresponding to C54x and C55x family */
#if defined (_C54x_) || (_C55x_) || (_C28x_)
void *mem_align(size_t alignment, size_t size);
void  mem_free(void *ptr);
#endif

Bool _ALG_allocMemory(IALG_MemRec memTab[], Int n);
Void _ALG_freeMemory(IALG_MemRec memTab[], Int n);

/*******************************************************************************
 * PRIVATE DECLARATIONS Defined here, used only here
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/* None */

/*-------------------------data declarations ---------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/
/* None */



/**
********************************************************************************
 *  @func   ALG_activate
 *  @brief  This function activates algorithm by initializing memory.
 *
 *  @param[in ]    alg    : pointer to ALG_Handle
 *
 *  @returns NULL
********************************************************************************
*/
Void ALG_activate(ALG_Handle alg)
{
  /* restore all persistant shared memory */
    ;   /* nothing to do since memory allocation never shares any data */

  /* do app specific initialization of scratch memory */
  if (alg->fxns->algActivate != NULL) {
    alg->fxns->algActivate(alg);
  }
}



/**
********************************************************************************
 *  @func   ALG_deactivate
 *  @brief  This function deactivates algorithm.
 *
 *  @param[in ]    alg     : pointer to ALG_Handle
 *
 *  @returns NULL
********************************************************************************
*/
Void ALG_deactivate(ALG_Handle alg)
{
  /* do app specific store of persistent data */
  if (alg->fxns->algDeactivate != NULL) {
    alg->fxns->algDeactivate(alg);
  }

  /* save all persistant shared memory */
    ;   /* nothing to do since memory allocation never shares any data */

}



/**
********************************************************************************
 *  @func   ALG_init
 *  @brief  Entry point for an algorithm
 *
 *  @param[in ] : None
 *
 *  @returns None
********************************************************************************
*/
Void ALG_init(Void)
{
}



/**
********************************************************************************
 *  @func   ALG_exit
 *  @brief  Exit point for an algorithm
 *
 *  @param[in ] : None
 *
 *  @returns None
********************************************************************************
*/
Void ALG_exit(Void)
{
}



/**
********************************************************************************
 *  @func   _ALG_allocMemory
 *  @brief  Allocates memory for alg.
 *
 *  @param[in ]    *memTab      : pointer to memtab
 *                  n           : number of Memtabs
 *
 *  @returns TRUE
********************************************************************************
*/
Bool _ALG_allocMemory(IALG_MemRec memTab[], Int n)
{
  Int i;

  for (i = 0; i < n; i++) {
    memTab[i].base = (void *)myMemalign(memTab[i].alignment, memTab[i].size);

    if (memTab[i].base == NULL) {
      _ALG_freeMemory(memTab, i);
      return (FALSE);
    }
    memset(memTab[i].base, 0, memTab[i].size);
  }
  return (TRUE);
}



/**
********************************************************************************
 *  @func   _ALG_freeMemory
 *  @brief  Free memory allocated for alg.
 *
 *  @param[in ]    *memTab      : pointer to memtab
 *                  n           : number of Memtabs
 *
 *  @returns None
********************************************************************************
*/
Void _ALG_freeMemory(IALG_MemRec memTab[], Int n)
{
  Int i;

  for (i = 0; i < n; i++) {
    if (memTab[i].base != NULL) {
      myFree(memTab[i].base);
    }
  }
}



#if defined (_C54x_) || (_C55x_) || (_C28x_)
/**
********************************************************************************
 *  @func   mem_align
 *  @brief  Align memory for C54x or C55x.
 *
 *  @param[in ]    alignment    : block alignment
 *                 size         : size of block
 *
 *  @returns None
********************************************************************************
*/
void *mem_align(size_t alignment, size_t size)
{
  void     **mallocPtr;
  void     **retPtr;

  /* return if invalid size value */
  if (size <= 0) {
    return (0);
  }

  /*
   * If alignment is not a power of two, return what malloc returns. This is
   * how memalign behaves on the c6x.
   */
  if ((alignment & (alignment - 1)) || (alignment <= 1)) {
    if( (mallocPtr = malloc(size + sizeof(mallocPtr))) != NULL ) {
      *mallocPtr = mallocPtr;
      mallocPtr++;
    }
    return ((void *)mallocPtr);
  }

  /* allocate block of memory */
  if ( !(mallocPtr = malloc(alignment + size)) ) {
    return (0);
  }

  /* Calculate aligned memory address */
  retPtr = (void *)(((Uns)mallocPtr + alignment) & ~(alignment - 1));

  /* Set pointer to be used in the mem_free() fxn */
  retPtr[-1] = mallocPtr;

  /* return aligned memory */
  return ((void *)retPtr);
}



/**
********************************************************************************
 *  @func   mem_free
 *  @brief  To free memory pointed by ptr.
 *
 *  @param[in ]    *ptr      : memory pointer to be freed
 *
 *  @returns None
********************************************************************************
*/
Void mem_free(void *ptr)
{
  free((void *)((void **)ptr)[-1]);
}

#endif

/* alg_malloc.c - EOF */

