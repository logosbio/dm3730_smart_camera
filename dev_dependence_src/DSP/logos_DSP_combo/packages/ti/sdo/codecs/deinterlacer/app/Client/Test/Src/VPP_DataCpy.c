/*****************************************************************************
****************************************************************************
*
* NAME:        VPP_DataCpy.c
*
* DESCRIPTION: Wrapper defination for  QDMA and memcpy functions.
*
* AUTHORS:     Patil  
*
* CREATED:     9 Sep 2006
*
* HISTORY:
* ---------------------------------------------------------------------------
* STR   REV     DATE    AUTHOR  DESCRIPTION
* ---------------------------------------------------------------------------
*
*
* ---------------------------------------------------------------------------
*
****************************************************************************/
#include "VPP_DataCpy.h"


uint32_t  VPP_DataCpy2d(uint32_t  type, void *src, void *dst, uint16_t lineLen, 
                uint16_t lineCnt, uint16_t linePitch)
{

#ifndef _VPP_QDMA
    uint32_t i;
    uint8_t*iPtr = src;
    uint8_t*oPtr = dst;

    if( type == DATA_2D1D ){
        for(i = 0; i < lineCnt; i++){
            memcpy(oPtr, iPtr, lineLen);
            iPtr += linePitch;
            oPtr += lineLen;
        }
    }
    if( type == DATA_1D2D ){
        for(i = 0; i < lineCnt; i++){
            memcpy(oPtr, iPtr, lineLen);
            iPtr += lineLen;
            oPtr += linePitch;
        }
    }
    if( type == DATA_2D2D ){
        for(i = 0; i < lineCnt; i++){
            memcpy(oPtr, iPtr, lineLen);
            iPtr += linePitch;
            oPtr += linePitch;
        }
    }
    return(1);
#else
    uint32_t tx_Channel;

    switch(type){
        case DATA_1D2D:
            /* start the QDMA (1D2D) */
            tx_Channel = VPP_QDMA_copy2d(VPPD_DATA_1D2D,
                                src,
                                dst,
                                lineLen,
                                lineCnt,
                                linePitch
                            );

            break;
        case DATA_2D1D:
            /* start the QDMA (1D2D) */
            tx_Channel = VPP_QDMA_copy2d(VPPD_DATA_2D1D,
                                src,
                                dst,
                                lineLen,
                                lineCnt,
                                linePitch
                            );

            break;  
        case DATA_2D2D:          
            /* start the QDMA (1D2D) */
            tx_Channel = VPP_QDMA_copy2d(VPPD_DATA_2D2D,
                                src,
                                dst,
                                lineLen,
                                lineCnt,
                                linePitch
                            );
        break;
    }
return(tx_Channel);

#endif 


}



uint16_t VPP_LinkCpy( void     *restrict        src[],
					  void     *restrict        dst[],
					  int32_t                   num_bytes[],
					  int32_t                   num_transfers	)

{
    uint32_t tx_Channel;
    /* start the QDMA (memcpy)*/
    tx_Channel =  VPP_QDMA_copyLinked(src,
                                dst,
                                num_bytes,
							    num_transfers
                                );

    return (tx_Channel);
}




uint32_t  VPP_DataCpy(void *src, void *dst, uint32_t numOfBytes)
{
#ifndef _VPP_QDMA
    memcpy(dst, src, numOfBytes);
    return(1);
#else
    uint32_t tx_Channel;
    /* start the QDMA (memcpy)*/
    tx_Channel =  VPP_QDMA_copy(src,
                                dst,
                                numOfBytes
                                );

    return (tx_Channel);
#endif
}


void VPP_DataWait(uint32_t id_PingIn)
{
#ifndef _VPP_QDMA 
    return;
#else
    /* Wait for QDMA to Tx to complete */
    VPP_QDMA_wait(id_PingIn);

#endif
}

uint32_t VPP_DataOpen(int32_t chaNum,int32_t priority,uint32_t flags)
{
#ifdef _VPP_QDMA 
    /* Configure the QDMA */
    VPP_QDMA_config(MAX_CHANNELS);
#endif
    return(1);

}
void VPP_DataClose()
{
#ifdef _VPP_QDMA 
    /* Configure the QDMA */
    VPP_QDMA_close();
#endif
}
/*VPP_DataCpy.c - Nothing beyond this */

