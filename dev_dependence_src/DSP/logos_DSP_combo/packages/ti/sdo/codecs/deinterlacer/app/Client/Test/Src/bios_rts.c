/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's            *
 *  DM class of SOCs.                                                          *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  bios_rts.c
 *  @brief  This file contains Implementation of SYS_abort() and non-BIOS
 *          versions of MEM functions, MEM_valloc() and MEM_free().
 *
 *  @rev  1.0
 *******************************************************************************
 */

/*******************************************************************************
*                             Link Control Pragmas
*******************************************************************************/
/* None */

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <std.h>
#include <stdlib.h>
#include <stdio.h>
/*-------------------------program files -------------------------------------*/
#include "bios_rts.h"

/*******************************************************************************
 * EXTERNAL REFERENCES NOTE : only use if not found in header file
*******************************************************************************/

/*------------------------ data declarations ---------------------------------*/
/* None */

/*----------------------- function prototypes --------------------------------*/
/* None */

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/

#ifdef _RTS_MODE
#define PTRSIZE sizeof(Void *)
#endif

/*------------------------ data declarations ---------------------------------*/
/* None */

/*----------------------- function prototypes --------------------------------*/
/* None */

/*******************************************************************************
 * PRIVATE DECLARATIONS Defined here, used only here
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/* None */

/*-------------------------data declarations ---------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/
/* None */



#ifdef _RTS_MODE
/**
********************************************************************************
 *  @func   MEM_free
 *  @brief  Non-BIOS versions of MEM_free
 *
 *  @param[in ]  size    : Buffer size
 *               alignBuf: Buffer address
 *               segid   : Segment id
 *
 *  @returns TRUE or FALSE
********************************************************************************
*/
Bool MEM_free(Int segid, Void * alignBuf, size_t size)
{
  LgUns *addrPtr;
  Void *buf;
  Uns remainBytes = 0;

  if ((PTRSIZE - 1) & size) {
    remainBytes = PTRSIZE - ((PTRSIZE - 1) & size);
  }

  addrPtr = (LgUns *)((LgUns)alignBuf + size + remainBytes);
  buf = (Void *)*addrPtr;

  free(buf);

  return (TRUE);
}



/**
********************************************************************************
 *  @func   MEM_valloc
 *  @brief  Non-BIOS versions of MEM_valloc, allocate buffer with particular 
 *          value
 *
 *  @param[in ]  size    : Buffer size
 *               align   : Buffer alignment
 *               segid   : Segment id
 *               val     : value need to be filled in the buffer
 *
 *  @returns void pointer
********************************************************************************
*/
Void * MEM_valloc(Int segid, size_t size, size_t align, Char val)
{
  Uns     alignSize;
  Uns     alignBytes;
  Uns     remainBytes;
  Void   *buf;
  Uns     tmpAlign;
  LgUns  *addrPtr;
  Void   *alignBuf;


  /*
   *  Must at least align on 32-bit boundary since we're storing the
   *  32-bit address of the malloc'd buffer.
   */
  alignBytes = (align < PTRSIZE) ? PTRSIZE : align;

  /* alignment must be a power of 2 */
  DBC_require((alignBytes & (alignBytes - 1)) == 0);

  /*
   *  We will allocate a buffer larger than "size", to allow for alignment
   *  and to hold the address of the buffer returned by malloc() at the
   *  end of the buffer.
   *  Make size a multiple of 32-bits so it will be easier to find the
   *  address of the buffer returned by malloc().
   */
  alignSize = size;
  if ((remainBytes = alignSize & (PTRSIZE - 1)) != 0) {
    /* size is not a multiple of 32-bits */
    alignSize += PTRSIZE - remainBytes;
    remainBytes = PTRSIZE - remainBytes;
  }

  /* Add 32-bits to store pointer of allocated buffer */
  alignSize += PTRSIZE;

  alignSize += alignBytes;
  buf = malloc(alignSize);

  tmpAlign = (Uns)buf & (alignBytes - 1);
  if (tmpAlign) {
    alignBuf = (Void *)((Uns)buf + alignBytes - tmpAlign);
  }
  else {
    alignBuf = buf;
  }

  /* Store the malloc'd address for freeing later. */
  addrPtr = (LgUns *)((LgUns)alignBuf + size + remainBytes);
  *addrPtr = (LgUns)buf;

  /* Initialize the aligned buffer with 'val' */
  memset(alignBuf, val, size);

  DBC_ensure(!((LgUns)alignBuf & (alignBytes - 1)));

  return (alignBuf);
}



/**
********************************************************************************
 *  @func   SYS_abort
 *  @brief  SYS_abort function implementaion
 *
 *  @param[in ]  s       : String to be print while system is aborting
 *
 *  @returns NULL
********************************************************************************
*/
void SYS_abort(char *s, ...)
{
  printf("*** %s ***\n", s );
  exit(1);
}



/**
********************************************************************************
 *  @func   TSK_enable
 *  @brief  TSK_enable API implementaion
 *
 *  @param[in ]  None
 *
 *  @returns None
********************************************************************************
*/
Void TSK_enable(Void)
{
}



/**
********************************************************************************
 *  @func   TSK_disable
 *  @brief  TSK_disable API implementaion
 *
 *  @param[in ]  None
 *
 *  @returns None
********************************************************************************
*/
Void TSK_disable(Void)
{
}

#endif

/* bios_rts.c - EOF */










