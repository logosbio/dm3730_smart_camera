/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's DM         *
 *  class of SOCs.                                                             *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  test_main.c
 *  @brief  This file contains the test application for Deinterlacer Source
 *          component which is an TI's implementation.
 *
 *          Deinterlacer algorithm is a component that uses TI's DSP platform
 *          for execution. This file contains methods that tests the
 *          functionality of deinterlacer algorithm
 *
 *  @rev  1.0
 *******************************************************************************
 */

/*******************************************************************************
*                             Link Control Pragmas
*******************************************************************************/
/* Defined in header file */

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <std.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <bcache.h>
#include <dman3.h>
#include <ti/xdais/idma3.h>
#include <ti/xdais/dm/iuniversal.h>

/*-------------------------program files -------------------------------------*/
#include "ii2p_ti.h"
#include "i2p_ti.h"
#include "i2p.h"
#include "i2p_test.h"

/*******************************************************************************
* EXTERNAL REFERENCES NOTE : only use if not found in header file
*******************************************************************************/

/*------------------------ data declarations ---------------------------------*/
/* None */

/*----------------------- function prototypes --------------------------------*/
/* None */

/*******************************************************************************
* PUBLIC DECLARATIONS Defined here, used elsewhere
*******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/**
 * Cache Enable External Memory Space
 * BaseAddr (60000000), length (10000000)
 * Cache 0x60000000 --- 0x6FFFFFF
 */
#define EXT_MEM_BASE (0x60000000)
#define EXT_MEM_SIZE (0x10000000)


/*------------------------ data declarations ---------------------------------*/
#ifdef _MCPF /* Million clock cycles per frame */
  double startTick, endTick, procTick;
#endif

XDAS_Int8              DataIn[MAX_FRAME_SIZE];
XDAS_Int8              DataOut[MAX_FRAME_SIZE];
XDAS_Int8              PrevData[MAX_FRAME_SIZE] = {0};
static XDAS_Int8       prmsBuf[PARAMSBUFSIZE]   = {0};

I2P_Handle             i2pAlg;
I2P_Status             i2pStatus;

extern IUNIVERSAL_Fxns I2P_TI_II2P;

/*----------------------- function prototypes --------------------------------*/
/*! For Profiling on Davinci */
#ifdef _MCPF
  extern void       TSC_enable(void);
  extern long long  TSC_read(void);
#endif

extern XDAS_Int32   frame_apply(void);

extern XDAS_Void    TestApp_EnableCache(void);

/*******************************************************************************
 * PRIVATE DECLARATIONS Defined here, used only here
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/* None */

/*-------------------------data declarations ---------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/
/* None */



/**
********************************************************************************
 *  @func   TestApp_EnableCache
 *  @brief  This function enable the Cache module
 *
 *          This API will enable the settings of Cache
 *
 *  @param[in]  N
 *
 *  @returns None
********************************************************************************
*/
XDAS_Void TestApp_EnableCache(void)
{
  BCACHE_Size size;

  size.l1psize  = BCACHE_L1_32K; /* L1P cache size */

  size.l1dsize  = BCACHE_L1_16K; /* L1D cache size */

  size.l2size   = BCACHE_L2_64K; /* L2  cache size */

  /* Set L1P, L1D and L2 cache sizes */
  BCACHE_setSize(&size);

  /**
   * Cache Enable External Memory Space BaseAddr, length, MAR enable/disable
   * Cache 0x60000000 --- 0x6FFFFFFF
   */
  BCACHE_setMar((Ptr *)EXT_MEM_BASE, EXT_MEM_SIZE, BCACHE_MAR_ENABLE);

  BCACHE_wbInvAll(); /* No need to call BCACHE_wait() */

} /* DoCacheSettings */



/**
********************************************************************************
 * @func   frame_apply
 * @brief  This function do necessary initialization of Universal varibales and
 *         do video deinterlaceing for specified number of frames
 *
 * @param [in] None
 *
 * @return    XDM_EOK or XDM_EFAIL
********************************************************************************
*/
XDAS_Int32 frame_apply(void)
{
  FILE        *fpInput             = NULL;
  FILE        *fpOutput            = NULL;
  FILE        *fpCfg               = NULL;
  FILE        *fpCfgParams         = NULL;
  XDAS_UInt32  nBytes              = NULL;
  XDAS_UInt32  frameCount          = NULL;
  /* Flag for checking ela5 only or vd6 + ela5*/
  Bool         testEla5DirOnly     = FALSE;

  XDAS_UInt32  pitch               = NULL;
  XDAS_UInt32  frameSize           = NULL;
  XDAS_UInt32  paramsFileSize      = NULL;
  XDAS_UInt32  bufferIndex         = NULL;
  XDAS_UInt32  returnStr           = NULL;
  XDAS_Int32   intContent          = NULL;
  XDAS_Int32   inParams[NOOFPRMS]  = {0};
  XDAS_Int8   *paramsBuf           = prmsBuf;
  XDAS_Int8   *paramsFileEnd       = NULL;
  XDAS_Int8    searchChar          = '=';
  unsigned long int totalBytesRead = NULL;

  XDAS_UInt8 inFileName[FILENAMESIZE]      = {0};
  XDAS_UInt8 cfgPrmsFileName[FILENAMESIZE] = {0};
  XDAS_UInt8 outFileName[FILENAMESIZE]     = {0};

  /* universal Input Buffer Descriptors */
  XDM1_BufDesc        universalInBufDesc;
  /* universal Output Buffer Descriptors */
  XDM1_BufDesc        universalOutBufDesc;

  IUNIVERSAL_InArgs   universalInArgs;   /* universal Input arguments */
  IUNIVERSAL_OutArgs  universalOutArgs;  /* universal Output arguments */

  IDEINTER_InArgs     inputArgs; /* Input arguments of Deinterlacer algorithm */
  IDEINTER_OutArgs    outputArgs;/* Outpt arguments of Deinterlacer algorithm */

  /* Initialize YPrevData Buffer */
  memset(PrevData, 0, MAX_FRAME_SIZE);
  
  /******************************Parameters Reading****************************/
  /* 1st field in Testvecs.cfg is config params file frame *
   * 2nd field in Testvecs.cfg is input file name          *
   * 3rd field in Testvecs.cfg is output file name         */
  fpCfg = fopen("../../Test/TestVecs/Config/Testvecs.cfg", "r");
  if (!fpCfg) {
    printf("Couldn't open parameter file %s",fpCfg);
    return XDM_EFAIL;
  }
  fscanf(fpCfg,"%s", cfgPrmsFileName);
  fscanf(fpCfg,"%s", inFileName);
  fscanf(fpCfg,"%s", outFileName);
  fclose(fpCfg);

  fpCfgParams  = fopen((const char *)cfgPrmsFileName,"rb");
  if(fpCfgParams == NULL) {
    fprintf(stdout, "Can not open Config parameters file \n");
    return XDM_EFAIL;
  }
  fseek (fpCfgParams, 0, SEEK_END);
  paramsFileSize = ftell (fpCfgParams);
  fseek (fpCfgParams, 0, SEEK_SET);
  paramsFileSize = fread (prmsBuf, 1, paramsFileSize, fpCfgParams);
  prmsBuf[paramsFileSize] = '\0';
  paramsFileEnd = &prmsBuf[paramsFileSize];
  fclose (fpCfgParams);

  /* 1st field in inParams[bufferIndex] array is frameWidth       *  
   * 2nd field in inParams[bufferIndex] array is frameheight      *
   * 3rd field in inParams[bufferIndex] array is starting frame   *
   * 4th field in inParams[bufferIndex] array is number of frames */
  while(paramsBuf < paramsFileEnd) 
  {
    XDAS_Int8 curChar = *paramsBuf;
    returnStr = (curChar - searchChar);

    if(returnStr) {
      paramsBuf++;
    }
    else {
      paramsBuf++;
      paramsBuf++;
      sscanf ((const char *)paramsBuf, "%d", &intContent) ;	
      paramsBuf++;
      inParams[bufferIndex++]=intContent;
    }
  }
  /******************************E/Parameters Reading**************************/

  pitch     = inParams[0] * BPP;
  frameSize = pitch * inParams[1];
  /* inParams[2] is the startFrame */
  totalBytesRead = frameSize * inParams[2];

  fpInput  = fopen((const char *)inFileName,"rb");
  fpOutput = fopen((const char *)outFileName,"wb");

  if(fpInput == NULL) {
    fprintf(stdout, "Can not open input file \n");
    return XDM_EFAIL;
  }
  if(fpOutput == NULL) {
    fprintf(stdout, "Can not open output file \n");
    fclose(fpInput);
    return XDM_EFAIL;
  }

  /* for going to starting frame */
  fseek(fpInput, totalBytesRead, SEEK_SET);

  universalInArgs.size                  = sizeof(universalInArgs);
  universalOutArgs.size                 = sizeof(universalOutArgs);

  /* Intialize Output Arguments of I2P */
  outputArgs.universalOutArgs           = universalOutArgs;
  outputArgs.universalOutArgs.size      = sizeof(outputArgs);
  outputArgs.done                       = NULL;

  /* Intialize Input Arguments of I2P */
  inputArgs.universalInArgs             = universalInArgs;
  inputArgs.universalInArgs.size        = sizeof(inputArgs);
  inputArgs.inObject.width              = inParams[0];/* width of the frame */
  inputArgs.inObject.height             = inParams[1];/* height of the frame */
  /* II2P_FOUR22_ILE for telling imageFormat is YUV422ILE to DSP*/
  inputArgs.inObject.imageFormat        = II2P_FOUR22_ILE;
  inputArgs.inObject.pitch_cur_frame    = pitch;
  inputArgs.inObject.pitch_prev_fld     = pitch;
  inputArgs.inObject.pitch_out_frame    = pitch;
  inputArgs.inObject.useHistoryData     = TRUE;

  /* Intialize Input and Output Buffer Descriptors */
  universalInBufDesc.numBufs            = NUM_INPUT_BUFS;
  universalOutBufDesc.numBufs           = NUM_OUTPUT_BUFS;

  universalInBufDesc.descs[0].bufSize   = frameSize;
  universalInBufDesc.descs[1].bufSize   = frameSize;
  universalOutBufDesc.descs[0].bufSize  = frameSize;

  /* Input Buffer, Current frame */
  universalInBufDesc.descs[0].buf       = DataIn;
  /* Output Buffer, holds deinterlaced output of current frame */
  universalOutBufDesc.descs[0].buf      = DataOut;

  /*inParams[2] is the starting frame and inParams[3] is the number of frames*/
  for(frameCount = inParams[2]; 
      frameCount<(inParams[2]+(inParams[3])); frameCount++) {
    printf("Reading input Frame\n");
    nBytes = fread( DataIn, 1, frameSize, fpInput);
    printf("nBytes read = %d \n",nBytes);

    if ((frameCount == inParams[2]) || (testEla5DirOnly == TRUE)) {
      inputArgs.inObject.useHistoryData = FALSE;
      universalInBufDesc.descs[1].buf   = NULL;
    }
    else {
      inputArgs.inObject.useHistoryData = TRUE;
      /* Buffer for Previous frame */
      universalInBufDesc.descs[1].buf   = PrevData;
    }

    /* For profiling */
    #ifdef _MCPF
      startTick = TSC_read();
    #endif /*_MCPS*/

    /*------------------------------------------------------------------------*/
    /* Deinterlace the current frame                                          */
    /*------------------------------------------------------------------------*/
    I2P_frameapply((IUNIVERSAL_Handle)i2pAlg, &universalInBufDesc,
                    &universalOutBufDesc, NULL,
                    (IUNIVERSAL_InArgs *)&inputArgs,
                    (IUNIVERSAL_OutArgs *)&outputArgs);

    /* For profiling */
    #ifdef _MCPF
      endTick  = TSC_read();
      procTick = endTick - startTick;
      if(endTick<startTick){
        fprintf(stderr,"Error\n");
      }
      fprintf(stdout, "\nI2P_frameApply : MCPF = %lf\n", procTick/1000000);
    #endif /*_MCPF*/

    /* Store Datain in PrevData */
    memcpy (PrevData, DataIn, frameSize);
    BCACHE_wb(PrevData, frameSize, TRUE);

    fprintf(stdout, "Writting Output Frame\n");
    nBytes = fwrite( DataOut, 1, frameSize, fpOutput);
    fprintf(stdout, "nBytes Written = %d \n",nBytes);
    fprintf(stdout, "frameCount = %d processed \n", (frameCount + 1));
  }/* End of for */
  fclose(fpInput);
  fclose(fpOutput);

  return XDM_EOK;
} /* End of frame_apply */



/**
********************************************************************************
 *  @func     main
 *  @brief    This is the entering point of the code execution
 *
 *  @param[in ]  Number of arguments and arguments
 *
 *  @returns APP_EOK
 *           APP_EFAIL
********************************************************************************
*/
XDAS_Int32 main(int argc, int8_t** argv)
{
  I2P_Params     i2pParams  = II2P_PARAMS;
  XDAS_Int32     cntstatus  = NULL;
  XDAS_Int32     dma_status = NULL;
  XDAS_Int32     status     = NULL;

  /* DMAN3 specific variables */
  XDAS_Int32     groupId, numAlgs;
  IDMA3_Fxns    *dmaFxns[NUMALGS];
  IALG_Handle    alg[NUMALGS];

  /* Initializing DMAN3 parameters                             *
   * 1. Base Index Value from where DMA params are written     *
   * 2. Number of QDMA channels used by the Algorithm          *
   * 3. Number of Algorithms: System Parameter                 *
   * 4. Group Id needed by DMAN3 library: System Parameter     *
   * 5. DMAN3_NUM_CONTIGUOUS_PARAM_ENTRIES                     *
   * 6. DMAN3_NUMPARAM_GROUP                                   */
  DMAN3_PARAMS.paRamBaseIndex         = 78;
  DMAN3_PARAMS.numQdmaChannels        = 4;
  numAlgs                             = NUMALGS;
  groupId                             = 0;
  DMAN3_PARAMS.numPaRamGroup[groupId] = 48;
  DMAN3_PARAMS.numPaRamEntries        = 48;

  /* Cache Config Function */
  TestApp_EnableCache();

#ifdef _MCPF
  /* enable timer */
  TSC_enable();
#endif

  /*--------------------------------------------------------------------------*/
  /* Initialize Interlacer to progressive (I2P) module                        */
  /*--------------------------------------------------------------------------*/
  I2P_TI_init();

  /*--------------------------------------------------------------------------*/
  /* Create the I2P algorithm object (instance)                               */
  /*--------------------------------------------------------------------------*/
  i2pAlg = I2P_create(&I2P_TI_II2P, &i2pParams);
  if ( i2pAlg == NULL) {
    printf("\nI2P_create failed");
    return XDM_EFAIL;
  }

  /*--------------------------------------------------------------------------*/
  /* Initialize DMA manager for XDAIS algorithms                              */
  /* and grant DMA resources                                                  */
  /*--------------------------------------------------------------------------*/

  /* DMA allocation using DMAN3 API's */
  DMAN3_init();

  /* Provide the Algorithm handle to DMAN3 Interface for granting channels */
  alg[0]     = (IALG_Handle)i2pAlg;
  dmaFxns[0] = &I2P_TI_IDMA3;

  dma_status = DMAN3_grantDmaChannels(groupId, alg, dmaFxns, numAlgs);
  if (dma_status != DMAN3_SOK) {
    SYS_abort("Problem adding algorithm's dma resources");
  }


  /*--------------------------------------------------------------------------*/
  /*  The GETSTATUS call gets information of default parameters               */
  /*  Update the I2P Structure with i2pStatus                                 */
  /*--------------------------------------------------------------------------*/
  cntstatus = I2P_control(i2pAlg, I2P_GETSTATUS, &i2pStatus);
  if (cntstatus!=IUNIVERSAL_EOK) {
    printf("control status=%d \n",cntstatus);
    exit(0);
  }

  /*--------------------------------------------------------------------------*/
  /* For testing v6 and ela_5 algorithms */
  /*--------------------------------------------------------------------------*/
  status = frame_apply();

  if(!status) {
    printf("Deinterlaceing of frames done successfully\n");
  }

  /*--------------------------------------------------------------------------*/
  /* Reclaim DMA resources from algorithm and deinitialize the DMA            */
  /* manager and ACPY3 library                                                */
  /*--------------------------------------------------------------------------*/
  if (DMAN3_releaseDmaChannels(alg, dmaFxns, NUMALGS) != DMAN3_SOK) {
    SYS_abort("Problem removing algorithm's dma resources");
  }

  DMAN3_exit();

  I2P_TI_exit();

  return XDM_EOK;
}/* End of main */

/* test_main.c - EOF */
