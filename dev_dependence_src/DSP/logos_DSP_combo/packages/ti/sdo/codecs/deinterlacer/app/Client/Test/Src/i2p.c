/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's            *
 *  DM class of SOCs.                                                          *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/
/**
 *******************************************************************************
 *  @file  I2P.c
 *  @brief Caller-friendly interface for the II2P algorithm
 *
 *         Obtained from the vendor-provided i2p.c where all ALGRF_ prefixes
 *         were replaced with ALGRF_ (as in ALGRF_activate -> ALGRF_activate)
 *         to use the ALGRF module.
 *
 *  Original comments:
 *         I2P Filter Module - implements all functions and defines all constant
 *         structures common to all I2P filter algorithm implementations.
 *  @rev  1.0
 *******************************************************************************
 */

/*******************************************************************************
*                             Link Control pragmas
*******************************************************************************/
/* None */
/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <std.h>
/*-------------------------program files -------------------------------------*/


#include "i2p.h"

/*******************************************************************************
 * EXTERNAL REFERENCES NOTE : only use if not found in header file
*******************************************************************************/

/*------------------------ data declarations ---------------------------------*/
/* None */

/*----------------------- function prototypes --------------------------------*/
/* None */

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/* None */

/*-------------------------data declarations ---------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/
/* None */

/*******************************************************************************
 * PRIVATE DECLARATIONS Defined here, used only here
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/* None */

/*-------------------------data declarations ---------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/
/* None */



/**
********************************************************************************
 *  @func   I2P_frameapply
 *  @brief  Apply a I2P Algorithm to the input array and place results in the
 *          output array
 *
 *  @param[in ]   alg       : pointer to alg
 *
 *  @returns None
********************************************************************************
*/
XDAS_Void I2P_frameapply(IUNIVERSAL_Handle handle, XDM1_BufDesc *inBufs,
                        XDM1_BufDesc *outBufs, XDM1_BufDesc *inOutBufs,
                        IUNIVERSAL_InArgs *inArgs,
                        IUNIVERSAL_OutArgs *outArgs)
{
  /* activate instance object */
  ALG_activate( (ALG_Handle)handle );
  
  /* Process the frame */
  handle->fxns->process(handle, inBufs, outBufs, inOutBufs, inArgs, outArgs);
  
  /* deactivate instance object */
  ALG_deactivate( (ALG_Handle)handle );
}

/*i2p.c - Nothing beyond this */
