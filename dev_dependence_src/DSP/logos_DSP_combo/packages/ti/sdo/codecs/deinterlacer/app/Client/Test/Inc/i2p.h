/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's DM         *
 *  class of SOCs.                                                             *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  i2p.h
 *  @brief  Caller-friendly interface for the I2P algorithm; made from
 *          vendor's i2p.h by replacing all ALG_ prefixes with ALGRF_
 *          (as in ALG_create with ALGRF_create)
 *
 *          Original comments:
 *          This header defines all types, constants, and functions used by
 *          applications that use the I2P algorithm.
 *
 *          Applications that use this interface enjoy type safety and
 *          the ability to incorporate multiple implementations of the I2P
 *          algorithm in a single application at the expense of some
 *          additional indirection.
 *
 *  @rev  1.0
 *******************************************************************************
 */

#ifndef _I2P_H
#define _I2P_H

#ifdef _cplusplus
extern "C"
{
#endif /* _cplusplus */
/*******************************************************************************
*                             Link Control Pragmas
*******************************************************************************/
/* None */

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <alg.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/dm/iuniversal.h>

/*-------------------------program files -------------------------------------*/
#include "ii2p_ti.h"

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/*
 *  ======== I2P_PARAMS ========
 *  Default instance parameters
 */
#define I2P_PARAMS II2P_PARAMS

/* control commands */
#define I2P_GETSTATUS II2P_GETSTATUS
#define I2P_SETSTATUS II2P_SETSTATUS

/*--------------------------Data declarations --------------------------------*/

/*--------------------------Enumerated Types  --------------------------------*/
/* None */

/*------------------------- Strutures ----------------------------------------*/
/*
 *******************************************************************************
 *  ======== I2P_Handle ========
 *  I2P algorithm instance 
 *******************************************************************************
 */
typedef struct II2P_Obj *I2P_Handle;

/*
 *  ======== I2P_Params ========
 *  I2P algorithm instance creation parameters
 */
typedef struct II2P_Params I2P_Params;

/*
 *******************************************************************************
 *  ======== I2P_Cmd ========
 *  I2P instance control commands.
 *******************************************************************************
 */
typedef II2P_Cmd I2P_Cmd;


/*
 *******************************************************************************
 *  ======== I2P_Status ========
 *  I2P control/status structure.
 *******************************************************************************
 */
typedef II2P_Status I2P_Status;

/*------------------------ Inline functions ----------------------------------*/

/*
 *******************************************************************************
 *  ======== I2P_create ========
 *  Create an instance of an I2P object.
 *******************************************************************************
 */
static inline I2P_Handle I2P_create (IUNIVERSAL_Fxns *fxns,
                                    const I2P_Params *prms) {
  return ((I2P_Handle)
           ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms));
}

/*
 *******************************************************************************
 *  ======== I2P_frameapply ========
 *  Apply an I2P filter to the input array and place results in the
 *  output array.
 *******************************************************************************
 */

extern XDAS_Void I2P_frameapply(IUNIVERSAL_Handle handle, XDM1_BufDesc *inBufs,
                                XDM1_BufDesc *outBufs, XDM1_BufDesc *inOutBufs,
                                IUNIVERSAL_InArgs *inArgs,
                                IUNIVERSAL_OutArgs *outArgs);


/*
 *******************************************************************************
 *  ======== I2P_control ========
 *  Invoke the I2P control function with the specified parameters.
 *******************************************************************************
 */
static inline Int I2P_control (I2P_Handle i2p,
                               I2P_Cmd cmd, I2P_Status *status ) {
    return (i2p->fxns->control((IUNIVERSAL_Handle)i2p,
    (IUNIVERSAL_Cmd)cmd, NULL,(IUNIVERSAL_Status *)status ));
}

/*
 *******************************************************************************
 *  ======== I2P_delete ========
 *  Delete a I2P instance object
 *******************************************************************************
 */
static inline Void I2P_delete(I2P_Handle handle)
{
    ALG_delete((ALG_Handle)handle);
}

/*---------------------- function prototypes ---------------------------------*/

/*
 *  ======== I2P_TI_exit ========
 *  Module finalization
 */
extern Void I2P_TI_exit(Void);

/*
 *  ======== I2P_TI_init ========
 *  Module initialization
 */
extern Void I2P_TI_init(Void);

#ifdef _cplusplus
}
#endif/* __cplusplus */

#endif  /* I2P_ */

/*i2p.h - EOF. */

