/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's DM         *
 *  class of SOCs.                                                             *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  lnk.i2p.cmd
 *  @brief  Linker command file for deinterlacer algorithm.
 *          This is for use with the C64xx Device Simulator.
 *
 *          If this tutorial is run on a different execution platform, this file
 *          may need changes to account for differences in the memory map
 *
 *  @rev  1.0
 *******************************************************************************
 */
-c
-heap  0x400000
-stack 0x10000
-i ../../Lib

/* Memory Map 1 - the default */
_I2P_II2P=_I2P_TI_II2P;

MEMORY {

  L1DSRAM     : origin = 0x10f04000,  len = 0x10000

  DDR2        : origin = 0x80000000,  len = 0x01000000

}
 
SECTIONS
{
  .bios             {} > DDR2
  .text:            {} > DDR2
  .stack            {} > DDR2
  .const            {} > DDR2
  .bss:             {} > DDR2
  .cinit:           {} > DDR2
  .cio:             {} > DDR2
  .data:            {} > DDR2
  .switch:          {} > DDR2         
  .sysmem           {} > DDR2
  .far:             {} > DDR2
  GROUP : {
    /* Functions in call order */
    .text:init
    .text:algInit
    .text:algAlloc
    .text:algActivate
    .text:algnumAlloc
    .text:algControl
    .text:algProcess
    .text:i2p_frameApply
    .text:dmaInit
    .text:dmaGetChannels
    .text:dmaGetChannelCnt
    .text:dmaChangeChannels
    .text:i2p_algs
    .text:algFree
    .text:algDeactivate
    .text:exit
    .text:i2p_qdma
  }                    > DDR2
  .text_rts            > DDR2 
  {
    -lrts64plus.lib(.text)
  }                    > DDR2
  .bss_rts             > DDR2 
  {
     -lrts64plus.lib(.bss)
  }                    > DDR2
	    
  UNION: {
    .vd   {}                              
    .vd6  {}                                  
  }                    > L1DSRAM
}
