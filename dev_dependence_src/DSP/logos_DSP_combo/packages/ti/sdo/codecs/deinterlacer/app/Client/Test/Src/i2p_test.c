/*
 *  Copyright 2010 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 *  18/03/2010..Modified to xDM compliance algorithm......AnilKumar 
 */
#include <std.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <idma3.h>
#include <dman3.h>
#include <bcache.h>
#include <iuniversal.h>

#include "ii2p_ti.h"
#include "i2p_ti.h"
#include "i2p.h"
#include "i2p_test.h"
#include "VPP_DataCpy.h"

#define NUMALGS  (1)

#define PITCH     (LINE_SZ*2)
#define WIDTH     (LINE_SZ*2)
#define FRAME_SIZE  (PITCH*NUM_LINES)
#define FRAME_CNT   (10)

#define _MCPS
#ifdef _MCPS
#define EXT_MEM_BASE (0x60000000)
#define EXT_MEM_SIZE (0x10000000)

/*! For Profiling on Davinci */
extern void TSC_enable(void);
extern long long  TSC_read(void);
double t0, t1, mt;

/* Cache Settings */
XDAS_Void TestApp_EnableCache(void)
{
    BCACHE_Size size;

    size.l1psize  = BCACHE_L1_32K; /* L1P cache size */

    size.l1dsize  = BCACHE_L1_16K; /* L1D cache size */

    size.l2size   = BCACHE_L2_64K; /* L2  cache size */

    /* Set L1P, L1D and L2 cache sizes */
    BCACHE_setSize(&size);

    /****************************************
     * Cache Enable External Memory Space   *
     * BaseAddr, length, MAR enable/disable *
     * Cache 0x60000000 --- 0x6FFFFFFF      *
     ***************************************/
    BCACHE_setMar((Ptr *)EXT_MEM_BASE, EXT_MEM_SIZE, BCACHE_MAR_ENABLE);

    BCACHE_wbInvAll(); /* No need to call BCACHE_wait() */

} /* DoCacheSettings */

#endif

/******************************************************************
 * Declaration of GT's trace implementation functions
 */
typedef Void   (*GT_PrintFxn)(String fmt, ...);
typedef Ptr    (*GT_MallocFxn)(Int size);
typedef Void   (*GT_FreeFxn)(Ptr addr, Int size);
typedef uint32_t (*GT_TimeFxn)(Void);

typedef struct {
    GT_PrintFxn  PRINTFXN;
    Fxn          PIDFXN;
    Fxn          TIDFXN;
    GT_PrintFxn  ERRORFXN;
    GT_MallocFxn MALLOCFXN;
    GT_FreeFxn   FREEFXN;
    GT_TimeFxn   TIMEFXN;
    Fxn          LOCKFXN;
    Fxn          UNLOCKFXN;
    Fxn          INITFXN;
} GT_Config;

extern Ptr myMalloc(Int size);
extern Void printfCaller(String fmt, ...);
extern far GT_Config *GT;

Ptr myMalloc(Int size)
{
    void *mall = malloc( size );
    return (mall);
}

Void printfCaller(String format, ...)
{
    va_list va;

    va_start(va, format);
    vprintf(format, va);
    va_end(va);

     fflush(stdout);
}
/*****************************************************************/

uint8_t Ydata[FRAME_SIZE];
uint8_t YdataOut[FRAME_SIZE];
uint8_t YBufFieldA[LINE_SZ*NUM_LINES*2] = {0};
uint8_t YBufFieldB[LINE_SZ*NUM_LINES*2] = {0};

uint32_t mythreshold; 
I2P_Handle i2pAlg;
I2P_Status i2pStatus;

/* Global Defination */
II2P_InputObject inObject;
II2P_OutputObject outObject;

/* Function Proto Type */
void VideoDeinterlace(uint16_t deinterlaceID);

void test(void)
{
    FILE    *fpInput, *fpOutput;
    int32_t   n_bytes = 0;
    int32_t   frameCount = 0;
    uint16_t  deinterlaceID = 0;
  

    fpInput  = fopen("../../Test/TestVecs/Input/chess_720x480_422Ile.yuv","rb"); 
    fpOutput = fopen("../../Test/TestVecs/Output/chess_720x480_422Ile_Out.yuv","wb");

    /* DO some Testing*/
    i2pStatus.threshold   = 0x05050505;
    i2pStatus.algSelect   = II2P_VD3;   /*  II2P_VD4 / II2P_VD3 / II2P_ELA */

    /* Set Params */    
    I2P_control(i2pAlg, I2P_SETSTATUS, &i2pStatus);

    /* Update the i2pStatus with I2P Structure */
    I2P_control(i2pAlg, I2P_GETSTATUS, &i2pStatus);

    switch(i2pStatus.algSelect){
        case II2P_VD4:
            printf("Algo deinterlaceID = 4 :- VD4 Algorithm Selected\n");
            deinterlaceID = 4;
            break;
        case II2P_VD3:
            printf("Algo deinterlaceID = 3 :- VD3 Algorithm Selected\n");
            deinterlaceID = 3;
            break;
        case II2P_ELA:
            printf("Algo deinterlaceID = 2 :- ELA Algorithm Selected\n");
            deinterlaceID = 2;
            break;
        default:
            deinterlaceID = 1;
            break;
     }  
    
#ifdef _VPP_QDMA 
    /* Configure the QDMA */
    VPP_QDMA_config(MAX_CHANNELS);
#endif

  for(frameCount = 1;frameCount<(FRAME_CNT +1);frameCount++)  
  { 
    printf("Reading input Frame\n");   
    n_bytes = fread( Ydata,1,FRAME_SIZE,fpInput);
    printf("n_bytes read = %d \n",n_bytes);


     /* Intialize Default Input Parameters*/    
     inObject.width     =   LINE_SZ; 
     inObject.height    =   NUM_LINES;
   
     /* call the Video Interlacing */
     VideoDeinterlace(deinterlaceID);   

     printf("Writting Output Frame\n");       
     n_bytes = fwrite( Ydata,1,FRAME_SIZE,fpOutput);
     printf("n_bytes Written = %d \n",n_bytes);
     printf("frameCount = %d \n",frameCount);

  } 

#ifdef _VPP_QDMA 
    VPP_QDMA_close();
#endif

 fclose(fpInput);
 fclose(fpOutput);


}

void test_frame_apply(void);
extern IUNIVERSAL_Fxns I2P_TI_II2P;
static Uns MP4D_QDMA_CHANNELS[] = {4, 5, 6, 7};

int main(int argc, int8_t** argv)
{
    I2P_Params i2pParams;

    IDMA3_Fxns *dmaFxns[NUMALGS];
    IALG_Handle alg[NUMALGS];
    int dma_status;

	XDAS_Int32  groupId, numAlgs;
	DMAN3_PARAMS.paRamBaseIndex  = 78;
    /* Number of QDMA channels used by the Algorithm                          */
    DMAN3_PARAMS.numQdmaChannels = 4;
    /* Number of Algorithms: System Parameter                                 */
    numAlgs                      = NUMALGS;
    /* Group Id needed by DMAN3 library: System Parameter                     */
    groupId                      = 0;

    DMAN3_PARAMS.numPaRamGroup[groupId] = 32 ;
	DMAN3_PARAMS.numPaRamEntries = 32 ;


#ifdef _MCPS
  /* Cache Config Function */
    TestApp_EnableCache();

    /* enable timer */
    TSC_enable();
#endif
	/* The lines below, are to setup GT trace *Added by anil*/
    GT->PRINTFXN = printfCaller;
    GT->ERRORFXN = printfCaller;
    GT->MALLOCFXN = myMalloc;
    /*   
     * Initialize DMA manager  for XDAIS algorithms
     * and grant DMA resources 
     */ 
    DMAN3_init();

    I2P_init();

    /* Initialize YBufFieldA,YBufFieldB Buffers */    
    memset(YBufFieldA, 0, (LINE_SZ*NUM_LINES*2));
    memset(YBufFieldB, 0, (LINE_SZ*NUM_LINES*2));


    i2pParams = II2P_PARAMS;
	i2pAlg = I2P_create(&I2P_TI_II2P, &i2pParams);
    
    /* DMAN3 Specific Calls */ 
    alg[0] = (IALG_Handle)i2pAlg;
    dmaFxns[0] = &I2P_TI_IDMA3;

    dma_status = DMAN3_grantDmaChannels(groupId, alg, dmaFxns, numAlgs);
    if (dma_status != DMAN3_SOK) {
        SYS_abort("Problem adding algorithm's dma resources");
    }    

    /* Update the I2P Structure with i2pStatus */
    I2P_control(i2pAlg, I2P_GETSTATUS, &i2pStatus);

    /* For testing v6 and ela_5 algorithms */
    test_frame_apply();
    /* For testing other algorithms */
    // test();

    /*
     * Reclaim DMA resources from algorithm and deinitialize the DMA 
     * manager and ACPY3 library   
     */                             
    if (DMAN3_releaseDmaChannels(alg, dmaFxns, NUMALGS) != DMAN3_SOK) {
        SYS_abort("Problem removing algorithm's dma resources");
    }    

    DMAN3_exit();
    I2P_exit();

    printf("All tests done successfully\n");
    return 0;
}


#pragma DATA_ALIGN(Ping_fieldRow, 8);
#pragma DATA_SECTION(Ping_fieldRow,".vd");

#pragma DATA_ALIGN(Pong_fieldRow, 8);
#pragma DATA_SECTION(Pong_fieldRow,".vd");

#pragma DATA_ALIGN(Ping_altFieldRow, 8);
#pragma DATA_SECTION(Ping_altFieldRow,".vd");

#pragma DATA_ALIGN(Pong_altFieldRow, 8);
#pragma DATA_SECTION(Pong_altFieldRow,".vd");

#pragma DATA_ALIGN(Ping_outRow, 8);
#pragma DATA_SECTION(Ping_outRow,".vd");

#pragma DATA_ALIGN(Pong_outRow, 8);
#pragma DATA_SECTION(Pong_outRow,".vd");

uint8_t Ping_fieldRow[WIDTH*3],  Pong_fieldRow[WIDTH*3];
uint8_t Ping_altFieldRow[WIDTH], Pong_altFieldRow[WIDTH];
uint8_t Ping_outRow[WIDTH],      Pong_outRow[WIDTH];

void VideoDeinterlace(uint16_t deinterlaceID )
{
    uint8_t *inBuf[1];
    int i;
    uint32_t id;
    uint32_t alt_Field_ix;

    uint32_t  id_Ping1In  = DAT_XFRID_WAITNONE,
              id_Pong1In  = DAT_XFRID_WAITNONE;
    uint32_t  id_PingOut  = DAT_XFRID_WAITNONE,
              id_PongOut  = DAT_XFRID_WAITNONE;
    uint32_t  id_Ping_alt_In = DAT_XFRID_WAITNONE,
              id_Pong_alt_In  = DAT_XFRID_WAITNONE;



    inBuf[0] = Ydata;
    /***************************************************/
    if (deinterlaceID == 1)
    {
       for( i = 0; i < (NUM_LINES >> 1); i++ ) {
         id = VPP_DataCpy (inBuf[0]+i*WIDTH*2,inBuf[0]+WIDTH +(i*WIDTH*2),WIDTH);
         VPP_DataWait (id);
       }
    }

    if (deinterlaceID == 2)
    {
      /* read ->0,1,2 */
      id_Ping1In = VPP_DataCpy (inBuf[0], (void*)Ping_fieldRow, WIDTH*3);  
      VPP_DataWait (id_Ping1In);

      /**************** YCrYCb *****************/
      for (i = 0; i< ((NUM_LINES>>1)-2); i+=2)
      {
        /* Trigger next xfer */
        /* read->2,3,4 */
        id_Pong1In = VPP_DataCpy (inBuf[0] + ((2*i+2)*WIDTH), (void*)Pong_fieldRow, WIDTH*3);  

        /* Intialize  Input Parameters Ping Buffer*/    
        inObject.fn        =   Ping_fieldRow;
        inObject.fn_3      =   NULL;
        inObject.outBuf_Fn =   Ping_outRow;

#ifdef _MCPS    
  t0 = TSC_read();
#endif 

        /* Do the I2P Process on 0,1,2*/   
        I2P_apply (i2pAlg, &inObject, &outObject); // process 1,2,3 - > Produce 2nd Line


#ifdef _MCPS    
  t1 = TSC_read();
  mt = t1 - t0;
  if(t1<t0) {
    fprintf(stderr,"Error\n");
  }
  fprintf(stdout, "\nI2P_apply     : MCPS = %lf\n", mt/1000000); 
#endif

        if (i) {
          VPP_DataWait (id_PingOut);
        }

        id_PingOut = VPP_DataCpy (Ping_outRow, inBuf[0]+WIDTH + (i*WIDTH*2) , WIDTH); // 2 line -> wriiten in to InBuff

        /*Wait for input xfer*/
        VPP_DataWait (id_Pong1In);
        id_Ping1In = VPP_DataCpy (inBuf[0] + ((i+2)*WIDTH*2), Ping_fieldRow , WIDTH*3); // read -> 5,6,7

        /* Intialize  Input Parameters Pong Buffer*/    
        inObject.fn        =   Pong_fieldRow;
        inObject.fn_3      =   NULL;
        inObject.outBuf_Fn =   Pong_outRow;

#ifdef _MCPS    
  t0 = TSC_read();
#endif 

        /* Do the I2P Process*/    
        I2P_apply (i2pAlg, &inObject, &outObject); // process 3,4,5 - > Produce 4th Line

#ifdef _MCPS    
  t1 = TSC_read();
  mt = t1 - t0;
  if(t1<t0) {
     fprintf(stderr,"Error\n");
  }
  fprintf(stdout, "\nI2P_apply     : MCPS = %lf\n", mt/1000000); 
#endif 
        VPP_DataWait (id_PingOut);
        id_PongOut = VPP_DataCpy (Pong_outRow, inBuf[0] + WIDTH +((i+1)*WIDTH*2), WIDTH); // 4th line -> wriiten in to InBuff

        /*Wait for input xfer*/
        VPP_DataWait (id_Ping1In);

       }/*for (i=0; i< ((NUM_LINES>>1)-2); i+=2)*/
       
       VPP_DataWait (id_PongOut);
       
     }/*if(deinterlaceID == 2)*/

     if((deinterlaceID == 3) || (deinterlaceID == 4))
     {
        /* Store Current Frame Bottom field in YBufFieldA*/
        for(i = 0;i<(NUM_LINES>>1);i++) {
          id = VPP_DataCpy (inBuf[0]+ WIDTH + (i*WIDTH*2), 
                            YBufFieldA+i*WIDTH, WIDTH); 
          VPP_DataWait (id);
        }
        alt_Field_ix = 0;
        /********** YCrYCb *****************************************/
        /* YBufFieldB contains the previouse Bottom Field*/

        /* read ->0,1,2 */
        id_Ping1In = VPP_DataCpy (inBuf[0], 
                                 (void*)Ping_fieldRow,   WIDTH * 3); 

        /* read ->0 */
        id_Ping_alt_In = VPP_DataCpy (YBufFieldB, Ping_altFieldRow, WIDTH);       // start copy  First Alternative line of Prvious Frame for Ping Buffer.
        alt_Field_ix ++;

        VPP_DataWait (id_Ping1In);
        VPP_DataWait (id_Ping_alt_In);

        for (i=0; i<((NUM_LINES>>1)-2); i+=2)
        {
          /* read ->2,3,4*/
          id_Pong1In = VPP_DataCpy (inBuf[0] + ((2*i+2)*WIDTH), Pong_fieldRow, 
                                    WIDTH*3); 

          /* read ->1*/
          id_Pong_alt_In = VPP_DataCpy (YBufFieldB+ (alt_Field_ix*WIDTH),
                                    Pong_altFieldRow,WIDTH);
          alt_Field_ix++;

          /* Intialize  Input Parameters Ping Buffer*/
          inObject.fn        = Ping_fieldRow;
          inObject.fn_3      = Ping_altFieldRow;
          inObject.outBuf_Fn = Ping_outRow;

#ifdef _MCPS
  t0 = TSC_read();
#endif /*_MCPS*/ 

          /* Do the I2P Process*/
          I2P_apply (i2pAlg, &inObject, &outObject); // (1,2,3)-Present, 2nd Field Previous  - > 2nd Line

#ifdef _MCPS
  t1 = TSC_read();
  mt = t1 - t0;
  if(t1<t0) {
     fprintf(stderr,"Error\n");
  }
  fprintf(stdout, "\nI2P_apply     : MCPS = %lf\n", mt/1000000); 
#endif /*_MCPS*/
          if (i) {
            VPP_DataWait (id_PongOut);
          }
          id_PingOut =  VPP_DataCpy (Ping_outRow,inBuf[0]+WIDTH+(i*WIDTH*2),WIDTH); 

          /*Wait for input xfer*/
          VPP_DataWait (id_Pong1In);
          VPP_DataWait (id_Pong_alt_In);

          /* read -> 4,5,6 */
          id_Ping1In =  VPP_DataCpy (inBuf[0]+((i+2)*WIDTH*2),
                                     Ping_fieldRow,WIDTH*3); 
          id_Ping_alt_In = VPP_DataCpy (YBufFieldB + (alt_Field_ix*WIDTH), 
                                     Ping_altFieldRow,WIDTH); 
          alt_Field_ix++;
          
          /* Intialize  Input Parameters Pong Buffer*/    
          inObject.fn        =   Pong_fieldRow;
          inObject.fn_3      =   Pong_altFieldRow;
          inObject.outBuf_Fn =   Pong_outRow;

#ifdef _MCPS    
  t0 = TSC_read();
#endif /*_MCPS*/ 
          /* Do the I2P Process*/    
          I2P_apply ( i2pAlg, &inObject, &outObject ); // (3,4,5), 4th Pre -> 4th Line

#ifdef _MCPS
  t1 = TSC_read();
  mt = t1 - t0;
  if(t1<t0){
     fprintf(stderr,"Error\n");
  }
  fprintf(stdout, "\nI2P_apply     : MCPS = %lf\n", mt/1000000); 
#endif /*_MCPS*/    

          VPP_DataWait (id_PingOut);
          id_PongOut = VPP_DataCpy (Pong_outRow,inBuf[0]+ WIDTH +(i+1)*WIDTH*2,WIDTH); // 4 line -> wriiten in to InBuff

          /*Wait for input xfer*/
          VPP_DataWait (id_Ping1In);
          VPP_DataWait (id_Ping_alt_In);

        }/*for (i=0; i<((NUM_LINES>>1)-2); i+=2)*/

        VPP_DataWait (id_PongOut);
        /* Copy YBufFieldA Bottom field in YBufFieldB*/
        for(i = 0; i<(NUM_LINES>>1); i++) {
          id = VPP_DataCpy (YBufFieldA+i*WIDTH,YBufFieldB+i*WIDTH,WIDTH);
          VPP_DataWait (id); 
        }/*Copy YBufFieldA Bottom field*/
        
     }/*if((deinterlaceID == 3) || (deinterlaceID == 4))*/
}/*VideoDeinterlace*/

/*i2p_test.c - Nothing beyond this */


/*********************************************************/
/*      Frame_apply                                      */
/*********************************************************/
void test_frame_apply(void)
{
    FILE    *fpInput, *fpOutput;
    int32_t   n_bytes = 0;
    int32_t   frameCount = 0;
    Bool test_ela_5_dir_only;

    /*Input, output and IO buffers*/
	XDM1_BufDesc universalInBufDesc;
    XDM1_BufDesc universalOutBufDesc;

    IDEINTER_InArgs         inputArgs;
    IDEINTER_OutArgs        outputArgs;

    IUNIVERSAL_InArgs        universalInArgs;
    IUNIVERSAL_OutArgs       universalOutArgs;

    fpInput  = fopen("../../Test/TestVecs/Input/shrek_720x480_422ILE.yuv","rb"); //bhc_uff.yuv
    fpOutput = fopen("../../Test/TestVecs/Output/shrek_720x480_422P.yuv","wb"); //bhc_uff_vd6_test_out.yuv

    if(fpInput == NULL) {
        fprintf(stdout, "Can not open input file \n");
        exit(0);
    }
    if(fpOutput == NULL) {
        fprintf(stdout, "Can not open output file \n");
        fclose(fpInput);
        exit(0);
    }

    inputArgs.universalInArgs             = universalInArgs;
    outputArgs.universalOutArgs           = universalOutArgs;
    inputArgs.universalInArgs.size        = sizeof(inputArgs);
    outputArgs.universalOutArgs.size      = sizeof(outputArgs);

    /* Intialize Default Input Parameters */
    inputArgs.inObject.width              = LINE_SZ;
    inputArgs.inObject.height             = NUM_LINES;
    inputArgs.inObject.imageFormat        = II2P_FOUR22_ILE; // II2P_FOUR20=0, II2P_FOUR22, II2P_FOUR22_ILE =1, II2P_FOUR44
    inputArgs.inObject.pitch_cur_frame    = PITCH;
    inputArgs.inObject.pitch_prev_fld     = PITCH;
    inputArgs.inObject.pitch_out_frame    = PITCH;
    inputArgs.inObject.useHistoryData     = TRUE;
    inputArgs.inObject.pitch_out_frame    = PITCH;

    universalInBufDesc.numBufs            = 2;
    universalOutBufDesc.numBufs           = 1;
    outputArgs.done                       = 0;

    universalInBufDesc.descs[0].bufSize   = FRAME_SIZE*sizeof(XDAS_UInt8);
    universalInBufDesc.descs[1].bufSize   = FRAME_SIZE*sizeof(XDAS_UInt8);
    universalOutBufDesc.descs[0].bufSize  = FRAME_SIZE*sizeof(XDAS_UInt8);

    /*Input and output Arguments*/
    universalInBufDesc.descs[0].buf       = (XDAS_Int8 *)Ydata;
    universalOutBufDesc.descs[0].buf      = (XDAS_Int8 *)YdataOut;
    test_ela_5_dir_only = FALSE;

    for(frameCount = 0; frameCount< FRAME_CNT; frameCount++)  
    { 
        printf("Reading input Frame\n");   
        n_bytes = fread( Ydata,1,FRAME_SIZE,fpInput);
        printf("n_bytes read = %d \n",n_bytes);

        /* Store Current Frame in YBufFieldA*/
        //memcpy (YBufFieldA, Ydata, LINE_SZ*NUM_LINES*2); 

        if ((!frameCount) || (test_ela_5_dir_only == TRUE)) {
        	inputArgs.inObject.useHistoryData = FALSE;
        	universalInBufDesc.descs[1].buf   = NULL;
        }
		else
		{
        	inputArgs.inObject.useHistoryData = TRUE;
        	universalInBufDesc.descs[1].buf   = (XDAS_Int8 *)YBufFieldB;
		}

    #ifdef _MCPS    
        t0 = TSC_read();
    #endif /*_MCPS*/ 

        /* call the i2p frame apply */
        i2pAlg->fxns->process((IUNIVERSAL_Handle)i2pAlg, &universalInBufDesc, &universalOutBufDesc, 
                             NULL, (IUNIVERSAL_InArgs *)&inputArgs, (IUNIVERSAL_OutArgs *)&outputArgs);

    #ifdef _MCPS
        t1 = TSC_read();
        mt = t1 - t0;
        if(t1<t0){
            fprintf(stderr,"Error\n");
        }
        fprintf(stdout, "\nI2P_frameApply : MCPS = %lf\n", mt/1000000); 
    #endif /*_MCPS*/    

        /* Store YBufFieldA in YBufFieldB */
        memcpy (YBufFieldB, Ydata, LINE_SZ*NUM_LINES*2); 

    #ifdef _MCPS        
        BCACHE_wb(YdataOut, FRAME_SIZE, TRUE);
    #endif

        fprintf(stdout, "Writting Output Frame\n");
        n_bytes = fwrite( YdataOut, 1, FRAME_SIZE, fpOutput);
        fprintf(stdout, "n_bytes Written = %d \n",n_bytes);
        fprintf(stdout, "frameCount = %d processed \n", (frameCount + 1));
    } 
    fclose(fpInput);
    fclose(fpOutput);
}

/* i2p_test.c - Nothing beyond this */
