/*!
 ******************************************************************************
 * \file  halinit.c
 * \brief File having HW initialization routines
 *
 * Power up time h/w initializaion routines. This file is for Davinci processor
 * The following routines are present in this file
 * a) Power up board/CSL initialization.
 * b) Timer Initialization
 * c) Cache Configuration
 * d) EDMA/QDMA configuration
 ******************************************************************************/
#ifndef _DEBUG
#include <stdio.h>
#include <csl.h>
#include <csl_edma.h>
#include <std.h>
#include <bcache.h>

#include "main_cache.h"
//#include <qdma.h>
/* Enable this define to profile DDR access */
//#define ENABLE_MEM_PROFILE     1

#define MGB_STATUS_FAIL 0
#define MGB_STATUS_SUCCESS 1

#ifdef ENABLE_MEM_PROFILE
unsigned char dummy_arr[1024 * 1024];
#endif

extern volatile cregister unsigned int TSCL;

extern void init_timer0(void);
extern void init_timer1(void);
#define PINMUX1 *(volatile unsigned int *)0x01C40004

/*!
 ******************************************************************************
 * \fn halInit
 * \brief  Init routine for platform/CSL timer/Cache/EDMA+QDMA. 
 *
 * This is the first function to be invoked after power on.It does init of
 * various on chip peripherals
 *
 * \param NONE
 * \return MGB_STATUS_SUCCESS or MGB_STATUS_FAIL
 ******************************************************************************/
Uint16 halInit ()
{
  CSL_Status        status;
  BCACHE_Size       cache_sizes;
  int i =0 ;


#ifdef ENABLE_MEM_PROFILE
  unsigned int beg_time;
  unsigned int end_time;
  int diff_time;
#endif /* ENABLE_MEM_PROFILE */

  /* Set the PINMUX1 for enabling TIMIN and ASP/McBSP */
  PINMUX1 |= (1<<18);
  PINMUX1 |= (1<<10);
  PINMUX1 |= 1;

#ifdef DSPONLY
  init_timer1();
#endif /* DSPONLY */

  /* Initialize various onchip peripherals. Some of these might
   * have been already initialized by BIOS, still...
   */
  //CSL_sysInit();
  /* Timer initialization, Timer 0 is used for
   * AV Sync. Ideally AV Sync should use TIMER 0 driven
   * by external VCXO to do the sync.
   */
  /* FIXME, this being done in the AV Sync module */
 // init_timer0();
/*
   size.l1psize  = BCACHE_L1_32K; // L1P cache size 
   size.l1dsize  = BCACHE_L1_16K; // L1D cache size 
   size.l2size   = BCACHE_L2_64K; // L2  cache size 
*/

  /*------------START OF CACHE CONFIG-----------------------------*/
  cache_sizes.l1psize = BCACHE_L1_32K ;//AVME_MEMCFG_L1PSIZE;
  cache_sizes.l1dsize = BCACHE_L1_16K;//AVME_MEMCFG_L1DSIZE;
  cache_sizes.l2size  = BCACHE_L2_64K;//AVME_MEMCFG_L2SIZE;

  BCACHE_setSize(&cache_sizes);

  /* Enable Caching of External memory only if Cache size > 0 */
  /* Cannot use Preprocessor #if because constants below are enums */
  if (BCACHE_L2_64K != BCACHE_L2_0K) {
    BCACHE_setMar((Ptr) 0x60000000, 0xFFFFFFF,BCACHE_MAR_ENABLE); 
 /* We migrated from CSL CACHE APIs to the BIOS CACHE APIs which enable
  * MAR registers in single go if given the Base address and length
  */ 
 /* CACHE_enableCaching(CACHE_EMIFA_CE00);       Cache 0x80000000 --- 0x80FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE01);       Cache 0x81000000 --- 0x81FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE02);       Cache 0x82000000 --- 0x82FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE03);       Cache 0x83000000 --- 0x83FFFFFF   */
  
 /* CACHE_enableCaching(CACHE_EMIFA_CE04);       Cache 0x84000000 --- 0x84FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE05);       Cache 0x85000000 --- 0x85FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE06);       Cache 0x86000000 --- 0x86FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE07);       Cache 0x87000000 --- 0x87FFFFFF   */
  
 /* CACHE_enableCaching(CACHE_EMIFA_CE08);       Cache 0x88000000 --- 0x88FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE09);       Cache 0x89000000 --- 0x89FFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE10);       Cache 0x8A000000 --- 0x8AFFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE11);       Cache 0x8B000000 --- 0x8BFFFFFF   */
  
 /* CACHE_enableCaching(CACHE_EMIFA_CE12);       Cache 0x8C000000 --- 0x8CFFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE13);       Cache 0x8D000000 --- 0x8DFFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE14);       Cache 0x8E000000 --- 0x8EFFFFFF   */
 /* CACHE_enableCaching(CACHE_EMIFA_CE15);       Cache 0x8F000000 --- 0x8FFFFFFF   */
  } 

  /* Do a cache clean of all data caches */
  BCACHE_wbInvAll();
  //CACHE_wbInvAllL2(CACHE_WAIT);
  //CACHE_wbInvAllL1d(CACHE_WAIT);

#ifdef ENABLE_MEM_PROFILE
  beg_time = TSCL;
  beg_time = TSC_read();
  for (i = 0; i < sizeof(dummy_arr); i += 16){
    unsigned long long temp1 = _amem8(dummy_arr + i);
    unsigned long long temp2 = _amem8(dummy_arr + i + 8);
   
    _amem8(dummy_arr + i) = temp2;
    _amem8(dummy_arr + i + 8) = temp1;
  }
  end_time = TSCL;
  end_time = TSC_read();	
  diff_time = end_time - beg_time;
  if (diff_time < 0){
    diff_time = 0xFFFFFFFFul + 1 - end_time + beg_time;
  }
/*  fprintf(stdout, " To touch %d bytes of memory CPU cycles %d \n",
          sizeof(dummy_arr),diff_time);*/
 printf(" To touch %d bytes of memory CPU cycles %d \n",sizeof(dummy_arr),diff_time);

#endif
  /*------------START OF DMA CONFIG-----------------------------*/
  /* Currently agreed upon EDMA resource allocation
   *
   *  Events
   *  0-1   - Unused
   *  12-15 - Unused
   *  25    - Unused
   *  30-31 - Unused
   *  45-47 - Unused
   *  55-63 - Unused
   *
   *  2-3   - McBSP
   *  4-7   - VPSS
   *  8-11  - IMCOP
   *  16-17 - SPI
   *  18-23 - UART
   *  24    - MEMSTK
   *  26-27 - MMCR
   *  28-29 - I2C
   *  32-44 - GPIO
   *  48-51 - Timer
   *  52-54 - PWM
   *
   *  Param Allocation
   *  Unused 0-31 - ARM
   *  Unused 32-63 - DSP
   *  64-105 - DSP
   *  106-127 - ARM
   *
   *  QDMA channels
   *  0-3 DSP
   *  4-5 IMCOP
   *  6-7 ARM
   */
    
  /* Assuming decoders will use all 32 (4*2) Params
   * Following is the Param allocation for QDMA channels
   *     QDMA_CHA0 - 63 - 58
   *     QDMA_CHA1 - 69 - 64
   *     QDMA_CHA2 - 75 - 70
   *     QDMA_CHA3 - 81 - 76
   * We have changed our system QDMA param allocation to match
   * DMAN3 allocation.
   */
  /* McBSP driver will use PARAMs, TCC bits 2,3, since
   * the McBSP events are mapped to these channels.  
   */
    
  /* EDMA initialization */
  
  status = CSL_edmaInit(NULL);
  if (CSL_SOK != status) {
    return MGB_STATUS_FAIL;
  }
  
  /* We have migrated to xMI codecs for all codecs except H.264. xMI codec
   * do DMAN 3 QDMA_open & close at creation time. We still need system QDMA functions
   * only for the non-xMI H.264 .Hence QDMA_open & close is moved to H.264 creation
   * time
   */
    /* FIXME, what other initialization????*/
    
    /* FIXME 08/09/2005
     * Now that BIOS supports MSGQ, do we really need the
     * RF5?.
     */
  return MGB_STATUS_SUCCESS;
}
#endif
