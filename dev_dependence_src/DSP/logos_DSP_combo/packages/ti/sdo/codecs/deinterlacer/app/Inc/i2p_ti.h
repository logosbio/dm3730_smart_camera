/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's DM         *
 *  class of SOCs.                                                             *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  i2p_ti.h
 *  @brief  Interface for the I2P_TI module; TI's implementation of
 *          the II2P interface
 *
 *  @rev  1.0
 *******************************************************************************
 */

#ifndef I2P_TI_
#define I2P_TI_

#ifdef _cplusplus
extern "C"
{
#endif /* _cplusplus */
/*******************************************************************************
*                             Link Control Pragmas
*******************************************************************************/
/* None */

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/xdas.h>
#include <ti/xdais/idma3.h>

/*-------------------------program files -------------------------------------*/
#include "ii2p_ti.h"

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/* None */

/*--------------------------Data declarations --------------------------------*/

/*--------------------------Enumerated Types  --------------------------------*/
/* None */

/*------------------------- Strutures ----------------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/

/*
 *  ======== I2P_TI_IDMA3 ========
 *  TI's implementation of M4H3 IDMA3 interface
 */
extern IDMA3_Fxns I2P_TI_IDMA3;

/*
 *  ======== I2P_TI_IALG ========
 *  TI's implementation of the IALG interface for I2P
 */
extern far IALG_Fxns I2P_TI_IALG;

/*
 *  ======== I2P_TI_II2P ========
 *  TI's implementation of the II2P interface
 */
extern IUNIVERSAL_Fxns I2P_TI_II2P;


/*
 *  ======== I2P_TI_exit ========
 *  Required module finalization function
 */
extern XDAS_Void I2P_TI_exit(Void);

/*
 *  ======== I2P_TI_init ========
 *  Required module initialization function
 */
extern XDAS_Void I2P_TI_init(Void);


#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* I2P_TI_ */

/*i2p_ti.h - EOF */
