/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's            *
 *  DM class of SOCs.                                                          *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  alg_create.c
 *  @brief  This file contains a simple implementation of the ALG_create
 *          API operation.
 *
 *  @rev  1.0
 *******************************************************************************
 */

/*******************************************************************************
*                             Link Control pragmas
*******************************************************************************/
/*------------------------- pragma definitions -------------------------------*/
#if defined(_TMS320C6X) || defined(__TMS320C55X__)
#pragma CODE_SECTION(ALG_create, ".text:create")
#pragma CODE_SECTION(ALG_delete, ".text:delete")
#endif

#pragma DATA_SECTION(persistBuf,   ".vd6:persistMem")
#pragma DATA_SECTION(L1scratchPad, ".vd6:scratchMem")

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <std.h>
#include <alg.h>
#include <ti/xdais/ialg.h>
#include <stdlib.h>

#ifdef _DEBUG
  #include <stdio.h>
#endif
/*-------------------------program files -------------------------------------*/
#include "_alg.h"

/*******************************************************************************
 * EXTERNAL REFERENCES NOTE : only use if not found in header file
*******************************************************************************/

/*------------------------ data declarations ---------------------------------*/
/* None */

/*----------------------- function prototypes --------------------------------*/
/* None */

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/**
 * Architecture specific macros
 */
#define C54x         54
#define C55x         55
#define C28x         28

/**
 * Scratch patch macros
 */
#define PERSTBUFSIZE 10
#define L1STHPADSIZE 64000

#if !defined (_C54x_) || (_C55x_) || (_C28x_)
  /**
   * Memory related API's
   */
  #define myMemalign  memalign
  #define myFree      free
#endif

/*------------------------ data declarations ---------------------------------*/
SmUns persistBuf[PERSTBUFSIZE];
SmUns L1scratchPad[L1STHPADSIZE];

/*----------------------- function prototypes --------------------------------*/
/* None */

/*******************************************************************************
 * PRIVATE DECLARATIONS Defined here, used only here
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/* None */

/*-------------------------data declarations ---------------------------------*/
/* None */

/*---------------------function prototypes -----------------------------------*/
/* None */



#ifdef _DEBUG
/**
********************************************************************************
*  @func   print_mem
*  @brief  API for printing all the allocated buffers
*
*  @param[in ]  memTab  : pointer to Memory records
*               n       : number of buffers
*
*  @returns None
********************************************************************************
*/
static void print_mem(IALG_MemRec *memTab, int n)
{
  int i;
  printf("Memory Usage :\n");
  for (i = 0; i < n; i++) {
    printf("\tBlock %d : %p => %d bytes, %d alignment\n",
           i, memTab[i].base, memTab[i].size, memTab[i].alignment);
  }
}
#endif



/**
********************************************************************************
*  @func   ALG_create
*  @brief  This function creates an instance of algorithm..
*
*  @param[in ]   *fxns     : pointer to IALG_Fxns structure
*                 p        : IALG_Handle
*                *params   :  pointer to IALG_Params structure
*
*  @returns ALG_Handle or NULL
********************************************************************************
*/
ALG_Handle ALG_create(IALG_Fxns *fxns, IALG_Handle p, IALG_Params *params)
{
  IALG_MemRec *memTab;
  Int n;
  ALG_Handle alg;
  IALG_Fxns *fxnsPtr;

  Int i;
  static int holdPerstBufsize = 0, holdScratchBufsize = 0;

  if (fxns != NULL) {
    n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;

    if ((memTab = (IALG_MemRec *)malloc(n * sizeof (IALG_MemRec)))) {
      n = fxns->algAlloc(params, &fxnsPtr, memTab);
        if (n <= 0) {
          return (NULL);
        }

      for(i = 0; i < n; i++) {
        if (memTab[i].space == IALG_EXTERNAL) {
          memTab[i].base = (void *)myMemalign(memTab[i].alignment,
                    									memTab[i].size);
        }
        else if (memTab[i].attrs == IALG_SCRATCH) {
          memTab[i].base = &L1scratchPad[holdScratchBufsize];
          memTab[i].base = (void *)(((Uns)memTab[i].base
                           + memTab[i].alignment - 1)
                           & ~(memTab[i].alignment - 1));
          holdScratchBufsize += memTab[i].size;
        }
        else if (memTab[i].attrs == IALG_PERSIST) {
          memTab[i].base = &persistBuf[holdPerstBufsize];
          memTab[i].base = (void *)(((Uns)memTab[i].base
                           + memTab[i].alignment - 1)
                           & ~(memTab[i].alignment - 1));
          holdPerstBufsize += memTab[i].size;
        }

        if (memTab[i].base == NULL) {
          _ALG_freeMemory(memTab, i);
          return (FALSE);
        }
        memset(memTab[i].base, 0, memTab[i].size);
      } /* for */

      alg = (IALG_Handle)memTab[0].base;
      alg->fxns = fxns;
      #ifdef _DEBUG
        print_mem(memTab, n);
      #endif

      if (fxns->algInit(alg, memTab, p, params) == IALG_EOK) {
        free(memTab);
        return (alg);
      }
      fxns->algFree(alg, memTab);
      _ALG_freeMemory(memTab, n);
      free(memTab);
    } /* memTab */
  } /* fxns != NULL */
  return (NULL);
} /* ALG_create */



/**
********************************************************************************
*  @func   ALG_delete
*  @brief  This function deletes instance of algorithm..
*
*  @param[in ]   alg       : pointer to alg
*
*  @returns None
********************************************************************************
*/
Void ALG_delete(ALG_Handle alg)
{
  IALG_MemRec *memTab;
  Int n;
  IALG_Fxns *fxns;

  if (alg != NULL && alg->fxns != NULL) {
    fxns = alg->fxns;
    n = fxns->algNumAlloc != NULL ? fxns->algNumAlloc() : IALG_DEFMEMRECS;

    if ((memTab = (IALG_MemRec *)malloc(n * sizeof (IALG_MemRec)))) {
      memTab[0].base = alg;
      n = fxns->algFree(alg, memTab);
      _ALG_freeMemory(memTab, n);

      free(memTab);
    }
  }
}

/* alg_create.c - EOF */

