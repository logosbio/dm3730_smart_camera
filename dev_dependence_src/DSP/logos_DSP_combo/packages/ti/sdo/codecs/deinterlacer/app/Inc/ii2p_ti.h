/*******************************************************************************
 * Texas Instruments Deinterlacer Algorithm                                    *
 *                                                                             *
 * "Deinterlacer Algorithm " is a software module developed on TI's DM         *
 *  class of SOCs.                                                             *
 *                                                                             *
 * Copyright (c) 2010 Texas Instruments Incorporated - http://www.ti.com/      *
 *                        ALL RIGHTS RESERVED                                  *
 ******************************************************************************/

/**
 *******************************************************************************
 *   @file  ii2p_ti.h
 *  @brief  This is the header file contains structure definitions, function
 *          Headers for the Deinterlacer algorithm
 *
 *  @rev  1.0
 *******************************************************************************
 */

#ifndef II2P_TI_
#define II2P_TI_

/*******************************************************************************
*                             Link Control Pragmas
*******************************************************************************/
/* None */

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
/*--------------------- system and platform files ----------------------------*/
#include <ti/xdais/ialg.h>
#include <ti/xdais/xdas.h>
#include <ti/xdais/dm/iuniversal.h>

#ifdef __cplusplus
extern "C"
{
#endif /* _cplusplus */
/*-------------------------program files -------------------------------------*/
/* None */

/*******************************************************************************
 * PUBLIC DECLARATIONS Defined here, used elsewhere
 ******************************************************************************/

/*--------------------------- macros  ----------------------------------------*/
/**
 *  Return constants
 *  II2P_SUCCESS           - 0
 *  II2P_FAIL              - -1
*/
#define II2P_SUCCESS (XDAS_Int32) 0
#define II2P_FAIL    (XDAS_Int32) -1


/*--------------------------Data declarations --------------------------------*/
extern const XDAS_Int8 I2PDate[];
extern const XDAS_Int8 I2PLibVer[];
extern const XDAS_Int8 I2PCopyright[];

/*--------------------------Enumerated Types  --------------------------------*/
/*
 *  ======== II2P_VideoFormat ========
 *  The Alg enumeration defines the video formats.
 *
 */
typedef enum II2P_VideoFormat {
  II2P_FOUR20=0,
  II2P_FOUR22_ILE =1,
  II2P_FOUR22,
  II2P_FOUR44
} II2P_VideoFormat_e;

/*
 *  ======== II2P_Alg ========
 *  The Alg enumeration defines the control commands for the I2P
 *  control method.
 */
typedef enum II2P_Alg {
  II2P_VD4=0,
  II2P_VD3,
  II2P_ELA
} II2P_Alg;


/*
 *  ======== II2P_Cmd ========
 *  The Cmd enumeration defines the control commands for the I2P
 *  control method.
 */

typedef enum II2P_Cmd {
  II2P_GETSTATUS,
  II2P_SETSTATUS
} II2P_Cmd;


/*------------------------- Strutures ---------------------------------------*/
/*
 *  ======== II2P_Handle ========
 *  This handle is used to reference all I2P instance objects
 */
typedef struct II2P_Obj *II2P_Handle;

/*
 *  ======== II2P_Obj ========
 *  This structure must be the first field of all I2P instance objects
 */
typedef struct II2P_Obj {
  struct II2P_Fxns *fxns;
} II2P_Obj;

/*
 * ======== InFrm========
 * width            : width of interlaced frame
 * height           : numLines of interlaced Frame
 * fn               : Pointer to an even/odd image line in current video frame.
                      (input + pitch) points to the image line in alternative
                      field. (input + 2*pitch) points to the even/odd line with
                      the same parity as input points to.
                      Must double-word aligned.
 * fn_3             : Pointer to an image line in previous frame
                      with the same parity as (input+pitch) points to
 * outBuf_Fn        : Pointer to an outbut frame
 * imageFormat      : Format of input video 422ILE / 420Planar
 * curFrame_y       : pointer to first y line of current frame for planar input
 * curFrame_cb      : pointer to first cb line of current frame for planar input
 * curFrame_cr      : pointer to first cr line of current frame for planar input
 * curFrame_yuv_ile : pointer to first byte of current frame for ILE input
 * pitch_cur_frame  : offset to the next line in the current frame
 * prevFld          : pointer to first byte of previous frame for ILE input
 * prevFld_y        : pointer to first y line of previous frame for planar input
 * prevFld_cb       : pointer to first cb line of previous frame for planar inpt
 * prevFld_Cr       : pointer to first cr line of previous frame for planar inpt
 * pitch_prev_fld   : pitch of the previous frame
 * outFrame         : pointer to first byte of output frame for ILE output 
 * outFrame_y       : pointer to first y line of output frame for planar output
 * outFrame_cb      : pointer to first cb line of output frame for planar output
 * outFrame_cr      : pointer to first cr line of output frame for planar output
 * pitch_out_frame  : offset to the next alternate line in the output frame
 * useHistoryData   : Flag that indicates to I2P whether the previous
                      frame/field are present for the deinterlacing algorithm
                      (TRUE or FALSE)
 * mvs              : Pointer to an array of motion vectors out of the video
                      decoder, based on which the threshold for motion
                      detection will be calculated. Assigned to NULL if not
                      present and algorithm uses its own threshold value for
                      motion detection.
 * nmvs             : number of motion vectors.

*/
typedef struct II2P_InputObject{
  XDAS_UInt16  width;
  XDAS_UInt16  height;
  XDAS_UInt8  *fn;      /*Input*/
  XDAS_UInt8  *fn_3;    /*Input2*/
  XDAS_UInt8  *outBuf_Fn; /*OutBuf*/

  II2P_VideoFormat_e  imageFormat;
  XDAS_UInt8  *curFrame_y;
  XDAS_UInt8  *curFrame_cb;
  XDAS_UInt8  *curFrame_cr;
  XDAS_UInt8  *curFrame_yuv_ile;
  XDAS_UInt16  pitch_cur_frame;

  XDAS_UInt8  *prevFld;
  XDAS_UInt8  *prevFld_y;
  XDAS_UInt8  *prevFld_cb;
  XDAS_UInt8  *prevFld_Cr;
  XDAS_UInt16  pitch_prev_fld;

  XDAS_UInt8  *outFrame;
  XDAS_UInt8  *outFrame_y;
  XDAS_UInt8  *outFrame_cb;
  XDAS_UInt8  *outFrame_cr;

  XDAS_UInt16  pitch_out_frame;
  XDAS_Bool    useHistoryData;
  XDAS_UInt16 *mvs;
  XDAS_UInt16  nmvs;

} II2P_InputObject;

typedef II2P_InputObject II2P_InputParams_t;

/*
 * ======== OutFrm ========
 * outBuff_Fn   : Pointer to the de-interlaced line. Must double-word aligned.
 * status       : Status of the algorithm for checking whether its done the
 *                process correctly or not
 */
typedef struct II2P_OutputObject {
  XDAS_UInt8 *outBuf_Fn; /*OutBuf*/
  XDAS_Bool   status;
} II2P_OutputObject;
typedef II2P_OutputObject II2P_OutputParams_t;

/*
 *  ======== II2P_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the alogrithm.
 *
 * threshold        : A threshold value to decide how much information to
 *                    carry on from input2.
 * algSelect        : Current Algorithm.
 */
typedef struct II2P_Status {
  XDAS_Int32     size;   /* must be first field of all status structures */
  /* Defined for I2P Params */
  XDAS_UInt32    threshold;
  II2P_Alg       algSelect;
} II2P_Status;


/*
 *  ======== II2P_Params ========
 *  This structure defines the creation parameters for all I2P objects
 * width            : width of interlaced frame
 * height           : numLines of interlaced Frame
 * VideoFormat      : Format of Video 422, 420
 * threshold        : A threshold value to decide how much information to
 *                    carry on from input2.
 * algSelect        : Current Algorithm.
 */
typedef struct II2P_Params {
  XDAS_Int32         size;   /* must be first field of all params structures */
  XDAS_UInt16        width;
  XDAS_UInt16        height;
  II2P_VideoFormat_e videoFormat;
  XDAS_UInt32        threshold;
  II2P_Alg           algSelect;
} II2P_Params;

/*
 *  ======== IDEINTER_InArgs ========
 * This structure defines the input arguments of the deinterlacer module
 * universalInArgs     : universal input arguments.
 * inObject            : Input Object structure.
 */
typedef struct IDEINTER_InArgs{
  IUNIVERSAL_InArgs  universalInArgs;
  II2P_InputParams_t inObject;
}IDEINTER_InArgs;

/*
 *  ======== IDEINTER_OutArgs ========
 * This structure defines the output arguments of the deinterlacer module
 * universalOutArgs     : universal output arguments.
 * outObject            : Output Object structure.
 * done                 : Flag for checking wheather process complete all the
 *                        steps properly
 */
typedef struct IDEINTER_OutArgs{
  IUNIVERSAL_OutArgs  universalOutArgs;
  II2P_OutputParams_t outObject;
  XDAS_UInt32 done;
}IDEINTER_OutArgs;

/*
 *  ======== II2P_Fxns ========
 *  This structure defines all of the operations on I2P objects
 */
typedef struct II2P_Fxns {
  IALG_Fxns   ialg;   /* II2P extends IALG
                       *< XDAIS algorithm interface.
                       *
                       *   @sa      IALG_Fxns
                       */
/**
 *  @brief      Basic decoding call.
 *
 *  @param[in]  handle          Handle to an algorithm instance.
 *  @param[in,out] inBufs       Input buffer descriptors.
 *  @param[in,out] outBufs      Output buffer descriptors.
 *  @param[in,out] inOutBufs    Input/Output buffer descriptors.
 *  @param[in]  inArgs          Input arguments.  This is a required
 *                              parameter.
 *  @param[out] outArgs         Ouput results.  This is a required parameter.
 *
 *  @remarks    process() is a blocking call.  When process() returns, the
 *              algorithm's processing is complete.
 *
 *  @pre        @c handle must be a valid algorithm instance handle.
 *
 *  @pre        @c inArgs must not be NULL, and must point to a valid
 *              IUNIVERSAL_InArgs structure.
 *
 *  @pre        @c outArgs must not be NULL, and must point to a valid
 *              IUNIVERSAL_OutArgs structure.
 *
 *  @pre        @c inBufs must either be NULL or point to a valid
 *              XDM1_BufDesc structure.
 *
 *  @pre        @c outBufs must either be NULL or point to a valid
 *              XDM1_BufDesc structure.
 *
 *  @pre        @c inOutBufs must either be NULL or point to a valid
 *              XDM1_BufDesc structure.
 *
 *  @pre        The buffers in @c inBufs, @c outBufs and @c inOutBufs are
 *              physically contiguous and owned by the calling application.
 *
 *  @post       The algorithm <b>must not</b> modify the contents of @c inArgs.
 *
 *  @post       The algorithm <b>must not</b> modify the contents of
 *              @c inBufs, with the exception of @c inBufs.descs[].accessMask.
 *              That is, the data and buffers pointed to by these parameters
 *              must be treated as read-only.
 *
 *  @post       The algorithm <b>must</b> appropriately set/clear the
 *              @c XDM1_BufDesc::descs[].accessMask field in @c inBufs,
 *              @c outBufs, and @c inOutBufs.
 *              For example, if the algorithm only read from
 *              @c inBufs.descs[0].buf using the algorithm processor, it
 *              could utilize #XDM_SETACCESSMODE_READ to update the appropriate
 *              @c accessMask fields.
 *              The application <i>may</i> utilize these
 *              returned values to appropriately manage cache.
 *
 *  @post       The buffers in @c inBufs, @c outBufs, and @c inOutBufs are
 *              owned by the calling application.
 *
 *  @retval     IUNIVERSAL_EOK          @copydoc IUNIVERSAL_EOK
 *  @retval     IUNIVERSAL_EFAIL        @copydoc IUNIVERSAL_EFAIL
 *                                      See IUNIVERSAL_Status#extendedError
 *                                      for more detailed further error
 *                                      conditions.
 *  @retval     IUNIVERSAL_EUNSUPPORTED @copydoc IUNIVERSAL_EUNSUPPORTED
 */
  XDAS_Int32 (*process)(IUNIVERSAL_Handle handle, XDM1_BufDesc *inBufs,
                        XDM1_BufDesc *outBufs, XDM1_BufDesc *inOutBufs,
                        IUNIVERSAL_InArgs *inArgs,
                        IUNIVERSAL_OutArgs *outArgs);
/**
 *  @brief      Control behaviour of an algorithm.
 *
 *  @param[in]  handle          Handle to an algorithm instance.
 *  @param[in]  id              Command id.  See #XDM_CmdId.
 *  @param[in]  params          Dynamic parameters.  This is a required
 *                              parameter.
 *  @param[out] status          Output results.  This is a required parameter.
 *
 *  @pre        @c handle must be a valid algorithm instance handle.
 *
 *  @pre        @c params must not be NULL, and must point to a valid
 *              IUNIVERSAL_DynamicParams structure.
 *
 *  @pre        @c status must not be NULL, and must point to a valid
 *              IUNIVERSAL_Status structure.
 *
 *  @pre        If a buffer is provided in the @c status->data field,
 *              it must be physically contiguous and owned by the calling
 *              application.
 *
 *  @post       The algorithm <b>must not</b> modify the contents of @c params.
 *              That is, the data pointed to by this parameter must be
 *              treated as read-only.
 *
 *  @post       If a buffer was provided in the @c status->data field,
 *              it is owned by the calling application.
 *
 *  @retval     IUNIVERSAL_EOK          @copydoc IUNIVERSAL_EOK
 *  @retval     IUNIVERSAL_EFAIL        @copydoc IUNIVERSAL_EFAIL
 *                                      See IUNIVERSAL_Status#extendedError
 *                                      for more detailed further error
 *                                      conditions.
 *  @retval     IUNIVERSAL_EUNSUPPORTED @copydoc IUNIVERSAL_EUNSUPPORTED
 */
  XDAS_Int32 (*control)(IUNIVERSAL_Handle handle, IUNIVERSAL_Cmd id,
                        IUNIVERSAL_DynamicParams *params,
                        IUNIVERSAL_Status *status);
} II2P_Fxns;

/*------------------------ Dependent Data declarations -----------------------*/
/*
 *  ======== II2P_PARAMS ========
 *  Default parameter values for I2P instance objects
 */
extern const II2P_Params II2P_PARAMS;

/*---------------------function prototypes -----------------------------------*/
/* None */


#ifdef __cplusplus
}
#endif/* __cplusplus */

#endif /* II2P_TI_ */

/*ii2p_ti.h - EOF */


