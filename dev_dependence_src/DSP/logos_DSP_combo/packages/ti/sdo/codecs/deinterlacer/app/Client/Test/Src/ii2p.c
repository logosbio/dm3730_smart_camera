/*
 *  Copyright 2010 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 */
/* "@(#) ReferenceFrameworks 1.20.00 08-02-02 (swat-c05.1)" */
/*
 *  ======== ii2p.c ========
 *  This file defines the default parameter structure for ii2p.h
 */
#include <std.h>
#include <ii2p_ti.h>


/*
 *  ======== I2P_PARAMS ========
 *  This constant structure defines the default parameters for I2P objects
 */

/*const II2P_Params II2P_PARAMS = {
   sizeof(II2P_Params),
   720,					//MAX Width
   480,					//MAX height
   II2P_FOUR22_ILE,   	// 422 interleaved Fixed
   0x01010101,			// Threshold
   II2P_ELA				// Select vd4 algorithm
};*/

/*
typedef struct II2P_Params {
    XDAS_Int32  size;   
    XDAS_UInt16 width;
    XDAS_UInt16 height;
    II2P_VideoFormat_e videoFormat;
    XDAS_UInt32 threshold;
    II2P_Alg algSelect;
  } II2P_Params;

*/
/*ii2p.c - Nothing beyond this */
