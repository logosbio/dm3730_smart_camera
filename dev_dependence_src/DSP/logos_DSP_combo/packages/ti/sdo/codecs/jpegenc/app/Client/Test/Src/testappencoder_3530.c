/******************************************************************************/
/*            Copyright (c) 2006 Texas Instruments, Incorporated.             */
/*                           All Rights Reserved.                             */
/******************************************************************************/

/*!
********************************************************************************
  @file     TestAppEncoder.c
  @brief    This is the top level client file that drives the H264
            (Baseline Profile) Video Encoder Call using XDM Interface
  @author   Multimedia Codecs TI India
  @version  0.0 - Jan 24,2006    initial version
********************************************************************************
*/

/* Standard C header files                                                    */
#include <stdio.h>
#include <stdlib.h>
#include <std.h>
#include <alg.h>

/* Client header file                                                         */
#include "TestAppEncoder.h"

/* CSL and DMAN3 header files                                                 */
#ifdef C6000
#include "bcache.h"
#include "dman3.h"
#else
//  #include <memory.h>
#endif

unsigned int   t0, t1, EncodeTime;

double cycles;
/* Input buffer allocation                                                    */
#ifdef C6000
#pragma    DATA_ALIGN(inputData,  128)
#pragma    DATA_SECTION(inputData,  ".ccdData")
#endif //C6000
XDAS_Int8  inputData[INPUT_BUFFER_SIZE];
/* Output buffer allocation */
#ifdef C6000
#pragma    DATA_ALIGN(outputData,  128)
#pragma    DATA_SECTION(outputData, ".bitsData")
#endif //C6000
XDAS_Int8  outputData[OUTPUT_BUFFER_SIZE];
/* Reference buffer allocation */
#ifdef C6000
#pragma    DATA_ALIGN(refData,  128)
#pragma    DATA_SECTION(refData,    ".ref_buffer")
#endif //C6000
XDAS_Int8  refData[OUTPUT_BUFFER_SIZE];
int total_size = 0;

#include <idmjpge.h>
#include <dmjpge_tigem.h>
/* Base Class Structures                                                      */
IIMGENC1_Params          params;
IIMGENC1_DynamicParams   dynamicParams;
IIMGENC1_InArgs        inArgs;
IIMGENC1_OutArgs       outArgs;
IIMGENC1_Status        status;
XDM1_BufDesc                  InBuffers;
XDM1_BufDesc                  OutBuffers;

XDAS_UInt32        TotalBytes;
extern long               timerJPEG;
 Uint32                      Total_BytesRead;
Uint32 captureHeight;
Uint32	DRI_Interval;
/* Mapping of Prameters from config file string to the place holder           */
sTokenMapping sTokenMap[] =
{
  {"MaxImageWidth",     &params.maxWidth          },
  {"MaxImageHeight",    &params.maxHeight         },
  {"Scan",    &params.maxScans         },
  {"IPImageWidth",     &dynamicParams.inputWidth          },
  {"IPImageHeight",    &dynamicParams.inputHeight         },
  {"ForceChromaFormat",    &params.forceChromaFormat         },
  {"inputChromaFormat",    &dynamicParams.inputChromaFormat  },
  {"QP",             &dynamicParams.qValue         },
  {"numMCU",             &dynamicParams.numAU         },
  {"captureWidth",             &dynamicParams.captureWidth         },
  {"captureHeight",             &captureHeight         },
  {"DRI_Interval",             &DRI_Interval         },

  {NULL,              NULL                      }
};
#ifdef  DAVINCI_INTERRUPT

#include "c6x.h"
#include "intc.h"

void Interrupt_start(void);

/*****************************************************************************/

void Interrupt_start(void)
{
 *((unsigned int *)0x01C21420) = 0x0;
 *((unsigned int *)0x01C21424) = 0x0;
 *((unsigned int *)0x01C21424) = 0x0007;
    *((unsigned int *)0x01C2141C) = 300;//count
    *((unsigned int *)0x01C21414) = 0x0;
 *((unsigned int *)0x01C21404) = 0x1;
 *((unsigned int *)0x01C21420) = 0x800000;
}
#endif

int InputBuffSize[4];
int num_buf,Capture_Height;
void Initialize_original_buffer(IDMJPGE_TIGEM_DynamicParams *params)
{
 int temp;
 if(params->params.size == sizeof(IIMGENC1_DynamicParams))
         Capture_Height = 0;
 else
   Capture_Height = params->captureHeight;

 if (params->params.captureWidth == 0 && Capture_Height==0)
   temp = params->params.inputHeight * params->params.inputWidth;

 else if (params->params.captureWidth != 0 && Capture_Height==0)
   temp = params->params.inputHeight * params->params.captureWidth;

  else if (params->params.captureWidth == 0 && Capture_Height!=0)
   temp = Capture_Height * params->params.inputWidth;

 else
   temp = Capture_Height * params->params.captureWidth;  //REM

 switch(params->params.inputChromaFormat)
 {
 case XDM_YUV_422ILE:
   {
     num_buf  = 1;
     InputBuffSize[0] = temp<<1 ;
   }; break;

    case XDM_GRAY:
   {
     num_buf  = 1;
     InputBuffSize[0] = temp ;
   }; break;

    case XDM_YUV_444P:
   {
     num_buf  = 3;
     InputBuffSize[0] = temp ;
     InputBuffSize[1] = temp ;
     InputBuffSize[2] = temp ;
   }; break;
    case XDM_YUV_420P:
   {
     num_buf  = 3;
     InputBuffSize[0] = temp ;
     InputBuffSize[1] = temp >> 2 ;
     InputBuffSize[2] = temp >> 2;
   }; break;

    case XDM_YUV_422P:
   {
     num_buf = 3;
     InputBuffSize[0] = temp ;
     InputBuffSize[1] = temp >> 1;
     InputBuffSize[2] = temp >> 1;
   }; break;

    case XDM_YUV_411P:
   {
     num_buf = 3;
     InputBuffSize[0] = temp ;
     InputBuffSize[1] = temp >> 2;
     InputBuffSize[2] = temp >> 2;
   }; break;

    case XDM_DEFAULT:
   {
     num_buf = 3;
     InputBuffSize[0] = temp ;
     InputBuffSize[1] = temp ;
     InputBuffSize[2] = temp ;
   }; break;

 }
}

/* Main Function acting as a client for Video Encode Call                     */
XDAS_Int32 main ()
{
  /* File I/O variables */
  FILE *fConfigFile, *ftestFile, *finFile, *fparamsFile,*fReportFile;
  XDAS_Int8 line[STRING_SIZE],inFile[STRING_SIZE], testFile[STRING_SIZE],
    paramsFile[STRING_SIZE], testCompliance, ii;

#ifdef C6000
  XDAS_Int8 *fname = "..\\..\\Test\\TestVecs\\Config\\Testvecs.cfg ";
  XDAS_Int8 *freport = "..\\..\\Test\\TestVecs\\Config\\Report.txt";
#else
  XDAS_Int8 *fname = "..\\..\\Client\\Test\\TestVecs\\Config\\Testvecs_script_vc.cfg";
  XDAS_Int8 *freport = "..\\..\\Client\\Test\\TestVecs\\Config\\Report.txt";
#endif

  /* Input/Output  buffers  and their sizes                                   */
 /* XDAS_Int8*  pInputBuf   [XDM_MAX_IO_BUFFERS];
  XDAS_Int8*  pOutputBuf  [XDM_MAX_IO_BUFFERS];
  XDAS_Int32  inBufSize   [XDM_MAX_IO_BUFFERS];
  XDAS_Int32  outBufSize  [XDM_MAX_IO_BUFFERS];
*/

  IIMGENC1_Fxns            *IIMGENC1fxns;

  IIMGENC1_Status          status;
  IIMGENC1_InArgs          inArgs;
  IIMGENC1_OutArgs         outArgs;

  /* Algorithm specific handle                                                */
  IALG_Handle handle;
#ifdef C6000
  IALG_Handle handleArray[NUM_ALGS];
  IDMA3_Fxns *dmaFxns[NUM_ALGS];
#endif

  /* Input/Output Buffer Descriptor variables */
  XDM1_BufDesc inputBufDesc, outputBufDesc;

  /* Other variables                                                          */
  XDAS_UInt32  ImageCount, countConfigSet;
  XDAS_Int32   retVal, testVal ;

  /* Enable Cache Settings                                                    */
  TestApp_EnableCache();

#ifdef DAVINCI_INTERRUPT
   INTC_init();
#endif
#ifdef C6000
  /* Enables the Time Stamp Counter */
    TSC_enable();
#endif

   /* Setting the sizes of Base Class Objects */
 params.size                    = sizeof(IIMGENC1_Params);
 params.maxHeight               = 3172;
 params.maxWidth                = 2048;
 params.maxScans                = 3;
 params.dataEndianness          = XDM_BYTE;
 dynamicParams.size              = sizeof(IIMGENC1_DynamicParams);
 dynamicParams.numAU             = 0;
 dynamicParams.generateHeader    = XDM_ENCODE_AU;

 status.size                     = sizeof(IIMGENC1_Status);

 inArgs.size                     = sizeof(IIMGENC1_InArgs);

 outArgs.size                    = sizeof(IIMGENC1_OutArgs);
 outArgs.extendedError           = 0;
 outArgs.bytesGenerated          = 0;
 outArgs.currentAU               = 0;

  /* Open Test Config File                                                    */
  fReportFile = fopen(freport,"w");

  if (!fReportFile)
  {
      printf("Couldn't openReport file %s",freport);
      return XDM_EFAIL;
  }

  /* Open Test Config File                                                    */
  fConfigFile = fopen(fname,"r");

  if (!fConfigFile)
  {
      printf("Couldn't open parameter file %s",fname);
      return XDM_EFAIL;
  }

  countConfigSet = 1; /* Reset countConfigSet value to 1 */

    /* Initializing Variables */
    ImageCount                   = 0; /* Tracks the number of frames decoded */

  /* Read the Config File until it reaches the end of file                    */
  while(!feof(fConfigFile))
  {
    /* Read Compliance Checking parameter */
    if(fgets(line,254,fConfigFile))
    {
      sscanf(line,"%d",&testCompliance);
    }
    else
    {
      break ;
    }
    /* Read Parameters file name */
    if(fgets(line,STRING_SIZE,fConfigFile))
    {
       sscanf(line,"%s",paramsFile);

    }
    else
    {
      break ;
    }
    /* Read Input file name */
    if(fgets(line,STRING_SIZE,fConfigFile))
    {
      sscanf(line,"%s",inFile);
    }
    else
    {
      break ;
    }

    /* Read Output/Reference file name */
    if(fgets(line,254,fConfigFile))
    {
      sscanf(line,"%s",testFile);
    }
    else
    {
      break ;
    }

    printf("\n*******************************************");
    printf("\nRead Configuration Set %d",countConfigSet);
    printf("\n*******************************************");
   countConfigSet++;

    /* Open Parameters file */
    fparamsFile = fopen (paramsFile, "rb");
    if (!fparamsFile)
    {
      printf("\nCouldn't open Parameters file...   %s\n ",paramsFile);
      printf("Exiting for this configuration...\n");
   continue;
    }

    if(readparamfile(fparamsFile) < 0 )
    {
      printf("\nSyntax Error in %s\n ",paramsFile);
      printf("Exiting for this configuration...\n");
   continue;
    }

    /* Close Parameters File */
    fclose(fparamsFile);
    if(testCompliance)
    {
      printf("\nRunning in Compliance Mode");
    }
    else
    {
      printf("\nRunning in Output Dump Mode");
    }

    /* Open input file */
    finFile = fopen (inFile, "rb");
    if (!finFile)
    {
      printf("\nCouldn't open Input file...  %s  ",inFile);
      printf("\nExiting for this configuration...");
   continue;
    }
    printf("\nInput file: %s",inFile);

    /* Open output/reference file */
    if(testCompliance)
    {
        ftestFile = fopen (testFile, "rb");
    }
    else
    {
        ftestFile = fopen (testFile, "wb");
    }
    if( !ftestFile)
    {
      printf("\nCouldn't open Test File... %s",testFile);
      printf("\nExiting for this configuration..." );
   continue;
    }
    printf("\nOutput/Reference File: %s",testFile);

    /* Initialization of parameters needed for Algorithm Instance create */
    TestApp_SetInitParams(&params);



    /* Create the Algorithm object (instance) */
    printf("\nCreating Algorithm Instance...");


    if ((handle =  (IALG_Handle)ALG_create (
      (IALG_Fxns *) &DMJPGE_TIGEM_IDMJPGE,
      (IALG_Handle) NULL,
      (IALG_Params *) &params)) == NULL)
    {
      printf("\nFailed to Create Instance... Exiting for this configuration..");
     continue;
    }

    /* Assigning Algorithm handle fxns field to IIMGdecfxns */
    IIMGENC1fxns = (IIMGENC1_Fxns *)handle->fxns ;

    printf("\nAlgorithm Instance Creation Done...\n");

#ifdef ENABLE_QDMA
 {
 unsigned int DMAN3_QDMA_CHANNELS_NUMS[6] = {0,1,2,3,4,5};
 Int groupId = 1;
 Int numAlgs = 1;    /* DMA allocation using DMAN3 API's */
 DMAN3_PARAMS.numQdmaChannels = 6;
 DMAN3_PARAMS.numPaRamEntries = 48;
 DMAN3_PARAMS.maxQdmaChannels = 8;
 DMAN3_PARAMS.qdmaChannels = DMAN3_QDMA_CHANNELS_NUMS;
 DMAN3_PARAMS.paRamBaseIndex  = 78;

    DMAN3_init();

    /* Provide the Algorithm handle to DMAN3 Interface for granting channels */
    handleArray[0] = (IALG_Handle) handle;
    dmaFxns[0]     = &DMJPGE_TIGEM_IDMA3;

    if (DMAN3_grantDmaChannels(groupId, handleArray, dmaFxns, numAlgs)
         != DMAN3_SOK)
    {
      printf("\nProblem adding algorithm's dma resources");
      printf("... Exiting for this configuration... \n");
   abort();
    }
 printf("DMAN3 initialization succesfully done...\n");// fflush(fpout);
 }
#endif  //C6000

#ifdef C6000
     BCACHE_wbInvAll(); /* No need to call BCACHE_wait() */
#endif

    /* Set run time dynamic parameters */
    TestApp_SetDynamicParams(&dynamicParams);

    /* set space for buffer descriptors                                       */
    /*inputBufDesc.bufs = (XDAS_Int8 **)pInputBuf;
    outputBufDesc.bufs = (XDAS_Int8 **)pOutputBuf;
    inputBufDesc.bufSizes = (XDAS_Int32 *)inBufSize;
    outputBufDesc.bufSizes = (XDAS_Int32 *)outBufSize;*/

    /* Activate the Algorithm                                                 */
    handle->fxns->algActivate(handle);
     /* Optional: Set Run time parameters in the Algorithm via control()     */
    IIMGENC1fxns->control((IIMGENC1_Handle)handle, XDM_SETPARAMS,
        (IIMGENC1_DynamicParams *)&dynamicParams, (IIMGENC1_Status *)&status);

    /* Get Buffer information                                                 */
    IIMGENC1fxns->control((IIMGENC1_Handle)handle,
                         XDM_GETBUFINFO,
                         (IIMGENC1_DynamicParams *)&dynamicParams,
                         (IIMGENC1_Status *)&status);

    Initialize_original_buffer((IDMJPGE_TIGEM_DynamicParams *) &dynamicParams);

    /* DeActivate the Algorithm                                               */
    handle->fxns->algDeactivate(handle);

    /*Fill up the buffers as required by algorithm                            */
    inputBufDesc.numBufs  = status.bufInfo.minNumInBufs ;
    inputBufDesc.descs[0].buf     = inputData;
    inputBufDesc.descs[0].bufSize = status.bufInfo.minInBufSize[0];

    for(ii=0; ii< (status.bufInfo.minNumInBufs - 1);ii++ )
    {
      inputBufDesc.descs[ii+1].buf = inputBufDesc.descs[ii].buf +
        status.bufInfo.minInBufSize[ii];
      inputBufDesc.descs[ii +1].bufSize =
        status.bufInfo.minInBufSize[ii +1];
    }

    outputBufDesc.numBufs     = status.bufInfo.minNumOutBufs ;
    outputBufDesc.descs[0].buf     = outputData;
    outputBufDesc.descs[0].bufSize = status.bufInfo.minOutBufSize[0];
	if(outputBufDesc.descs[0].bufSize > OUTPUT_BUFFER_SIZE)
	{
		outputBufDesc.descs[0].bufSize = OUTPUT_BUFFER_SIZE;
	}

    for(ii=0; ii< (status.bufInfo.minNumOutBufs-1); ii++ )
    {
		if(outputBufDesc.descs[0].bufSize > OUTPUT_BUFFER_SIZE)
		{
			exit(0);
		}

     outputBufDesc.descs[ii+1].buf = outputBufDesc.descs[ii].buf +
      status.bufInfo.minOutBufSize[ii];
      outputBufDesc.descs[ii+1].bufSize =
        status.bufInfo.minOutBufSize[ii+1];
    }

    /* Assigning Algorithm handle fxns field to IIMGENC1fxns                   */
    IIMGENC1fxns = (IIMGENC1_Fxns *)handle->fxns;

 /* Read from Input File                                                 */
      TestApp_ReadInputData(finFile,
        &inputBufDesc);


#ifdef C6000
    /* Cache Invalidate for Input Buffer                                   */
      for(ii=0 ; ii < inputBufDesc.numBufs ; ii++ )
      {
        /* Cache Invalidate for Input Buffer                                 */
		BCACHE_inv(inputBufDesc.descs[ii].buf, inputBufDesc.descs[ii].bufSize, TRUE);

      }
#endif  //C6000

#ifdef DAVINCI_INTERRUPT
    INTC_enable(INT_EVT_TMR1);
 INTC_attachIsr(INT_EVT_TMR1,NULL);
    Interrupt_start();
#endif

      /* Activate the Algorithm                                               */
      handle->fxns->algActivate(handle);
     /* Optional: Set Run time parameters in the Algorithm via control()     */

      IIMGENC1fxns->control((IIMGENC1_Handle)handle, XDM_SETPARAMS,
        (IIMGENC1_DynamicParams *)&dynamicParams, (IIMGENC1_Status *)&status);

#ifdef C6000
     BCACHE_wbInvAll(); /* No need to call BCACHE_wait() */
#endif

     TotalBytes = 0;
#ifdef C6000
      EncodeTime = 0;
      /* Timer value before start of decoding */
      t0  = TSC_read();
#endif  //C6000
       /* Basic Algorithm process() call */
      retVal = IIMGENC1fxns->process((IIMGENC1_Handle)handle,
        (XDM1_BufDesc *)&inputBufDesc,
        (XDM1_BufDesc *)&outputBufDesc,
        (IIMGENC1_InArgs *)&inArgs,
        (IIMGENC1_OutArgs *)&outArgs);
#ifdef C6000
 /* Timer value before start of decoding */
    t1  = TSC_read();
 EncodeTime = EncodeTime + (t1-t0);
#endif

   TotalBytes +=outArgs.bytesGenerated;
      if(retVal == XDM_EFAIL)
      {
         printf("\nProcess function returned an Error...  ");
     continue;
   }

      /* Optional: Set Run time parameters in the Algorithm via control()     */
      IIMGENC1fxns->control((IIMGENC1_Handle)handle, XDM_GETSTATUS,
        (IIMGENC1_DynamicParams *)&dynamicParams, (IIMGENC1_Status *)&status);
   if((!testCompliance) && (dynamicParams.numAU))
 TestApp_WriteOutputData(ftestFile, &outputBufDesc, &outArgs);
      while (outArgs.currentAU < status.totalAU)
      {

#ifdef C6000
     BCACHE_wbInvAll(); /* No need to call BCACHE_wait() */
#endif
#ifdef C6000
      /* Timer value before start of decoding */
      t0  = TSC_read();
#endif  //C6000
       /* Basic Algorithm process() call */
       retVal = IIMGENC1fxns->process((IIMGENC1_Handle)handle,
         (XDM1_BufDesc *)&inputBufDesc,
         (XDM1_BufDesc *)&outputBufDesc,
         (IIMGENC1_InArgs *)&inArgs,
         (IIMGENC1_OutArgs *)&outArgs);
#ifdef C6000
 /* Timer value before start of decoding */
    t1  = TSC_read();
 EncodeTime = EncodeTime + (t1-t0);
#endif

       TotalBytes +=outArgs.bytesGenerated;
       if(retVal == XDM_EFAIL)
       {
         printf("\nProcess function returned an Error...  ");
         break; /* Error Condition: Application may want to break off          */
       }
    if((!testCompliance) && (dynamicParams.numAU))
    TestApp_WriteOutputData(ftestFile, &outputBufDesc, &outArgs);
   }
      /* Optional: Read status via control()                                  */
      IIMGENC1fxns->control((IIMGENC1_Handle)handle,
        XDM_GETSTATUS,
        (IIMGENC1_DynamicParams *)&dynamicParams,
        (IIMGENC1_Status *)&status);


    /* DeActivate the Algorithm                                             */
    handle->fxns->algDeactivate(handle);



#ifdef DAVINCI_INTERRUPT
    INTC_disable(INT_EVT_TMR1);
#endif

#ifdef C6000
      /* Cache Writeback Invalidate for Output Buffers                        */
      for(ii=0 ; ii < outputBufDesc.numBufs ; ii++ )
      {
	   BCACHE_wbInv(outputBufDesc.descs[ii].buf,
          outputBufDesc.descs[ii].bufSize, TRUE);
      }
#endif //C6000

      /* Check for frame ready via recon buffer information                   */
     // if(outArgs.reconBufs.numBufs)
      {
          fflush(stdout);

     testVal = XDM_EOK;

     if(!dynamicParams.numAU)
     {
          if(testCompliance)
          {
            /* Compare the output frames with the Reference File */
            testVal = TestApp_CompareOutputData(ftestFile,
                                                (XDM1_BufDesc *)&outputBufDesc,
                                                &outArgs);
            if(testVal != XDM_EOK)
            {
              /* Test Compliance Failed... Breaking...*/
              printf("\nEncoder compliance test failed for this Image. ");
       break;

            }
          }
          else
          {
            /* Write the output frames in the display order */
            TestApp_WriteOutputData(ftestFile, &outputBufDesc, &outArgs);
          }
     }
          ImageCount++;
      }
       fclose(ftestFile);

    if(testVal == XDM_EOK && testCompliance )
    {
      printf("\nEncoder compliance test passed \n");
    }

    if(testVal == XDM_EOK && (!testCompliance) )
    {
      printf("\nEncoder output dump completed \n");
    }
    /* Output file close */
    fclose(ftestFile);


    /* Delete the Algorithm instance object specified by handle */
    ALG_delete (handle);

    if(ImageCount == 0)
    {
      ImageCount = 1; /* To avoid division with zero */
    }

   cycles = (double)EncodeTime / ((double)(dynamicParams.inputHeight * dynamicParams.inputWidth ) ) ;


    printf("Image Number: %d\n",(XDAS_UInt32)ImageCount);
    printf("Width and Height: %d, %d\n",dynamicParams.inputWidth,dynamicParams.inputHeight);
    printf("QP: %d\n", dynamicParams.qValue);
 printf("Cycles Taken: %u\n",EncodeTime);
 printf("Cycles Per Pixel: %f\n",cycles);
 printf("Total Input Bytes Read: %d\n",Total_BytesRead);
 printf("Total Output Bytes Generated: %d\n",TotalBytes);
 printf("Compression Ratio: %f\n",(double)Total_BytesRead / (double)TotalBytes);

    fprintf(fReportFile,"-------------- Image Number %d -----------\n",(XDAS_UInt32)ImageCount);
    fprintf(fReportFile,"I/P Image Encoded: %s\n",inFile);
    fprintf(fReportFile,"O/P or Ref Image Encoded: %s\n",testFile);
    fprintf(fReportFile,"Parameter File: %s\n",paramsFile);
    fprintf(fReportFile,"InputChromaFormat: %d\n",(XDAS_UInt32)dynamicParams.inputChromaFormat);
    fprintf(fReportFile,"ForceChromaFormat: %d\n",(XDAS_UInt32)params.forceChromaFormat);
    fprintf(fReportFile,"Width and Height: %d, %d\n",dynamicParams.inputWidth,dynamicParams.inputHeight);
    fprintf(fReportFile,"QP: %d\n", dynamicParams.qValue);
 fprintf(fReportFile,"Cycles Taken: %u\n",EncodeTime);
 fprintf(fReportFile,"Cycles Per Pixel: %f\n",cycles);
 fprintf(fReportFile,"Total Input Bytes Read: %d\n",Total_BytesRead);
 fprintf(fReportFile,"Total Output Bytes Generated: %d\n",TotalBytes);
 fprintf(fReportFile,"Compression Ratio: %f\n",(double)Total_BytesRead / (double)TotalBytes);
    fprintf(fReportFile,"\n\n");

 fflush(fReportFile);
 fflush(stdout);
  } /* Read the Config File until it reaches the end of file */

  printf("\n----------------\n");
  printf("\nEnd of execution\n");
  printf("\n----------------\n");
  /* Close the config files */
  fclose(fConfigFile);

  return XDM_EOK;
} /* main() */

/*
//============================================================================
// TestApp_ReadInputData
//  Reading Byte Stream from a File
*/

XDAS_Int32 TestApp_ReadInputData(FILE *finFile,
                                  XDM1_BufDesc* inputBufDesc)
{
    XDAS_Int32 i, bytesRead ;
    Total_BytesRead = 0;
    for(i=0; i < num_buf ; i++)
    {
      /*Read the input buffers from FILE                                      */
      bytesRead = fread(inputBufDesc->descs[i].buf,
                        1,
                        InputBuffSize[i],
                        finFile);
  Total_BytesRead += bytesRead;
  if (Total_BytesRead > INPUT_BUFFER_SIZE)
    {
      printf( "\nWarning : File size exceeds the application input buffer size %d ",
        INPUT_BUFFER_SIZE);
      printf( "\nContinuing decoding for %d bytes.\n", INPUT_BUFFER_SIZE);
    }

#ifdef C6000
      /* Make sure that external memory contain correct copy of input data    */

       /* Cache Invalidate for Input Buffer */
    BCACHE_wbInv(inputBufDesc->descs[i].buf, inputBufDesc->descs[i].bufSize, TRUE);


#endif //C6000

      if(bytesRead != InputBuffSize[i])
      {
        bytesRead = -1 ;
   printf("Input Buffer Size is less than the Image Size");//REM
//   exit(0);
        break ;
      }
    }
 fclose(finFile);
    return (bytesRead);
}

/*
//============================================================================
// TestApp_CompareOutputData
//  Comparing Output Data with Reference File data
*/

XDAS_Int32 TestApp_CompareOutputData(FILE *fRefFile,
                                     XDM1_BufDesc * outputBufDesc,
                                     IIMGENC1_OutArgs *outArgs)
{
  XDAS_Int32 i, retVal ;

  retVal = XDM_EOK;

  /* Compare all the output Buffers with the ref File                         */
  for(i=0; i < outputBufDesc->numBufs ; i++)
  {
    fread(refData, 1, outArgs->bytesGenerated, fRefFile);
    if(memcmp(refData, outputBufDesc->descs[i].buf, outArgs->bytesGenerated))
    {
      retVal = XDM_EFAIL;
    }
    break ;
  }
  fclose(fRefFile);
  return retVal;
}

/*
//============================================================================
// TestApp_WriteOutputData
//  Writing Output Data in a File
*/


XDAS_Void TestApp_WriteOutputData(FILE *fOutFile,
                                  XDM1_BufDesc * outputBufDesc,
                                  IIMGENC1_OutArgs *outArgs)
{

  XDAS_Int32 i = 0;

  if(TotalBytes > OUTPUT_BUFFER_SIZE)
  {
    printf( "\nWarning : Output size exceeds output  buffer size %d OUTPUT_BUFFER_SIZE %d",
        TotalBytes,OUTPUT_BUFFER_SIZE);

  }
  /****************************************************************************/
  /* Write the bitstream                                                      */
  /****************************************************************************/
  for(i=0; i < outputBufDesc->numBufs ; i++)
  {
    if(outArgs->bytesGenerated != 0 )
    {
      fwrite( outputBufDesc->descs[i].buf,
        1,
        outArgs->bytesGenerated,
        fOutFile);
    }
  }

}

/*
//============================================================================
// TestApp_SetInitParams
//  setting of creation time parameters
*/

XDAS_Void TestApp_SetInitParams(IIMGENC1_Params *params)
{

  return;
}

/*
//============================================================================
// TestApp_SetDynamicParams
//  setting of run time parameters
*/

XDAS_Void TestApp_SetDynamicParams(IIMGENC1_DynamicParams *dynamicParams)
{

  dynamicParams->generateHeader  = XDM_ENCODE_AU       ;

  return;
}


  /* Cache Settings */
XDAS_Void TestApp_EnableCache(void)
{
#ifdef C6000

    BCACHE_Size size;

 size.l1psize  = BCACHE_L1_32K; /* L1P cache size */
 size.l1dsize  = BCACHE_L1_32K; /* L1D cache size */
 size.l2size   = BCACHE_L2_64K; /* L2  cache size */

 /* Set L1P, L1D and L2 cache sizes */
    BCACHE_setSize(&size);

    /* Cache Enable External Memory Space */
 /* BaseAddr, length, MAR enable/disable */
    /* Cache 0x80000000 --- 0x8FFFFFFF   */
    BCACHE_setMar((Ptr *)0x80000000, 0x10000000, BCACHE_MAR_ENABLE);


   /* Cache Enable External Memory Space */



#ifdef C6000
     BCACHE_wbInvAll(); /* No need to call BCACHE_wait() */
#endif

#endif  //C6000

} /* DoCacheSettings */

/******************************************************************************/
/*    Copyright (c) 2006 Texas Instruments, Incorporated                      */
/*    All Rights Reserved                                                     */
/******************************************************************************/
