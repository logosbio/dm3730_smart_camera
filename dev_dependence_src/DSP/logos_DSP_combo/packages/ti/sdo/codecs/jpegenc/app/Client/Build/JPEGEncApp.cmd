/******************************************************************************
            TEXAS INSTRUMENTS INCORPORATED PROPRIETARY INFORMATION           
                                                                             
   Property of Texas Instruments  
   For  Unrestricted  Internal  Use  Only 
   Unauthorized reproduction and/or distribution is strictly prohibited.  
   This product  is  protected  under  copyright  law  and  trade  secret law 
   as an unpublished work.  

   Created 2007, (C) Copyright 2007 Texas Instruments.  All rights reserved.
                                                           
   Filename         : JPEGEncApp.cmd

   Description      : Linker command file for OMAP3430
   
   Project          : OMAP3430

  *******************************************************************************/

_CLK_REGS     = 0;
_CLK_USETIMER = 0;

-stack 0x2000
-heap     0x2400000 /*    Heap area size       */
-l rts64plus.lib

/* Use C linking conventions: auto-init vars at runtime */
-c                  

/* assign IVA2.2 ivlcd memories address */
 _IVLCD_IBUF0A = 0x00084000;
 _IVLCD_IBUF0B = 0x00085000;
 _IVLCD_IBUF1  = 0x00086000;
 _IVLCD_QMEM   = 0x00088000;
 _IVLCD_HMEM   = 0x0008c000;

 _GEM_L1_RAM  = 0x10F04000;
 _IVA2_2_SL2_MEM_ADDRESS = 0x107f8000;

MEMORY
{

	DMC (RWI)		:   o=0x10F04000    l=0x00014000  /* 80k for L1 Data memory   */
	PMC (X) 		:   o=0x10E00000    l=0x00008000  /* 32k for cache only       */
	L2_RAM	(RWXI)	:   o=0x10800000    l=0x00010000  /* 64 kB L2 internal memory */
	SL2_RAM	(RWI)	:   o=0x107f8000    l=0x00008000  /* Shared 32k  L2 ram       */
	E_RAM	(RWXI)	:   o=0x80000000    l=0x04000000  /* 64 MBytes external memory*/
    IVLCD_CONFIG    :   o=0x00080000 	l = 0x2000    /*  8 kB                    */
    IVLCD_IBUF0A    :   o=0x00084000 	l = 0x400     /*  1 kB                    */
    IVLCD_IBUF0B    :   o=0x00085000 	l = 0x400     /*  1 kB                    */
    IVLCD_IBUF1     :   o=0x00086000 	l = 0xc00     /*  3 kB                    */
    IVLCD_QMEM      :   o=0x00088000 	l = 0x400     /*  1 kB                    */
    IVLCD_HMEM      :   o=0x0008c000 	l = 0x1c00    /*  7 kB                    */

}

SECTIONS {
     /* vect tables must be in L2 at boot time */
     .stack         >       E_RAM
     .text          >       E_RAM
     .bss           >       E_RAM
     .const         >       E_RAM
     .const:JPEGIENC_TI_dSect > (E_RAM align(80h))
     .data          >       E_RAM
     .far           >       E_RAM
     .switch        >       E_RAM
     .sysmem        >       E_RAM
     .cio           >       E_RAM
     .bios          >       E_RAM
     .text:qdma     >       E_RAM
     .cinit         >       E_RAM
     .intDataMem    >       DMC	 
     .ccdData	    >  	    E_RAM
     .bitsData	    >       E_RAM
     .ref_buffer    >       E_RAM
     

    .rtstext > E_RAM 
    {
    -lrts64plus.lib(.text)
	-ldman3.a64P(.text)
    } > E_RAM

}
