
SECTIONS
{
    /* H-264 MP Decoder Section   */
  
    UNION run = L1PSRAM
    {
        .text:ittiam_h264dec_ciram_full_1  : LOAD = DDR2
            LOAD_START(_g_h264dec_ciram_full_1_load_addr), 
            RUN_START (_g_h264dec_ciram_full_1_run_addr), 
            LOAD_SIZE (_g_h264dec_ciram_full_1_size)

        .text:l1p_mpeg4vdec_csect0:    LOAD = DDR2
            LOAD_START(_MPEG4VDEC_TI_load0),
            RUN_START (_MPEG4VDEC_TI_run0),
            LOAD_SIZE (_MPEG4VDEC_TI_size0)
                
        .text:l1p_h264vdec_csect0:    LOAD = DDR2
            LOAD_START(_H264VDEC_TI_load0),
            RUN_START (_H264VDEC_TI_run0),
            LOAD_SIZE (_H264VDEC_TI_size0)

    }
}

/* Common utility(CAL, ACPY3, buffer API) Sections */
SECTIONS
{ 
    /* ACPY3 section */
    acpy_dsect_isram_0                  > L1DSRAM
    sect_format_conv                    >DDR2
    /* profiler section */
    data_dm420_profile                  > DDR2
    code_dm420_profile                  > DDR2
    .text:MPEG2DEC_ITTIAM_csect_sdram   > DDR2
    .text:MPEG2DEC_ITTIAM_csect_sddram  > DDR2
    .const:MPEG2DEC_ITTIAM_dsect_isram  > DDR2
}


                        
