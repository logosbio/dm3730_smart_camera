#ifndef __TYPES_H
#define __TYPES_H

typedef unsigned char  __u8;        //!< Type for unsigned 8 bits
typedef unsigned short __u16;       //!< Type for unsigned 16 bits
typedef unsigned int   __u32;       //!< Type for unsigned 32 bits

#endif // __TYPES_H
