#ifndef _FIFOUTIL_H
#define _FIFOUTIL_H

//#define __DEBUG_FIFO

#include <unistd.h>

typedef struct FifoUtil_Obj {
    size_t size;
    int pipes[2];
} FifoUtil_Obj;

typedef FifoUtil_Obj *FifoUtil_Handle;

#define FIFOUTIL_SUCCESS 0

#define FIFOUTIL_FAILURE -1


static inline int FifoUtil_open(FifoUtil_Handle hFifo, size_t size)
{
    if (pipe(hFifo->pipes)) {
        return FIFOUTIL_FAILURE;
    }

    hFifo->size = size;

    return FIFOUTIL_SUCCESS;
}

static inline int FifoUtil_close(FifoUtil_Handle hFifo)
{
    int ret = FIFOUTIL_SUCCESS;

    if (close(hFifo->pipes[0])) {
        ret = FIFOUTIL_FAILURE;
    }

    if (close(hFifo->pipes[1])) {
        ret = FIFOUTIL_FAILURE;
    }

    return ret;
}

static inline int oFifoUtil_get(FifoUtil_Handle hFifo, void *buffer)
{
	fprintf(stderr,"##### FifoUtil_get : %d(%p), size = %d \n  ->  ",hFifo->pipes[0], buffer, hFifo->size);
    if (read(hFifo->pipes[0], buffer, hFifo->size) != hFifo->size) {
		fprintf(stderr,"#ERR# FifoUtil_get : %d(%p), size = %d \n\t  ->  ",hFifo->pipes[0], buffer, hFifo->size);
        return FIFOUTIL_FAILURE;
    }

    return FIFOUTIL_SUCCESS;
}

static inline int oFifoUtil_put(FifoUtil_Handle hFifo, void *buffer)
{
	fprintf(stderr,"##### FifoUtil_put : %d(%p), size = %d \n  ->  ",hFifo->pipes[1], buffer, hFifo->size);
    if (write(hFifo->pipes[1], buffer, hFifo->size) != hFifo->size) {
		fprintf(stderr,"#ERR# FifoUtil_put : %d(%p), size = %d \n",hFifo->pipes[1], buffer, hFifo->size);
        return FIFOUTIL_FAILURE;
    }

    return FIFOUTIL_SUCCESS;
}

#ifndef __DEBUG_FIFO
static inline int FifoUtil_get(FifoUtil_Handle hFifo, void *buffer)
{
    if (read(hFifo->pipes[0], buffer, hFifo->size) != hFifo->size) {
        return FIFOUTIL_FAILURE;
    }

    return FIFOUTIL_SUCCESS;
}

static inline int FifoUtil_put(FifoUtil_Handle hFifo, void *buffer)
{
    if (write(hFifo->pipes[1], buffer, hFifo->size) != hFifo->size) {
        return FIFOUTIL_FAILURE;
    }

    return FIFOUTIL_SUCCESS;
}
#else
#define FifoUtil_put(x, y) dFifoUtil_put(x,y,__func__,__LINE__)
#define FifoUtil_get(x, y) dFifoUtil_get(x,y,__func__,__LINE__)
#endif


static inline int dFifoUtil_get(FifoUtil_Handle hFifo, void *buffer, void *func, int line)
{
	if (read(hFifo->pipes[0], buffer, hFifo->size) != hFifo->size) {
		fprintf(stderr,"#ERR# FifoUtil_get  %s ->%d line : id %2d(%p), size = %d \n\n\n", func, line ,hFifo->pipes[0], buffer, hFifo->size);
		return FIFOUTIL_FAILURE;
	}
	fprintf(stderr,"# FifoGET %s ->%d line : id %2d(%p), size = %d \n", func, line,hFifo->pipes[0], buffer, hFifo->size);

	return FIFOUTIL_SUCCESS;
}

static inline int dFifoUtil_put(FifoUtil_Handle hFifo, void *buffer, void *func, int line)
{

	if (write(hFifo->pipes[1], buffer, hFifo->size) != hFifo->size) {
		fprintf(stderr,"#ERR# FifoUtil_put  %s ->%d line : id %2d(%p), size = %d \n\n\n", func, line ,hFifo->pipes[0], buffer, hFifo->size);
		return FIFOUTIL_FAILURE;
	}
	fprintf(stderr,"# FifoPUT %s -> %d line : id %2d(%p), size = %d \n",func, line,hFifo->pipes[0], buffer, hFifo->size);

	return FIFOUTIL_SUCCESS;
}



#endif // _FIFOUTIL_H


