/*
 * capture.h
 */

#ifndef _CAPTURE_H
#define _CAPTURE_H

#include <rendezvous.h>
#include "encode.h"

#define CAPTURE_FLUSH		-1
#define CAPTURE_PRIME		-2

#define RGB_MAX				3

typedef struct CaptureBufferElement {
    int           id;
    char         *virtBuf;
    unsigned long physBuf;
    int           bufSize;
    double		  timestamp;
} CaptureBufferElement;

typedef struct CaptureEnv {
    Rendezvous_Handle	hRendezvousInit;
    Rendezvous_Handle	hRendezvousCleanup;
    Rendezvous_Handle	hRendezvousPrime;
    Pause_Handle    	hPause;
    FifoUtil_Handle 	hDisplayInFifo;
    FifoUtil_Handle 	hDisplayOutFifo;
    FifoUtil_Obj    	capOutFifo;
    FifoUtil_Obj    	capInFifo;
    int             	imageWidth;
    int             	imageHeight;
    int             	statusOsdFlag;
    int					prevFlag;
    int					ccdcFlag;
	VideoEncoder		videoEncoder;
} CaptureEnv;

extern void *captureThrFxn(void *arg);
int osdDelete(DrawTextParams *drawParams);
int osdDrawCreate(DrawTextParams *drawParams);
int osdDrawText(char *string, int x, int y, int screenIdx);


#endif /* _CAPTURE_H */
