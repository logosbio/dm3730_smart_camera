#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "simplewidget.h"
#include "_simplescreen.h"
#include "_simplebutton.h"
#include "_simpletext.h"

#include "_gfx.h"
#include "_font.h"
#include "_types.h"

/* Enables or disables debug output */
#ifdef __DEBUG
#define __D(fmt, args...) fprintf(stderr, "Simplewidget Debug: " fmt, ## args)
#else
#define __D(fmt, args...)
#endif

#define __E(fmt, args...) fprintf(stderr, "Simplewidget Error: " fmt, ## args)

#define FONT_FILE "data/fonts/decker.ttf"

#define DEPTH_OFFSET 0x10
#define PRESSED_OFFSET 0x40         // Color difference for pressed button.

int simplewidget_screen_init(void *fbp, int fbw, int fbh,
                             simplewidget_screen *swsp)
{
    _simplescreen *scrp;

    scrp = malloc(sizeof(_simplescreen));
    if (scrp == NULL) {
        __E("Failed to allocate memory for screen object\n");
        return SIMPLEWIDGET_FAILURE;
    }

    scrp->bufp = fbp;
    scrp->w = fbw;
    scrp->h = fbh;

    *swsp = (simplewidget_screen) scrp;

    return SIMPLEWIDGET_SUCCESS;
}

int simplewidget_screen_clear(simplewidget_screen sws,
                              int x, int y, int w, int h)
{
    _simplescreen *scrp = (_simplescreen *) sws;
    unsigned short *ptr;
    int i;

    if (!scrp)
        return SIMPLEWIDGET_FAILURE;

    /* Does the clearing area fit on screen? */
    if (y + h > scrp->h || x + w > scrp->w) {
        __E("Screen clearing area does not fit on screen\n");
        return SIMPLEWIDGET_FAILURE;
    }

    ptr = scrp->bufp + y * scrp->w + x;

    for (i = 0; i < h; i++) {
        memset(ptr, 0, w * 2);

        ptr += scrp->w;
    } 

    return SIMPLEWIDGET_SUCCESS;
}

int simplewidget_screen_draw_rectangle(simplewidget_screen sws, int x, int y,
                                       int w, int h, int r, int g, int b)
{
    _simplescreen *scrp = (_simplescreen *) sws;

    if (!scrp)
        return SIMPLEWIDGET_FAILURE;

    draw_rectangle(scrp, x, y, w, h, r, g, b);

    return SIMPLEWIDGET_SUCCESS;
}

int simplewidget_screen_exit(simplewidget_screen sws)
{
    _simplescreen *scrp = (_simplescreen *) sws;

    if (!scrp)
        return SIMPLEWIDGET_FAILURE;

    free(scrp);

    return SIMPLEWIDGET_SUCCESS;
}


int simplewidget_text_create(int x, int y, int height, char *txt,
                             simplewidget_text *swtp)
{
    _simpletext *txtp;

    txtp = malloc(sizeof(_simpletext));

    if (txtp == NULL) {
        __E("Failed to allocate memory for text line\n");
        return SIMPLEWIDGET_FAILURE;
    }

    txtp->x = x;
    txtp->y = y;
    txtp->h = height;

    strncpy(txtp->txt, txt, TEXT_MAX_TEXT_LENGTH);

    *swtp = txtp;

    return SIMPLEWIDGET_SUCCESS;
}

int simplewidget_text_show(simplewidget_text swt, simplewidget_screen sws)
{
    _simpletext *txtp = (_simpletext *) swt;
    _simplescreen *scrp = (_simplescreen *) sws;

    if (!txtp || !scrp)
        return SIMPLEWIDGET_FAILURE;

    font_init(FONT_FILE);

    if (font_render(txtp->txt, txtp->x, txtp->y, 0xff, txtp->h, scrp) < 0)
        return SIMPLEWIDGET_FAILURE;

    return SIMPLEWIDGET_SUCCESS;
}

int simplewidget_text_showYUV(simplewidget_text swt, simplewidget_screen sws)
{
    _simpletext *txtp = (_simpletext *) swt;
    _simplescreen *scrp = (_simplescreen *) sws;

    if (!txtp || !scrp)
        return SIMPLEWIDGET_FAILURE;

    font_init(FONT_FILE);

    if (font_renderYUV(txtp->txt, txtp->x, txtp->y, 0xff, txtp->h, scrp) < 0)
        return SIMPLEWIDGET_FAILURE;

    return SIMPLEWIDGET_SUCCESS;
}


int simplewidget_text_delete(simplewidget_text swt)
{
    _simplebutton *txtp = (_simplebutton *) swt;

    if (!txtp)
        return SIMPLEWIDGET_FAILURE;

    free(txtp);

    return SIMPLEWIDGET_SUCCESS;
}

