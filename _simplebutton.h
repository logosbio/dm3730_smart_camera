#ifndef _SIMPLEBUTTON_H
#define _SIMPLEBUTTON_H

#define BUTTON_MAX_TEXT_LENGTH 100

typedef struct _simplebutton {
    int x;                            // The x position of the button.
    int y;                            // The y position of the button.
    int w;                            // The width of the button.
    int h;                            // The height of the button.
    int r;                            // The red component of the button col.
    int g;                            // The green component of the button col.
    int b;                            // The blue component of the button col.
    int border;                       // The border size for button "depth"
    int font_height;                  // Height of the font to render in button.
    char txt[BUTTON_MAX_TEXT_LENGTH]; // The text to render into the button.
    unsigned short *bufp;             // Pointer to button buffer.
} _simplebutton;

#endif // _SIMPLEBUTTON_H
