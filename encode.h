/*
 * encode.h
 */

#ifndef _ENCODE_H
#define _ENCODE_H

#include <stdio.h>
#include <pthread.h>

#define __DEBUG
//#define UI_ON
#define CODEC_ENGINE_DSP

#ifdef __DEBUG
#define DBG(fmt, args...) fprintf(stderr, "Debug: " fmt, ## args)
#else
#define DBG(fmt, args...)
#endif

#define ERR(fmt, args...) fprintf(stderr, "Error: " fmt, ## args)

#define CLEAR(x) memset (&(x), 0, sizeof (x))

#define SUCCESS         0
#define FAILURE         -1

#define THREAD_SUCCESS  (void *) 0
#define THREAD_FAILURE  (void *) -1

#define ENGINE_NAME     "encode"

#define V4L2_DEVICE     "/dev/video0"
#define FBVID_DEVICE    "/dev/video1"
#define OSD_DEVICE      "/dev/fb0"
#define DISPLAY_NAME	"VideoFB"
#define TRUE            1
#define FALSE           0

#define PAUSE           100

#define SCREEN_BPP      	16
#define ENCODE_BUFFERS		(2)
#define NUM_CAPTURE_BUFS	(3)
#define NUM_DISPLAY_BUFS	(3)

#define PTSIZE				15

#define	ROUNDUP(x, y)		((((x) + ((y) - 1)) / (y)) * (y))

#define SET_CCDC_ENABLE			0x1
#define SET_PREV_ENABLE			0x2
#define SET_RAW_DATA_SAVE		0x4
#define SET_CCDC_RESET			0x8
#define SET_PREV_RESET			0x10

#define RAW_IMG_MAX_SIZE 	2592 * 1944 * 2 + 1024

#ifndef ALIGN
 #define ALIGN(x, mask) (((x)+(mask-1))&~(mask-1))
#endif

#define roundup(x, y) ((((x) + ((y) - 1)) / (y)) * (y))

typedef enum VideoEncoder {
    NONE_VIDEO_ENCODER,
    JPEG_VIDEO_ENCODER,
    MPEG4_VIDEO_ENCODER,
    H264_VIDEO_ENCODER,
    LZO_VIDEO_ENCODER,
    RLE_VIDEO_ENCODER,
    NUM_VIDEO_ENCODERS
} VideoEncoder;

typedef enum DisplayOption {
    NONE_DISPLAY,
    RESIZE_DISPLAY,
    MEMCPY_DISPLAY,
    NUM_DISPLAY_OPTION
} DisplayOption;

typedef struct DrawTextParams {
	int					numBuf;
	int					imageWidth;
	int					imageHeight;
	char				*osdDisplays[3];
} DrawTextParams;


typedef struct GlobalData {
    int             quit;                /* Global quit flag */
    int             record;              /* Whether to record or pause */
    int             frames;              /* Video frame counter */
    int             videoBytesEncoded;   /* Video bytes encoded counter */
    pthread_mutex_t mutex;               /* Mutex to protect the global data */
} GlobalData;

typedef struct RawDataSendInfo_t {
	unsigned int		imageSize;
	int					imageWidth;
	int					imageHeight;
	int					imageType;
} rdsInfo;

typedef struct RawDataSend_t {
	rdsInfo				info;
	char gRawData[RAW_IMG_MAX_SIZE];
} RawDataSend;

extern GlobalData gbl;

static inline int gblGetQuit(void)
{
    int quit;

    pthread_mutex_lock(&gbl.mutex);
    quit = gbl.quit;
    pthread_mutex_unlock(&gbl.mutex);

    return quit;
}

static inline void gblSetQuit(void)
{
    pthread_mutex_lock(&gbl.mutex);
    gbl.quit = TRUE;
    pthread_mutex_unlock(&gbl.mutex);
}


static inline int gblGetAndResetFrames(void)
{
    int frames;

    pthread_mutex_lock(&gbl.mutex);
    frames = gbl.frames;
    gbl.frames = 0;
    pthread_mutex_unlock(&gbl.mutex);

    return frames;
}

static inline int gblGetFrames(void)
{
    int frames;

    pthread_mutex_lock(&gbl.mutex);
    frames = gbl.frames;
    pthread_mutex_unlock(&gbl.mutex);

    return frames;
}

static inline void gblIncFrames(void)
{
    pthread_mutex_lock(&gbl.mutex);
    gbl.frames++;
    pthread_mutex_unlock(&gbl.mutex);
}

static inline int gblGetAndResetVideoBytesEncoded(void)
{
    int videoBytesEncoded;

    pthread_mutex_lock(&gbl.mutex);
    videoBytesEncoded = gbl.videoBytesEncoded;
    gbl.videoBytesEncoded = 0;
    pthread_mutex_unlock(&gbl.mutex);

    return videoBytesEncoded;
}

static inline void gblIncVideoBytesEncoded(int videoBytesEncoded)
{
    pthread_mutex_lock(&gbl.mutex);
    gbl.videoBytesEncoded += videoBytesEncoded;
    pthread_mutex_unlock(&gbl.mutex);
}

static inline void aligned_copy_neon( int32_t * __restrict dst, 
                              int32_t * __restrict src, 
                              int                  size) {

	asm volatile (
			"	0: \n"
//			"       PLD [%[src],  #0xC0] \n"
			"       VLDM %[src]!, {d0-d7} \n"
			"       VSTM %[dst]!, {d0-d7} \n"
			"       SUBS %[len], %[len], #0x40 \n"
			"       BGE 0b \n"
			 : [dst] "+r" (dst),
			   [src] "+r" (src),
			   [len] "+r" (size)
	);
}

#define cleanup(x)                                  \
    status = (x);                                   \
    gblSetQuit();                                   \
    goto cleanup

#define breakLoop(x)                                \
    status = (x);                                   \
    ERR("%s breakLoop %d\n", __func__, __LINE__);\
    gblSetQuit();                                   \
    break


int  getArmCpuLoad(int *procLoad, int *cpuLoad);
extern int gEncFps;
extern int gDspLoad;
extern int gArmLoad;
extern int v4l2_fd;
extern int gRaw_save;
extern int gRaw_apply;
extern RawDataSend gRDS;
extern int gRaw_data_size;


int updatePreview(int fd, int pset);
int resetPreview(int fd, int pset);

#endif /* _ENCODE_H */
