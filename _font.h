#ifndef __FONT_H
#define __FONT_H

#include "simplewidget.h"
#include "_simplescreen.h"

extern int font_string_size(char *text, int font_height, int *string_width,
                            int *string_height);
extern int font_init(char *filename);
extern int font_render(char *text, int x, int y, int fade, int font_height,
                       _simplescreen *scrp);
extern int font_renderYUV(char *text, int x, int y, int fade, int font_height,
                       _simplescreen *scrp);

#endif // __FONT_H
