/*
 * ctrl.c
 */

/* Standard Linux headers */
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <linux/videodev.h>
#include <linux/videodev2.h>
#include <linux/omapfb.h>

#include <sys/socket.h>
#include <netinet/in.h>

#include <xdc/std.h>
#include <ti/sdo/ce/Engine.h>

#include <rendezvous.h>
#include "encode.h"
#include "ctrl.h"
#include "preview.h"

#define COMMAND_PROMPT		"Command [ 'help' for usage ] > "
#define MAX_CMD_LENGTH		80

#define ENGINEOPENED		0x1
#define UICREATED			0x2

#define PREV_CNT_TEST		2
int gPrevFlagCnt = 0;

int gEncFps = 0;
int gDspLoad;
int gArmLoad;
int gRaw_save;
int gRaw_apply;
RawDataSend gRDS;
//char gRawData[RAW_IMG_MAX_SIZE];
int gRaw_data_size = 0;
set_isp_config g_Isp;

static int statusDynamicDataUpdate(Engine_Handle hEngine, int osdFd, int *time);

static int statusDynamicDataUpdate(Engine_Handle hEngine, int osdFd, int *time)
{
	static unsigned long	firstTime = 0;
	static unsigned long	prevTime;
	unsigned long			newTime;
	unsigned long			deltaTime;
	struct timeval			tv;
	struct tm				*timePassed;
	time_t					spentTime;
	int						procLoad;
	int						armLoad;
	int						fps;
	int						videoKbps;
	char tempString[20];
	float					fpsf;
	float					videoKbpsf;

	static int prev_cnt = 0;

	*time = -1;

	if (gettimeofday(&tv, NULL) == -1) {
		ERR("Failed to get os time\n");
		return -1;
	}

	newTime = tv.tv_sec * 1000 + tv.tv_usec / 1000;
	if (!firstTime) {
		firstTime = newTime;
		prevTime = newTime;
		return -1;
	}


	deltaTime = newTime - prevTime;
	if (deltaTime <= 1000) {
		return -1;
	}

	prevTime = newTime;

	spentTime = (newTime - firstTime) / 1000;
	if (spentTime <= 0) {
		return -1;
	}

	prev_cnt++;

	if ( prev_cnt % PREV_CNT_TEST == 0 ) {
		;
		gPrevFlagCnt = 1;
	}
	*time = spentTime;

	fpsf	= gblGetAndResetFrames() * 1000.0 / deltaTime;
	fps		= fpsf + 0.5;

	videoKbpsf	= gblGetAndResetVideoBytesEncoded() * 8.0 / deltaTime;
	videoKbps	= videoKbpsf + 0.5;

	gDspLoad = Engine_getCpuLoad(hEngine);

	if (getArmCpuLoad(&procLoad, &armLoad) != FAILURE) {
		fprintf(stderr, "# Proc Load :%3d%%, ARM Load :%3d%%, ", procLoad, armLoad);
	}
	else {
		ERR("Failed to get ARM CPU load\n");
	}
	gArmLoad = armLoad;

	if (gDspLoad > -1) {
		fprintf(stderr, "DSP Load :%3d%% ", gDspLoad);
	}
	else {
		ERR("Failed to get DSP CPU load\n");
	}

	timePassed = localtime(&spentTime);
	if (timePassed == NULL) {
		return -1;
	}
	sprintf(tempString, "%.2d:%.2d:%.2d.%06d", timePassed->tm_hour,
										  timePassed->tm_min,
										  timePassed->tm_sec, tv.tv_usec);


	fprintf(stderr, " -> %2d fps, %5d kbps, %s    \r", fps, videoKbps ,tempString);

	return fps;
}


int getKbdCommand(Pause_Handle hPauseProcess)
{
	struct timeval tv;
	fd_set		 fds;
	int			ret;
	char			string[MAX_CMD_LENGTH];

	FD_ZERO(&fds);
	FD_SET(fileno(stdin), &fds);

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	ret = select(FD_SETSIZE, &fds, NULL, NULL, &tv);

	if (ret == -1) {
		ERR("Select failed on stdin\n");
		return FAILURE;
	}

	if (ret == 0) {
		return SUCCESS;
	}

	if (FD_ISSET(fileno(stdin), &fds)) {
		if (fgets(string, MAX_CMD_LENGTH, stdin) == NULL) {
			return FAILURE;
		}

		strtok(string, "\n");

		if (strcmp(string, "play") == 0) {
			Pause_off(hPauseProcess);
		}
		else if (strcmp(string, "pause") == 0) {
			Pause_on(hPauseProcess);
		}
		else if (strcmp(string, "q") == 0) {
			gblSetQuit();
		}
		else if (strcmp(string, "help") == 0) {
			printf("\nAvailable commands:\n"
					"	play  - Play\n"
					"	pause - Pause\n"
					"	q : stop  - Quit App\n"
					"	help  - Show this help screen\n\n");
		}
		else {
			printf("Unknown command: [ %s ]\n", string);
		}

		printf("\n");
	}

	return SUCCESS;
}


void *ctrlThrFxn(void *arg)
{
	Engine_Handle		hEngine		  	= NULL;
	unsigned int		initMask		= 0;
	CtrlEnv				*envp			= (CtrlEnv *) arg;
	void				*status			= THREAD_SUCCESS;
	int					displayIdx		= 0;
	int					workingIdx		= 1;
	int					osdFd;
	int					timeSpent;
	int					ret = 0;


	hEngine = Engine_open(ENGINE_NAME, NULL, NULL);

	if (hEngine == NULL) {
		ERR("Failed to open codec engine %s\n", ENGINE_NAME);
		cleanup(THREAD_FAILURE);
	}

	DBG("Codec Engine opened in control thread\n");

	initMask |= ENGINEOPENED;

	DBG("User interface created\n");

    Rendezvous_meet(envp->hRendezvousInit);


	/* Initialize the ARM cpu load */
	getArmCpuLoad(NULL, NULL);

	printf(COMMAND_PROMPT);
	printf("\n");
	fflush(stdout);

	DBG("Entering control main loop.\n");
	while (!gblGetQuit()) {
		ret = statusDynamicDataUpdate(hEngine, osdFd, &timeSpent);
		if (ret > 0) 
			gEncFps = ret;

		if (envp->time > FOREVER && timeSpent >= envp->time) {
			breakLoop(THREAD_SUCCESS);
		}

		getKbdCommand(envp->hPause);
		usleep(500000);
		//system("sync");
	}

cleanup:
	Rendezvous_force(envp->hRendezvousInit);
	Pause_off(envp->hPause);
    Rendezvous_meet(envp->hRendezvousCleanup);

	if (initMask & ENGINEOPENED) {
		Engine_close(hEngine);
	}

    fprintf(stderr,"ctrlThrFxn closing...\n");

	return status;
}

void *ispThrFxn(void *arg)
{
	unsigned int		initMask		= 0;
	CtrlEnv				*envp			= (CtrlEnv *) arg;
	void				*status			= THREAD_SUCCESS;
	int					ret = 0;
	int listenfd, connfd,n;
	struct sockaddr_in servaddr,cliaddr;
	socklen_t clilen;
	char mesg[1000];
    int sendSize = 0;
	char * tmpPoint = &gRDS.gRawData;

	DBG("isp interface created\n");

	listenfd = socket(AF_INET,SOCK_STREAM, IPPROTO_TCP);

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
	servaddr.sin_port=htons(666);
	bind(listenfd,(struct sockaddr *)&servaddr,sizeof(servaddr));

	listen(listenfd,1024);


	DBG("Entering control isp loop.\n");
	while (!gblGetQuit()) {

		clilen=sizeof(cliaddr);
		connfd = accept(listenfd,(struct sockaddr *)&cliaddr,&clilen);
		for(;;)
		{
			n = recv(connfd, &g_Isp, sizeof(set_isp_config), 0);
			if (n < 1) {
				printf("Received the following: %d\n", n);
				break;
			}

			if (g_Isp.flag != 0) {
				gRaw_apply = 1;
			}

			if (g_Isp.raw_save == 1) {
				g_Isp.raw_save = 0;

				gRDS.info.imageSize = gRaw_data_size;
				gRDS.info.imageWidth = envp->imageWidth;
				gRDS.info.imageHeight = envp->imageHeight;
				gRDS.info.imageType = 0;


				gRaw_save = 1;
				while(gRaw_save){
					usleep(0);
				}
				sendSize = gRaw_data_size + sizeof(rdsInfo);
				n = send(connfd, (char *)&sendSize, sizeof(int), 0);
				ERR("\nRaw data size Send : %d (%d)", n, gRaw_data_size);
				n = send(connfd, (char *)&gRDS, sendSize , 0);

#if 1
				char buf[128];
				int m_file_hndl;

				sprintf( buf, "./UYVY_%dx%d_raw_orig.yuv",envp->imageWidth, envp->imageHeight);
				m_file_hndl = fopen( buf, "a+" );
				fwrite( &gRDS.gRawData, gRaw_data_size, sizeof(char), m_file_hndl );
				fclose( m_file_hndl );
#endif
				ERR("\nRaw data Send : %d\n", n);
				g_Isp.raw_save = 0;
			}

		}
		usleep(500000);
		//system("sync");
	}

	close (listenfd);

    fprintf(stderr,"ispThrFxn closing...\n");

	return status;
}
