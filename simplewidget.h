/**
 * @file    simplewidget.h
 */

#ifndef _SIMPLEWIDGET_H
#define _SIMPLEWIDGET_H

#define BYTES_PER_PIXEL 2           //!< Only 16 bit color supported.

#define SIMPLEWIDGET_SUCCESS 0      //!< Success return code.
#define SIMPLEWIDGET_FAILURE -1     //!< Failure return code.

typedef void * simplewidget_screen; //!< Opaque handle to a screen
typedef void * simplewidget_text;   //!< Opaque handle to a line of text

#if defined (__cplusplus)
extern "C" {
#endif

/**
 * @brief Initializes a simplewidget screen. Must be called before any
 * other simplewidget API call on this screen.
 * @param fbp The pointer to the framebuffer.
 * @param fbw The width of the framebuffer.
 * @param fbh The height of the framebuffer.
 * @param swsp Screen pointer returned.
 * @return SIMPLEWIDGET_SUCCESS on success and SIMPLEWIDGET_FAILURE on failure.
 */
extern int simplewidget_screen_init(void *fbp, int fbw, int fbh,
                                    simplewidget_screen *swsp);

/**
 * @brief Clear an area of the screen.
 * @param sws Simplewidget screen to clear on.
 * @param x X coordinate of area to clear.
 * @param y Y coordinate of area to clear.
 * @param w Width of area to clear.
 * @param h Height of area to clear.
 * @return SIMPLEWIDGET_SUCCESS on success and SIMPLEWIDGET_FAILURE on failure.
 */
extern int simplewidget_screen_clear(simplewidget_screen sws,
                                     int x, int y, int w, int h);

/**
 * @brief Draw a rectangle on the screen.
 * @param sws Simplewidget screen to draw rectangle on.
 * @param x X coordinate of the rectangle.
 * @param y Y coordinate of the rectangle.
 * @param w Width of the rectangle.
 * @param h Height of the rectangle.
 * @param r Red component of rectangle color.
 * @param g Green component of rectangle color.
 * @param b Blue component of rectangle color.
 * @return SIMPLEWIDGET_SUCCESS on success and SIMPLEWIDGET_FAILURE on failure.
 */
int simplewidget_screen_draw_rectangle(simplewidget_screen sws, int x, int y,
                                       int w, int h, int r, int g, int b);

/**
 * @brief Deinitalize the simplewidget screen and free up associated resources.
 * @param sws Simplewidget screen to deinitalize.
 * @return SIMPLEWIDGET_SUCCESS on success and SIMPLEWIDGET_FAILURE on failure.
 */
extern int simplewidget_screen_exit(simplewidget_screen sws);


/**
 * @brief Creates a line of text.
 * @param x Starting X position of the line of text.
 * @param y Starting Y position of the line of text.
 * @param height Height of font to render text with.
 * @param txt Text to render inside button.
 * @param swtp Simplewidget handle returned.
 * @return SIMPLEWIDGET_SUCCESS on success and SIMPLEWIDGET_FAILURE on failure.
 */
extern int simplewidget_text_create(int x, int y, int height, char *txt,
                                    simplewidget_text *swtp);

/**
 * @brief Show the line of text on the screen.
 * @param swt Simplewidget handle of the text line to show.
 * @param sws Simplewidget screen to show text line on.
 * @return SIMPLEWIDGET_SUCCESS on success and SIMPLEWIDGET_FAILURE on failure.
 */
extern int simplewidget_text_show(simplewidget_text swt,
                                  simplewidget_screen sws);
extern int simplewidget_text_showYUV(simplewidget_text swt, simplewidget_screen sws);

/**
 * @brief Delete the line of text and resources associated with it.
 * @param swt Simplewidget handle of the text line to delete.
 * @return SIMPLEWIDGET_SUCCESS on success and SIMPLEWIDGET_FAILURE on failure.
 */
extern int simplewidget_text_delete(simplewidget_text swt);


#if defined (__cplusplus)
}
#endif

#endif // _SIMPLEWIDGET_H
