#ifndef __SIMPLESCREEN_H
#define __SIMPLESCREEN_H

typedef struct _simplescreen {
    unsigned short *bufp;
    int w;
    int h;
} _simplescreen;

#endif // __SIMPLESCREEN_H
